import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './website/home/home.component';
import { PageNotFoundComponent } from './website/page-not-found/page-not-found.component';

const routes: Routes = [

    { path: '', component: HomeComponent, },
    /*{
        path: 'admin/:username',
        loadChildren: './admin-section/admin.module#AdminModule',
        canLoad: [ AdminGuardService ],
    },*/
    /*{
        path: 's/:username',
        loadChildren: './student-section/student.module#StudentModule',
        canLoad: [ StudentGuardService ],
    },*/
    /*{
        path: 'r/:username',
        loadChildren: './parent-section/parent.module#ParentModule',
        canLoad: [ ParentGuardService ],
    },*/
    /*{
        path: 'p/:username',
        loadChildren: './professor-section/professor.module#ProfessorModule',
        canLoad: [ ProfessorGuardService ],
    },*/
    /*{
        path: 'i/:institute_slug',
        loadChildren: './institute-section/institute.module#InstituteModule',
        canLoad: [ InstituteGuardService ],
    },*/
    /*{
        path: '',
        loadChildren: './security/security.module#SecurityModule',
    },*/
    { path: '**', component: PageNotFoundComponent, },
];

@NgModule( {
    imports: [ RouterModule.forRoot( routes, { preloadingStrategy: PreloadAllModules } ) ],
    exports: [ RouterModule ],
} )
export class AppRoutingModule {
}

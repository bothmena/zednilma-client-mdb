import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {WebsiteComponent} from './website/website.component';
import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';
import {ContactUsComponent} from './contact-us/contact-us.component';

const websiteRoutes: Routes = [
    { path: '', component: HomeComponent },                             // w_hm
    {
        path: '', component: WebsiteComponent, children: [
            { path: 'about-us', component: AboutComponent },                       // w_abt
            { path: 'contact-us', component: ContactUsComponent },              // w_ctu
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(websiteRoutes)],
    exports: [RouterModule]
})
export class WebsiteRoutingModule {
}

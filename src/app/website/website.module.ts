import {NgModule} from '@angular/core';

import {WebsiteRoutingModule} from './website-routing.module';
import {AboutComponent} from './about/about.component';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {HomeComponent} from './home/home.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {WebsiteComponent} from './website/website.component';
import {ZdlmCommonModule} from '../shared/modules/zdlm-common.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        ZdlmCommonModule,
        TranslateModule,
        WebsiteRoutingModule
    ],
    declarations: [
        AboutComponent,
        ContactUsComponent,
        HomeComponent,
        PageNotFoundComponent,
        WebsiteComponent
    ]
})
export class WebsiteModule {
}

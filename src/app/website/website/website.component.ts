import {Component} from '@angular/core';

@Component({
    template: `
        <zdlm-topnav section="website"></zdlm-topnav>
        <div class="container-fluid">
            <router-outlet></router-outlet>
        </div>
        <zdlm-footer></zdlm-footer>
    `,
})
export class WebsiteComponent {

    constructor() {
    }
}

import {Component, DoCheck, KeyValueDiffers, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SessionContainer} from '../../../shared/models/session-container';
import {Timetable} from '../../../shared/models/timetable';
import {TranslateService} from '@ngx-translate/core';
import {IOption, ToastService} from 'ng-uikit-pro-standard';
import {TimetableService} from '../../../shared/services/models/timetable.service';
import {takeUntil} from 'rxjs/operators';
import {AppActions} from '../../../shared/redux/app/app.actions';
import {NgRedux} from '@angular-redux/store';
import {State} from '../../../shared/redux/models/state';
import {HttpResponse} from '@angular/common/http';
import {AuthService} from '../../../shared/services/auth.service';
import {Subject} from 'rxjs';

@Component({
    templateUrl: './timetable-settings.component.html',
    styleUrls: ['./timetable-settings.component.scss']
})
export class TimetableSettingsComponent implements OnInit, DoCheck, OnDestroy {

    containerForm: FormGroup;
    timeForm: FormGroup;
    timetable: Timetable;
    timetables: Timetable[] = [];
    hours = [];
    insId: number;
    start: string;
    end: string;
    editTime: boolean;
    timetableOptions: Array<{ value: number, label: string, selected?: boolean }>;
    dayOptions: Array<{ value: number, label: string }> = [];
    @ViewChild('containerModal') containerModal;
    @ViewChild('timeModal') timeModal;
    differ: any;
    selectedValue: number;

    private ngUnsubscribe: Subject<boolean> = new Subject();

    constructor(private fb: FormBuilder,
                private differs: KeyValueDiffers,
                private translator: TranslateService,
                private toast: ToastService,
                private ngRedux: NgRedux<State>,
                private timeService: TimetableService,
                private authService: AuthService,
    ) {
    }

    ngOnInit() {

        this.authService.insObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            (institute) => {
                if (!!institute && institute.id > 0) {
                    this.insId = institute.id;
                    this.timeService.getTimetables(institute.id).subscribe(
                        timetables => {
                            this.timetables = timetables;
                            for (const time of this.timetables)
                                time.dirty = false;
                            if (this.timetables.length > 0)
                                this.timetable = this.timetables[0];
                            this.updateTimeSelect();
                            this.hours = this.getHours(this.timetable.maxMinutes);
                        });
                }
            }
        );
        this.buildForm();
        this.differ = this.differs.find({}).create();
        this.translator.get(['timetable.1', 'timetable.2', 'timetable.3', 'timetable.4', 'timetable.5', 'timetable.6', 'timetable.7']).subscribe(
            trans => {
                this.dayOptions = [];
                for (const key in trans)
                    this.dayOptions.push({value: +key.substring(key.length - 1), label: trans[key]});
            }
        );
    }

    ngDoCheck(): void {

        if (this.differ.diff({'start': this.start, 'end': this.end})) {
            if (!!this.start)
                this.containerForm.get('startAt').setValue(this.start + ':00');
            if (!!this.end)
                this.containerForm.get('endAt').setValue(this.end + ':00');
        }
    }

    ngOnDestroy(): void {

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    saveContainers() {

        this.timeService.saveTimetableSessionContainers(this.insId, this.timetable.id, this.timetable.getContainersFormData()).subscribe(
            () => {
                this.translator.get('timetables.new_tt_success').subscribe(trans => this.toast.success(trans));
                this.ngRedux.dispatch(AppActions.decrementLoading());
                this.timetable.dirty = false;
            },
            () => {
                this.translator.get('timetables.new_tt_error').subscribe(trans => this.toast.error(trans));
                this.ngRedux.dispatch(AppActions.decrementLoading());
            }
        )
    }

    submit() {

        let i = 1;
        for (const day of this.containerForm.get('day').value) {

            const container = new SessionContainer();
            const start = new Date();
            start.setFullYear(1970, 1, 1);
            start.setHours(+this.start.substr(0, 2), +this.start.substr(3, 2), 0);
            container.startAt = start;
            const end = new Date();
            end.setFullYear(1970, 1, 1);
            end.setHours(+this.end.substr(0, 2), +this.end.substr(3, 2), 0);
            container.endAt = end;
            container.type = 'session';
            container.day = day;
            container.calculateMinutes();

            try {
                this.timetable.addSessionContainer(container);
                if (i === this.containerForm.get('day').value.length) {
                    this.containerForm.reset();
                    this.start = '';
                    this.end = '';
                    this.containerModal.hide();
                } else
                    i++;
            } catch (e) {
                this.translator.get(e.message).subscribe(trans => this.toast.error(trans));
            }
        }
        this.timetable.fillBreaks();
        this.hours = this.getHours(this.timetable.maxMinutes);
    }

    submitTimetable() {

        this.ngRedux.dispatch(AppActions.incrementLoading());
        if (this.editTime) {
            this.timeService.editTimetable(this.insId, this.timetable.id, this.timeForm.value).subscribe(
                () => {
                    this.translator.get('timetables.edit_tt_success').subscribe(trans => this.toast.success(trans));
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.timetable.name = this.timeForm.get('name').value;
                    this.updateTimeSelect();
                    this.timeModal.hide();
                },
                () => {
                    this.translator.get('timetables.edit_tt_error').subscribe(trans => this.toast.error(trans));
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.timeModal.hide();
                }
            )
        } else {
            this.timeService.newTimetable(this.insId, this.timeForm.value).subscribe(
                (response: HttpResponse<any>) => {
                    this.translator.get('timetables.new_tt_success').subscribe(trans => this.toast.success(trans));
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.timetable = new Timetable(0, this.timeForm.get('name').value);
                    const arr = response.headers.get('location').split('/');
                    this.timetable.id = +arr[arr.length - 1];
                    this.timetables.push(this.timetable);
                    this.updateTimeSelect();
                    this.timeModal.hide();
                },
                () => {
                    this.translator.get('timetables.new_tt_error').subscribe(trans => this.toast.error(trans));
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.timeModal.hide();
                }
            )
        }
    }

    onTimetableSelect(event: IOption) {

        for (const time of this.timetables) {
            if (time.id === +event.value)
                this.timetable = time;
        }
    }

    prepTimeForm(edit: boolean) {

        if (edit) {
            this.timeForm.setValue({name: this.timetable.name})
        } else {
            this.timeForm.reset();
        }
        this.editTime = edit;
        this.timeModal.show();
    }

    private getHours(max): number[] {
        let i = max;
        const hours = [];
        while (i > 0) {
            if (i / 60 >= 1) {
                i -= 60;
                hours.push(60);
            } else {
                hours.push(i % 60);
                i = 0;
            }
        }
        return hours;
    }

    private buildForm() {
        this.containerForm = this.fb.group({
            'startAt': ['', Validators.compose([
                Validators.required,
            ])],
            'endAt': ['', Validators.compose([
                Validators.required,
            ])],
            'day': ['', Validators.compose([
                Validators.required,
            ])],
        });
        this.timeForm = this.fb.group({
            'name': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(50),
            ])],
        });
    }

    private updateTimeSelect() {

        this.timetableOptions = [];
        for (const time of this.timetables)
            this.timetableOptions.push({value: time.id, label: time.name});
        this.selectedValue = this.timetable.id;
    }
}

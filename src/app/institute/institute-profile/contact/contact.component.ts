import {Component, OnInit, ViewChild} from '@angular/core';
import {Email} from '../../../shared/models/email';
import {Phone} from '../../../shared/models/phone';
import {ActivatedRoute, Router} from '@angular/router';
import {State} from '../../../shared/redux/models/state';
import {NgRedux} from '@angular-redux/store';
import {PhoneService} from '../../../shared/services/models/phone.service';
import {EmailService} from '../../../shared/services/models/email.service';
import {AuthService} from '../../../shared/services/auth.service';
import {TranslateService} from '@ngx-translate/core';
import {ToastService} from 'ng-uikit-pro-standard';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ABOValidators} from '../../../shared/utils/abo.validators';
import {AppActions} from '../../../shared/redux/app/app.actions';
import {HttpResponse} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {first} from 'rxjs/operators';

@Component({
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.min.css'],
})
export class ContactComponent implements OnInit {

    emailSubmitting = false;
    phoneSubmitting = false;
    emailForm: FormGroup;
    phoneForm: FormGroup;
    currentEmailId: number = null;
    currentPhoneId: number = null;
    toDeleteId: number = null;
    toDeleteEntity: string = null;
    emails: Email[] = [];
    phones: Phone[] = [];
    selectedType: string = null;
    typeOptionsSubject = new BehaviorSubject<Array<{ value: string, label: string }>>([]);
    typeOptions$: Observable<Array<{ value: string, label: string }>> = this.typeOptionsSubject.asObservable();
    insId: number;
    @ViewChild('emailModal') emailModal;
    @ViewChild('phoneModal') phoneModal;
    @ViewChild('warning') warning;

    constructor(private emailService: EmailService,
                private phoneService: PhoneService,
                private fb: FormBuilder,
                public router: Router,
                public translator: TranslateService,
                public activatedRoute: ActivatedRoute,
                public authService: AuthService,
                public toast: ToastService,
                private ngRedux: NgRedux<State>) {
    }

    ngOnInit() {

        this.authService.insObservable$.subscribe(
            institute => {
                if (!!institute) {
                    this.emailService.getInstituteEmails(institute.id).pipe(first()).subscribe(
                        (emails: Email[]) => this.emails = emails,
                        () => this.translator.get('app.get_data_error').pipe(first()).subscribe(message => this.toast.error(message))
                    );
                    this.phoneService.getInstitutePhones(institute.id).pipe(first()).subscribe(
                        (phones: Phone[]) => this.phones = phones,
                        () => this.translator.get('app.get_data_error').pipe(first()).subscribe(message => this.toast.error(message))
                    );
                }
            }
        );
        for (const type of ['MBL', 'FIX', 'FAX']) {
            this.translator.get('phone.type.' + type).pipe(first()).subscribe(msg => {
                const cloned = this.typeOptionsSubject.value.map(x => Object.assign({}, x));
                cloned.push({value: type, label: msg});
                this.typeOptionsSubject.next(cloned);
            });
        }
        this.buildForms();
    }

    submitEmail() {

        if (this.emailForm.valid) {

            this.emailSubmitting = true;
            this.ngRedux.dispatch(AppActions.incrementLoading());
            if (!!this.currentEmailId) {
                this.emailService.patchInstituteEmail(this.authService.institute.id, this.currentEmailId, this.emailForm.value).subscribe(
                    () => this.onPatchEmailSuccess(),
                    () => {
                        this.emailSubmitting = false;
                        this.onSubmitError();
                    },
                );
            } else {
                this.emailService.newInstituteEmail(this.authService.institute.id, this.emailForm.value).subscribe(
                    (response: HttpResponse<any>) => {

                        const arr = response.headers.get('location').split('/');
                        this.onNewEmailSuccess(+arr[arr.length - 1]);
                    },
                    () => {
                        this.emailSubmitting = false;
                        this.onSubmitError();
                    },
                );
            }
        }
    }

    submitPhone() {

        if (this.phoneForm.valid) {

            this.phoneSubmitting = true;
            this.ngRedux.dispatch(AppActions.incrementLoading());
            if (!!this.currentPhoneId) {
                this.phoneService.patchInstitutePhone(this.authService.institute.id, this.currentPhoneId, this.phoneForm.value).subscribe(
                    () => this.onPatchPhoneSuccess(),
                    () => {
                        this.phoneSubmitting = false;
                        this.onSubmitError();
                    },
                );
            } else {
                this.phoneService.newInstitutePhone(this.authService.institute.id, this.phoneForm.value).subscribe(
                    (response: HttpResponse<any>) => {

                        const arr = response.headers.get('location').split('/');
                        this.onNewPhoneSuccess(+arr[arr.length - 1]);
                    },
                    () => {
                        this.phoneSubmitting = false;
                        this.onSubmitError();
                    },
                );
            }
        }
    }

    initEmailForm(email: Email) {

        this.resetEmailForm(email);
        this.emailModal.show();
    }

    initPhoneForm(phone: Phone) {

        this.resetPhoneForm(phone);
        this.phoneModal.show();
    }

    onTypeChange(event) {
        this.phoneForm.get('type').setValue(event.value);
        this.phoneForm.get('type').markAsDirty();
    }

    deleteEntity() {

        if (this.toDeleteEntity === 'email' && !!this.toDeleteId) {

            this.emailSubmitting = true;
            this.ngRedux.dispatch(AppActions.decrementLoading());
            this.emailService.deleteInstituteEmail(this.authService.institute.id, this.toDeleteId).pipe(first()).subscribe(
                () => this.onEmailDeleteSuccess(),
                () => {

                    this.translator.get('app.submit_error').pipe(first()).subscribe(message => this.toast.error(message));
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.toDeleteId = null;
                    this.toDeleteEntity = null;
                    this.emailSubmitting = false;
                    this.warning.hide();
                }
            );
        } else if (this.toDeleteEntity === 'phone' && !!this.toDeleteId) {

            this.phoneSubmitting = true;
            this.ngRedux.dispatch(AppActions.decrementLoading());
            this.phoneService.deleteInstitutePhone(this.authService.institute.id, this.toDeleteId).pipe(first()).subscribe(
                () => this.onPhoneDeleteSuccess(),
                () => {

                    this.translator.get('app.submit_error').pipe(first()).subscribe(message => this.toast.error(message));
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.toDeleteId = null;
                    this.toDeleteEntity = null;
                    this.phoneSubmitting = false;
                    this.warning.hide();
                }
            );
        }
    }

    private onSubmitError() {

        this.translator.get('app.submit_error').pipe(first()).subscribe(message => this.toast.error(message));
        this.ngRedux.dispatch(AppActions.decrementLoading());
    }

    private resetEmailForm(email: Email) {

        if (!!email) {

            this.emailForm.setValue({
                email: email.email,
                role: email.role,
                isPrimary: email.isPrimary,
            });
            this.currentEmailId = email.id;
        } else {
            this.emailForm.reset();
            this.currentEmailId = null;
        }
    }

    private resetPhoneForm(phone: Phone) {

        if (!!phone) {

            this.phoneForm.setValue({
                type: phone.type,
                number: phone.number,
                role: phone.role,
                ccode: phone.ccode,
            });
            this.currentPhoneId = phone.id;
        } else {
            this.phoneForm.reset();
            this.currentPhoneId = null;
        }
    }

    private buildForms() {

        this.emailForm = this.fb.group({
            'email': ['', Validators.compose([
                Validators.required,
                Validators.email,
            ])],
            'isPrimary': [false],
            'role': ['', Validators.compose([
                Validators.minLength(3),
                Validators.maxLength(31),
            ])],
        });

        this.phoneForm = this.fb.group({
            'type': ['', Validators.compose([
                Validators.required,
                ABOValidators.inArray(['MBL', 'FIX', 'FAX']),
            ])],
            'role': ['', Validators.compose([
                Validators.minLength(3),
                Validators.maxLength(31),
            ])],
            'ccode': ['', Validators.compose([
                Validators.required,
                Validators.pattern(/^(\+|00)\d{1,3}$/),
            ])],
            'number': ['', Validators.compose([
                Validators.required,
                Validators.pattern(/^\d{2}\s?(\d{3}\s?\d{3}|\d{2}\s\d{2}\s\d{2})$/),
            ])],
        });
    }

    private onPatchEmailSuccess() {

        this.emailSubmitting = false;
        for (const email of this.emails) {
            if (email.id === this.currentEmailId) {
                if (email.email === this.emailForm.get('email').value) {

                    this.translator.get('email.edit_no_verif_success').pipe(first()).subscribe(message => this.toast.success(message));
                } else {

                    this.translator.get('email.edit_verif_success').pipe(first()).subscribe(message => this.toast.success(message));
                    email.isVerified = false;
                }
                email.email = this.emailForm.get('email').value;
                email.isPrimary = this.emailForm.get('isPrimary').value;
                email.role = this.emailForm.get('role').value;
                break;
            }
        }
        this.emailForm.reset();
        this.emailModal.hide();
        this.currentEmailId = null;
    }

    private onNewEmailSuccess(id: number) {

        this.emailSubmitting = false;
        this.translator.get('email.new_save_success').pipe(first()).subscribe(message => this.toast.success(message));
        const email = new Email();
        email.id = id;
        email.isVerified = false;
        email.email = this.emailForm.get('email').value;
        email.isPrimary = this.emailForm.get('isPrimary').value;
        email.role = this.emailForm.get('role').value;
        this.emails.push(email);
        this.emailForm.reset();
        this.emailModal.hide();
        this.currentEmailId = null;
    }

    private onPatchPhoneSuccess() {

        this.phoneSubmitting = false;
        for (const phone of this.phones) {
            if (phone.id === this.currentPhoneId) {
                this.translator.get('phone.edit_save_success').pipe(first()).subscribe(message => this.toast.success(message));
                phone.isVerified = false;
                phone.number = this.phoneForm.get('number').value;
                phone.ccode = this.phoneForm.get('ccode').value;
                phone.role = this.phoneForm.get('role').value;
                phone.type = this.phoneForm.get('type').value;
                break;
            }
        }
        this.phoneForm.reset();
        this.phoneModal.hide();
        this.currentPhoneId = null;
    }

    private onNewPhoneSuccess(id: number) {

        this.phoneSubmitting = false;
        this.translator.get('phone.new_save_success').pipe(first()).subscribe(message => this.toast.success(message));
        const phone = new Phone();
        phone.id = id;
        phone.isVerified = false;
        phone.number = this.phoneForm.get('number').value;
        phone.ccode = this.phoneForm.get('ccode').value;
        phone.role = this.phoneForm.get('role').value;
        phone.type = this.phoneForm.get('type').value;
        this.phones.push(phone);
        this.phoneForm.reset();
        this.phoneModal.hide();
        this.currentPhoneId = null;
    }

    private onEmailDeleteSuccess() {

        this.emailSubmitting = true;
        this.ngRedux.dispatch(AppActions.decrementLoading());
        for (const email of this.emails) {
            if (email.id === this.toDeleteId) {
                const index = this.emails.indexOf(email);
                if (index >= 0) {
                    this.emails.splice(index, 1);
                }
                break;
            }
        }
        this.toDeleteId = null;
        this.toDeleteEntity = null;
        this.emailSubmitting = false;
        this.warning.hide();
    }

    private onPhoneDeleteSuccess() {

        this.phoneSubmitting = true;
        this.ngRedux.dispatch(AppActions.decrementLoading());
        for (const phone of this.phones) {
            if (phone.id === this.toDeleteId) {
                const index = this.phones.indexOf(phone);
                if (index >= 0) {
                    this.phones.splice(index, 1);
                }
                break;
            }
        }
        this.toDeleteId = null;
        this.toDeleteEntity = null;
        this.phoneSubmitting = false;
        this.warning.hide();
    }
}

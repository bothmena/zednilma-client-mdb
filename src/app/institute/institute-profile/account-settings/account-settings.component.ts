import {NgRedux} from '@angular-redux/store';
import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {AppActions} from '../../../shared/redux/app/app.actions';
import {State} from '../../../shared/redux/models/state';
import {InstituteService} from '../../../shared/services/models/institute.service';
import {ABOValidators} from '../../../shared/utils/abo.validators';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../shared/services/auth.service';
import {TranslateService} from '@ngx-translate/core';
import {ToastService, IMyOptions} from 'ng-uikit-pro-standard';
import {getDate} from '../../../shared/utils/util-functions';
import {first} from 'rxjs/operators';

@Component({
    templateUrl: './account-settings.component.html',
    styleUrls: ['./account-settings.component.scss'],
})
export class AccountSettingsComponent implements OnInit {

    settingForm: FormGroup;
    isSubmitting = false;
    @ViewChild('form') form;
    types = ['PRIMARY_SCHOOL', 'BASIC_SCHOOL', 'SECONDARY_SCHOOL', 'UNIVERSITY'];
    parentalControls = ['NONE', 'MEDIUM', 'FULL'];
    selectedType: string = null;
    selectedCtrl: string = null;
    selectedFDay: string = null;
    pCtrlOptions: Array<{ value: string, label: string }> = [
        {value: 'NONE', label: 'NONE: full control to student'},
        {value: 'MEDIUM', label: 'MEDIUM: Control shared between parent and student'},
        {value: 'FULL', label: 'FULL: full control to the parent'}
    ];
    typeOptions: Array<{ value: string, label: string }> = [
        {value: 'PRIMARY_SCHOOL', label: 'Primary School'},
        {value: 'BASIC_SCHOOL', label: 'Basic School'},
        {value: 'SECONDARY_SCHOOL', label: 'Secondary School'},
        {value: 'UNIVERSITY', label: 'University'}
    ];
    dpOptions: IMyOptions = {
        todayBtnTxt: 'Today',
        clearBtnTxt: 'Clear',
        closeBtnTxt: 'Close',
        dateFormat: 'dd/mm/yyyy',
        showTodayBtn: false,
    };

    constructor(private fb: FormBuilder,
                private instituteService: InstituteService,
                private translator: TranslateService,
                private toast: ToastService,
                public router: Router,
                public activatedRoute: ActivatedRoute,
                public authService: AuthService,
                private ngRedux: NgRedux<State>) {

    }

    ngOnInit() {

        this.buildForm();
        this.resetForm();
    }

    onDateChanged(event) {
        this.settingForm.get('foundationDate').setValue(event.value);
        this.settingForm.get('foundationDate').markAsDirty();
    }

    onPCtrlChange(event) {
        this.settingForm.get('parentalControl').setValue(event.value);
        this.settingForm.get('parentalControl').markAsDirty();
    }

    onTypeChange(event) {
        this.settingForm.get('type').setValue(event.value);
        this.settingForm.get('type').markAsDirty();
    }

    buildForm(): void {

        this.settingForm = this.fb.group({
            'name': ['', Validators.compose([
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(101),
            ])],
            'slogan': ['', Validators.compose([
                Validators.minLength(10),
                Validators.maxLength(101),
            ])],
            'description': ['', Validators.compose([
                Validators.minLength(45),
                Validators.maxLength(500),
            ])],
            'foundationDate': ['', Validators.compose([
                Validators.required,
            ])],
            'website': ['', Validators.compose([
                CustomValidators.url,
            ])],
            'type': ['', Validators.compose([
                Validators.required,
                ABOValidators.inArray(this.types),
            ])],
            'parentalControl': ['', Validators.compose([
                Validators.required,
                ABOValidators.inArray(this.parentalControls),
            ])],
        });
    }

    submit() {

        this.isSubmitting = true;
        this.ngRedux.dispatch(AppActions.incrementLoading());
        this.instituteService.patchInstitute(this.authService.institute.id, this.settingForm.value).subscribe(
            () => {
                this.isSubmitting = false;
                this.instituteService.getInstituteBySlug(this.authService.institute.slug).pipe(first()).subscribe(
                    institute => {
                        if (!!institute) {
                            this.authService.insSubject.next(institute);
                        }
                    }
                );
                this.ngRedux.dispatch(AppActions.decrementLoading());
                this.translator.get('app.account.prf_updated').pipe(first())
                    .subscribe(message => this.toast.success(message));
                this.settingForm.reset();
                this.form.hide();
            },
            () => {
                this.isSubmitting = false;
                this.translator.get('app.submit_error').pipe(first())
                    .subscribe(message => this.toast.error(message, 5000));
                this.ngRedux.dispatch(AppActions.decrementLoading());
            }
        );
    }

    resetForm() {

        this.authService.insObservable$.subscribe(
            institute => {
                if (!!institute) {
                    this.selectedCtrl = !!institute.parentalControl ? institute.parentalControl : null;
                    this.selectedType = !!institute.type ? institute.type : null;
                    this.selectedFDay = !!institute.foundationDate ? getDate(institute.foundationDate) : null;
                    this.settingForm.setValue({
                        description: institute.description,
                        name: institute.name,
                        parentalControl: this.selectedCtrl,
                        foundationDate: this.selectedFDay,
                        slogan: institute.slogan,
                        type: this.selectedType,
                        website: institute.website
                    });
                }
            }
        );
    }
}

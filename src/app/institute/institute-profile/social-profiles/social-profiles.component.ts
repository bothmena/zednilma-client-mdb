import {NgRedux} from '@angular-redux/store';
import {Component, OnInit, ViewChild} from '@angular/core';
import {SocialProfile} from '../../../shared/models/social-profile';
import {AppActions} from '../../../shared/redux/app/app.actions';
import {State} from '../../../shared/redux/models/state';
import {SocialProfileService} from '../../../shared/services/models/social-profile.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastService} from 'ng-uikit-pro-standard';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../shared/services/auth.service';
import {TranslateService} from '@ngx-translate/core';
import {ABOValidators} from '../../../shared/utils/abo.validators';
import {CustomValidators} from 'ng2-validation';
import {HttpResponse} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {first} from 'rxjs/operators';

@Component({
    templateUrl: './social-profiles.component.html',
    styleUrls: ['./social-profiles.component.min.css'],
})
export class SocialProfilesComponent implements OnInit {

    settingForm: FormGroup;
    isSubmitting = false;
    currentSPId: number = null;
    toDeleteId: number = null;
    selectedWebsite: string = null;
    prefix = '';
    placeholder = '';
    socialProfiles: SocialProfile[] = [];
    @ViewChild('form') form;
    @ViewChild('warning') warning;
    websiteOptions: Array<{ value: string, label: string }> = [
        {value: 'linkedin', label: 'LinkedIn'},
        {value: 'github', label: 'GitHub'},
        {value: 'gitlab', label: 'GitLab'},
        {value: 'bitbucket', label: 'Bitbucket'},
        {value: 'facebook', label: 'Facebook'},
        {value: 'twitter', label: 'Twitter'},
        {value: 'youtube', label: 'YouTube'},
        {value: 'google-plus', label: 'Google+'},
        {value: 'instagram', label: 'Instagram'},
        {value: 'vimeo', label: 'Vimeo'},
        {value: 'skype', label: 'Skype'},
        {value: 'whatsapp', label: 'WhatsApp'},
    ];
    websiteOptionsSubject = new BehaviorSubject<Array<{ value: string, label: string }>>([]);
    websiteOptions$: Observable<Array<{ value: string, label: string }>> = this.websiteOptionsSubject.asObservable();
    websites = {
        'linkedin': {
            prefix: 'https://www.linkedin.com/',
            placeholder: 'in/username | company/company_id'
        },
        'github': {
            prefix: 'https://github.com/',
            placeholder: 'username'
        },
        'gitlab': {
            prefix: 'https://gitlab.com/',
            placeholder: 'username'
        },
        'bitbucket': {
            prefix: 'https://bitbucket.org/',
            placeholder: 'username'
        },
        'facebook': {
            prefix: 'https://www.facebook.com/',
            placeholder: 'username'
        },
        'twitter': {
            prefix: 'https://twitter.com/',
            placeholder: 'username'
        },
        'youtube': {
            prefix: 'https://www.youtube.com/channel/',
            placeholder: 'channel_id'
        },
        'google-plus': {
            prefix: 'https://plus.google.com/',
            placeholder: 'profile_id | +username'
        },
        'instagram': {
            prefix: 'https://www.instagram.com/',
            placeholder: 'username'
        },
        'vimeo': {
            prefix: 'https://vimeo.com/',
            placeholder: 'username'
        },
        'skype': {
            prefix: 'Skype/',
            placeholder: 'username'
        },
        'whatsapp': {
            prefix: 'WhatsApp/',
            placeholder: 'username'
        },
    };

    constructor(private ngRedux: NgRedux<State>,
                public router: Router,
                public activatedRoute: ActivatedRoute,
                private spService: SocialProfileService,
                private fb: FormBuilder,
                public authService: AuthService,
                private translator: TranslateService,
                private toast: ToastService) {
    }

    ngOnInit() {

        this.ngRedux.dispatch(AppActions.spWebsiteInit());
        this.authService.insObservable$.subscribe(
            institute => {
                if (!!institute) {
                    this.spService.getInstituteSPs(institute.id).pipe(first()).subscribe(
                        sps => {
                            this.socialProfiles = sps;
                            this.updateWebsiteOptions();
                        }
                    );
                }
            }
        );
        this.buildForm();
    }

    submit() {

        if (this.settingForm.valid) {

            this.isSubmitting = true;
            this.ngRedux.dispatch(AppActions.incrementLoading());
            if (!!this.currentSPId) {
                this.spService.patchInstituteSP(this.authService.institute.id, this.currentSPId, this.settingForm.value).subscribe(
                    () => this.onPatchSPSuccess(),
                    () => {
                        this.isSubmitting = false;
                        this.onSubmitError();
                    },
                );
            } else {
                this.spService.newInstituteSP(this.authService.institute.id, this.settingForm.value).subscribe(
                    (response: HttpResponse<any>) => {

                        const arr = response.headers.get('location').split('/');
                        this.onNewSPSuccess(+arr[arr.length - 1]);
                    },
                    () => {
                        this.isSubmitting = false;
                        this.onSubmitError();
                    },
                );
            }
        }
    }

    initForm(sp: SocialProfile) {

        if (!!sp) {

            this.settingForm.setValue({
                website: sp.website,
                url: sp.url,
                username: sp.username,
            });
            this.currentSPId = sp.id;
            this.selectedWebsite = sp.website;
        } else {
            this.settingForm.reset();
            this.currentSPId = null;
            this.selectedWebsite = null;
        }
        this.form.show();
    }

    onWebsiteChange(event) {
        this.settingForm.get('website').setValue(event.value);
        this.settingForm.get('website').markAsDirty();
        if (!!this.websites[event.value]) {
            this.placeholder = this.websites[event.value].placeholder;
            this.prefix = this.websites[event.value].prefix;
        }
    }

    updateUrl(event) {

        if ( this.selectedWebsite === 'skype' ) {
            this.settingForm.get('url').setValue('https://www.skype.com/' + event.target.value);
        } else if ( this.selectedWebsite === 'whatsapp' ) {
            this.settingForm.get('url').setValue('https://www.whatsapp.com/' + event.target.value);
        } else {
            this.settingForm.get('url').setValue(this.prefix + event.target.value);
        }
    }

    deleteSP() {

        if (!!this.toDeleteId) {

            this.isSubmitting = true;
            this.ngRedux.dispatch(AppActions.decrementLoading());
            this.spService.deleteInstituteSP(this.authService.institute.id, this.toDeleteId).pipe(first()).subscribe(
                () => this.onSPDeleteSuccess(),
                () => {

                    this.translator.get('app.submit_error').pipe(first()).subscribe(message => this.toast.error(message));
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.toDeleteId = null;
                    this.isSubmitting = false;
                    this.warning.hide();
                }
            );
        }
    }

    private buildForm() {

        this.settingForm = this.fb.group({
            'url': ['', Validators.compose([
                Validators.required,
                CustomValidators.url,
            ])],
            'username': ['', Validators.compose([
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(71),
                Validators.pattern(/^[\w.+-\/@]+$/),
            ])],
            'website': ['', Validators.compose([
                Validators.required,
                ABOValidators.inArray(Object.keys(this.websites)),
            ])],
        });
    }

    private onPatchSPSuccess() {

        this.isSubmitting = false;
        this.translator.get('socialProfile.edit_save_success').pipe(first()).subscribe(message => this.toast.success(message));
        for (const sp of this.socialProfiles) {
            if (sp.id === this.currentSPId) {
                sp.url = this.settingForm.get('url').value;
                sp.username = this.settingForm.get('username').value;
                sp.website = this.settingForm.get('website').value;
                break;
            }
        }
        this.settingForm.reset();
        this.form.hide();
        this.currentSPId = null;
    }

    private onNewSPSuccess(id: number) {

        this.isSubmitting = false;
        this.translator.get('socialProfile.new_save_success').pipe(first()).subscribe(message => this.toast.success(message));
        const sp = new SocialProfile();
        sp.id = id;
        sp.url = this.settingForm.get('url').value;
        sp.username = this.settingForm.get('username').value;
        sp.website = this.settingForm.get('website').value;
        this.socialProfiles.push(sp);
        this.updateWebsiteOptions();
        this.settingForm.reset();
        this.form.hide();
        this.currentSPId = null;
    }

    private onSPDeleteSuccess() {

        this.isSubmitting = true;
        this.ngRedux.dispatch(AppActions.decrementLoading());
        for (const email of this.socialProfiles) {
            if (email.id === this.toDeleteId) {
                const index = this.socialProfiles.indexOf(email);
                if (index >= 0) {
                    this.socialProfiles.splice(index, 1);
                }
                break;
            }
        }
        this.updateWebsiteOptions();
        this.toDeleteId = null;
        this.isSubmitting = false;
        this.warning.hide();
    }

    private onSubmitError() {

        this.translator.get('app.submit_error').pipe(first()).subscribe(message => this.toast.error(message));
        this.ngRedux.dispatch(AppActions.decrementLoading());
    }

    private updateWebsiteOptions() {

        const spsWebsites = [];
        for ( const sp of this.socialProfiles ) {
            spsWebsites.push(sp.website);
        }
        const websiteOptions = [];
        for ( const webOption of this.websiteOptions ) {

            if ( spsWebsites.indexOf(webOption.value) === -1 ) {
                websiteOptions.push(webOption);
            }
            this.websiteOptionsSubject.next(websiteOptions);
        }
    }
}

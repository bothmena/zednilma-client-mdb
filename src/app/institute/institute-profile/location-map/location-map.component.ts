import {Component, OnInit, ViewChild} from '@angular/core';
import {Location} from '../../../shared/models/location';
import {ActivatedRoute, Router} from '@angular/router';
import {LocationService} from '../../../shared/services/models/location.service';
import {AuthService} from '../../../shared/services/auth.service';
import {ABOValidators} from '../../../shared/utils/abo.validators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {isoCountries} from '../../../shared/utils/util-functions';
import {ToastService} from 'ng-uikit-pro-standard';
import {AppActions} from '../../../shared/redux/app/app.actions';
import {State} from '../../../shared/redux/models/state';
import {NgRedux} from '@angular-redux/store';
import {TranslateService} from '@ngx-translate/core';
import {HttpResponse} from '@angular/common/http';
import {first} from 'rxjs/operators';

@Component({
    templateUrl: './location-map.component.html',
    styleUrls: ['./location-map.component.min.css'],
})
export class LocationMapComponent implements OnInit {

    settingForm: FormGroup;
    isSubmitting = false;
    currentLocationId: number = null;
    toDeleteId: number = null;
    locations: Location[] = [];
    selectedCountry: string = null;
    countryOptions: Array<{ value: string, label: string }> = [];
    @ViewChild('form') form;

    constructor(private locationService: LocationService,
                private fb: FormBuilder,
                public router: Router,
                public activatedRoute: ActivatedRoute,
                public authService: AuthService,
                private translator: TranslateService,
                private toast: ToastService,
                private ngRedux: NgRedux<State>) {
    }

    ngOnInit() {

        this.authService.insObservable$.subscribe(
            institute => {
                if (!!institute) {
                    this.locationService.getLocations(institute.id).pipe(first()).subscribe(
                        locations => this.locations = locations
                    );
                }
            }
        );
        this.buildForm();
        for (const ccode in isoCountries) {
            if (isoCountries.hasOwnProperty(ccode)) {
                this.countryOptions.push({value: ccode, label: isoCountries[ccode]});
            }
        }
    }

    submit() {

        if (this.settingForm.valid) {

            this.isSubmitting = true;
            this.ngRedux.dispatch(AppActions.incrementLoading());
            if (!!this.currentLocationId) {
                this.locationService.patchLocation(this.authService.institute.id, this.currentLocationId, this.settingForm.value).subscribe(
                    () => this.onSubmitSuccess(),
                    () => this.onSubmitError(),
                );
            } else {
                this.locationService.newLocation(this.authService.institute.id, this.settingForm.value).subscribe(
                    (response: HttpResponse<any>) => {
                        const arr = response.headers.get('location').split('/');
                        this.onSubmitSuccess(+arr[arr.length - 1]);
                    },
                    () => this.onSubmitError(),
                );
            }
        }
    }

    private onSubmitError() {

        this.isSubmitting = false;
        this.translator.get('app.submit_error').pipe(first())
            .subscribe(message => this.toast.error(message, 5000));
        this.ngRedux.dispatch(AppActions.decrementLoading());
    }

    onCountryChange(event) {
        this.settingForm.get('country').setValue(event.value);
        this.settingForm.get('country').markAsDirty();
    }

    initForm(location: Location) {

        this.resetForm(location);
        this.form.show();
    }

    private resetForm(location: Location) {

        if (!!location) {

            this.settingForm.setValue({
                country: location.country,
                state: location.state,
                city: location.city,
                zipCode: location.zipCode,
                address: location.address,
                mapLong: location.mapLong,
                mapLat: location.mapLat,
            });
            this.selectedCountry = location.country;
            this.currentLocationId = location.id;
        } else {
            this.settingForm.reset();
            this.selectedCountry = null;
            this.currentLocationId = null;
        }
    }

    deleteLocation() {

        if (!!this.toDeleteId) {
            this.isSubmitting = true;
            this.ngRedux.dispatch(AppActions.incrementLoading());
            this.locationService.deleteLocation(this.authService.institute.id, this.toDeleteId).subscribe(
                () => {

                    this.translator.get('location.delete_success').pipe(first()).subscribe(message => this.toast.success(message));
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    for ( const location of this.locations ) {
                        if ( location.id === this.toDeleteId ) {
                            const index = this.locations.indexOf(location);
                            if ( index >= 0 ) {
                                this.locations.splice(index, 1);
                            }
                            break;
                        }
                    }
                    this.toDeleteId = null;
                    this.isSubmitting = false;
                },
                () => {

                    this.translator.get('app.submit_error').pipe(first()).subscribe(message => this.toast.error(message));
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.toDeleteId = null;
                    this.isSubmitting = false;
                }
            );
        }
    }

    private onSubmitSuccess(id?: number) {

        this.isSubmitting = false;
        this.ngRedux.dispatch(AppActions.decrementLoading());
        if ( !!this.currentLocationId ) {

            this.translator.get('location.edit_save_success').pipe(first()).subscribe(message => this.toast.success(message));
            for ( let location of this.locations ) {
                if (location.id === this.currentLocationId) {
                    location = this.copyLocationValues(location);
                    break;
                }
            }
        } else {

            this.translator.get('location.new_save_success').pipe(first()).subscribe(message => this.toast.success(message));
            const location = this.copyLocationValues(new Location());
            location.id = id;
            this.locations.push(location);
        }
        this.settingForm.reset();
        this.form.hide();
    }

    private copyLocationValues(location: Location) {

        location.country = this.settingForm.get('country').value;
        location.state = this.settingForm.get('state').value;
        location.city = this.settingForm.get('city').value;
        location.address = this.settingForm.get('address').value;
        location.zipCode = this.settingForm.get('zipCode').value;
        location.mapLat = this.settingForm.get('mapLat').value;
        location.mapLong = this.settingForm.get('mapLong').value;
        return location;
    }

    private buildForm() {

        this.settingForm = this.fb.group({
            'country': ['', Validators.compose([
                Validators.required,
                ABOValidators.inArray(Object.keys(isoCountries)),
            ])],
            'state': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(61),
            ])],
            'city': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(61),
            ])],
            'zipCode': ['', Validators.compose([
                Validators.required,
                Validators.minLength(4),
                Validators.maxLength(4),
                Validators.pattern(/^\d+$/),
            ])],
            'address': ['', Validators.compose([
                Validators.required,
                Validators.required,
                Validators.minLength(10),
                Validators.maxLength(101),
            ])],
            'mapLat': ['', Validators.compose([
                Validators.min(-90),
                Validators.max(90),
            ])],
            'mapLong': ['', Validators.compose([
                Validators.min(-180),
                Validators.max(180),
            ])],
        });
    }
}

import {Component, OnDestroy, OnInit} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {AuthService} from '../../../shared/services/auth.service';
import {ActivatedRoute} from '@angular/router';
import {ABONavigatorService} from '../../../shared/services/abo-navigator.service';

@Component({
    templateUrl: './curriculum.component.html',
    styleUrls: ['./curriculum.component.css']
})
export class CurriculumComponent implements OnInit, OnDestroy {


    insId: number;
    insSlug: string;
    subId: number;
    depSlug: string;
    private ngUnsubscribe: Subject<boolean> = new Subject();

    constructor(private navigator: ABONavigatorService,
                private route: ActivatedRoute,
                private authService: AuthService) {
    }

    ngOnInit() {

        this.authService.insObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            institute => {
                if (!!institute && institute.id > 0) {
                    this.insId = institute.id;
                    this.insSlug = institute.slug;
                    this.subId = +this.route.parent.params['value']['subject_id'];
                    this.depSlug = this.route.parent.params['value']['dep_slug'];
                }
            }
        );
    }

    ngOnDestroy(): void {

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    changePath() {

        this.navigator.goto('i_str_depse', this.insSlug, this.depSlug, '' + this.subId);
    }
}

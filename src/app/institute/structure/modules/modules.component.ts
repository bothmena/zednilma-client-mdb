import {Component, OnInit, ViewChild} from '@angular/core';
import {ABONavigatorService} from '../../../shared/services/abo-navigator.service';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../shared/services/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {ModuleService} from '../../../shared/services/models/module.service';
import {TranslateService} from '@ngx-translate/core';
import {ToastService} from 'ng-uikit-pro-standard';
import {Module} from '../../../shared/models/module';
import {Subject} from '../../../shared/models/subject';
import {InstituteService} from '../../../shared/services/models/institute.service';
import {SpecialityService} from '../../../shared/services/models/speciality.service';
import {InstituteSettings} from '../../../shared/models/institute-settings';
import {ABOValidators} from '../../../shared/utils/abo.validators';
import {SubjectService} from '../../../shared/services/models/subject.service';

@Component({
    templateUrl: './modules.component.html',
    styleUrls: ['./modules.component.css']
})
export class ModulesComponent implements OnInit {

    insId: number;
    insSlug: string;
    specId: number;
    moduleId: number;
    moduleSubjectId: number;
    moduleForm: FormGroup;
    mdlSbjForm: FormGroup;
    maxYears = 1;
    years = [];
    modules: {} = {};
    moduleSubjectIds = {};
    modulesOptions: Array<{value: number, label: string}> = [];
    subjectsOptions: Array<{value: number, label: string}> = [{value: 1, label: 'Subject 1'}, {value: 2, label: 'Subject 2'}];
    yearOptions: Array<{}> = [];
    termOptions: Array<{}> = [];
    @ViewChild('moduleModal') moduleModal;
    @ViewChild('mdlSbjModal') mdlSbjModal;
    @ViewChild('warning') warning;

    constructor(private navigator: ABONavigatorService,
                private fb: FormBuilder,
                private translator: TranslateService,
                private toast: ToastService,
                private route: ActivatedRoute,
                private insService: InstituteService,
                private subService: SubjectService,
                private specService: SpecialityService,
                private mdlService: ModuleService,
                private authService: AuthService) {
    }

    ngOnInit() {

        this.specId = +this.route.parent.snapshot.params['spec_id'];
        this.authService.insObservable$.subscribe(
            institute => {
                if (!!institute && institute.id > 0) {
                    this.insId = institute.id;
                    this.insSlug = institute.slug;
                    this.insService.getInstituteSettings(this.insId).subscribe(
                        settings => this.setTermChoices(settings)
                    );
                    this.specService.getSpeciality(this.insId, this.specId).subscribe(
                        speciality => {
                            this.specService.getSpeciality(this.insId, speciality.parentId).subscribe(specParent => {
                                this.yearOptions = [];
                                this.maxYears = specParent.years;
                                this.buildForms();
                                for (let i = 1; i <= specParent.years; i++)
                                    this.yearOptions.push({value: i, label: i})
                            });
                        }
                    );
                    this.mdlService.getSpecModulesSubjects(this.insId, this.specId).subscribe(
                        moduleSubjects => {

                            this.modules = {};
                            this.modulesOptions = [];
                            outerLoop:
                            for (const moduleSubject of moduleSubjects) {
                                const module: Module = moduleSubject.module;
                                moduleSubject.subject.coefficient = moduleSubject.coefficient;
                                if (!!moduleSubject.subject.id)
                                    this.moduleSubjectIds[module.id + '-' + moduleSubject.subject.id] = moduleSubject.id;

                                if (!!this.modules[module.year + '-' + module.term]) {
                                    for (const mdl of this.modules[module.year + '-' + module.term]) {
                                        if (mdl.id === module.id) {
                                            mdl.addSubject(moduleSubject.subject);
                                            continue outerLoop;
                                        }
                                    }
                                    module.addSubject(moduleSubject.subject);
                                    this.modules[module.year + '-' + module.term].push(module);
                                } else {
                                    module.addSubject(moduleSubject.subject);
                                    this.modules[module.year + '-' + module.term] = [module];
                                }
                                this.modulesOptions.push({value: module.id, label: module.name});
                            }
                            this.years = Object.keys(this.modules);
                        },
                        () => {
                            this.translator.get('specialities.load_mdl_error')
                                .subscribe(trans => this.toast.error(trans));
                        }
                    );
                    this.subService.getSubjects(this.insId).subscribe(
                        subjects => {
                            this.subjectsOptions = [];
                            for (const subject of subjects)
                                this.subjectsOptions.push({value: subject.id, label: subject.name});
                        }
                    )
                }
            }
        );
        this.buildForms();
    }

    submit() {

        if (this.moduleId > 0) {

            this.mdlService.patchModule(this.insId, this.specId, this.moduleId, this.moduleForm.value).subscribe(
                () => { // response
                    this.translator.get('specialities.edit_mdl_success').subscribe(
                        trans => this.toast.success(trans)
                    );
                    this.moduleForm.reset();
                    this.moduleModal.hide();
                },
                () => {
                    this.translator.get('specialities.edit_mdl_error').subscribe(
                        trans => this.toast.error(trans)
                    );
                    this.moduleModal.hide();
                }
            );
        } else {

            this.mdlService.newModule(this.insId, this.specId, this.moduleForm.value).subscribe(
                () => { // response
                    this.translator.get('specialities.new_mdl_success').subscribe(
                        trans => this.toast.success(trans)
                    );
                    this.moduleForm.reset();
                    this.moduleModal.hide();
                },
                () => {
                    this.translator.get('specialities.new_mdl_error').subscribe(
                        trans => this.toast.error(trans)
                    );
                    this.moduleModal.hide();
                }
            );
        }
    }

    submitMS() {

        if (this.moduleSubjectId > 0) {

            const data = {coefficient: this.mdlSbjForm.value['coefficient']};
            this.mdlService.patchModuleSubject(this.insId, this.specId, this.moduleSubjectId, data).subscribe(
                () => { // response
                    this.translator.get('specialities.edit_mdl_sbj_success').subscribe(
                        trans => this.toast.success(trans)
                    );
                    this.mdlSbjForm.reset();
                    this.mdlSbjModal.hide();
                },
                () => {
                    this.translator.get('specialities.edit_mdl_sbj_error').subscribe(
                        trans => this.toast.error(trans)
                    );
                    this.mdlSbjModal.hide();
                }
            );
        } else {

            this.mdlService.newSpecModuleSubject(this.insId, this.specId, this.mdlSbjForm.value).subscribe(
                () => { // response
                    this.translator.get('specialities.add_mdl_sbj_success').subscribe(
                        trans => this.toast.success(trans)
                    );
                    this.mdlSbjForm.reset();
                    this.mdlSbjModal.hide();
                },
                () => {
                    this.translator.get('specialities.add_mdl_sbj_error').subscribe(
                        trans => this.toast.error(trans)
                    );
                    this.mdlSbjModal.hide();
                }
            );
        }
    }

    remove() {

        if (this.moduleId > 0) {
            this.mdlService.deleteModule(this.insId, this.specId, this.moduleId).subscribe(
                () => { // response
                    this.translator.get('specialities.del_mdl_success').subscribe(
                        trans => this.toast.success(trans)
                    );
                    this.warning.hide();
                },
                () => {
                    this.translator.get('specialities.del_mdl_error').subscribe(
                        trans => this.toast.error(trans)
                    );
                    this.warning.hide();
                }
            )
        } else if (this.moduleSubjectId > 0) {
            this.mdlService.deleteModuleSubject(this.insId, this.specId, this.moduleSubjectId).subscribe(
                () => {
                    this.translator.get('specialities.del_mdl_sbj_success').subscribe(
                        trans => this.toast.success(trans)
                    );
                    this.warning.hide();
                },
                () => {
                    this.translator.get('specialities.del_mdl_sbj_error').subscribe(
                        trans => this.toast.error(trans)
                    );
                    this.warning.hide();
                }
            )
        }
    }

    moduleEditPrep(module: Module) {
        this.moduleId = module.id;
        this.moduleSubjectId = 0;
        this.moduleForm.get('name').setValue(module.name);
        this.moduleForm.get('description').setValue(module.description);
        this.moduleForm.get('year').setValue(module.year);
        this.moduleModal.show();
    }

    mdlSbjEditPrep(module: Module, subject: Subject) {

        this.moduleId = 0;
        this.moduleSubjectId = this.moduleSubjectIds[module.id + '-' + subject.id];
        this.mdlSbjForm.get('coefficient').setValue(subject.coefficient);
        this.mdlSbjForm.get('module').setValue(module.id);
        this.mdlSbjForm.get('subject').setValue(subject.id);
        this.mdlSbjModal.show();
    }

    changePath() {

        this.navigator.goto('i_str_spccs', this.insSlug, '' + this.specId)
    }

    splitYear(year: string, index: number) {

        if (index === 1)
            return year.split('-')[index];
        else {
            const yr = +year.split('-')[0];
            return yr > 3 ? '4y_plus' : yr + 'y';
        }

    }

    private buildForms() {

        this.moduleForm = this.fb.group({
            'name': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(61),
            ])],
            'description': ['', Validators.compose([
                Validators.minLength(25),
                Validators.maxLength(500),
            ])],
            'year': ['', Validators.compose([
                CustomValidators.gte(1),
                CustomValidators.lte(this.maxYears),
            ])],
            'term': ['', Validators.compose([
                ABOValidators.inArray(['y1', 's1', 's2', 't1', 't2', 't3', 'q1', 'q2', 'q3', 'q4']),
            ])],
        });

        this.mdlSbjForm = this.fb.group({
            'module': ['', Validators.compose([
                Validators.required,
            ])],
            'subject': ['', Validators.compose([
                Validators.required,
            ])],
            'coefficient': ['', Validators.compose([
                Validators.required,
                CustomValidators.gt(0),
                CustomValidators.lte(10),
            ])],
        });
    }

    private setTermChoices(settings: InstituteSettings) {

        switch (settings.terms) {
            case 'y':
                this.termOptions = [{value: 'y1', label: 'y1'}];
                break;
            case 's':
                this.termOptions = [
                    {value: 's1', label: 's1'},
                    {value: 's2', label: 's2'},
                ];
                break;
            case 't':
                this.termOptions = [
                    {value: 't1', label: 't1'},
                    {value: 't2', label: 't2'},
                    {value: 't3', label: 't3'},
                    ];
                break;
            case 'q':
                this.termOptions = [
                    {value: 'q1', label: 'q1'},
                    {value: 'q2', label: 'q2'},
                    {value: 'q3', label: 'q3'},
                    {value: 'q4', label: 'q4'},
                ];
                break;
        }
    }
}

import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {ABOValidators} from '../../../shared/utils/abo.validators';
import {TranslateService} from '@ngx-translate/core';
import {Exam} from '../../../shared/models/exam';
import {ExamService} from '../../../shared/services/models/exam.service';
import {ToastService} from 'ng-uikit-pro-standard';
import {AuthService} from '../../../shared/services/auth.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {ABONavigatorService} from '../../../shared/services/abo-navigator.service';

@Component({
    templateUrl: './exam.component.html',
    styleUrls: ['./exam.component.css']
})
export class ExamComponent implements OnInit, OnDestroy {


    examForm: FormGroup;
    typeOptions: Array<{ value: number, label: string }> = [];
    exam: Exam = null;
    exams: Exam[] = [];
    insId: number;
    insSlug: string;
    subId: number;
    depSlug: string;
    @ViewChild('examModal') examModal;
    @ViewChild('warning') warning;
    private ngUnsubscribe: Subject<boolean> = new Subject();
    coefSum: number;

    constructor(private navigator: ABONavigatorService,
                private fb: FormBuilder,
                private translator: TranslateService,
                private toast: ToastService,
                private route: ActivatedRoute,
                private examService: ExamService,
                private authService: AuthService) {
    }

    ngOnInit() {

        this.authService.insObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            institute => {
                if (!!institute && institute.id > 0) {
                    this.insId = institute.id;
                    this.insSlug = institute.slug;
                    this.subId = +this.route.parent.params['value']['subject_id'];
                    this.depSlug = this.route.parent.params['value']['dep_slug'];
                    this.examService.getSubjectExams(this.insId, this.subId).subscribe(
                        exams => {
                            this.exams = exams;
                            this.getCoefSum();
                        }
                    )
                }
            }
        );
        const options = [];
        for (let i = 1; i < 6; i++)
            this.translator.get('exam.type.' + i).subscribe(trans => {
                options.push({value: i, label: trans});
                if (options.length === 5)
                    this.typeOptions = options;
            });
        this.buildForms();
    }

    ngOnDestroy(): void {

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    getCoefSum() {

        this.coefSum = 0;
        for (const exam of this.exams) {
            this.coefSum += exam.coefficient;
        }
    }

    isCoefValid() {
        if (this.exam === null)
            return (this.coefSum + this.examForm.get('coefficient').value) <= 100;
        else
            return (this.coefSum - this.exam.coefficient + this.examForm.get('coefficient').value) <= 100;
    }

    changePath() {

        this.navigator.goto('i_str_depsc', this.insSlug, this.depSlug, '' + this.subId);
    }

    submit() {

        if (this.examForm.valid && ((this.coefSum + this.examForm.get('coefficient').value) <= 100)) {
            if (this.exam === null) {
                this.examService.newExam(this.insId, this.subId, this.examForm.value).subscribe(
                    (res: Response) => { // response
                        this.translator.get('exams.new_success').subscribe(
                            trans => this.toast.success(trans)
                        );
                        const exam = new Exam();
                        if (res.headers.has('Location')) {
                            const arr = res.headers.get('Location').split('/');
                            exam.id = +arr[arr.length - 1];
                        }
                        exam.name = this.examForm.get('name').value;
                        exam.type = this.examForm.get('type').value;
                        exam.coefficient = this.examForm.get('coefficient').value;
                        this.exams.push(exam);
                        this.getCoefSum();
                        this.examForm.reset();
                        this.examModal.hide();
                    },
                    () => {
                        this.translator.get('exams.new_error').subscribe(
                            trans => this.toast.error(trans)
                        );
                        this.examModal.hide();
                    }
                );
            } else {

                this.examService.patchExam(this.insId, this.subId, this.exam.id, this.examForm.value).subscribe(
                    () => {
                        this.translator.get('exams.edit_success').subscribe(
                            trans => this.toast.success(trans)
                        );
                        for (const exam of this.exams) {
                            if (exam.id === this.exam.id) {
                                exam.name = this.examForm.get('name').value;
                                exam.type = this.examForm.get('type').value;
                                exam.coefficient = this.examForm.get('coefficient').value;
                            }
                        }
                        this.getCoefSum();
                        this.examForm.reset();
                        this.examModal.hide();
                    },
                    () => {
                        this.translator.get('exams.edit_error').subscribe(
                            trans => this.toast.error(trans)
                        );
                        this.examModal.hide();
                    }
                );
            }
        }
    }

    remove() {

        if (!!this.exam && this.exam.id > 0) {
            this.examService.deleteExam(this.insId, this.subId, this.exam.id).subscribe(
                () => {
                    this.translator.get('exams.delete_success').subscribe(
                        trans => this.toast.success(trans)
                    );
                    for (const index in this.exams) {
                        if (this.exams[index].id === this.exam.id)
                            this.exams.splice(+index, 1);
                    }
                    this.getCoefSum();
                    this.warning.hide();
                },
                () => {
                    this.translator.get('exams.delete_error').subscribe(
                        trans => this.toast.error(trans)
                    );
                    this.warning.hide();
                }
            )
        }
    }

    formPrep() {

        if (this.exam === null)
            this.examForm.reset();
        else {
            this.examForm.setValue({
                'name': this.exam.name,
                'type': this.exam.type,
                'coefficient': this.exam.coefficient,
            });
        }
        this.examModal.show();
    }

    private buildForms() {
        this.examForm = this.fb.group({
            'name': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(50),
            ])],
            'type': ['', Validators.compose([
                Validators.required,
                ABOValidators.inArray([1, 2, 3, 4, 5])
            ])],
            'coefficient': ['', Validators.compose([
                Validators.required,
                CustomValidators.gt(0),
                CustomValidators.lte(100),
            ])],
        });
    }
}

import {Component, OnInit, ViewChild} from '@angular/core';
import {ABONavigatorService} from '../../../shared/services/abo-navigator.service';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../shared/services/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {ClasseService} from '../../../shared/services/models/classe.service';
import {TranslateService} from '@ngx-translate/core';
import {ToastService} from 'ng-uikit-pro-standard';
import {Classe} from '../../../shared/models/classe';
import {SpecialityService} from '../../../shared/services/models/speciality.service';

@Component({
    templateUrl: './classes.component.html',
    styleUrls: ['./classes.component.css']
})
export class ClassesComponent implements OnInit {

    insId: number;
    insSlug: string;
    specId: number;
    classId: number;
    parentYears = 1;
    classeForm: FormGroup;
    classes: Classe[] = [];
    yearOptions: Array<{label: number, value: number}> = [];
    @ViewChild('classeModal') classeModal;
    @ViewChild('warning') warning;

    constructor(private navigator: ABONavigatorService,
                private route: ActivatedRoute,
                private translator: TranslateService,
                private toast: ToastService,
                private fb: FormBuilder,
                private classeService: ClasseService,
                private specService: SpecialityService,
                private authService: AuthService) {
    }

    ngOnInit() {

        this.specId = +this.route.parent.snapshot.params['spec_id'];
        this.authService.insObservable$.subscribe(
            institute => {
                if (!!institute && institute.id > 0) {
                    this.insId = institute.id;
                    this.insSlug = institute.slug;
                    this.specService.getSpeciality(this.insId, this.specId).subscribe(
                        (spec) => {
                            this.specService.getSpeciality(this.insId, spec.parentId).subscribe(specParent => {
                                this.parentYears = specParent.years;
                                this.yearOptions = [];
                                for (let i = 1; i <= this.parentYears; i++)
                                    this.yearOptions.push({label: i, value: i});
                                this.buildForms();
                            });
                        });
                    this.classeService.getSpecClasses(institute.id, this.specId).subscribe(
                        classes => this.classes = classes
                    );
                }
            }
        );
        this.buildForms();
    }

    changePath() {

        this.navigator.goto('i_str_spcms', this.insSlug, '' + this.specId)
    }

    submit() {

        this.classeService.newClasse(this.insId, this.specId, this.classeForm.value).subscribe(
            () => {
                this.translator.get('specialities.new_classe_success', {'quantity': this.classeForm.get('quantity').value}).subscribe(
                    trans => this.toast.success(trans)
                );
                this.classeForm.reset();
                this.classeModal.hide();
            },
            () => {
                this.translator.get('specialities.new_classe_error').subscribe(
                    trans => this.toast.error(trans)
                );
                this.classeModal.hide();
            }
        )
    }

    remove() {

        this.classeService.deleteClasse(this.insId, this.specId, this.classId).subscribe(
            () => {
                this.translator.get('specialities.new_classe_success', {'quantity': this.classeForm.get('quantity').value}).subscribe(
                    trans => this.toast.success(trans)
                );
                this.warning.hide();
            },
            () => {
                this.translator.get('specialities.new_classe_error').subscribe(
                    trans => this.toast.error(trans)
                );
                this.warning.hide();
            }
        )
    }

    private buildForms() {

        this.classeForm = this.fb.group({
            'quantity': ['', Validators.compose([
                Validators.required,
                CustomValidators.lte(35),
                CustomValidators.gte(1),
            ])],
            'level': ['', Validators.compose([
                Validators.required,
                CustomValidators.lte(this.parentYears),
                CustomValidators.gte(1),
            ])],
        });
    }
}

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../shared/services/auth.service';
import {SubjectService} from '../../../shared/services/models/subject.service';
import {Subject} from '../../../shared/models/subject';

@Component({
    templateUrl: './subject.component.html',
    styleUrls: ['./subject.component.scss'],
})
export class SubjectComponent implements OnInit {

    insId: number;
    insSlug: string;
    depSlug: string;
    subId: number;
    subject: Subject = new Subject();

    constructor(private route: ActivatedRoute,
                private subService: SubjectService,
                private authService: AuthService) {
    }

    ngOnInit() {

        this.depSlug = this.route.params['value']['dep_slug'];
        this.subId = this.route.params['value']['subject_id'];
        this.authService.insObservable$.subscribe(
            institute => {
                if (!!institute && institute.id > 0) {
                    this.insId = institute.id;
                    this.insSlug = institute.slug;
                    this.subService.getSubject(institute.id, this.depSlug, this.subId).subscribe(
                        subject => this.subject = subject
                    )
                }
            }
        )
    }
}

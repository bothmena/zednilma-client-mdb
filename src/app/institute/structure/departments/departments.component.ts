import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {State} from '../../../shared/redux/models/state';
import {NgRedux} from '@angular-redux/store';
import {DepartmentService} from '../../../shared/services/models/department.service';
import {Subscription} from 'rxjs';
import {Department} from '../../../shared/models/department';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from '../../../shared/models/subject';
import {AppActions} from '../../../shared/redux/app/app.actions';
import {SubjectService} from '../../../shared/services/models/subject.service';
import {User} from '../../../shared/models/user';
import {InstituteService} from '../../../shared/services/models/institute.service';
import {AuthService} from '../../../shared/services/auth.service';
import {first} from 'rxjs/operators';
import {Institute} from '../../../shared/models/institute';
import {ToastService} from 'ng-uikit-pro-standard';
import {TranslateService} from '@ngx-translate/core';
import {PostService} from '../../../shared/services/models/post.service';
import {Post} from '../../../shared/models/post';
import {DepartmentMember} from '../../../shared/models/department-member';

@Component({
    templateUrl: './departments.component.html',
    styleUrls: ['./departments.component.min.css'],
})
export class DepartmentsComponent implements OnInit, OnDestroy {

    entity = ''; // departmentTemp subjectTemp memberTemp
    depForm: FormGroup;
    subForm: FormGroup;
    memForm: FormGroup;
    insId: number;
    insSlug: string;
    department: Department;
    subject: Subject;
    member: DepartmentMember;
    subscription: Subscription;
    departments: Department[] = [];
    memberOptions: Array<{ value: number, label: string, icon: string }> = [];
    postsOptions: Array<{ value: number, label: string }> = [];
    membersSelect: Array<{ value: number, label: string, icon: string, selected?: boolean }> = [];
    postsSelect: Array<{ value: number, label: string }> = [];
    @ViewChild('memberModal') memberModal;
    @ViewChild('subjectModal') subjectModal;
    @ViewChild('departmentModal') departmentModal;
    @ViewChild('warning') warning;

    constructor(private fb: FormBuilder,
                private depService: DepartmentService,
                private toast: ToastService,
                private insService: InstituteService,
                private postService: PostService,
                private translator: TranslateService,
                private subService: SubjectService,
                private authService: AuthService,
                private ngRedux: NgRedux<State>,
                private route: ActivatedRoute) {
    }

    ngOnInit() {

        this.route.data.subscribe((data: { departments: Array<Department> }) => {
            this.departments = data.departments;
        });
        this.subscription = this.authService.insObservable$.subscribe(
            (institute: Institute) => {
                if (!!institute) {
                    this.insId = institute.id;
                    this.insSlug = institute.slug;
                    this.insService.getInstituteMembers(institute.id, {'role': 'professor', 'order_by': 'first_name'}).pipe(first()).subscribe(
                        (users: User[]) => {
                            for (const user of users) {
                                this.memberOptions.push({value: user.id, label: user.fullName(), icon: user.imageUrl})
                            }
                        },
                        () => {
                            this.translator.get('departments.').pipe(first()).subscribe(
                                (trans) => this.toast.success(trans)
                            )
                        }
                    );

                    this.postService.getPosts(institute.id, {'type': 'department'}).pipe(first()).subscribe(
                        (posts: Post[]) => {
                            for (const post of posts) {
                                this.postsOptions.push({value: post.id, label: post.name})
                            }
                        },
                        () => {
                            this.translator.get('departments.').pipe(first()).subscribe(
                                (trans) => this.toast.error(trans)
                            )
                        }
                    );
                }
            }
        );
        this.buildForm();
    }

    ngOnDestroy(): void {

        this.subscription.unsubscribe();
    }

    openMemberModal(action: string): void {

        const membersIds = [];
        const postsIds = [];
        this.membersSelect = [];
        this.postsSelect = [];
        for (const member of this.department.members) {
            membersIds.push(member.user.id);
            if (!member.post.isMultiple) {
                postsIds.push(member.post.id);
            }
        }
        for (const option of this.memberOptions) {
            if (membersIds.indexOf(option.value) === -1) {
                this.membersSelect.push(option);
            }
        }
        for (const option of this.postsOptions) {
            if (postsIds.indexOf(option.value) === -1) {
                this.postsSelect.push(option);
            }
        }
        if (action === 'new') {
            this.memForm.get('post').setValue(null);
            this.memForm.get('user').setValue(null);
            this.memForm.get('user').enable();
        }
        this.memberModal.show();
    }

    buildForm() {

        this.depForm = this.fb.group({
            'name': ['', Validators.compose([
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(101),
            ])],
            'description': ['', Validators.compose([
                Validators.minLength(45),
                Validators.maxLength(500),
            ])],
        });
        this.subForm = this.fb.group({
            'name': ['', Validators.compose([
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(61),
            ])],
            'description': ['', Validators.compose([
                Validators.minLength(15),
                Validators.maxLength(500),
            ])],
        });
        this.memForm = this.fb.group({
            'user': [null, Validators.required],
            'post': [null, Validators.required],
        });
    }

    submitDepartment() {

        this.department.name = this.depForm.get('name').value;
        this.department.description = this.depForm.get('description').value;
        this.ngRedux.dispatch(AppActions.incrementLoading());
        if (!!this.department.id) {

            this.depService.patchDepartment(this.department.instituteId, this.department.id, this.depForm.value).pipe(first()).subscribe(
                () => {
                    for (const dep of this.departments) {
                        if (dep.id === this.department.id) {
                            dep.name = this.department.name;
                            dep.description = this.department.description;
                        }
                    }
                    this.translator.get('departments.edit_dep_success').pipe(first()).subscribe(
                        trans => this.toast.success(trans)
                    );
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.departmentModal.hide();
                },
                () => {
                    this.translator.get('departments.edit_dep_error').pipe(first()).subscribe(
                        trans => this.toast.error(trans)
                    );
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.departmentModal.hide();
                }
            );
        } else {

            this.depService.newDepartment(this.department.instituteId, this.depForm.value).pipe(first()).subscribe(
                (res: Response) => {
                    if (res.headers.has('Location')) {
                        const arr = res.headers.get('Location').split('/');
                        this.department.id = +arr[arr.length - 1];
                    }
                    this.departments.push(this.department);
                    this.translator.get('departments.add_dep_success').pipe(first()).subscribe(
                        trans => this.toast.success(trans)
                    );
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.departmentModal.hide();
                },
                () => {
                    this.translator.get('departments.add_dep_error').pipe(first()).subscribe(
                        trans => this.toast.error(trans)
                    );
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.departmentModal.hide();
                }
            );
        }
    }

    removeDepartment() {

        this.ngRedux.dispatch(AppActions.incrementLoading());
        this.departmentModal.hide();
        this.depService.deleteDepartment(this.department.instituteId, this.department.id).pipe(first()).subscribe(
            () => {
                for (const depIndex in this.departments) {
                    if (this.departments[depIndex].id === this.department.id) {
                        this.departments.splice(+depIndex, 1);
                    }
                }
                this.translator.get('departments.delete_dep_success').pipe(first()).subscribe(
                    trans => this.toast.success(trans)
                );
                this.ngRedux.dispatch(AppActions.decrementLoading());
            }, () => {
                this.translator.get('departments.delete_dep_error').pipe(first()).subscribe(
                    trans => this.toast.error(trans)
                );
                this.ngRedux.dispatch(AppActions.decrementLoading());
            }
        );
    }

    submitSubject() {

        this.subject.name = this.subForm.get('name').value;
        this.subject.description = this.subForm.get('description').value;
        this.ngRedux.dispatch(AppActions.incrementLoading());
        if (this.subject.id > 0) {

            this.subService.patchSubject(this.department.instituteId, this.department.id, this.subject.id, this.subForm.value).pipe(first()).subscribe(
                () => {
                    for (const dep of this.departments) {
                        if (dep.id === this.department.id) {
                            for (const sub of dep.subjects) {
                                if (sub.id === this.subject.id) {
                                    sub.name = this.subject.name;
                                    sub.description = this.subject.description;
                                }
                            }
                        }
                    }
                    this.translator.get('departments.edit_sub_success').pipe(first()).subscribe(
                        trans => this.toast.success(trans)
                    );
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.subjectModal.hide();
                },
                () => {
                    this.translator.get('departments.edit_sub_error').pipe(first()).subscribe(
                        trans => this.toast.error(trans)
                    );
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.subjectModal.hide();
                }
            );
        } else {

            this.subService.newSubject(this.department.instituteId, this.department.id, this.subForm.value).pipe(first()).subscribe(
                (res: Response) => {
                    if (res.headers.has('Location')) {
                        const arr = res.headers.get('Location').split('/');
                        this.subject.id = +arr[arr.length - 1];
                    }
                    for (const dep of this.departments) {
                        if (dep.id === this.department.id) {
                            dep.addSubject(this.subject);
                        }
                    }
                    this.translator.get('departments.add_sub_success').pipe(first()).subscribe(
                        trans => this.toast.success(trans)
                    );
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.subjectModal.hide();
                },
                () => {
                    this.translator.get('departments.add_sub_error').pipe(first()).subscribe(
                        trans => this.toast.error(trans)
                    );
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.subjectModal.hide();
                }
            );
        }
    }

    removeSubject() {

        this.ngRedux.dispatch(AppActions.incrementLoading());
        this.warning.hide();
        this.subService.deleteSubject(this.department.instituteId, this.department.id, this.subject.id).pipe(first()).subscribe(
            () => {
                for (const dep of this.departments) {
                    if (dep.id === this.department.id) {
                        for (const subIndex in dep.subjects) {
                            if (dep.subjects[subIndex].id === this.subject.id) {
                                dep.subjects.splice(+subIndex, 1);
                            }
                        }
                    }
                }
                this.translator.get('departments.delete_sub_success').pipe(first()).subscribe(
                    trans => this.toast.success(trans)
                );
                this.ngRedux.dispatch(AppActions.decrementLoading());
            }, () => {
                this.translator.get('departments.delete_sub_error').pipe(first()).subscribe(
                    trans => this.toast.error(trans)
                );
                this.ngRedux.dispatch(AppActions.decrementLoading());
            }
        );
    }

    submitMember() {

        if (!!this.member && this.memForm.get('post').valid) {

            const data = {user: this.member.user.id, post: this.memForm.get('post').value};
            this.depService.editMember(this.department.instituteId, this.department.id, this.member.id, data).pipe(first()).subscribe(
                () => {
                    this.translator.get('departments.edit_mem_success').pipe(first()).subscribe(
                        trans => this.toast.success(trans)
                    );
                    this.postService.getPost(this.department.instituteId, this.memForm.get('post').value).pipe(first()).subscribe(
                        post => {
                            for (const dep of this.departments) {
                                if (dep.id === this.department.id) {
                                    for (const member of dep.members) {
                                        if (member.id === this.member.id) {
                                            dep.members.splice(dep.members.indexOf(member), 1);
                                            this.member.post = post;
                                            dep.addMember(this.member);
                                        }
                                    }
                                }
                            }
                        }
                    );
                    this.memberModal.hide();
                },
                () => {
                    this.translator.get('departments.edit_mem_error').pipe(first()).subscribe(
                        trans => this.toast.error(trans)
                    );
                    this.memberModal.hide();
                }
            );
        } else if (this.memForm.valid) {

            this.depService.addMember(this.department.instituteId, this.department.id, this.memForm.value).pipe(first()).subscribe(
                (res: Response) => {
                    this.translator.get('departments.add_mem_success').pipe(first()).subscribe(
                        trans => this.toast.success(trans)
                    );
                    if (res.headers.has('Location')) {
                        const arr = res.headers.get('Location').split('/');
                        this.depService.getMember(this.department.instituteId, this.department.id, +arr[arr.length - 1]).subscribe(
                            (depMember) => {
                                for (const dep of this.departments) {
                                    if (dep.id === this.department.id) {
                                        dep.addMember(depMember);
                                        this.memberModal.hide();
                                        return;
                                    }
                                }
                            }
                        )
                    }
                    this.memberModal.hide();
                },
                () => {
                    this.translator.get('departments.add_mem_error').pipe(first()).subscribe(
                        trans => this.toast.error(trans)
                    );
                    this.memberModal.hide();
                }
            );
        }
    }

    removeMember() {

        this.depService.deleteMember(this.department.instituteId, this.department.id, this.member.id).pipe(first()).subscribe(
            () => {
                for (const index in this.department.members) {
                    if (this.department.members[index].id === this.member.id) {
                        this.department.members.splice(+index, 1);
                        break;
                    }
                }
                this.translator.get('departments.delete_mem_success').pipe(first()).subscribe(
                    trans => this.toast.success(trans)
                );
            },
            () => {
                this.translator.get('departments.delete_mem_error').pipe(first()).subscribe(
                    trans => this.toast.error(trans)
                );
            }
        )
    }

    remove() {
        switch (this.entity) {
            case 'department':
                this.removeDepartment();
                break;
            case 'subject':
                this.removeSubject();
                break;
            case 'member':
                this.removeMember();
                break;
        }
        this.warning.hide();
    }

    openDepartmentModal(department: Department) {

        if (!!department) {
            this.department = department;
        } else {
            this.department = new Department();
            this.department.instituteId = this.insId;
        }
        this.resetDepForm();
        this.departmentModal.show();
    }

    resetDepForm() {

        if (!!this.department.id) {
            this.depForm.setValue({
                'name': this.department.name,
                'description': this.department.description,
            })
        } else {
            this.depForm.setValue({'name': '', 'description': ''});
        }
    }

    openSubjectModal(subject: Subject) {

        console.log('opening sub modal !');
        this.subject = !!subject ? subject : new Subject();
        this.resetSubForm();
        this.subjectModal.show();
    }

    resetSubForm() {

        if (!!this.subject.id) {
            this.subForm.setValue({
                'name': this.subject.name,
                'description': this.subject.description,
            })
        } else {
            this.subForm.setValue({'name': '', 'description': ''});
        }
    }
}

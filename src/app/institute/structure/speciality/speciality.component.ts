import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SpecialityService} from '../../../shared/services/models/speciality.service';
import {AuthService} from '../../../shared/services/auth.service';
import {Speciality} from '../../../shared/models/speciality';

@Component({
    templateUrl: './speciality.component.html',
    styleUrls: ['./speciality.component.scss']
})
export class SpecialityComponent implements OnInit {

    insId: number;
    insSlug: string;
    specId: number;
    speciality: Speciality = new Speciality();

    constructor(private route: ActivatedRoute,
                private specService: SpecialityService,
                private authService: AuthService) {
    }

    ngOnInit() {

        this.specId = this.route.snapshot.params['spec_id'];
        this.authService.insObservable$.subscribe(
            institute => {
                if (!!institute && institute.id > 0) {
                    this.insId = institute.id;
                    this.insSlug = institute.slug;
                    this.specService.getSpeciality(institute.id, this.specId).subscribe(
                        speciality => this.speciality = speciality
                    )
                }
            }
        )
    }
}

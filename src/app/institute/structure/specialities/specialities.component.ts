import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Speciality} from '../../../shared/models/speciality';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ABOValidators} from '../../../shared/utils/abo.validators';
import {CustomValidators} from 'ng2-validation';
import {first, takeUntil} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {ToastService} from 'ng-uikit-pro-standard';
import {SpecialityService} from '../../../shared/services/models/speciality.service';
import {State} from '../../../shared/redux/models/state';
import {NgRedux} from '@angular-redux/store';
import {AppActions} from '../../../shared/redux/app/app.actions';
import {AuthService} from '../../../shared/services/auth.service';
import {Subject} from 'rxjs';
import {PathService} from '../../../shared/services/models/path.service';
import {SpecialityPath} from '../../../shared/models/speciality-path';

@Component({
    templateUrl: './specialities.component.html',
    styleUrls: ['./specialities.component.min.css'],
})
export class SpecialitiesComponent implements OnInit, OnDestroy {

    specsHierarchy: {} = {};
    // entity: string;
    insId: number;
    branchNbr = 1;
    branchesSum = 0;
    // typeOptions: Array<{ value: string, label: string }> = [];
    // customTypeOptions: Array<{ value: string, label: string }> = [];
    cycleOptions: Array<{ value: string, label: string }> = [];
    specialityOptions: Array<{ value: number, label: string }> = [];
    branch1Options: Array<{ value: number, label: string }> = [];
    branch2Options: Array<{ value: number, label: string }> = [];
    branch3Options: Array<{ value: number, label: string }> = [];
    branch4Options: Array<{ value: number, label: string }> = [];
    specForm: FormGroup;
    pathForm: FormGroup;
    speciality: Speciality;
    parentYears: number;
    @ViewChild('specialityModal') specialityModal;
    @ViewChild('pathModal') pathModal;
    @ViewChild('warningModal') warningModal;
    @ViewChild('pathRemoveModal') pathRemoveModal;
    private ngUnsubscribe: Subject<boolean> = new Subject();

    constructor(
        private fb: FormBuilder,
        private toast: ToastService,
        private translator: TranslateService,
        private ngRedux: NgRedux<State>,
        private specService: SpecialityService,
        private authService: AuthService,
        private pathService: PathService,
        private route: ActivatedRoute) {
    }

    ngOnInit() {

        this.route.data.subscribe((data: { specialities: Array<Speciality> }) => { // : { specialities: Array<Speciality>, paths: Array<Path> }

            const specialitiesTemp = {};
            for (const speciality of data.specialities) {

                specialitiesTemp[speciality.id] = speciality;
                for (const child of speciality.children)
                    specialitiesTemp[child.id] = child;
                for (const path of speciality.paths) {
                    const sortedBranches = path.branches.sort((a, b) => a.index === b.index ? 0 : a.index < b.index ? 1 : -1);
                    let i = 1;
                    while (i < sortedBranches.length) {
                        specialitiesTemp[sortedBranches[i].branch.id].addNext(specialitiesTemp[sortedBranches[i - 1].branch.id]);
                        i++;
                    }
                    speciality.addNext(specialitiesTemp[sortedBranches[i - 1].branch.id]);
                }

                if (this.specsHierarchy.hasOwnProperty(speciality.studyCycle))
                    this.specsHierarchy[speciality.studyCycle].push(speciality);
                else
                    this.specsHierarchy[speciality.studyCycle] = [speciality];
            }
        });
        this.authService.insObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(institute => {
            if (institute !== null) {
                this.insId = institute.id;
                this.specService.getSpecialities(this.insId).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
                    (specialities) => {
                        for (const speciality of specialities) {
                            if (speciality.parentId > 0) {
                                for (const cycle in this.specsHierarchy) {
                                    if (this.specsHierarchy.hasOwnProperty(cycle)) {
                                        for (const parent of this.specsHierarchy[cycle]) {
                                            if (parent.id === speciality.parentId)
                                                parent.addChild(speciality)
                                        }
                                    }
                                }
                            } else {
                                if (this.specsHierarchy.hasOwnProperty(speciality.studyCycle)) {
                                    let exist = false;
                                    for (const spec of this.specsHierarchy[speciality.studyCycle]) {
                                        if (spec.id === speciality.id)
                                            exist = true;
                                    }
                                    if (!exist)
                                        this.specsHierarchy[speciality.studyCycle].push(speciality);
                                } else {
                                    this.specsHierarchy[speciality.studyCycle] = [speciality];
                                }
                            }
                        }
                    }
                )
            }
        });
        this.buildForm();
        for (const value of ['ENGINEER_CYCLE', 'ENG_PREPARATORY_CYCLE', 'LICENCE_CYCLE', 'DOCTORATE_CYCLE', 'MASTERE_CYCLE']) {
            this.translator.get('speciality.studyCycle.options.' + value).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
                label => {
                    this.cycleOptions = this.cycleOptions.slice();
                    this.cycleOptions.push({value, label});
                }
            );
        }
    }

    ngOnDestroy(): void {

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    getCycles() {
        return Object.keys(this.specsHierarchy);
    }

    submitSpeciality() {

        if (this.specForm.valid) {
            if (this.speciality.id > 0) {

                this.specService.patchSpeciality(this.insId, this.speciality.id, this.specForm.value).pipe(first()).subscribe(
                    () => {

                        this.ngRedux.dispatch(AppActions.decrementLoading());
                        this.updateSpecValues();
                        for (const cycle in this.specsHierarchy) {
                            if (this.specsHierarchy.hasOwnProperty(cycle)) {
                                for (const specs of this.specsHierarchy[cycle]) {
                                    for (const spec of specs) {
                                        if (spec.id === this.speciality.id) {
                                            this.updateSpeciality(spec);
                                            return;
                                        } else if (spec.id === this.speciality.parentId) {
                                            for (const child of spec.children) {
                                                if (child.id === this.speciality.id) {
                                                    this.updateSpeciality(child);
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        this.translator.get('specialities.edit_spec_success').pipe(first()).subscribe(
                            trans => this.toast.success(trans)
                        );
                        this.specialityModal.hide();
                    },
                    () => {
                        this.translator.get('specialities.edit_spec_error').pipe(first()).subscribe(
                            trans => this.toast.error(trans)
                        );
                        this.specialityModal.hide()
                    }
                )
            } else {

                this.specService.newSpeciality(this.insId, this.specForm.value).pipe(first()).subscribe(
                    () => {

                        this.ngRedux.dispatch(AppActions.decrementLoading());
                        this.updateSpecValues();
                        if (this.speciality.parentId !== null) {
                            for (const cycle in this.specsHierarchy) {
                                if (this.specsHierarchy.hasOwnProperty(cycle)) {
                                    for (const specs of this.specsHierarchy[cycle]) {
                                        for (const spec of specs) {
                                            if (spec.id === this.speciality.parentId) {
                                                spec.addChild(this.speciality);
                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                        } else
                            this.specsHierarchy[this.speciality.studyCycle].push(this.speciality);
                        this.translator.get('specialities.add_spec_success').pipe(first()).subscribe(
                            trans => this.toast.success(trans)
                        );
                        this.specialityModal.hide();
                    },
                    () => {
                        this.translator.get('specialities.add_spec_error').pipe(first()).subscribe(
                            trans => this.toast.error(trans)
                        );
                        this.specialityModal.hide()
                    }
                )
            }
        }
    }

    submitPath() {

        if (this.pathForm.valid && this.speciality.years === this.branchesSum) {
            const data = {};
            for (const index of [1, 2, 3, 4]) {
                const branchId = this.pathForm.get('branch' + index).value;
                if (branchId != null) {
                    data[index] = branchId;
                }
            }
            this.pathService.newPath(this.insId, this.speciality.id, data).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
                () => {
                    this.translator.get('specialities.add_path_success').pipe(takeUntil(this.ngUnsubscribe)).subscribe(
                        trans => this.toast.success(trans)
                    );
                    this.pathModal.hide();
                },
                () => {
                    this.translator.get('specialities.add_path_error').pipe(takeUntil(this.ngUnsubscribe)).subscribe(
                        trans => this.toast.error(trans)
                    );
                    this.pathModal.hide();
                }
            )
        }
    }

    newPath() {

        this.branchNbr = 0;
        this.pathForm.reset();
        this.specialityOptions = [];
        for (const cycle in this.specsHierarchy) {
            if (this.specsHierarchy.hasOwnProperty(cycle)) {
                for (const speciality of this.specsHierarchy[cycle])
                    this.specialityOptions.push({value: speciality.id, label: speciality.name});
            }
        }
        this.pathModal.show();
    }

    onPathSpecChange() {

        this.branchNbr = 1;
        for (const cycle in this.specsHierarchy) {
            if (this.specsHierarchy.hasOwnProperty(cycle)) {
                for (const speciality of this.specsHierarchy[cycle]) {
                    if (speciality.id === this.pathForm.get('speciality').value) {
                        this.speciality = speciality;
                        break;
                    }
                }
            }
        }
        this.pathForm.get('branch1').reset();
        this.branch1Options = [];
        for (const child of this.speciality.children)
            this.branch1Options.push({value: child.id, label: child.name});
    }

    onBranchOneChange() {
        this.branchesYearsSum();
        if (this.branchesSum < this.speciality.years) {
            this.branch2Options = [];
            for (const child of this.speciality.children) {
                if (child.years <= (this.speciality.years - this.branchesSum) && child.id !== this.pathForm.get('branch1').value)
                    this.branch2Options.push({value: child.id, label: child.name});
            }
            this.branchNbr = 2;
        }
    }

    onBranchTwoChange() {
        this.branchesYearsSum();
        if (this.branchesSum < this.speciality.years) {
            this.branch3Options = [];
            const values = [this.pathForm.get('branch1').value, this.pathForm.get('branch2').value];
            for (const child of this.speciality.children) {
                if (child.years <= (this.speciality.years - this.branchesSum) && values.indexOf(child.id) === -1)
                    this.branch3Options.push({value: child.id, label: child.name});
            }
            this.branchNbr = 3;
        }
    }

    onBranchThreeChange() {
        this.branchesYearsSum();
        if (this.branchesSum < this.speciality.years) {
            this.branch4Options = [];
            const values = [this.pathForm.get('branch1').value, this.pathForm.get('branch2').value, this.pathForm.get('branch3').value];
            for (const child of this.speciality.children) {
                if (child.years <= (this.speciality.years - this.branchesSum) && values.indexOf(child.id) === -1)
                    this.branch4Options.push({value: child.id, label: child.name});
            }
            this.branchNbr = 4;
        }
    }

    branchesYearsSum() {
        this.branchesSum = 0;
        const branchesIds = [];
        for (const ctrl of ['branch1', 'branch2', 'branch3', 'branch4']) {
            if (this.pathForm.get(ctrl).value > 0)
                branchesIds.push(this.pathForm.get(ctrl).value);
        }
        for (const child of this.speciality.children) {
            if (branchesIds.indexOf(child.id) > -1) {
                this.branchesSum += child.years;
            }
        }
    }

    removeSpeciality(speciality: Speciality) {

        this.speciality = speciality;
        this.warningModal.show();
    }

    removePaths(speciality) {

        this.speciality = speciality;
        this.pathRemoveModal.show();
    }

    removeSpec() {

        this.specService.deleteSpeciality(this.insId, this.speciality.id).pipe(first()).subscribe(
            () => {
                this.translator.get('specialities.delete_spec_success').pipe(first()).subscribe(
                    trans => this.toast.success(trans)
                );
                const rmElement = (array: Array<any>, element: any) => {
                    const index = array.indexOf(element);
                    if (index > -1)
                        array.splice(index, 1);
                };
                for (const cycle in this.specsHierarchy) {
                    if (this.specsHierarchy.hasOwnProperty(cycle)) {
                        for (const speciality of this.specsHierarchy[cycle]) {
                            if (speciality.id === this.speciality.id) {
                                rmElement(this.specsHierarchy[cycle], speciality);
                                return;
                            }
                            for (const child of speciality.children) {
                                if (child.id === this.speciality.id) {
                                    rmElement(speciality.children, child);
                                    return;
                                }
                            }
                        }
                    }
                }
                this.warningModal.hide();
            },
            () => {
                this.translator.get('specialities.delete_spec_error').pipe(first()).subscribe(
                    trans => this.toast.error(trans)
                );
                this.warningModal.hide();
            }
        );
    }

    removePath(path: SpecialityPath) {

        this.pathService.deletePath(this.insId, this.speciality.id, path.id).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            () => {
                this.translator.get('specialities.delete_path_success').pipe(takeUntil(this.ngUnsubscribe)).subscribe(
                    trans => this.toast.success(trans)
                );
                this.pathRemoveModal.hide();
            },
            () => {
                this.translator.get('specialities.delete_path_error').pipe(takeUntil(this.ngUnsubscribe)).subscribe(
                    trans => this.toast.error(trans)
                );
                this.pathRemoveModal.hide();
            }
        )
    }

    getContainerClass(speciality: Speciality) {

        let clas = 'spec-container ' + speciality.type;
        if (speciality.next.length === 0)
            clas += ' speciality';
        switch (speciality.depth) {
            case 0:
                return clas + ' col-sm-12';
            case 1:
                return clas + ' col-sm-6';
            case 2:
                return clas + ' col-sm-4';
            case 3:
                return clas + ' col-sm-3';
        }
    }

    rowRest(speciality: Speciality) {

        switch (speciality.depth) {
            case 1:
                return ' col-sm-6';
            case 2:
                return ' col-sm-8';
            case 3:
                return ' col-sm-9';
        }
    }

    isSpecFormValid() {
        if (!!this.speciality && this.speciality.type === 'SPECIALITY') {
            return this.specForm.get('studyCycle').value != null && this.specForm.valid;
        } else if (!!this.speciality && this.speciality.type === 'BRANCH') {
            return this.specForm.get('years').value <= this.parentYears && this.specForm.valid;
        }
    }

    openSpecialityModal(speciality: Speciality = null, parentId: number) {

        if (speciality == null) {
            this.speciality = new Speciality();
            if (typeof parentId === 'undefined') {
                this.speciality.type = 'SPECIALITY';
            } else {
                this.speciality.type = 'BRANCH';
                this.speciality.parentId = parentId;
            }
        }
        else
            this.speciality = speciality;
        if (this.speciality.parentId > 0) {
            for (const cycle in this.specsHierarchy) {
                if (this.specsHierarchy.hasOwnProperty(cycle)) {
                    for (const spec of this.specsHierarchy[cycle]) {
                        if (spec.id === this.speciality.parentId) {
                            this.parentYears = spec.years;
                            break;
                        }
                    }
                }
            }
        }
        this.resetSpecForm();
        this.specialityModal.show();
    }

    private resetSpecForm() {

        this.specForm.reset();
        if (this.speciality.id > 0) {
            this.specForm.setValue({
                'name': this.speciality.name,
                'alias': this.speciality.alias,
                'description': this.speciality.description,
                'studyCycle': this.speciality.studyCycle,
                'years': this.speciality.years,
                'type': this.speciality.type,
            });
        } else if (this.speciality.parentId > 0)
            this.specForm.get('parent').setValue(this.speciality.parentId);
        this.specForm.get('type').setValue(this.speciality.type);
        this.specForm.markAsPristine();
    }

    private buildForm() {

        this.specForm = this.fb.group({
            'name': ['', Validators.compose([
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(41),
            ])],
            'alias': ['', Validators.compose([
                Validators.minLength(1),
                Validators.maxLength(10),
            ])],
            'description': [null, Validators.compose([
                Validators.minLength(15),
                Validators.maxLength(500),
            ])],
            'studyCycle': [null, Validators.compose([
                ABOValidators.inArray(['ENGINEER_CYCLE', 'ENG_PREPARATORY_CYCLE', 'LICENCE_CYCLE', 'DOCTORATE_CYCLE', 'MASTERE_CYCLE']),
            ])],
            'years': [null, Validators.compose([
                Validators.required,
                CustomValidators.gt(0),
                CustomValidators.lte((this.speciality && this.speciality.parentId > 0) ? this.speciality.parentId : 10),
            ])],
            'type': ['', Validators.compose([
                Validators.required,
                ABOValidators.inArray(['SPECIALITY', 'BRANCH']),
            ])],
            'parent': [null, CustomValidators.gt(0)],
        });
        this.pathForm = this.fb.group({
            'speciality': ['', Validators.compose([
                Validators.required,
                CustomValidators.gt(0),
            ])],
            'branch1': ['', Validators.compose([
                Validators.required,
                CustomValidators.gt(0),
            ])],
            'branch2': [null, Validators.compose([
                CustomValidators.gt(0),
            ])],
            'branch3': [null, Validators.compose([
                CustomValidators.gt(0),
            ])],
            'branch4': [null, Validators.compose([
                CustomValidators.gt(0),
            ])]
        });
    }

    private updateSpecValues() {

        this.speciality.name = this.specForm.get('name').value;
        this.speciality.alias = this.specForm.get('alias').value;
        this.speciality.description = this.specForm.get('description').value;
        this.speciality.studyCycle = this.specForm.get('studyCycle').value;
        this.speciality.years = this.specForm.get('years').value;
        this.speciality.type = this.specForm.get('type').value;
        this.speciality.parentId = this.specForm.get('parent').value;
    }

    private updateSpeciality(speciality: Speciality) {

        speciality.name = this.speciality.name;
        speciality.alias = this.speciality.alias;
        speciality.description = this.speciality.description;
        speciality.studyCycle = this.speciality.studyCycle;
        speciality.years = this.speciality.years;
        speciality.type = this.speciality.type;
        speciality.parentId = this.speciality.parentId;
    }
}

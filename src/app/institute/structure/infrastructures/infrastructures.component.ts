import {NgRedux} from '@angular-redux/store';
import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {Infrastructure} from '../../../shared/models/infrastructure';
import {AppActions} from '../../../shared/redux/app/app.actions';
import {State} from '../../../shared/redux/models/state';
import {ABOValidators} from '../../../shared/utils/abo.validators';
import {InfrastructureService} from '../../../shared/services/models/infrastructure.service';
import {TranslateService} from '@ngx-translate/core';
import {AuthService} from '../../../shared/services/auth.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {first} from 'rxjs/operators';

@Component({
    templateUrl: './infrastructures.component.html',
    styleUrls: ['./infrastructures.component.min.css'],
})
export class InfrastructuresComponent implements OnInit {

    infraForm: FormGroup;
    infrastructure = new Infrastructure();
    singleIndex = true;
    selectedBlock: number;
    selectedFloor: number;
    typeOptions: Array<{ value: string, label: string }> = [];
    selectedType: string;
    @ViewChild('form') form;
    @ViewChild('warning') warning;
    private plainInfraSubject = new BehaviorSubject<Array<Infrastructure>>([]);
    private nestedInfraSubject = new BehaviorSubject<Array<Infrastructure>>([]);
    infraObservable$: Observable<Array<Infrastructure>> = this.nestedInfraSubject.asObservable();

    constructor(private fb: FormBuilder,
                private authService: AuthService,
                private infraService: InfrastructureService,
                private translator: TranslateService,
                private ngRedux: NgRedux<State>) {
    }

    ngOnInit() {

        this.plainInfraSubject.subscribe((infrastructures: Array<Infrastructure>) => this.infraArray(infrastructures));
        this.authService.insObservable$.subscribe(
            institute => {
                if (!!institute) {

                    this.infraService.getInfrastructures(institute.id).pipe(first()).subscribe(
                        infras => this.plainInfraSubject.next(infras)
                    );
                }
            }
        );
        this.buildForm();
        for (const value of ['CLASSROOM', 'LABORATORY', 'WORKSHOP', 'LIBRARY']) {
            this.translator.get('infrastructure.type.' + value).pipe(first()).subscribe(
                label => {
                    this.typeOptions = this.typeOptions.slice();
                    this.typeOptions.push({value, label});
                }
            );
        }
    }

    submit() {

        this.updateInfraValues();
        if (this.infrastructure.id > 0) {

            this.ngRedux.dispatch(AppActions.incrementLoading());
            const data = this.infraForm.value;
            delete data.indexFrom;
            delete data.indexTo;
            this.infraService.patchInfrastructure(this.authService.institute.id, this.infrastructure.id, data).subscribe(
                () => {
                    this.updateLocalInfra();
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.form.hide();
                },
                () => {
                    // console.log('Oups, an error !', error);
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.form.hide();
                }
            );
            return;
        }

        const indexFrom = this.infraForm.get('indexFrom').value;
        const indexTo = this.infraForm.get('indexTo').value;
        if (!this.singleIndex && indexFrom != null && indexTo != null && indexFrom <= indexTo) {

            for (let i = indexFrom; i <= indexTo; i++) {

                this.infraForm.get('index').setValue(i);
                this.saveData();
            }
            return;
        }
        this.saveData();
    }

    showIndexError() {

        return !this.singleIndex && (0 > +this.infraForm.get('indexFrom').value || +this.infraForm.get('indexFrom').value >= +this.infraForm.get('indexTo').value);
    }

    newBlock() {

        this.infrastructure = new Infrastructure('BLOCK');
        this.resetForm();
        this.onTypeChange({value: 'BLOCK'});
        this.form.show();
    }

    newFloor(blockId: number) {

        this.infrastructure = new Infrastructure('FLOOR');
        this.resetForm();
        this.onTypeChange({value: 'FLOOR'});
        this.infraForm.get('parent').setValue(blockId);
        this.form.show();
    }

    newClassroom(floorId: number) {

        this.infrastructure = new Infrastructure();
        this.resetForm();
        this.infraForm.get('parent').setValue(floorId);
        this.form.show();
    }

    onTypeChange(event) {

        this.infraForm.get('type').setValue(event.value);
        this.infraForm.get('type').markAsDirty();
    }

    editInfrastructure(infraId: number) {

        for (const infra of this.plainInfraSubject.getValue().slice()) {

            if (infra.id === infraId) {
                this.infrastructure = infra;
                this.resetForm();
                this.form.show();
            }
        }
    }

    confirmRemoveInfrastructure(infraId: number) {

        for (const infra of this.plainInfraSubject.getValue().slice()) {

            if (infra.id === infraId) {
                this.infrastructure = infra;
                this.warning.show();
            }
        }
    }

    editBlock() {

        this.infraObservable$.pipe(first()).subscribe(
            (infras: Array<Infrastructure>) => {
                if (!!this.selectedBlock) {
                    for (const infra of infras) {
                        if (infra.id === this.selectedBlock) {
                            this.infrastructure = infra;
                            break;
                        }
                    }
                } else {
                    this.infrastructure = infras[0];
                }
                this.resetForm();
                this.form.show();
            }
        );
    }

    confirmRemoveBlock() {

        this.infraObservable$.pipe(first()).subscribe(
            (infras: Array<Infrastructure>) => {
                for (const infra of infras) {
                    if (infra.id === this.selectedBlock) {
                        this.infrastructure = infra;
                        this.warning.show();
                        break;
                    }
                }
            }
        );
    }

    removeInfra() {

        this.ngRedux.dispatch(AppActions.incrementLoading());
        this.infraService.deleteInfrastructure(this.authService.institute.id, this.infrastructure.id).subscribe(
            () => {
                this.removeLocalInfra();
            },
            () => {
            },
            () => {
                this.ngRedux.dispatch(AppActions.decrementLoading());
            },
        );
    }

    resetForm() {

        this.infraForm.reset();
        if (!!this.infrastructure.id) {
            this.infraForm.get('name').setValue(this.infrastructure.name);
            this.infraForm.get('alias').setValue(this.infrastructure.alias);
            this.infraForm.get('index').setValue(this.infrastructure.index);
            this.infraForm.get('capacity').setValue(this.infrastructure.capacity);
            this.infraForm.get('type').setValue(this.infrastructure.type);
            if (!!this.infrastructure.parentId) {
                this.infraForm.get('parent').setValue(this.infrastructure.parentId);
            }
        }
        this.singleIndex = true;
    }

    formValid(): boolean {

        const type = this.infraForm.get('type').value;
        if (!!type) {

            if (type === 'BLOCK') {

                return this.infraForm.get('name').valid && this.infraForm.get('alias').valid;
            } else if (type === 'FLOOR') {

                return this.infraForm.get('name').valid && this.infraForm.get('alias').valid && (this.infraForm.get('index').valid || (this.infraForm.get('indexFrom').valid &&
                    this.infraForm.get('indexTo').valid));
            } else {

                return this.infraForm.get('name').valid && this.infraForm.get('type').valid && (this.infraForm.get('index').valid || (this.infraForm.get('indexFrom').valid &&
                    this.infraForm.get('indexTo').valid));
            }
        }
        return false;
    }

    updateInfraValues() {

        this.infrastructure.name = this.infraForm.get('name').value;
        this.infrastructure.alias = this.infraForm.get('alias').value;
        this.infrastructure.index = this.infraForm.get('index').value;
        this.infrastructure.capacity = this.infraForm.get('capacity').value;
        this.infrastructure.parentId = this.infraForm.get('parent').value;
    }

    private infraArray(infrastructures: Infrastructure[]) {

        const arr: Array<Infrastructure> = [];
        for (const infra of infrastructures) {

            if (infra.type === 'BLOCK') {

                arr.push(infra);
            } else if (infra.type === 'FLOOR') {

                for (const block of arr) {
                    if (block.id === infra.parentId) {
                        block.addChild(infra);
                        break;
                    }
                }
            } else {

                for (const block of arr) {
                    for (const floor of block.children) {
                        if (floor.id === infra.parentId) {
                            floor.addChild(infra);
                            break;
                        }
                    }
                }
            }
        }
        this.nestedInfraSubject.next(arr);
    }

    private buildForm() {

        this.infraForm = this.fb.group({
            'name': ['', Validators.compose([
                Validators.minLength(3),
                Validators.maxLength(15),
            ])],
            'alias': ['', Validators.compose([
                Validators.minLength(1),
                Validators.maxLength(5),
            ])],
            'index': [null, Validators.compose([
                CustomValidators.gt(-1),
                CustomValidators.lt(101),
            ])],
            'indexFrom': [null, Validators.compose([
                CustomValidators.gt(-1),
                CustomValidators.lt(101),
            ])],
            'indexTo': [null, Validators.compose([
                CustomValidators.gt(-1),
                CustomValidators.lt(101),
            ])],
            'capacity': [null, Validators.compose([
                CustomValidators.gt(0),
            ])],
            'type': ['', Validators.compose([
                Validators.required,
                ABOValidators.inArray(['BLOCK', 'FLOOR', 'CLASSROOM', 'LABORATORY', 'WORKSHOP', 'LIBRARY']),
            ])],
            'parent': [null],
        });
    }

    private saveData() {

        this.ngRedux.dispatch(AppActions.incrementLoading());
        const data = this.infraForm.value;
        delete data.indexFrom;
        delete data.indexTo;
        this.infraService.newInfrastructure(data, this.authService.institute.id).subscribe(
            (res: Response) => {
                if (res.headers.has('Location')) {
                    const arr = res.headers.get('Location').split('/');
                    this.infrastructure.id = +arr[arr.length - 1];
                }
                this.form.hide();
                this.updateLocalInfra();
                this.ngRedux.dispatch(AppActions.decrementLoading());
            },
            () => {
                // console.log('Oups, an error !', error);
                this.ngRedux.dispatch(AppActions.decrementLoading());
            },
        );
    }

    private updateLocalInfra() {

        const infras = this.removeModifiedInfra(this.plainInfraSubject.getValue().slice());
        infras.push(this.infrastructure);
        this.plainInfraSubject.next(infras);
    }

    private removeLocalInfra() {

        const infras = this.removeModifiedInfra(this.plainInfraSubject.getValue().slice());
        this.plainInfraSubject.next(infras);
    }

    private removeModifiedInfra(infras: Array<Infrastructure>): Array<Infrastructure> {

        for (const infra of infras) {
            if (infra.id === this.infrastructure.id) {
                const index = infras.indexOf(infra);
                if (index > -1) {
                    infras.splice(index, 1);
                }
                break;
            }
        }
        return infras;
    }
}

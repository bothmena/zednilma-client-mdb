/**
 * Created by bothmena on 12/02/17.
 */

import {Injectable} from '@angular/core';
import {CanActivate, CanActivateChild, CanLoad,} from '@angular/router';

@Injectable()
export class InstituteGuardService implements CanActivate, CanActivateChild, CanLoad {

    constructor() {
    }

    canActivate(): boolean {

        return this.activate();
    }

    canActivateChild(): boolean {

        return this.activate();
    }

    canLoad(): boolean {

        return true;
        // return this.jwtToken.role === 'ins_admin' || this.jwtToken.role === 'ins_staff';
    }

    private activate(): boolean {

        return true;
        /*if (this.jwtToken.role === 'ins_admin' || this.jwtToken.role === 'ins_staff') {

            return true;
        }
        this.authService.goToProfile();
        return false;*/
    }
}

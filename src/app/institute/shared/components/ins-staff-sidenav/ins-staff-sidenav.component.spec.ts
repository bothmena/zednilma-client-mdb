import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsStaffSidenavComponent } from './ins-staff-sidenav.component';

describe('InsStaffSidenavComponent', () => {
  let component: InsStaffSidenavComponent;
  let fixture: ComponentFixture<InsStaffSidenavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsStaffSidenavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsStaffSidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

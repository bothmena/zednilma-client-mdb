import {Component} from '@angular/core';
import {AuthService} from '../../../../shared/services/auth.service';

@Component({
    selector: 'zdlm-ins-admin-sidenav',
    templateUrl: './ins-admin-sidenav.component.html',
    styleUrls: ['./ins-admin-sidenav.component.scss'],
})
export class InsAdminSidenavComponent {

    constructor(public authService: AuthService) {
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostsStaffComponent } from './posts-staff.component';

describe('PostsStaffComponent', () => {
  let component: PostsStaffComponent;
  let fixture: ComponentFixture<PostsStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostsStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

import {NgRedux} from '@angular-redux/store';
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Location} from '../../../../../shared/models/location';
import {AppActions} from '../../../../../shared/redux/app/app.actions';
import {State} from '../../../../../shared/redux/models/state';
import {LocationService} from '../../../../../shared/services/models/location.service';
import {tnCityState} from '../../../../../shared/utils/util-functions';

@Component({
    selector: 'zdlm-ins-location,[zdlm-ins-location]',
    templateUrl: './location.component.html',
    styleUrls: ['./location.component.min.css'],
})
export class LocationComponent implements OnInit {

    @Input() location: Location;
    @Output() popOut = new EventEmitter<any>();
    @Output() onSaved = new EventEmitter<any>();
    @Output() onDelete = new EventEmitter<any>();
    insId: number;
    stateCities = tnCityState;
    cities: string[] = [];
    locationForm: FormGroup;
    editMode = false;
    isOpen = false;
    isNew = false;

    constructor(private locationService: LocationService,
                private fb: FormBuilder,
                // private translator: TranslateService,
                private ngRedux: NgRedux<State>,) {
    }

    ngOnInit(): void {

        this.ngRedux.select(['firebase', 'data', 'institute_id']).subscribe(
            (insId: number) => {
                this.insId = insId;
                this.onSaved.emit();
            }
        );
        this.isNew = !(this.location.id > 0);
        this.editMode = this.isNew;
        this.isOpen = this.isNew;
        this.buildForm();
        this.resetForm();
    }

    headerClick(event) {

        event.stopPropagation();
    }

    buildForm() {

        this.locationForm = this.fb.group({
            'country': ['TN'],
            'state': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(31),
            ])],
            'city': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(31),
            ])],
            'address': ['', Validators.compose([
                Validators.required,
                Validators.minLength(15),
                Validators.maxLength(101),
            ])],
            'zipCode': ['', Validators.compose([
                Validators.required,
                Validators.pattern(/^\d+$/),
                Validators.minLength(4),
                Validators.maxLength(4),
            ])],
            'isPrimary': [false],
        });
    }

    open() {

        this.popOut.emit(this.isOpen ? 'close' : 'open');
        this.isOpen = !this.isOpen;
    }

    save() {

        console.log(this.locationForm.value);
        this.updateLocation();
        this.ngRedux.dispatch(AppActions.incrementLoading());
        if (this.isNew) {

            this.locationService.newLocation(this.insId, this.locationForm.value).toPromise()
                .then((res: Response) => this.onNewLocationSuccess(res))
                .catch((error) => this.onSubmitError(error));
        } else {

            this.locationService.patchLocation(this.insId, this.location.id, this.locationForm.value).toPromise()
                .then(() => this.onSubmitSuccess())
                .catch((error) => this.onSubmitError(error));
        }
    }

    deleteLocation() {

        if (this.isNew) {

            this.onDelete.emit();
            this.onSaved.emit();
        } else {

            this.ngRedux.dispatch(AppActions.incrementLoading());
            this.locationService.deleteLocation(this.insId, this.location.id).toPromise()
                .then(() => {

                    this.ngRedux.dispatch(AppActions.decrementLoading());
                    this.onDelete.emit();
                })
                .catch((error) => this.onSubmitError(error));
        }
    }

    resetForm() {

        this.locationForm.get('country').setValue('TN');
        this.locationForm.get('state').setValue(this.location.state);
        this.onStateChange();
        this.locationForm.get('city').setValue(this.location.city);
        this.locationForm.get('address').setValue(this.location.address);
        this.locationForm.get('zipCode').setValue(this.location.zipCode);
        this.locationForm.markAsPristine();
    }

    updateLocation() {

        this.location.country = this.locationForm.get('country').value;
        this.location.state = this.locationForm.get('state').value;
        this.location.city = this.locationForm.get('city').value;
        this.location.address = this.locationForm.get('address').value;
        this.location.zipCode = this.locationForm.get('zipCode').value;
        this.location.isPrimary = this.locationForm.get('isPrimary').value;
    }

    onStateChange() {

        const state = this.locationForm.get('state').value;
        if (state === '') {

            this.cities = [];
        } else {

            for (const stt of this.stateCities) {

                if (state === stt.name) {

                    this.cities = stt.cities;
                }
            }
        }
        this.locationForm.get('city').setValue(state);
    }

    onSubmitSuccess() {

        this.isOpen = false;
        this.popOut.emit();
        if (this.isNew) {

            this.isNew = false;
            this.onSaved.emit();
            // this.translator.get( 'location.new_save_success' ).toPromise()
            //     .then( message => toast( message, 5000 ) );
        } else {

            // this.translator.get( 'location.edit_save_success' ).toPromise()
            //     .then( message => toast( message, 5000 ) );
        }
        this.updateLocation();
        this.ngRedux.dispatch(AppActions.decrementLoading());
    }

    onSubmitError(error: any) {

        console.log('error', error.json());
        this.ngRedux.dispatch(AppActions.decrementLoading());
    }

    private onNewLocationSuccess(res: Response) {

        this.onSubmitSuccess();
        if (res.headers.has('Location')) {
            const arr = res.headers.get('Location').split('/');
            this.location.id = +arr[arr.length - 1];
        }
    }
}

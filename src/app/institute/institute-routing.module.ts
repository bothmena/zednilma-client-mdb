import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InstituteComponent} from './institute/institute.component';
import {InstituteGuardService} from './shared/services/institute-guard.service';
import {InstituteProfileComponent} from './institute-profile/institute-profile/institute-profile.component';
import {InstituteSettingsComponent} from './institute-profile/institute-settings/institute-settings.component';
import {AccountSettingsComponent} from './institute-profile/account-settings/account-settings.component';
import {LocationMapComponent} from './institute-profile/location-map/location-map.component';
import {ContactComponent} from './institute-profile/contact/contact.component';
import {SocialProfilesComponent} from './institute-profile/social-profiles/social-profiles.component';
import {ProfileComponent} from './admin/profile/profile.component';
import {SettingsComponent} from './institute-profile/settings/settings.component';
import {InfrastructuresComponent} from './structure/infrastructures/infrastructures.component';
import {InfrastructureComponent} from './structure/infrastructure/infrastructure.component';
import {SpecialitiesComponent} from './structure/specialities/specialities.component';
import {SpecialityComponent} from './structure/speciality/speciality.component';
import {ModulesComponent} from './structure/modules/modules.component';
import {ClassesComponent} from './structure/classes/classes.component';
import {DepartmentsComponent} from './structure/departments/departments.component';
import {DepartmentComponent} from './structure/department/department.component';
import {SubjectComponent} from './structure/subject/subject.component';
import {CurriculumComponent} from './structure/curriculum/curriculum.component';
import {ExamComponent} from './structure/exam/exam.component';
import {MembersComponent} from './members/members/members.component';
import {ProfessorsComponent} from './members/professors/professors.component';
import {PostsComponent} from './members/posts/posts.component';
import {GroupsComponent} from './members/groups/groups.component';
import {StaffComponent} from './members/staff/staff.component';
import {StaffsComponent} from './members/staffs/staffs.component';
import {StudentsComponent} from './members/students/students.component';
import {TimetablesComponent} from './timetables/timetables/timetables.component';
import {TimetableComponent} from './timetables/timetable/timetable.component';
import {WebsiteEditComponent} from '../shared/components/account/website-edit/website-edit.component';
import {CredentialsEditComponent} from '../shared/components/account/credentials-edit/credentials-edit.component';
import {SocialProfilesEditComponent} from '../shared/components/account/social-profiles-edit/social-profiles-edit.component';
import {ContactEditComponent} from '../shared/components/account/contact-edit/contact-edit.component';
import {LocationMapEditComponent} from '../shared/components/account/location-map-edit/location-map-edit.component';
import {AccountInfoEditComponent} from '../shared/components/account/account-info-edit/account-info-edit.component';
import {DepartmentsResolverService} from '../shared/services/resolvers/departments-resolver.service';
import {SpecialtiesResolverService} from '../shared/services/resolvers/specialties-resolver.service';
import {MembersResolverService} from '../shared/services/resolvers/members-resolver.service';
import {StudentComponent} from './members/student/student.component';
import {ProfessorComponent} from './members/professor/professor.component';
import {TimetableSettingsComponent} from './timetables/timetable-settings/timetable-settings.component';

const instituteRoutes: Routes = [
    {
        path: 'i/:institute_slug',
        component: InstituteComponent,
        canActivate: [InstituteGuardService],
        canActivateChild: [InstituteGuardService],
        children: [
            {path: '', component: InstituteProfileComponent},                                           //  i_prf
            {
                path: 'settings', component: InstituteSettingsComponent, children: [
                    {path: '', redirectTo: 'account-info', pathMatch: 'full'},
                    {path: 'account-info', component: AccountSettingsComponent},                        //  i_st_aci  yy -> done
                    {path: 'location-and-map', component: LocationMapComponent},                        //  i_st_lam  yy -> done
                    {path: 'contact-info', component: ContactComponent},                                //  i_st_cti  yy -> done
                    {path: 'social-profiles', component: SocialProfilesComponent},                      //  i_st_spr  yy -> done
                ],
            },
            {
                path: 'admin/:username', children: [
                    {path: '', component: ProfileComponent},                                            //  i_mprf
                    {
                        path: 'settings', component: SettingsComponent, children: [
                            {path: '', redirectTo: 'account-info', pathMatch: 'full'},
                            {path: 'account-info', component: AccountInfoEditComponent},                //  i_mst_aci  yn
                            {path: 'location', component: LocationMapEditComponent},                    //  i_mst_lam  yn
                            {path: 'contact-info', component: ContactEditComponent},                    //  i_mst_cti  yn
                            {path: 'social-profiles', component: SocialProfilesEditComponent},          //  i_mst_spr  yn
                            {path: 'credentials', component: CredentialsEditComponent},                 //  i_mst_crd  yn
                            {path: 'website-preferences', component: WebsiteEditComponent},             //  i_mst_wpr  yn
                        ],
                    },
                ],
            },
            {
                path: 'structure', children: [
                    {path: '', redirectTo: 'infrastructures', pathMatch: 'full'},
                    {
                        path: 'infrastructures', component: InfrastructuresComponent,
                        /*resolve: {
                            infrastructure: InfrastructureResolverService
                        }*/
                    },                                                                                  //  i_str_infs  yy
                    {path: 'infrastructures/:id', component: InfrastructureComponent},                  //  i_str_inf
                    {
                        path: 'specialities', component: SpecialitiesComponent, resolve: {
                            specialities: SpecialtiesResolverService
                        }
                    },                                                                                  //  i_str_spcs  yy
                    {
                        path: 's/:spec_id', component: SpecialityComponent, children: [
                            {path: '', redirectTo: 'modules', pathMatch: 'full'},
                            {path: 'modules', component: ModulesComponent},                             //  i_str_spcms
                            // { path: 'm/:module_slug', component: ModuleComponent },                  //  i_str_spcm
                            {path: 'classes', component: ClassesComponent},                             //  i_str_spccs
                            // { path: 'c/:class_slug', component: ClasseComponent },                   //  i_str_spcc
                        ],
                    },
                    {
                        path: 'departments', component: DepartmentsComponent,
                        resolve: {
                            departments: DepartmentsResolverService
                        }
                    },                                                                                  //  i_str_deps  yy
                    {path: 'd/:dep_slug', component: DepartmentComponent},                              //  i_str_dep
                    {
                        path: 'd/:dep_slug/subjects/:subject_id', component: SubjectComponent, children: [
                            {path: '', redirectTo: 'exams', pathMatch: 'full'},                         //  i_str_depss
                            {path: 'exams', component: ExamComponent},                                  //  i_str_depse,
                            {path: 'curriculum', component: CurriculumComponent},                       //  i_str_depsc
                        ],
                    },
                ],
            },
            {
                path: 'members', component: MembersComponent, children: [
                    {path: '', redirectTo: 'professors', pathMatch: 'full'},
                    {
                        path: 'professors', component: ProfessorsComponent,
                        resolve: {
                            members: MembersResolverService
                        }
                    },                                                                                  //  i_mmb_prfs
                    {path: 'professors/:username', component: ProfessorComponent},                      //  i_mmb_prf
                    {
                        path: 'students', component: StudentsComponent,
                        resolve: {
                            members: MembersResolverService
                        }
                    },                                                                                  //  i_mmb_stds
                    {path: 'students/:username', component: StudentComponent},                          //  i_mmb_std
                    {
                        path: 'staff', component: StaffsComponent,
                        resolve: {
                            members: MembersResolverService
                        }
                    },                                                                                  //  i_mmb_stfs
                    {path: 'staff/:username', component: StaffComponent},                               //  i_mmb_stf
                    {path: 'groups', component: GroupsComponent},                                       //  i_mmb_grps
                    // { path: 'groups/:group_slug', component: GroupComponent },                       //  i_mmb_grp
                    {path: 'posts', component: PostsComponent},                                         //  i_mmb_psts
                ],
            },
            {
                path: 'timetables', children: [
                    // timetables general settings, like sessions times 9:00 -> 10:30, 10:45 -> 12:15, etc...
                    {path: '', redirectTo: 'settings', pathMatch: 'full'},                              //  i_tmt
                    {path: 'settings', component: TimetableSettingsComponent},                          //  i_tmts
                    {path: 'professors', component: TimetablesComponent},                               //  i_tmt_prfs
                    {path: 'professors/:username', component: TimetableComponent},                      //  i_tmt_prf
                    {path: 'classes', component: TimetablesComponent},                                 //  i_tmt_stds
                    {path: 'classes/:class_slug', component: TimetableComponent},                      //  i_tmt_std
                ],
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(instituteRoutes)],
    exports: [RouterModule]
})
export class InstituteRoutingModule {
}

import {NgModule} from '@angular/core';

import {InstituteRoutingModule} from './institute-routing.module';
import {SocialProfilesComponent} from './institute-profile/social-profiles/social-profiles.component';
import {ContactComponent} from './institute-profile/contact/contact.component';
import {LocationMapComponent} from './institute-profile/location-map/location-map.component';
import {AccountSettingsComponent} from './institute-profile/account-settings/account-settings.component';
import {InstituteComponent} from './institute/institute.component';
import {DepartmentsComponent} from './structure/departments/departments.component';
import {ModulesComponent} from './structure/modules/modules.component';
import {CurriculumComponent} from './structure/curriculum/curriculum.component';
import {InfrastructuresComponent} from './structure/infrastructures/infrastructures.component';
import {GroupsComponent} from './members/groups/groups.component';
import {ClassesComponent} from './structure/classes/classes.component';
import {ClubsComponent} from './structure/clubs/clubs.component';
import {InstituteProfileComponent} from './institute-profile/institute-profile/institute-profile.component';
import {InstituteSettingsComponent} from './institute-profile/institute-settings/institute-settings.component';
import {ProfileComponent} from './admin/profile/profile.component';
import {SettingsComponent} from './institute-profile/settings/settings.component';
import {ExamComponent} from './structure/exam/exam.component';
import {TimetableComponent} from './timetables/timetable/timetable.component';
import {PostsComponent} from './members/posts/posts.component';
import {StaffComponent} from './members/staff/staff.component';
import {MembersComponent} from './members/members/members.component';
import {StudentsComponent} from './members/students/students.component';
import {ProfessorsComponent} from './members/professors/professors.component';
import {LocationComponent} from './shared/components/institute/location/location.component';
import {SpecialityComponent} from './structure/speciality/speciality.component';
import {TimetablesComponent} from './timetables/timetables/timetables.component';
import {DepartmentComponent} from './structure/department/department.component';
import {SubjectComponent} from './structure/subject/subject.component';
import {InfrastructureComponent} from './structure/infrastructure/infrastructure.component';
import {StaffsComponent} from './members/staffs/staffs.component';
import {SpecialitiesComponent} from './structure/specialities/specialities.component';
import {AccountModule} from '../shared/modules/account.module';
import {ZdlmCommonModule} from '../shared/modules/zdlm-common.module';
import {InsAdminSidenavComponent} from './shared/components/ins-admin-sidenav/ins-admin-sidenav.component';
import {TranslateModule} from '@ngx-translate/core';
import {StudentComponent} from './members/student/student.component';
import {ProfessorComponent} from './members/professor/professor.component';
import {TimetableSettingsComponent} from './timetables/timetable-settings/timetable-settings.component';

@NgModule({
    imports: [
        AccountModule,
        ZdlmCommonModule,
        TranslateModule,
        InstituteRoutingModule
    ],
    declarations: [
        InstituteComponent,
        AccountSettingsComponent,
        LocationMapComponent,
        ContactComponent,
        SocialProfilesComponent,
        DepartmentsComponent,
        ModulesComponent,
        CurriculumComponent,
        InfrastructuresComponent,
        GroupsComponent,
        ClassesComponent,
        ClubsComponent,
        InstituteProfileComponent,
        InstituteSettingsComponent,
        ProfileComponent,
        SettingsComponent,
        ExamComponent,
        TimetableComponent,
        PostsComponent,
        StaffComponent,
        MembersComponent,
        StudentsComponent,
        ProfessorsComponent,
        LocationComponent,
        SpecialityComponent,
        TimetablesComponent,
        DepartmentComponent,
        SubjectComponent,
        InfrastructureComponent,
        StaffsComponent,
        SpecialitiesComponent,
        InsAdminSidenavComponent,
        StudentComponent,
        ProfessorComponent,
        TimetableSettingsComponent,
    ]
})
export class InstituteModule {
}

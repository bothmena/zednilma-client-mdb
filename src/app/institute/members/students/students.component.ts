import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../../shared/models/user';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../../shared/services/models/user.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs/index';
import {ToastService} from 'ng-uikit-pro-standard';
import {TranslateService} from '@ngx-translate/core';

@Component({
    templateUrl: './students.component.html',
    styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit, OnDestroy {

    private sorted = false;
    private sortedBy = '';
    searchText: string;
    students: User[] = [];
    studentsPage: User[] = [];
    activePage = 1;
    lastPage = 1;
    itemsPerPage: number = +localStorage.getItem('items_per_page') || 20;
    private ngUnsubscribe: Subject<boolean> = new Subject();

    constructor(private route: ActivatedRoute,
                private toast: ToastService,
                private translator: TranslateService,
                private userService: UserService) {
    }

    ngOnInit() {

        this.route.data.subscribe((data: { members: Array<User> }) => {
            this.students = data.members;
            this.paginate();
        });
    }

    ngOnDestroy(): void {

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    sortBy(by: string | any): void {

        this.sortedBy = by;
        this.students.sort((a: any, b: any) => {
            if (a[by] < b[by]) {
                return this.sorted ? 1 : -1;
            }
            if (a[by] > b[by]) {
                return this.sorted ? -1 : 1;
            }
            return 0;
        });

        this.sorted = !this.sorted;
        this.paginate();
    }

    getSortedIcon(attribute: string) {

        if (attribute === this.sortedBy)
            return this.sorted ? 'sort-amount-asc' : 'sort-amount-desc';
        else
            return 'sort';
    }

    filterIt(arr, searchKey) {
        return arr.filter((user: User) => (user.fullName() + ' ' + user.email.toLowerCase() + ' ' + user.username.toLowerCase()).toLowerCase().includes(searchKey));
    }

    paginate() {

        const all = this.search();
        this.lastPage = Math.ceil(all.length / this.itemsPerPage);
        this.studentsPage = all.slice((this.activePage - 1) * this.itemsPerPage, this.activePage * this.itemsPerPage);
    }

    search() {
        if (!this.searchText) {
            return this.students;
        }
        if (this.searchText) {
            return this.filterIt(this.students, this.searchText.toLowerCase());
        }
    }

    changePage(page: number) {
        if (page !== this.activePage) {
            this.activePage = page;
            this.paginate();
        }
    }

    arrayOfPages() {
        return Array(this.lastPage).fill(5).map((_, i) => ++i);
    }

    onSearchKeyUp(value: any) {
        setTimeout(() => {
            if (this.searchText === value)
                this.paginate();
        }, 400);
    }

    changeUserStatus(userId: number, checked: boolean) {

        let action, key;
        if (checked) {
            action = 'enable';
            key = 'members.user.enable_';
        } else {
            action = 'disable';
            key = 'members.user.disable_';
        }

        this.userService.changeStatus(1, userId, action).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            () => {
                this.translator.get(key + 'suc').pipe(takeUntil(this.ngUnsubscribe)).subscribe(
                    trans => this.toast.success(trans)
                );
                for (const student of this.students) {
                    if (student.id === userId) {
                        student.status = action;
                        return;
                    }
                }
            },
            () => {
                this.translator.get(key + 'err').pipe(takeUntil(this.ngUnsubscribe)).subscribe(
                    trans => this.toast.error(trans)
                )
            }
        )
    }

    setItemsPerPage(number: number) {

        this.itemsPerPage = number;
        localStorage.setItem('items_per_page', '' + number);
        this.paginate();
    }
}

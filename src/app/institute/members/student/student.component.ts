import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from '../../../shared/services/models/user.service';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../shared/services/auth.service';
import {takeUntil} from 'rxjs/operators';
import {User} from '../../../shared/models/user';
import {UserDepartment} from '../../../shared/models/user-department';
import {Subject} from 'rxjs';

@Component({
    templateUrl: './student.component.html',
    styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit, OnDestroy {

    user = new User();
    userDep = new UserDepartment();
    insId = 0;
    private ngUnsubscribe: Subject<boolean> = new Subject();

    constructor(private userService: UserService,
                private route: ActivatedRoute,
                // private specService: SpecialityService,
                private authService: AuthService
    ) {
    }

    ngOnInit() {

        this.userService.getUser(this.route.snapshot.params['username']).subscribe(
            user => {
                this.user = user;
                this.authService.insObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
                    institute => {
                        if (institute.id > 0) {
                            this.insId = institute.id;
                            /*this.depService.getMemberDep(institute.id, professor.id).subscribe(
                                userDep => this.userDep = userDep
                            )*/
                        }
                    }
                );
            },
            () => {
                console.error('an error!')
            }
        );
    }

    ngOnDestroy(): void {

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}

import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from '../../../shared/services/models/user.service';
import {User} from '../../../shared/models/user';
import {ActivatedRoute} from '@angular/router';
import {DepartmentService} from '../../../shared/services/models/department.service';
import {AuthService} from '../../../shared/services/auth.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {UserDepartment} from '../../../shared/models/user-department';
import {SocialProfile} from '../../../shared/models/social-profile';
import {SocialProfileService} from '../../../shared/services/models/social-profile.service';
import {TranslateService} from '@ngx-translate/core';
import {ToastService} from 'ng-uikit-pro-standard';
import {SubjectClasseService} from '../../../shared/services/models/subject-classe.service';
import {SubjectClasse} from '../../../shared/models/subject-classe';
import {Subject as MySubject} from '../../../shared/models/subject';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ClasseService} from '../../../shared/services/models/classe.service';
import {SubjectService} from '../../../shared/services/models/subject.service';
import {Classe} from '../../../shared/models/classe';

@Component({
    templateUrl: './professor.component.html',
    styleUrls: ['./professor.component.scss']
})
export class ProfessorComponent implements OnInit, OnDestroy {

    user = new User();
    userDep = new UserDepartment();
    scIds: {};
    socialProfiles: SocialProfile[] = [];
    subjects: MySubject[] = [];
    availableSubjects: MySubject[] = [];
    classes: Classe[] = [];
    classeOptions: Array<any> = [];
    subjectOptions: Array<any> = [];
    insId = 0;
    scToRemove = 0;
    formGroup: FormGroup;
    private ngUnsubscribe: Subject<boolean> = new Subject();

    constructor(private userService: UserService,
                private fb: FormBuilder,
                private route: ActivatedRoute,
                private depService: DepartmentService,
                private spService: SocialProfileService,
                private scService: SubjectClasseService,
                private classeService: ClasseService,
                private subjectService: SubjectService,
                private translator: TranslateService,
                private toast: ToastService,
                private authService: AuthService
    ) {
    }

    ngOnInit() {

        this.userService.getUser(this.route.snapshot.params['username']).subscribe(
            user => {
                this.user = user;
                this.authService.insObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
                    institute => {
                        if (institute.id > 0) {
                            this.insId = institute.id;
                            this.depService.getMemberDep(institute.id, user.id).subscribe(
                                userDep => this.userDep = userDep
                            );
                            this.scService.getProfessorSubClasses(institute.id, user.id).subscribe(
                                scs => this.subjects = this.subClassesToSubjects(scs)
                            );
                            this.subjectService.getSubjects(institute.id).subscribe(
                                subjects => {
                                    this.availableSubjects = subjects;
                                    this.subjectOptions = [];
                                    for (const subject of subjects)
                                        this.subjectOptions.push({value: subject.id, label: subject.name})
                                }
                            );
                            this.classeService.getInsClasses(institute.id).subscribe(
                                classes => this.classes = classes
                            );
                        }
                    }
                );
                this.spService.getUserSPs(user.id).subscribe(
                    sps => this.socialProfiles = sps,
                    () => {
                        this.translator.get('socialProfile.loads_error').subscribe(trans => this.toast.error(trans))
                    }
                );
            },
            () => {
                console.error('an error!')
            }
        );
        this.buildForm();
    }

    submit() {

        const data = this.formGroup.value;
        this.scService.newProfessorSubClasses(this.insId, this.user.id, data).subscribe(
            () => {
                this.translator.get('members.new_sc_success').subscribe(trans => this.toast.success(trans));
            },
            () => this.translator.get('members.new_sc_error').subscribe(trans => this.toast.error(trans))
        )
    }

    removeSC() {

        if (!!this.scToRemove) {
            this.scService.deleteSubClasse(this.insId, this.scToRemove).subscribe(
                () => this.translator.get('members.sc_delete_success').subscribe(
                    trans => this.toast.success(trans)
                ),
                () => this.translator.get('members.sc_delete_error').subscribe(
                    trans => this.toast.error(trans)
                )
            )
        }
    }

    getSCId(subjectId: number, classeId: number) {

        if (this.scIds.hasOwnProperty(subjectId + '-' + classeId))
            return this.scIds[subjectId + '-' + classeId];
        return null;
    }

    ngOnDestroy(): void {

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    private subClassesToSubjects(scs: SubjectClasse[]) {

        const subjects = [];
        this.scIds = {};
        scsLoop:
            for (const sc of scs) {
                this.scIds[sc.subject.id + '-' + sc.classe.id] = sc.id;
                for (const subject of subjects) {
                    if (subject.id === sc.subject.id) {
                        subject.classes.push(sc.classe);
                        continue scsLoop;
                    }
                }
                sc.subject.classes.push(sc.classe);
                subjects.push(sc.subject);
            }
        return subjects;
    }

    buildForm() {

        this.formGroup = this.fb.group({
            'subject': [null, Validators.required],
            'classe': [null, Validators.required],
        });
    }

    onSubjectChange() {

        this.classeOptions = [];
        let subjectClasses: Classe[] = [];
        for (const subject of this.subjects) {
            if (subject.id === this.formGroup.get('subject').value) {
                subjectClasses = subject.classes;
                break;
            }
        }
        let lastBranchId = 0;
        classesLoop:
            for (const classe of this.classes) {
                if (classe.branch.id !== lastBranchId) {
                    lastBranchId = classe.branch.id;
                    this.classeOptions.push({value: '', label: classe.branch.name, group: true})
                }
                for (const cls of subjectClasses) {
                    if (cls.id === classe.id)
                        continue classesLoop;
                }
                this.classeOptions.push({value: classe.id, label: classe.name})
            }
    }
}

import { Component, OnInit } from '@angular/core';
import { ZdlmTranslationLoaderService } from '../../shared/services/zdlm-translation-loader.service';
import {AuthService} from '../../shared/services/auth.service';

@Component( {
    templateUrl: 'institute.component.html',
} )
export class InstituteComponent implements OnInit {

    constructor( private translationLoader: ZdlmTranslationLoaderService, public authService: AuthService) {
    }

    ngOnInit(): void {

        this.translationLoader.loadTranslations( 'institute' );
    }
}

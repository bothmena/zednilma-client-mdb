import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    templateUrl: './t-d-app-container.component.html',
    styleUrls: ['./t-d-app-container.component.scss']
})
export class TDAppContainerComponent implements OnInit {

    username: string;

    constructor(private route: ActivatedRoute) {
    }

    ngOnInit() {

        this.username = this.route.parent.params['value']['username'];
    }
}

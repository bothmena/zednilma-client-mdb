import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';
import {NotificationService} from '../../shared/services/models/notification.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {Notification} from '../../shared/models/notification';
import {NotificationDescriberService} from '../../shared/services/notification-describer.service';
import {ABONavigatorService} from '../../shared/services/abo-navigator.service';

@Component({
    templateUrl: './student.component.html',
    styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit, OnDestroy {

    private ngUnsubscribe: Subject<boolean> = new Subject();
    notifications: Notification[] = [];
    notification: Notification;
    private userId: number;
    private username: string;
    @ViewChild('notifInfoModal') notifInfoModal;

    constructor(
        public authService: AuthService,
        private notifService: NotificationService,
        private navigator: ABONavigatorService,
        public describer: NotificationDescriberService) {
    }

    ngOnInit(): void {

        this.authService.userObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            user => {
                if (!!user && user.id > 0) {
                    this.userId = user.id;
                    this.username = user.username;
                    this.loadNotifications();
                    setInterval(() => this.loadNotifications(), 5000);
                }
            }
        );
    }

    ngOnDestroy(): void {

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    markAsRead(event, notifId: number, mark: boolean) {

        event.stopPropagation();
        this.notifService.markNotification(this.userId, notifId, mark).subscribe(
            () => {
                for (const notif of this.notifications) {
                    if (notif.id === notifId)
                        notif.isRead = !notif.isRead;
                }
            }
        );
    }

    markAllRead() {

        this.notifService.markAllRead(this.userId).subscribe(
            () => {
                for (const notif of this.notifications)
                    notif.isRead = true;
            }
        );
    }

    unreadNotifs() {

        let sum = 0;
        for (const notif of this.notifications) {
            if (notif.isRead === false)
                sum += 1;
        }
        return sum;
    }

    onNotifClick(notif: Notification) {

        // event.stopPropagation();
        this.notification = notif;
        if (notif.type === 0)
            this.notifInfoModal.show();
        else {
            switch (notif.type) {
                case 1:
                    this.navigator.goto('s_tt', this.username);
                    break;
                case 156:
                    this.navigator.goto('s_td', this.username);
                    break;
                case 158:
                    this.navigator.goto('s_td', this.username);
                    break;
            }
        }
    }

    private loadNotifications() {

        this.notifService.getUserNotifications(this.userId).subscribe(
            notifications => {
                this.notifications = notifications;
                for (const notification of this.notifications)
                    notification.description$ = this.describer.describe(notification);
            }
        );
    }
}

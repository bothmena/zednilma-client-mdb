import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../shared/services/auth.service';

@Component({
    templateUrl: './s-r-container.component.html',
    styleUrls: ['./s-r-container.component.scss']
})
export class SRContainerComponent implements OnInit {

    username: string;

    constructor(private route: ActivatedRoute,
                public authService: AuthService) {
    }

    ngOnInit() {

        this.username = this.route.parent.params['value']['username'];
    }
}

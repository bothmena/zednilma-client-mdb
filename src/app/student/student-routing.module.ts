import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StudentComponent} from './student/student.component';
import {SettingsComponent} from '../institute/institute-profile/settings/settings.component';
import {AccountInfoEditComponent} from '../shared/components/account/account-info-edit/account-info-edit.component';
import {LocationMapEditComponent} from '../shared/components/account/location-map-edit/location-map-edit.component';
import {ContactEditComponent} from '../shared/components/account/contact-edit/contact-edit.component';
import {SocialProfilesEditComponent} from '../shared/components/account/social-profiles-edit/social-profiles-edit.component';
import {CredentialsEditComponent} from '../shared/components/account/credentials-edit/credentials-edit.component';
import {WebsiteEditComponent} from '../shared/components/account/website-edit/website-edit.component';
import {StudentProfileComponent} from './student-profile/student-profile.component';
import {TTContainerComponent} from './t-t-container/t-t-container.component';
import {SRContainerComponent} from './s-r-container/s-r-container.component';
import {TDAppContainerComponent} from './t-d-app-container/t-d-app-container.component';

const routes: Routes = [
    {
        path: 's/:username',
        component: StudentComponent,
        // canActivate: [InstituteGuardService],
        // canActivateChild: [InstituteGuardService],
        children: [
            {path: '', component: StudentProfileComponent},                                     //  s_prf
            {
                path: 'settings', component: SettingsComponent, children: [                     //  s_stg
                    {path: '', redirectTo: 'account-info', pathMatch: 'full'},
                    {path: 'account-info', component: AccountInfoEditComponent},                //  s_st_aci
                    {path: 'location', component: LocationMapEditComponent},                    //  s_st_lam
                    {path: 'contact-info', component: ContactEditComponent},                    //  s_st_cti
                    {path: 'social-profiles', component: SocialProfilesEditComponent},          //  s_st_spr
                    {path: 'credentials', component: CredentialsEditComponent},                 //  s_st_crd
                    {path: 'website-preferences', component: WebsiteEditComponent},             //  s_st_wpr
                ],
            },
            {path: 'todo-app', component: TDAppContainerComponent},                                    // s_td
            {path: 'timetable', component: TTContainerComponent},                               // s_tt
            {path: 'session-reports', component: SRContainerComponent},                         // s_sr
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class StudentRoutingModule {
}

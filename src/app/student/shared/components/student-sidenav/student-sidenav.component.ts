import {Component} from '@angular/core';
import {AuthService} from '../../../../shared/services/auth.service';

@Component({
    selector: 'zdlm-student-sidenav',
    templateUrl: './student-sidenav.component.html',
    styleUrls: ['./student-sidenav.component.scss'],
})
export class StudentSidenavComponent {

    constructor(public authService: AuthService) {
    }
}

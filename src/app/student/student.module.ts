import {NgModule} from '@angular/core';

import {StudentRoutingModule} from './student-routing.module';
import {StudentComponent} from './student/student.component';
import {StudentSidenavComponent} from './shared/components/student-sidenav/student-sidenav.component';
import {AccountModule} from '../shared/modules/account.module';
import {ZdlmCommonModule} from '../shared/modules/zdlm-common.module';
import {TranslateModule} from '@ngx-translate/core';
import { StudentProfileComponent } from './student-profile/student-profile.component';
import { TTContainerComponent } from './t-t-container/t-t-container.component';
import { TDAppContainerComponent } from './t-d-app-container/t-d-app-container.component';
import { SRContainerComponent } from './s-r-container/s-r-container.component';

@NgModule({
    imports: [
        AccountModule,
        ZdlmCommonModule,
        TranslateModule,
        StudentRoutingModule,
    ],
    declarations: [
        StudentComponent,
        StudentSidenavComponent,
        StudentProfileComponent,
        TTContainerComponent,
        TDAppContainerComponent,
        SRContainerComponent,
    ]
})
export class StudentModule {
}

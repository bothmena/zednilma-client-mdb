import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../shared/services/auth.service';

@Component({
    templateUrl: './t-t-container.component.html',
    styleUrls: ['./t-t-container.component.scss']
})
export class TTContainerComponent implements OnInit {

    username: string;

    constructor(private route: ActivatedRoute,
                public authService: AuthService) {
    }

    ngOnInit() {

        this.username = this.route.parent.params['value']['username'];
    }
}

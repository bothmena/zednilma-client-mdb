import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';
import {SubjectClasseService} from '../../shared/services/models/subject-classe.service';
import {first, takeUntil} from 'rxjs/operators';
import {SubjectClasse} from '../../shared/models/subject-classe';
import {Subject} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IMyOptions, MDBDatePickerComponent, ToastService} from 'ng-uikit-pro-standard';
import {TranslateService} from '@ngx-translate/core';
import {NgRedux} from '@angular-redux/store';
import {State} from '../../shared/redux/models/state';
import {CustomValidators} from 'ng2-validation';
import {AppActions} from '../../shared/redux/app/app.actions';
import {TaskService} from '../../shared/services/models/task.service';
import {Task} from '../../shared/models/task';

@Component({
    templateUrl: './assignments.component.html',
    styleUrls: ['./assignments.component.scss']
})
export class AssignmentsComponent implements OnInit, OnDestroy {

    taskForm: FormGroup;
    time: string;
    date: string;
    quantity: number;
    unit: string;
    checkbox = false;
    private ngUnsubscribe: Subject<boolean> = new Subject();
    private insId = 0;
    private userId = 0;
    subjects: SubjectClasse[] = [];
    listsByThree = [];
    optionsSelect: { value: number, label: string }[] = [];
    currentTask: Task;
    currentSC: SubjectClasse = null;
    @ViewChild('formModal') formModal;
    @ViewChild('infoModal') infoModal;
    @ViewChild('datePicker') datePicker: MDBDatePickerComponent;
    /**
     * @todo change the year range.
     */
    public myDatePickerOptions: IMyOptions = {
        dateFormat: 'd/m/yyyy',
        minYear: 2018,
        maxYear: 2019
    };

    constructor(private fb: FormBuilder,
                private translate: TranslateService,
                private ngRedux: NgRedux<State>,
                private toast: ToastService,
                private authService: AuthService,
                private scService: SubjectClasseService,
                private taskService: TaskService,
    ) {
    }

    ngOnInit() {

        this.authService.insObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            institute => {
                if (!!institute && institute.id > 0) {
                    this.insId = institute.id;
                    this.authService.userObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
                        user => {
                            if (!!user && user.id > 0) {
                                this.userId = user.id;
                                this.initFunc();
                            }
                        }
                    )
                }
            }
        );
        this.translate.get(['task.type.1', 'task.type.2', 'task.type.3', 'task.type.4']).subscribe(
            trans => {
                this.optionsSelect = [];
                for (const index in trans) {
                    if (trans.hasOwnProperty(index)) {

                        this.optionsSelect.push({value: +index.substr(-1), label: trans[index]})
                    }
                }
            }
        );
        this.buildForm();
    }

    ngOnDestroy(): void {

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    submit() {

        if (!!this.date && !!this.time)
            this.taskForm.get('dueDate').setValue(this.date + ' ' + this.time + ':00');

        if (this.checkbox && !!this.unit && !!this.quantity)
            this.taskForm.get('repeat').setValue(this.quantity + this.unit);
        else
            this.taskForm.get('repeat').setValue('s');

        this.ngRedux.dispatch(AppActions.incrementLoading());
        if (this.taskForm.valid) {

            if (!!this.currentTask) {

                this.taskService.patchTask(this.userId, this.currentTask.id, this.taskForm.value).subscribe(
                    () => {
                        this.initFunc();
                        this.translate.get('task.edit_success').pipe(first()).subscribe(
                            trans => this.toast.success(trans)
                        );
                        this.ngRedux.dispatch(AppActions.decrementLoading());
                        this.formModal.hide();
                    },
                    () => {
                        this.translate.get('task.edit_error').pipe(first()).subscribe(
                            trans => this.toast.error(trans)
                        );
                        this.ngRedux.dispatch(AppActions.decrementLoading());
                        this.formModal.hide();
                    }
                );
            } else {

                this.taskService.newTask(this.userId, this.taskForm.value).subscribe(
                    () => { // response
                        this.initFunc();
                        this.translate.get('task.new_success').pipe(first()).subscribe(
                            trans => this.toast.success(trans)
                        );
                        this.ngRedux.dispatch(AppActions.decrementLoading());
                        this.formModal.hide();
                    },
                    () => {
                        this.translate.get('task.new_error').pipe(first()).subscribe(
                            trans => this.toast.error(trans)
                        );
                        this.ngRedux.dispatch(AppActions.decrementLoading());
                        this.formModal.hide();
                    }
                );
            }
        }
    }

    prepFormModel(subClsId: number) {

        if (!!this.currentTask) {
            this.taskForm.setValue({
                'name': this.currentTask.name,
                'description': this.currentTask.description,
                'dueDate': this.currentTask.dueDate.toLocaleString('en-GB').replace(',', ''),
                'repeat': this.currentTask.repeat,
                'link': this.currentTask.link,
                'subjectClasse': subClsId,
                'list': this.currentTask.list,
                'type': this.currentTask.type,
            });
            this.time = this.currentTask.dueDate.toLocaleTimeString('en-GB').substr(0, 5);
            this.date = this.currentTask.dueDate.getDate() + '/' + (this.currentTask.dueDate.getMonth() + 1) + '/' + this.currentTask.dueDate.getFullYear();
            this.datePicker.onUserDateInput(this.date);
            if (this.currentTask.repeat !== 's') {
                this.quantity = parseInt(this.currentTask.repeat, 10);
                this.unit = this.currentTask.repeat.substr(-1, 1);
            }
        } else {
            this.date = '';
            this.time = '';
            this.taskForm.reset();
            this.taskForm.get('subjectClasse').setValue(subClsId);
            this.taskForm.get('type').setValue(5);
        }

        this.formModal.show();
    }

    getClockClass(dueDate: Date) {

        const now = new Date();
        // @ts-ignore
        const diff = dueDate - now;
        if (diff < 86400000)
            return 'red-text';
        else if (diff < 259200000)
            return 'amber-text';
        return '';
    }

    getRepeat(repeat: string) {

        if (!!repeat) {
            const i = parseInt(repeat, 10);
            return (!!i && i > 1) ? this.translate.get('task.repeat.n_' + repeat.substr(-1, 1), {'n': i}) :
                this.translate.get('task.repeat.1_' + repeat.substr(-1, 1));
        }
        return '';
    }

    private scByThree(scs: SubjectClasse[]) {

        this.subjects = scs;
        let subList = [];
        for (const index in this.subjects) {

            if (+index % 3 === 0) {

                if (subList.length === 0)
                    subList.push(this.subjects[index]);
                else {
                    this.listsByThree.push(subList);
                    subList = [this.subjects[index]];
                }
            } else
                subList.push(this.subjects[index]);
        }
        if (subList.length > 0)
            this.listsByThree.push(subList);
    }

    private buildForm() {

        this.taskForm = this.fb.group({
            'name': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(60),
            ])],
            'description': ['', Validators.compose([
                Validators.minLength(45),
                Validators.maxLength(1000),
            ])],
            'dueDate': ['', Validators.compose([
                Validators.required,
            ])],
            'repeat': ['', Validators.compose([
                Validators.required,
            ])],
            'link': ['', Validators.compose([
                CustomValidators.url,
            ])],
            'subjectClasse': ['', Validators.compose([
                Validators.required,
            ])],
            'type': [5],
            'list': [null],
        });
    }

    private initFunc() {

        this.scService.getProfessorSubClasses(this.insId, this.userId).subscribe(scs => this.scByThree(scs));
    }
}

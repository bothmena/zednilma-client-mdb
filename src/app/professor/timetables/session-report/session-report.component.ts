import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../../shared/services/auth.service';
import {SessionService} from '../../../shared/services/models/session.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {Session} from '../../../shared/models/session';
import {Institute} from '../../../shared/models/institute';
import {AppActions} from '../../../shared/redux/app/app.actions';
import {TranslateService} from '@ngx-translate/core';
import {ToastService} from 'ng-uikit-pro-standard';
import {NgRedux} from '@angular-redux/store';
import {State} from '../../../shared/redux/models/state';
import {ObservableMedia} from '@angular/flex-layout';

@Component({
    templateUrl: './session-report.component.html',
    styleUrls: ['./session-report.component.scss']
})
export class SessionReportComponent implements OnInit, OnDestroy {

    insId: number;
    profId: number;
    editMode = false;
    currentSession: Session;
    sessions: Session[] = [];
    ngUnsubscribe: Subject<boolean> = new Subject();

    constructor(private authService: AuthService,
                private translator: TranslateService,
                private toast: ToastService,
                private ngRedux: NgRedux<State>,
                private sessService: SessionService,
                public media: ObservableMedia) {
    }

    ngOnInit() {

        this.authService.insObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            (institute: Institute) => {
                if (!!institute && institute.id > 0) {
                    this.authService.userObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
                        user => {
                            if (!!user && user.id > 0) {
                                this.sessService.getProfessorSessionReports(institute.id, user.id).subscribe(
                                    sessions => {
                                        if (sessions.length > 0) {
                                            this.currentSession = sessions[0];
                                            this.sessions = sessions;
                                        }
                                    }
                                );
                                this.profId = user.id;
                            }
                        }
                    );
                    this.insId = institute.id;
                }
            }
        )
    }

    ngOnDestroy(): void {

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    submitReport(session: Session, value: string) {

        this.ngRedux.dispatch(AppActions.incrementLoading());
        this.sessService.patchSession(this.insId, session.id, {report: value}).subscribe(
            () => {
                this.translator.get('session.report.save_succ').subscribe(trans => this.toast.success(trans));
                this.ngRedux.dispatch(AppActions.decrementLoading());
                this.editMode = false;
                session.report = value;
            },
            () => {
                this.translator.get('session.report.save_err').subscribe(trans => this.toast.error(trans));
                this.ngRedux.dispatch(AppActions.decrementLoading());
            }
        )
    }
}

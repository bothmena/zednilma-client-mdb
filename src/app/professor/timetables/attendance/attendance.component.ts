import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {User} from '../../../shared/models/user';
import {AuthService} from '../../../shared/services/auth.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {SessionService} from '../../../shared/services/models/session.service';
import {Session} from '../../../shared/models/session';
import {ToastService} from 'ng-uikit-pro-standard';
import {TranslateService} from '@ngx-translate/core';

@Component({
    templateUrl: './attendance.component.html',
    styleUrls: ['./attendance.component.scss']
})
export class AttendanceComponent implements OnInit, OnDestroy {

    students: User[] = [];
    assistants: User[] = [];
    session: Session;
    lastSlide = false;
    usersNbr = 0;
    markedUsers = 0;
    percentageStd = 0;
    percentageAsp = 0;
    insId: number;
    @ViewChild('carouselRef') carouselRef;
    professor: User;
    private ngUnsubscribe: Subject<boolean> = new Subject();

    constructor(private authService: AuthService,
                private toast: ToastService,
                private translator: TranslateService,
                private sessionService: SessionService, ) {
    }

    ngOnInit() {

        this.authService.insObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            institute => {
                if (!!institute && institute.id > 0) {
                    this.insId = institute.id;
                    this.sessionService.getActualSession(institute.id).subscribe(
                        sessionData => {
                            this.students = sessionData['students'];
                            this.assistants = sessionData['assistants'];
                            this.session = sessionData['session'];
                            this.usersNbr = this.students.length + this.assistants.length;
                        }
                    );
                }
            }
        );
        this.authService.userObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            user => {
                if (!!user && user.id > 0)
                    this.professor = user;
            }
        );
    }

    ngOnDestroy(): void {

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    markPresence(user: User, presence: boolean) {

        if (this.markedUsers < this.usersNbr && user.present === null)
            this.markedUsers++;
        user.present = presence;
        this.carouselRef.nextSlide();
        this.lastSlide = true;

        if (this.markedUsers === this.usersNbr)
            this.calculatePercentages();
    }

    submit() {
        const data = [];
        for (const user of this.students)
            data.push({'user': user.id, 'session': this.session.id, 'isPresent': user.present, 'role': 1});

        for (const assistant of this.assistants)
            data.push({'user': assistant.id, 'session': this.session.id, 'isPresent': assistant.present, 'role': 2});

        data.push({'user': this.professor.id, 'session': this.session.id, 'isPresent': true, 'role': 3});

        this.sessionService.saveAttendances(this.insId, data).subscribe(
            (response: Array<any>) => {
                if (response.length === 0) {
                    this.translator.get('attendance.att_save_success').subscribe(
                        trans => this.toast.success(trans)
                    )
                }
            },
            () => {
                this.translator.get('attendance.att_save_error').subscribe(
                    trans => this.toast.error(trans)
                )
            }
        )
    }

    restart() {
        this.carouselRef.selectSlide(0);
        this.markedUsers = 0;
        for (const student of this.students)
            student.present = null;
        for (const assistant of this.assistants)
            assistant.present = null;
    }

    private calculatePercentages() {

        this.percentageStd = 0;
        this.percentageAsp = 0;
        const s_unit = 100 / this.students.length;
        const ap_unit = 100 / this.assistants.length;
        for (const student of this.students) {
            if (student.present === true)
                this.percentageStd += s_unit;
        }
        for (const prof of this.assistants) {
            if (prof.present === true)
                this.percentageAsp += ap_unit;
        }
    }
}

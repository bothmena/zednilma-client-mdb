import {NgModule} from '@angular/core';

import {ProfessorRoutingModule} from './professor-routing.module';
import {TimetableComponent} from './timetables/timetable/timetable.component';
import {AttendanceComponent} from './timetables/attendance/attendance.component';
import {ProfessorComponent} from './professor/professor.component';
import {ZdlmCommonModule} from '../shared/modules/zdlm-common.module';
import {AccountModule} from '../shared/modules/account.module';
import {TranslateModule} from '@ngx-translate/core';
import {ProfSidenavComponent} from './shared/components/prof-sidenav/prof-sidenav.component';
import { SessionReportComponent } from './timetables/session-report/session-report.component';
import { ProfessorProfileComponent } from './professor-profile/professor-profile.component';
import { ProfessorTimetableComponent } from './professor-timetable/professor-timetable.component';
import { AssignmentsComponent } from './assignments/assignments.component';
import { TDAppContainerComponent } from './t-d-app-container/t-d-app-container.component';

@NgModule({
    imports: [
        AccountModule,
        ZdlmCommonModule,
        TranslateModule,
        ProfessorRoutingModule
    ],
    declarations: [
        TimetableComponent,
        AttendanceComponent,
        ProfessorComponent,
        ProfSidenavComponent,
        SessionReportComponent,
        ProfessorProfileComponent,
        ProfessorTimetableComponent,
        AssignmentsComponent,
        TDAppContainerComponent,
    ]
})
export class ProfessorModule {
}

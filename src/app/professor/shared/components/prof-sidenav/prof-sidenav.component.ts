import {Component} from '@angular/core';
import {AuthService} from '../../../../shared/services/auth.service';

@Component({
    selector: 'zdlm-prof-sidenav',
    templateUrl: './prof-sidenav.component.html',
    styleUrls: ['./prof-sidenav.component.scss'],
})
export class ProfSidenavComponent {

    constructor(public authService: AuthService) {
    }
}

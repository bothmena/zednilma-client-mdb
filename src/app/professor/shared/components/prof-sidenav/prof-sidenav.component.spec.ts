import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfSidenavComponent } from './prof-sidenav.component';

describe('StudentSidenavComponent', () => {
  let component: ProfSidenavComponent;
  let fixture: ComponentFixture<ProfSidenavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfSidenavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfSidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

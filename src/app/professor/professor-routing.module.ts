import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AttendanceComponent} from './timetables/attendance/attendance.component';
import {ProfessorComponent} from './professor/professor.component';
import {SessionReportComponent} from './timetables/session-report/session-report.component';
import {ProfessorProfileComponent} from './professor-profile/professor-profile.component';
import {ProfessorTimetableComponent} from './professor-timetable/professor-timetable.component';
import {SettingsComponent} from '../institute/institute-profile/settings/settings.component';
import {AccountInfoEditComponent} from '../shared/components/account/account-info-edit/account-info-edit.component';
import {LocationMapEditComponent} from '../shared/components/account/location-map-edit/location-map-edit.component';
import {ContactEditComponent} from '../shared/components/account/contact-edit/contact-edit.component';
import {SocialProfilesEditComponent} from '../shared/components/account/social-profiles-edit/social-profiles-edit.component';
import {CredentialsEditComponent} from '../shared/components/account/credentials-edit/credentials-edit.component';
import {WebsiteEditComponent} from '../shared/components/account/website-edit/website-edit.component';
import {AssignmentsComponent} from './assignments/assignments.component';
import {TDAppContainerComponent} from './t-d-app-container/t-d-app-container.component';

const routes: Routes = [
    {
        path: 'p/:username',
        component: ProfessorComponent,
        // canActivate: [InstituteGuardService],
        // canActivateChild: [InstituteGuardService],
        children: [
            {path: '', component: ProfessorProfileComponent},                                   //  p_prf
            {
                path: 'settings', component: SettingsComponent, children: [
                    {path: '', redirectTo: 'account-info', pathMatch: 'full'},
                    {path: 'account-info', component: AccountInfoEditComponent},                //  p_st_aci
                    {path: 'location', component: LocationMapEditComponent},                    //  p_st_lam
                    {path: 'contact-info', component: ContactEditComponent},                    //  p_st_cti
                    {path: 'social-profiles', component: SocialProfilesEditComponent},          //  p_st_spr
                    {path: 'credentials', component: CredentialsEditComponent},                 //  p_st_crd
                    {path: 'website-preferences', component: WebsiteEditComponent},             //  p_st_wpr
                ],
            },
            {path: 'todo-app', component: TDAppContainerComponent},                             //  p_td
            {path: 'assignments', component: AssignmentsComponent},                             //  p_asg
            {path: 'timetable', component: ProfessorTimetableComponent},                        //  p_tt
            {path: 'attendance', component: AttendanceComponent},                               //  p_atd
            {path: 'session-reports', component: SessionReportComponent},                       //  p_sr
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProfessorRoutingModule {
}

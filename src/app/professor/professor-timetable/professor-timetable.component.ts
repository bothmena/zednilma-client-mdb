import {Component, OnInit} from '@angular/core';
import {Timetable} from '../../shared/models/timetable';
import {SessionService} from '../../shared/services/models/session.service';
import {AuthService} from '../../shared/services/auth.service';
import {IOption} from 'ng-uikit-pro-standard';
import {weekNbr} from '../../shared/utils/util-functions';

@Component({
    templateUrl: './professor-timetable.component.html',
    styleUrls: ['./professor-timetable.component.scss']
})
export class ProfessorTimetableComponent implements OnInit {

    weeksOptions: Array<{ value: number, label: string }>;
    selectedValue: number;
    selectedWeek: number;
    hours = [];
    timetable: Timetable;
    timetables: { int?: Timetable } = {};
    weekDataLoading = false;
    insId: number;
    userId: number;

    constructor(private sessionService: SessionService, private authService: AuthService) {
    }

    ngOnInit() {

        this.authService.insObservable$.subscribe(institute => {
            if (!!institute && institute.id > 0) {
                this.insId = institute.id;
                this.authService.userObservable$.subscribe(user => {
                    if (!!user && user.id > 0) {
                        this.userId = user.id;
                        this.loadWeekData();
                    }
                })
            }
        });
        this.getCurrentAndNextWeeks();
    }

    onWeekSelect(event: IOption) {

        this.selectedWeek = +event.value;
        this.loadWeekData();
    }

    private getHours(max): number[] {
        let i = max;
        const hours = [];
        while (i > 0) {
            if (i / 60 >= 1) {
                i -= 60;
                hours.push(60);
            } else {
                hours.push(i % 60);
                i = 0;
            }
        }
        return hours;
    }

    getCurrentAndNextWeeks() {
        this.weeksOptions = [];
        const firstDate = new Date();
        const end = new Date();

        this.selectedWeek = weekNbr(firstDate);
        this.selectedValue = this.selectedWeek;
        firstDate.setDate(firstDate.getDate() - firstDate.getDay());
        end.setDate(firstDate.getDate() + 6);
        this.weeksOptions.push({value: this.selectedWeek, label: firstDate.toLocaleDateString('en-GB') + ' - ' + end.toLocaleDateString('en-GB')});

        firstDate.setDate(end.getDate() + 1);
        end.setDate(end.getDate() + 7);
        this.weeksOptions.push({value: weekNbr(firstDate), label: firstDate.toLocaleDateString('en-GB') + ' - ' + end.toLocaleDateString('en-GB')});
    }

    private loadWeekData() {

        this.weekDataLoading = true;
        if (!!this.timetables[this.selectedWeek]) {
            this.timetable = this.timetables[this.selectedWeek];
            this.weekDataLoading = false;
        }
        else {
            this.sessionService.getProfWeekTT(this.insId, this.userId, this.selectedWeek).subscribe(timetable => {
                this.timetable = timetable;
                this.timetables[this.selectedWeek] = timetable;
                this.hours = this.getHours(this.timetable.maxMinutes);
                this.weekDataLoading = false;
            });
        }
    }
}

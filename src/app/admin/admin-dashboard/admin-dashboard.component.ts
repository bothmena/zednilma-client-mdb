import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';
import {first} from 'rxjs/operators';

@Component({
    templateUrl: './admin-dashboard.component.html',
    styleUrls: ['./admin-dashboard.component.css'],
})
export class AdminDashboardComponent implements OnInit {

    username: string;

    constructor(private authService: AuthService) {
    }

    ngOnInit() {

        this.authService.jwtObservable.pipe(first()).subscribe(
            jwt => this.username = jwt.username
        )
    }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from './admin/admin.component';
import {AdminDashboardComponent} from './admin-dashboard/admin-dashboard.component';
import {InstitutesComponent} from './institutes/institutes.component';
import {InsAdminsComponent} from './ins-admins/ins-admins.component';

const routes: Routes = [
    {
        path            : 'admin/:username',
        component       : AdminComponent,
        // canActivate     : [ AdminGuardService ],
        // canActivateChild: [ AdminGuardService ],
        children        : [
            { path: 'dashboard', component: AdminDashboardComponent },                      // a_dsh
            { path: 'institutes', component: InstitutesComponent },                         // a_ins
            { path: 'ins-admins', component: InsAdminsComponent },                          // a_ina
        ],
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

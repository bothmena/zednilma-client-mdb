import { Component, OnInit } from '@angular/core';
import { ZdlmTranslationLoaderService } from '../../shared/services/zdlm-translation-loader.service';

@Component( {
    template: `
        <zdlm-topnav section="admin"></zdlm-topnav>
        <div class="container-fluid">
            <router-outlet></router-outlet>
        </div>
        <zdlm-footer></zdlm-footer>

    `,
} )
export class AdminComponent implements OnInit {

    constructor( private translationLoader: ZdlmTranslationLoaderService ) {
    }

    ngOnInit() {

        this.translationLoader.loadTranslations( 'admin' );
    }
}

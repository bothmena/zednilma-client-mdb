import {Component, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Institute } from '../../shared/models/institute';
import { ABOValidators } from '../../shared/utils/abo.validators';
import {InstituteService} from '../../shared/services/models/institute.service';
import {ToastService} from 'ng-uikit-pro-standard';
import {ModalDirective} from 'ng-uikit-pro-standard';
import {first} from 'rxjs/operators';

@Component( {
    templateUrl: './institutes.component.html',
    styleUrls  : [ './institutes.component.css' ],
} )
export class InstitutesComponent implements OnInit {

    instForm: FormGroup;
    isLoading       = false;
    types           = [ 'PRIMARY_SCHOOL', 'BASIC_SCHOOL', 'SECONDARY_SCHOOL', 'UNIVERSITY' ];
    pCtrl           = [ 'NONE', 'MEDIUM', 'FULL', 'CUSTOM' ];
    typesOptions = [
        { value: 'PRIMARY_SCHOOL', label: 'PRIMARY_SCHOOL' },
        { value: 'BASIC_SCHOOL', label: 'BASIC_SCHOOL' },
        { value: 'SECONDARY_SCHOOL', label: 'SECONDARY_SCHOOL' },
        { value: 'UNIVERSITY', label: 'UNIVERSITY' },
    ];
    pCtrlOptions = [
        { value: 'NONE', label: 'NONE' },
        { value: 'MEDIUM', label: 'MEDIUM' },
        { value: 'FULL', label: 'FULL' },
        { value: 'CUSTOM', label: 'CUSTOM' },
    ];
    private toastOptions = {progressBar: true, timeOut: 6000, positionClass: 'toast-bottom-right'};
    @ViewChild('formModal') private formModal: ModalDirective;

    institutes: Institute[] = [];

    constructor( private fb: FormBuilder,
                 private instituteService: InstituteService,
                 private toast: ToastService ) {
    }

    ngOnInit() {

        this.instituteService.getInstitutes().pipe(first())
            .subscribe( institutes => this.institutes = institutes );
        this.buildForm();
    }

    buildForm(): void {

        this.instForm = this.fb.group( {
            'name'           : [ '', [
                Validators.required,
                Validators.minLength( 5 ),
                Validators.maxLength( 101 ),
            ] ],
            'slogan'         : [ '', [
                Validators.minLength( 10 ),
                Validators.maxLength( 101 ),
            ] ],
            'website'        : [ '', CustomValidators.url ],
            'type'           : [ '', ABOValidators.inArray( this.types ) ],
            'parentalControl': [ '', ABOValidators.inArray( this.pCtrl ) ],
        } );
    }

    onTypeChange(event) {
        this.instForm.get('type').setValue(event.value);
        this.instForm.get('type').markAsDirty();
    }

    onPCtrlChange(event) {
        this.instForm.get('parentalControl').setValue(event.value);
        this.instForm.get('parentalControl').markAsDirty();
    }

    submit() {

        this.isLoading = true;
        this.instituteService.newInstitute( this.instForm.value ).subscribe(
            () => {
                this.toast.success( 'A new institute has been created.', null, this.toastOptions );
                this.isLoading = false;
                this.formModal.hide();
                const institute = new Institute();
                institute.name = this.instForm.get('name').value;
                institute.slogan = this.instForm.get('slogan').value;
                institute.website = this.instForm.get('website').value;
                institute.type = this.instForm.get('type').value;
                institute.parentalControl = this.instForm.get('parentalControl').value;
                this.institutes.push( institute );
                this.instForm.reset();
            },
            ( error ) => {
                console.log( error );
                this.isLoading = false;
            },
        );
    }
}

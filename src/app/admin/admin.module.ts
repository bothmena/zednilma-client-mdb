import {NgModule} from '@angular/core';

import {AdminRoutingModule} from './admin-routing.module';
import {ZdlmCommonModule} from '../shared/modules/zdlm-common.module';
import {AdminComponent} from './admin/admin.component';
import {AdminDashboardComponent} from './admin-dashboard/admin-dashboard.component';
import {InsAdminsComponent} from './ins-admins/ins-admins.component';
import {InstitutesComponent} from './institutes/institutes.component';

@NgModule({
    imports: [
        ZdlmCommonModule,
        AdminRoutingModule
    ],
    declarations: [
        AdminComponent,
        AdminDashboardComponent,
        InsAdminsComponent,
        InstitutesComponent,
    ]
})
export class AdminModule {
}

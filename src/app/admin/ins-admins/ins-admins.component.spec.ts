import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsAdminsComponent } from './ins-admins.component';

describe('InsAdminsComponent', () => {
  let component: InsAdminsComponent;
  let fixture: ComponentFixture<InsAdminsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsAdminsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsAdminsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

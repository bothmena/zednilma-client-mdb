import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {Institute} from '../../shared/models/institute';
import {User} from '../../shared/models/user';
import {UserService} from '../../shared/services/models/user.service';
import {InstituteService} from '../../shared/services/models/institute.service';
import {ModalDirective} from 'ng-uikit-pro-standard';
import {ToastService} from 'ng-uikit-pro-standard';
import {first} from 'rxjs/operators';

@Component({
    templateUrl: './ins-admins.component.html',
    styleUrls: ['./ins-admins.component.min.css'],
})
export class InsAdminsComponent implements OnInit {

    userForm: FormGroup;
    isLoading = false;
    institutes: Array<{ value: string, label: string }>;
    users: User[] = [];
    institutesModels: Institute[];
    private toastOptions = {progressBar: true, timeOut: 6000, positionClass: 'toast-bottom-right'};
    @ViewChild('formModal') private formModal: ModalDirective;

    constructor(private fb: FormBuilder,
                private instituteService: InstituteService,
                private toast: ToastService,
                private userService: UserService ) {
    }

    ngOnInit() {

        this.userService.getUsers({role: 'ins_admin', 'order_by': 'username'}).pipe(first())
            .subscribe(
                users => {
                    this.users = users;
                },
                (error) => console.log(error)
            );
        this.instituteService.getInstitutes({fields: 'id,name'}).pipe(first())
            .subscribe(
                (institutes) => {

                    this.institutes = [];
                    this.institutesModels = institutes;
                    for (const institute of institutes) {

                        this.institutes.push({value: '' + institute.id, label: institute.name});
                    }
                },
                (error) => {
                    console.log(error);
                },
            );
        this.buildForm();
    }

    buildForm(): void {

        this.userForm = this.fb.group({
            'username': ['', [
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(21),
            ]],
            'email': ['', [
                Validators.required,
                Validators.email,
            ]],
            'institute': ['', [
                Validators.required,
                CustomValidators.notEqual(''),
            ]],
            'roles': [['ROLE_INS_ADMIN'], CustomValidators.equal(['ROLE_INS_ADMIN'])],
        });
    }

    onInstituteChange(event) {
        this.userForm.get('institute').setValue(event.value);
        this.userForm.get('institute').markAsDirty();
    }

    setInstituteValue(instName: string) {

        for (const institute of this.institutesModels) {

            if (instName === institute.name) {
                this.userForm.get('institute').setValue(institute.id);
            }
        }
    }

    submit() {

        this.isLoading = true;
        this.userService.newUser( this.userForm.value ).subscribe(
            () => {
                this.toast.success( 'A new professor has been created.', null, this.toastOptions );
                this.isLoading = false;
                this.formModal.hide();
                const user = new User();
                user.username = this.userForm.get( 'username' ).value;
                user.email = this.userForm.get( 'email' ).value;
                user.role = 'ins_admin';
                this.users.push( user );
            },
            ( error ) => {
                console.log( error );
                this.isLoading = false;
            },
        );
    }
}

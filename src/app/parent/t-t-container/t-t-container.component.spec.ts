import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TTContainerComponent } from './t-t-container.component';

describe('TTContainerComponent', () => {
  let component: TTContainerComponent;
  let fixture: ComponentFixture<TTContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TTContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TTContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../shared/services/auth.service';

@Component({
    templateUrl: './t-t-container.component.html',
    styleUrls: ['./t-t-container.component.scss']
})
export class TTContainerComponent implements OnInit {

    childUsername: string;

    constructor(private route: ActivatedRoute,
                public authService: AuthService) {
    }

    ngOnInit() {

        this.childUsername = this.route.params['value']['c_username'];
    }
}

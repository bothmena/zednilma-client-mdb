import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {Notification} from '../../shared/models/notification';
import {AuthService} from '../../shared/services/auth.service';
import {NotificationService} from '../../shared/services/models/notification.service';
import {takeUntil} from 'rxjs/operators';
import {NotificationDescriberService} from '../../shared/services/notification-describer.service';
import {ABONavigatorService} from '../../shared/services/abo-navigator.service';

@Component({
    templateUrl: './parent.component.html',
    styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit, OnDestroy {

    private ngUnsubscribe: Subject<boolean> = new Subject();
    notifications: Notification[] = [];
    notification: Notification;
    private userId: number;
    private username: string;
    @ViewChild('notifInfoModal') notifInfoModal;

    constructor(
        public authService: AuthService,
        private notifService: NotificationService,
        private navigator: ABONavigatorService,
        public describer: NotificationDescriberService) {
    }

    ngOnInit(): void {

        this.authService.userObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            user => {
                if (!!user && user.id > 0) {
                    this.userId = user.id;
                    this.username = user.username;
                    this.loadNotifications();
                    setInterval(() => this.loadNotifications(), 5000);
                }
            }
        );
    }

    ngOnDestroy(): void {

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    markAsRead(event, notifId: number, mark: boolean) {

        event.stopPropagation();
        this.notifService.markNotification(this.userId, notifId, mark).subscribe(
            () => {
                for (const notif of this.notifications) {
                    if (notif.id === notifId)
                        notif.isRead = !notif.isRead;
                }
            }
        );
    }

    markAllRead() {

        this.notifService.markAllRead(this.userId).subscribe(
            () => {
                for (const notif of this.notifications)
                    notif.isRead = true;
            }
        );
    }

    unreadNotifs() {

        let sum = 0;
        for (const notif of this.notifications) {
            if (notif.isRead === false)
                sum += 1;
        }
        return sum;
    }

    onNotifClick(notif: Notification) {

        // event.stopPropagation();
        this.notification = notif;
        if (notif.type === 0 || notif.type === 121)
            this.notifInfoModal.show();
        else {
            switch (notif.type) {
                case 2:
                    this.navigator.goto('r_ttc', this.username, notif.data['student']['username']);
                    break;
                case 157:
                    this.navigator.goto('r_tdc', this.username, notif.data['assignee']['username']);
                    break;
                case 159:
                    this.navigator.goto('r_tdc', this.username, notif.data['assignee']['username']);
                    break;
            }
        }
    }

    getTime(dateStr: string) {

        return (new Date(dateStr)).toLocaleTimeString('en-GB').substr(0, 5);
    }

    private loadNotifications() {

        this.notifService.getUserNotifications(this.userId).subscribe(
            notifications => {
                this.notifications = notifications;
                for (const notification of this.notifications)
                    notification.description$ = this.describer.describe(notification);
            }
        );
    }
}

import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../../shared/services/auth.service';
import {UserService} from '../../../../shared/services/models/user.service';
import {User} from '../../../../shared/models/user';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
    selector: 'zdlm-parent-sidenav',
    templateUrl: './parent-sidenav.component.html',
    styleUrls: ['./parent-sidenav.component.scss']
})
export class ParentSidenavComponent implements OnInit {

    children: User[] = [];
    private ngUnsubscribe: Subject<boolean> = new Subject();

    constructor(public authService: AuthService, private userService: UserService) {
    }

    ngOnInit() {

        this.authService.userObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            user => {
                if (!!user && user.id) {
                    this.userService.getChildren(user.id).subscribe(children => this.children = children)
                }
            }
        );
    }
}

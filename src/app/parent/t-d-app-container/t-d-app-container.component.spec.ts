import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TDAppContainerComponent } from './t-d-app-container.component';

describe('TDAppContainerComponent', () => {
  let component: TDAppContainerComponent;
  let fixture: ComponentFixture<TDAppContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TDAppContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TDAppContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

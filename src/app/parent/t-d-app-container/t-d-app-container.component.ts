import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    templateUrl: './t-d-app-container.component.html',
    styleUrls: ['./t-d-app-container.component.scss']
})
export class TDAppContainerComponent implements OnInit {

    childUsername: string;

    constructor(private route: ActivatedRoute) {
    }

    ngOnInit() {

        this.childUsername = this.route.params['value']['c_username'];
    }
}

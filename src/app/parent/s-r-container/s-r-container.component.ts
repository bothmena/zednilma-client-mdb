import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../shared/services/auth.service';

@Component({
    templateUrl: './s-r-container.component.html',
    styleUrls: ['./s-r-container.component.scss']
})
export class SRContainerComponent implements OnInit {

    childUsername: string;

    constructor(private route: ActivatedRoute,
                public authService: AuthService) {
    }

    ngOnInit() {

        this.childUsername = this.route.params['value']['c_username'];
    }
}

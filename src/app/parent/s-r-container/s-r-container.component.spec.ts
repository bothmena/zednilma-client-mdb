import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SRContainerComponent } from './s-r-container.component';

describe('SRContainerComponent', () => {
  let component: SRContainerComponent;
  let fixture: ComponentFixture<SRContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SRContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SRContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

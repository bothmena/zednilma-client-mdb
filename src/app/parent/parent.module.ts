import {NgModule} from '@angular/core';

import {ParentRoutingModule} from './parent-routing.module';
import {ParentComponent} from './parent/parent.component';
import {ParentProfileComponent} from './parent-profile/parent-profile.component';
import {ParentSidenavComponent} from './shared/components/parent-sidenav/parent-sidenav.component';
import {AccountModule} from '../shared/modules/account.module';
import {ZdlmCommonModule} from '../shared/modules/zdlm-common.module';
import {TranslateModule} from '@ngx-translate/core';
import { TTContainerComponent } from './t-t-container/t-t-container.component';
import { TDAppContainerComponent } from './t-d-app-container/t-d-app-container.component';
import { SRContainerComponent } from './s-r-container/s-r-container.component';

@NgModule({
    imports: [
        AccountModule,
        ZdlmCommonModule,
        TranslateModule,
        ParentRoutingModule
    ],
    declarations: [
        ParentComponent,
        ParentProfileComponent,
        ParentSidenavComponent,
        TTContainerComponent,
        TDAppContainerComponent,
        SRContainerComponent
    ]
})
export class ParentModule {
}

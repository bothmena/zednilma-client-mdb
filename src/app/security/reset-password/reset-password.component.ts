import {Component, OnInit} from '@angular/core';
import {ABONavigatorService} from '../../shared/services/abo-navigator.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastService} from 'ng-uikit-pro-standard';
import {TranslateService} from '@ngx-translate/core';
import {UserService} from '../../shared/services/models/user.service';
import {HttpErrorResponse} from '@angular/common/http';
import {CustomValidators} from 'ng2-validation';
import * as zxcvbn from 'zxcvbn';
import {ActivatedRoute} from '@angular/router';
import {first} from 'rxjs/operators';

@Component({
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

    resetForm: FormGroup;
    isLoading = false;
    token: string;
    passClass = {value: 0, type: 'danger'};
    private toastOptions = {progressBar: true, timeOut: 6000, positionClass: 'toast-bottom-right'};

    constructor(private fb: FormBuilder,
                private route: ActivatedRoute,
                private translator: TranslateService,
                private toast: ToastService,
                private navigator: ABONavigatorService,
                private userService: UserService) {
    }

    ngOnInit() {

        this.route.params.pipe(first()).subscribe(
            params => {
                this.token = params['token'];
                this.userService.getUserExistByToken(this.token, true).pipe(first()).subscribe(
                    exist => {
                        if (!exist) {
                            this.navigator.goto('w_hm');
                            this.translator.get('server_err.user.not_found.by_token', {token: this.token}).pipe(first()).subscribe(
                                trans => this.toast.error(trans, null, this.toastOptions)
                            );
                        }
                    }
                );
            }
        );
        this.buildForm();
    }

    buildForm(): void {

        const password = new FormControl('', Validators.compose([
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(51),
            Validators.pattern(/^(?=\S*\d)(?=\S*[a-z])(?=\S*[A-Z])\S*$/),
        ]));
        const confirmPassword = new FormControl('', CustomValidators.equalTo(password));
        this.resetForm = this.fb.group({
            'plainPassword': this.fb.group({
                'password': password,
                'confirm_password': confirmPassword,
            }),
        });
    }

    resetPassword() {

        if (this.resetForm.valid) {

            this.isLoading = true;
            this.userService.resetPassword(this.token, this.resetForm.value).subscribe(
                () => {

                    this.isLoading = false;
                    this.translator.get('w_pg.login.pass_reset_succ').pipe(first()).subscribe(
                        (trans) => this.toast.success(trans, null, this.toastOptions)
                    );
                    this.navigator.goto('c_lgi');
                },
                (error: HttpErrorResponse) => {

                    if (error.status === 404) {
                        this.translator.get('server_err.user.not_found.by_token', {token: this.token}).pipe(first()).subscribe(
                            (trans) => this.toast.error(trans, null, this.toastOptions)
                        )
                    } else {
                        this.translator.get('server_err.general').pipe(first()).subscribe(
                            (trans) => this.toast.error(trans, null, this.toastOptions)
                        )
                    }
                    this.isLoading = false;
                }
            )
        }
    }

    passStrengthFn() {

        switch (zxcvbn(this.resetForm.get('plainPassword.password').value).score) {

            case 0:
                this.passClass = {value: 20, type: 'danger'};
                break;
            case 1:
                this.passClass = {value: 40, type: 'warning'};
                break;
            case 2:
                this.passClass = {value: 60, type: 'info'};
                break;
            case 3:
                this.passClass = {value: 80, type: 'primary'};
                break;
            case 4:
                this.passClass = {value: 100, type: 'success'};
                break;
        }
    }
}

import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {AuthService} from '../../shared/services/auth.service';
import {HttpErrorResponse} from '@angular/common/http';
import {ToastService} from 'ng-uikit-pro-standard';
import {first} from 'rxjs/operators';

@Component({
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    isLoading = false;
    private toastOptions = {progressBar: true, timeOut: 6000, positionClass: 'toast-bottom-right'};

    constructor(private fb: FormBuilder,
                private translator: TranslateService,
                private toast: ToastService,
                private authService: AuthService) {
    }

    ngOnInit() {

        this.buildForms();
    }

    buildForms(): void {

        this.loginForm = this.fb.group({
            'username': ['', [
                Validators.required,
            ]],
            'password': ['', [
                Validators.required,
            ]],
        });
    }

    login() {

        if (this.loginForm.valid) {

            this.isLoading = true;
            this.authService.login(this.loginForm.value).subscribe(
                () => {
                    this.authService.goToProfile();
                },
                (error: HttpErrorResponse) => {
                    if (401 === +error.status) {
                        this.translator.get('app.bad_creds').pipe(first()).subscribe(
                            (trans) => this.toast.error(trans, null, this.toastOptions)
                        )
                    } else {
                        this.translator.get('app.login_err').pipe(first()).subscribe(
                            (trans) => this.toast.error(trans, null, this.toastOptions)
                        )
                    }
                    this.loginForm.get('password').setValue('');
                    this.isLoading = false;
                }
            );
        }
    }
}

import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ABONavigatorService} from '../../shared/services/abo-navigator.service';
import {UserService} from '../../shared/services/models/user.service';
import {isoCountries, tnCityState} from '../../shared/utils/util-functions';
import {Subscription} from 'rxjs';
import * as zxcvbn from 'zxcvbn';
import {CustomValidators} from 'ng2-validation';
import {ABOValidators} from '../../shared/utils/abo.validators';
import {HttpErrorResponse} from '@angular/common/http';
import {ToastService, IMyOptions} from 'ng-uikit-pro-standard';
import {first} from 'rxjs/operators';

@Component({
    templateUrl: './init-account.component.html',
    styleUrls: ['./init-account.component.scss']
})
export class InitAccountComponent implements OnInit, OnDestroy {

    initForm: FormGroup;
    isLoading = false;
    viewPass = false;
    agreed = false;
    @ViewChild('tabs') tabs: ElementRef;
    stateCities = tnCityState;
    passClass = {value: 0, type: 'danger'};
    step = 1;
    genderOptions: Array<{ value: string, label: string }> = [];
    langOptions: Array<{ value: string, label: string }> = [];
    countryOptions: Array<{ value: string, label: string }> = [];
    stateOptions: Array<{ value: string, label: string }> = [];
    cityOptions: Array<{ value: string, label: string }> = [];
    dateNow = new Date();
    private toastOptions = {progressBar: true, timeOut: 6000, positionClass: 'toast-bottom-right'};
    dpOptions: IMyOptions = {
        todayBtnTxt: 'Today',
        clearBtnTxt: 'Clear',
        closeBtnTxt: 'Close',
        dateFormat: 'dd/mm/yyyy',
        disableUntil: {year: (this.dateNow.getFullYear() - 70), month: this.dateNow.getMonth(), day: this.dateNow.getDay()},
        disableSince: {year: (this.dateNow.getFullYear() - 4), month: this.dateNow.getMonth(), day: this.dateNow.getDay()},
        minYear: this.dateNow.getFullYear() - 70,
        maxYear: this.dateNow.getFullYear() - 4,
        showTodayBtn: false,
    };
    private token: string;
    private paramSub: Subscription;

    constructor(private fb: FormBuilder,
                private route: ActivatedRoute,
                private translate: TranslateService,
                private toast: ToastService,
                private navigator: ABONavigatorService,
                private userService: UserService) {
    }

    ngOnInit(): void {

        this.route.params.pipe(first()).subscribe(
            params => {
                this.userService.getUserExistByToken(params['token'], false).pipe(first()).subscribe(
                    exist => {
                        if (!exist) {
                            this.navigator.goto('w_hm');
                            this.translate.get('server_err.user.not_found.by_token', {token: params['token']}).pipe(first()).subscribe(
                                trans => this.toast.error(trans, null, this.toastOptions)
                            );
                        }
                    }
                );
            }
        );
        this.buildForm();
        this.paramSub = this.route.params
            .subscribe((params: Params) => this.token = params['token']);
        this.translate.get('user.gender.male').pipe(first()).subscribe(
            (trans) => this.genderOptions.push({value: 'male', label: trans})
        );
        this.translate.get('user.gender.female').pipe(first()).subscribe(
            (trans) => this.genderOptions.push({value: 'female', label: trans})
        );
        this.translate.get('languages.ar').pipe(first()).subscribe(
            (trans) => this.langOptions.push({value: 'ar', label: trans})
        );
        this.translate.get('languages.en').pipe(first()).subscribe(
            (trans) => this.langOptions.push({value: 'en', label: trans})
        );
        this.translate.get('languages.fr').pipe(first()).subscribe(
            (trans) => this.langOptions.push({value: 'fr', label: trans})
        );
        for (const country in isoCountries) {
            this.countryOptions.push({value: country, label: isoCountries[country]})
        }
        for (const state in tnCityState) {
            this.stateOptions.push({value: tnCityState[state].name, label: tnCityState[state].name})
        }
    }

    ngOnDestroy(): void {

        this.paramSub.unsubscribe();
    }

    onGenderChange(event) {
        this.initForm.get('gender').setValue(event.value);
        this.initForm.get('gender').markAsDirty();
    }

    onLangChange(event) {
        this.initForm.get('language').setValue(event.value);
        this.initForm.get('language').markAsDirty();
    }

    onDateChanged(event) {
        this.initForm.get('birthday').setValue(event.value);
        this.initForm.get('birthday').markAsDirty();
    }

    passStrengthFn() {

        switch (zxcvbn(this.initForm.get('plainPassword').get('password').value).score) {

            case 0:
                this.passClass = {value: 20, type: 'danger'};
                break;
            case 1:
                this.passClass = {value: 40, type: 'warning'};
                break;
            case 2:
                this.passClass = {value: 60, type: 'info'};
                break;
            case 3:
                this.passClass = {value: 80, type: 'primary'};
                break;
            case 4:
                this.passClass = {value: 100, type: 'success'};
                break;
        }
    }

    levelOneValid(): boolean {

        return this.initForm.get('username').valid && this.initForm.get('plainPassword').valid;
    }

    levelTwoValid(): boolean {

        return this.initForm.get('firstName').valid && this.initForm.get('lastName').valid &&
            this.initForm.get('gender').valid && this.initForm.get('language').valid &&
            this.initForm.get('birthday').valid && this.initForm.get('cin').valid;
    }

    buildForm(): void {

        const password = new FormControl('', Validators.compose([
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(51),
            Validators.pattern(/^(?=\S*\d)(?=\S*[a-z])(?=\S*[A-Z])\S*$/),
        ]));
        const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

        this.initForm = this.fb.group({
            'username': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(31),
                Validators.pattern(/^[a-zA-Z]{1}[\w.-]+$/),
            ])],
            'plainPassword': this.fb.group({
                'password': password,
                'confirm_password': confirmPassword,
            }),

            'firstName': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(31),
            ])],
            'lastName': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(31),
            ])],
            'gender': ['', Validators.compose([
                Validators.required,
                ABOValidators.inArray(['male', 'female']),
            ])],
            'language': ['', Validators.compose([
                Validators.required,
                ABOValidators.inArray(['ar', 'en', 'fr']),
            ])],
            'birthday': ['', Validators.compose([
                Validators.required,
                ABOValidators.dateLessThan(new Date(new Date().setFullYear(new Date().getUTCFullYear() - 4))),
                ABOValidators.dateGreaterThan(new Date(new Date().setFullYear(new Date().getUTCFullYear() - 70))),
            ])],
            'cin': ['', Validators.compose([
                Validators.required,
                Validators.pattern(/^\d+$/),
                Validators.minLength(8),
                Validators.maxLength(8),
            ])],

            'location': this.fb.group({
                'country': ['', [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(2),
                ]],
                'state': ['', Validators.compose([
                    Validators.required,
                    Validators.minLength(3),
                    Validators.maxLength(31),
                ])],
                'city': ['', Validators.compose([
                    Validators.required,
                    Validators.minLength(3),
                    Validators.maxLength(31),
                ])],
                'address': ['', Validators.compose([
                    Validators.required,
                    Validators.minLength(15),
                    Validators.maxLength(101),
                ])],
                'zipCode': ['', Validators.compose([
                    Validators.required,
                    Validators.pattern(/^\d+$/),
                    Validators.minLength(4),
                    Validators.maxLength(4),
                ])],
            }),
        });
    }

    submit() {

        this.isLoading = true;
        this.userService.initUser(this.token, this.initForm.value).subscribe(
            () => {

                this.isLoading = false;
                this.navigator.goto('w_hm');
                this.translate.get('init_account.submit_suc', {}).subscribe(res => this.toast.success(res, null, this.toastOptions));
            },
            (error: HttpErrorResponse) => {

                this.isLoading = false;
                switch (error.status) {
                    case 404:
                        this.translate.get('init_account.404', {token: this.token})
                            .subscribe(res => {this.toast.error(res, null, this.toastOptions)});
                        break;
                    case 500:
                        this.translate.get('server_err.500', {}).subscribe(res => this.toast.error(res, null, this.toastOptions));
                        break;
                    default:
                        this.translate.get('server_err.general', {}).subscribe(res => this.toast.error(res, null, this.toastOptions));
                        break;
                }
            },
        );
    }

    isNextDisabled(): boolean {

        if (this.step === 1) {
            return !this.levelOneValid();
        } else if (this.step === 2) {
            return !this.levelTwoValid();
        } else if (this.step === 3) {
            return this.initForm.invalid;
        }
        return true;
    }

    onCountryChange(event) {

        this.initForm.get('location').get('country').setValue(event.value);
        this.initForm.get('location').get('country').markAsDirty();
    }

    onStateChange(event) {

        this.cityOptions = [];
        this.initForm.get('location').get('state').setValue(event.value);
        this.initForm.get('location').get('state').markAsDirty();
        if (!!event.value) {

            for (const stt of this.stateCities) {

                if (event.value === stt.name) {

                    for (const city of stt.cities) {
                        this.cityOptions.push({value: city, label: city});
                    }
                }
            }
        }
    }

    onCityChange(event) {

        this.initForm.get('location').get('city').setValue(event.value);
        this.initForm.get('location').get('city').markAsDirty();
    }

    getPass() {

        const password = this.initForm.get('plainPassword').get('password').value;
        if (password === '') {

            return '';
        }
        return this.viewPass ? password : '**********';
    }
}

import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';
import {ABONavigatorService} from '../../shared/services/abo-navigator.service';

@Component({
    template: 'logging out...',
})
export class LogoutComponent implements OnInit {

    constructor(private authService: AuthService, private aboNavigator: ABONavigatorService) {
    }

    ngOnInit() {

        this.authService.logout();
        this.aboNavigator.goto('w_hm');
    }
}

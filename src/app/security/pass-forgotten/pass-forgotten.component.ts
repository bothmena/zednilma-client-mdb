import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {UserService} from '../../shared/services/models/user.service';
import {HttpErrorResponse} from '@angular/common/http';
import {ToastService} from 'ng-uikit-pro-standard';
import {ABONavigatorService} from '../../shared/services/abo-navigator.service';
import {first} from 'rxjs/operators';

@Component({
    templateUrl: './pass-forgotten.component.html',
    styleUrls: ['./pass-forgotten.component.scss']
})
export class PassForgottenComponent implements OnInit {

    forgottenForm: FormGroup;
    isLoading = false;
    private toastOptions = {progressBar: true, timeOut: 6000, positionClass: 'toast-bottom-right'};

    constructor(private fb: FormBuilder,
                private translator: TranslateService,
                private toast: ToastService,
                private navigator: ABONavigatorService,
                private userService: UserService) {
    }

    ngOnInit() {

        this.buildForm();
    }

    buildForm(): void {

        this.forgottenForm = this.fb.group({
            'email': ['', [
                Validators.required,
                Validators.email,
            ]],
        });
    }

    resetPassword() {

        if (this.forgottenForm.valid) {

            this.isLoading = true;
            const email = this.forgottenForm.get('email').value;
            this.userService.passwordForgotten(email).subscribe(
                () => {

                    this.isLoading = false;
                    this.translator.get('w_pg.login.forgotten_success').pipe(first()).subscribe(
                        (trans) => this.toast.success(trans, null, this.toastOptions)
                    );
                    this.navigator.goto('w_hm');
                },
                (error: HttpErrorResponse) => {

                    if (error.status === 404) {
                        this.translator.get('server_err.user.not_found.by_mail', {email})
                            .pipe(first()).subscribe(
                            (trans) => this.toast.error(trans, null, this.toastOptions)
                        )
                    } else {
                        this.translator.get('server_err.general').pipe(first()).subscribe(
                            (trans) => this.toast.error(trans, null, this.toastOptions)
                        )
                    }
                    this.isLoading = false;
                }
            )
        }
    }
}

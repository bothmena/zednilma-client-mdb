import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LogoutComponent} from './logout/logout.component';
import {InitAccountComponent} from './init-account/init-account.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {LoginComponent} from './login/login.component';
import {PassForgottenComponent} from './pass-forgotten/pass-forgotten.component';

const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
        // canActivate: [LogoutGuardService]
    },                                                                                  // c_lgi
    {
        path: 'password-forgotten',
        component: PassForgottenComponent,
        // canActivate: [LogoutGuardService]
    },                                                                                  // c_psf
    {
        path: 'reset-password/:token',
        component: ResetPasswordComponent,
        // canActivate: [SecurityGuardService],
    },                                                                                  // c_rpw
    {
        path: 'init-account/:token',
        component: InitAccountComponent,
        // canActivate: [InitAccountGuardService]
    },                                                                                  // c_int
    {
        path: 'logout',
        component: LogoutComponent,
        // canActivate: [LogoutGuardService]
    },                                                                                  // c_lgo
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SecurityRoutingModule {
}

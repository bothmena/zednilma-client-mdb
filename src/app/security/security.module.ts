import {NgModule} from '@angular/core';
// import {CommonModule} from '@angular/common';

import {SecurityRoutingModule} from './security-routing.module';
import {InitAccountComponent} from './init-account/init-account.component';
import {LogoutComponent} from './logout/logout.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {ZdlmCommonModule} from '../shared/modules/zdlm-common.module';
import {TranslateModule} from '@ngx-translate/core';
import { LoginComponent } from './login/login.component';
import { PassForgottenComponent } from './pass-forgotten/pass-forgotten.component';

@NgModule({
    imports: [
        // CommonModule,
        ZdlmCommonModule,
        TranslateModule,
        SecurityRoutingModule
    ],
    declarations: [
        InitAccountComponent,
        LogoutComponent,
        ResetPasswordComponent,
        LoginComponent,
        PassForgottenComponent,
    ]
})

export class SecurityModule {

    /*constructor( private tralationLoder: ZdlmTranslationLoaderService ) {

        this.tralationLoder.loadTranslations( 'security' );
    }*/
}

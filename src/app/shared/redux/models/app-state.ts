/**
 * Created by bothmena on 12/06/17.
 */

export interface AppState {

    spWebsites: any;
    state: {
        loading: number;
        activeRoute: string;
    };
}

export const INIT_APP_STATE = {

    spWebsites: null,
    state: {
        loading: 0,
        activeRoute: ''
    }
};

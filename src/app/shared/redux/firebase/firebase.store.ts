/**
 * Created by bothmena on 12/06/17.
 */

import { AppAction } from '../models/app-action';
import { FirebaseState, INIT_FIREBASE_STATE } from '../models/firebase-state';
import { FirebaseActions } from './firebase.actions';

export function firebaseReducer( lastState: FirebaseState = INIT_FIREBASE_STATE, action: AppAction ): FirebaseState {

    switch ( action.type ) {

        case FirebaseActions.FIREBASE_FETCH:
            return { data: action.payload };

        case FirebaseActions.FIREBASE_EMPTY:
            return {
                data: {
                    id: 0,
                    username: '',
                    email: '',
                    first_name: '',
                    middle_name: '',
                    last_name: '',
                    birthday: '',
                    gender: '',
                    cin: '',
                    language: '',
                    role: '',
                    image_url: '',
                    institute_id: 0,
                    institute_slug: '',
                    location: null,
                },
            };
    }

    return lastState;
}

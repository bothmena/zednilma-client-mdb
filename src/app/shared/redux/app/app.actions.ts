/**
 * Created by bothmena on 12/06/17.
 */

import { AppAction } from 'app/shared/redux/models/app-action';

export class AppActions {

    static LOADING_INCREMENT = 'LOADING_INCREMENT';
    static LOADING_DECREMENT = 'LOADING_DECREMENT';
    static SP_WEBSITE_INIT = 'SP_WEBSITE_INIT';
    static SP_WEBSITE_DESTROY = 'SP_WEBSITE_DESTROY';
    static SP_WEBSITE_ADD = 'SP_WEBSITE_ADD';
    static SP_WEBSITE_REMOVE = 'SP_WEBSITE_REMOVE';
    static ACTIVE_ROUTE_CHANGED = 'ACTIVE_ROUTE_CHANGED';

    static incrementLoading(): AppAction {

        return { type: AppActions.LOADING_INCREMENT };
    }

    static decrementLoading(): AppAction {

        return { type: AppActions.LOADING_DECREMENT };
    }

    static spWebsiteInit(): AppAction {

        return { type: AppActions.SP_WEBSITE_INIT };
    }

    static spWebsiteDestroy(): AppAction {

        return { type: AppActions.SP_WEBSITE_DESTROY };
    }

    static spWebsiteAdd( website: string ): AppAction {

        return { type: AppActions.SP_WEBSITE_ADD, payload: website };
    }

    static spWebsiteRemove( website: string ): AppAction {

        return { type: AppActions.SP_WEBSITE_REMOVE, payload: website };
    }

    static activeRouteChanged( routeName: string ): AppAction {

        return { type: AppActions.ACTIVE_ROUTE_CHANGED, payload: routeName };
    }
}

/**
 * Created by bothmena on 12/06/17.
 */

import { tassign } from 'tassign';
import { AppState } from '../models/app-state';

export class AppReducer {

    protected _state;
    protected _action;

    websites: string[] = [
        'linkedin', 'facebook', 'github', 'gitlab', 'bitbucket', 'youtube', 'google+',
        'twitter', 'instagram', 'whatsapp', 'skype', 'vimeo'
    ];

    constructor( state, action ) {

        this._state = state;
        this._action = action;
    }

    initSPWebsites(): AppState {

        return tassign( this._state, { spWebsites: this.websites } );
    }

    addSPWebsites(): AppState {

        const websites = this._state.spWebsites;
        websites.push( this._action.payload );
        return tassign( this._state, { spWebsites: websites } );
    }

    removeSPWebsites(): AppState {

        const websites = this._state.spWebsites;
        const index = websites.indexOf( this._action.payload );
        if (index > -1) {
            websites.splice(index, 1);
        }
        return tassign( this._state, { spWebsites: websites } );
    }
}

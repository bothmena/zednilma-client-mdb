import { AppAction } from '../models/app-action';
import { AppState, INIT_APP_STATE } from '../models/app-state';
import { AppReducer } from './app-reducer';
import { AppActions } from './app.actions';
import { tassign } from 'tassign';
/**
 * Created by bothmena on 12/06/17.
 */

export function appReducer( lastState: AppState = INIT_APP_STATE, action: AppAction ): AppState {

    const appReducer = new AppReducer( lastState, action );

    switch ( action.type ) {

        case AppActions.LOADING_INCREMENT:
            return tassign( lastState, {state: {
                loading: ++lastState.state.loading,
                activeRoute: lastState.state.activeRoute
            }} );

        case AppActions.LOADING_DECREMENT:
            return tassign( lastState, {state: {
                loading: --lastState.state.loading,
                activeRoute: lastState.state.activeRoute
            }} );

        case AppActions.SP_WEBSITE_INIT:
            return appReducer.initSPWebsites();

        case AppActions.SP_WEBSITE_DESTROY:
            return tassign( lastState, { spWebsites: null } );

        case AppActions.SP_WEBSITE_ADD:
            return appReducer.addSPWebsites();

        case AppActions.SP_WEBSITE_REMOVE:
            return appReducer.removeSPWebsites();

        case AppActions.ACTIVE_ROUTE_CHANGED:
            return tassign( lastState, {state: {
                loading: lastState.state.loading,
                activeRoute: action.payload
            }} );
    }

    // We don't care about any other actions right now.
    return lastState;
}

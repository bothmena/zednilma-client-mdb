import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AccountInfoEditComponent } from '../components/account/account-info-edit/account-info-edit.component';
import { AccountInfoComponent } from '../components/account/account-info/account-info.component';
import { ContactEditComponent } from '../components/account/contact-edit/contact-edit.component';
import { ContactComponent } from '../components/account/contact/contact.component';
import { CredentialsEditComponent } from '../components/account/credentials-edit/credentials-edit.component';
import { CredentialsComponent } from '../components/account/credentials/credentials.component';
import { LocationMapEditComponent } from '../components/account/location-map-edit/location-map-edit.component';
import { LocationMapComponent } from '../components/account/location-map/location-map.component';
import { ProfileComponent } from '../components/account/profile/profile.component';
import { SettingsComponent } from '../components/account/settings/settings.component';
import { SocialProfilesEditComponent } from '../components/account/social-profiles-edit/social-profiles-edit.component';
import { SocialProfilesComponent } from '../components/account/social-profiles/social-profiles.component';
import { ZdlmCommonModule } from './zdlm-common.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { EmailEditComponent } from '../components/account/email-edit/email-edit.component';
import { PhoneEditComponent } from '../components/account/phone-edit/phone-edit.component';
import { SocialProfileEditComponent } from '../components/account/social-profile-edit/social-profile-edit.component';
import { WebsiteEditComponent } from '../components/account/website-edit/website-edit.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule( {
    imports     : [
        CommonModule,
        HttpClientModule,
        RouterModule,
        TranslateModule,
        ZdlmCommonModule,
    ],
    declarations: [
        AccountInfoComponent,
        ContactComponent,
        CredentialsComponent,
        LocationMapComponent,
        ProfileComponent,
        SettingsComponent,
        SocialProfilesComponent,
        AccountInfoEditComponent,
        ContactEditComponent,
        CredentialsEditComponent,
        LocationMapEditComponent,
        SocialProfilesEditComponent,
        EmailEditComponent,
        PhoneEditComponent,
        SocialProfileEditComponent,
        WebsiteEditComponent,
    ],
    exports     : [
        AccountInfoComponent,
        ContactComponent,
        CredentialsComponent,
        LocationMapComponent,
        ProfileComponent,
        SettingsComponent,
        SocialProfilesComponent,
        AccountInfoEditComponent,
        ContactEditComponent,
        CredentialsEditComponent,
        LocationMapEditComponent,
        SocialProfilesEditComponent,
        WebsiteEditComponent,
        EmailEditComponent,
        PhoneEditComponent,
        SocialProfileEditComponent,
    ],
} )
export class AccountModule {
}

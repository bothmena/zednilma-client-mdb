/**
 * Created by bothmena on 30/06/17.
 */

import { NgModule } from '@angular/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {HttpClient} from '@angular/common/http';

// AoT requires an exported function for factories
export function httpLoaderFactory( http: HttpClient ) {
    // return new TranslateHttpLoader(http);
    return new TranslateHttpLoader( http, './assets/i18n/', '/app.json' );
}

@NgModule( {
    imports  : [
        TranslateModule.forRoot( {
            loader: {
                provide   : TranslateLoader,
                useFactory: httpLoaderFactory,
                deps      : [ HttpClient],
            },
        } ),
    ],
    providers: [],
    exports  : [
        TranslateModule,
    ],
} )
export class ZdlmTranslateModule {
}

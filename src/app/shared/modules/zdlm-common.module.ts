/**
 * Created by bothmena on 19/01/17.
 */

import {CommonModule} from '@angular/common';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FooterComponent} from '../components/footer/footer.component';
import {TopnavComponent} from '../components/topnav/topnav.component';
import {ZdlmNavigateDirective} from '../directives/zdlm-navigate.directive';
import {LocationPipe} from '../pipes/location.pipe';
import {PhoneNumberPipe} from '../pipes/phone-number.pipe';
import {TrustResourcePipe} from '../pipes/trust-resource.pipe';
import {ValidationMessageDirective} from '../directives/validation-message.directive';
import {ObjectKeysPipe} from '../pipes/object-keys.pipe';
import {HttpClientModule} from '@angular/common/http';
import {CardsModule, MDBBootstrapModule, MDBBootstrapModulePro, ToastModule, TooltipModule} from 'ng-uikit-pro-standard';
import {TranslateModule} from '@ngx-translate/core';
import {LoginComponent} from '../components/login/login.component';
import {PasswordForgottenComponent} from '../components/password-forgotten/password-forgotten.component';
import {AgmCoreModule} from '@agm/core';
import {OrderModule} from 'ngx-order-pipe';
import {TimeAgoPipe} from 'time-ago-pipe';
import {TimeDifferencePipe} from '../pipes/time-difference.pipe';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ClassTimetableComponent} from '../components/timetables/class-timetable/class-timetable.component';
import {SessionReportsComponent} from '../components/parent-student/session-reports/session-reports.component';
import {TrustHtmlPipe} from '../pipes/trust-html.pipe';
import {TimetableComponent} from '../components/parent-student/timetable/timetable.component';
import {ToDoAppComponent} from '../components/to-do-app/to-do-app.component';


@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        ToastModule.forRoot(),
        MDBBootstrapModule.forRoot(),
        MDBBootstrapModulePro.forRoot(),
        TooltipModule,
        CardsModule,
        TranslateModule,
        OrderModule,
        FlexLayoutModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyA3Z9gQmNQczmhqLWJIlEoQmTPqkcuvALY'
        })
    ],
    declarations: [
        TopnavComponent,
        FooterComponent,
        LoginComponent,
        PasswordForgottenComponent,
        ClassTimetableComponent,
        SessionReportsComponent,
        TimetableComponent,
        ToDoAppComponent,
        ZdlmNavigateDirective,
        ValidationMessageDirective,
        LocationPipe,
        PhoneNumberPipe,
        TrustResourcePipe,
        TrustHtmlPipe,
        ObjectKeysPipe,
        TimeAgoPipe,
        TimeDifferencePipe,
    ],
    exports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        MDBBootstrapModule,
        MDBBootstrapModulePro,
        CardsModule,
        TooltipModule,
        AgmCoreModule,
        ToastModule,
        OrderModule,
        FlexLayoutModule,
        TopnavComponent,
        FooterComponent,
        LoginComponent,
        PasswordForgottenComponent,
        ClassTimetableComponent,
        SessionReportsComponent,
        TimetableComponent,
        ToDoAppComponent,
        ZdlmNavigateDirective,
        ValidationMessageDirective,
        LocationPipe,
        PhoneNumberPipe,
        TrustResourcePipe,
        TrustHtmlPipe,
        ObjectKeysPipe,
        TimeAgoPipe,
        TimeDifferencePipe,
    ],
    schemas: [NO_ERRORS_SCHEMA],
})
export class ZdlmCommonModule {
}

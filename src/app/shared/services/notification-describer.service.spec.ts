import { TestBed, inject } from '@angular/core/testing';

import { NotificationDescriberService } from './notification-describer.service';

describe('NotificationDescriberService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotificationDescriberService]
    });
  });

  it('should be created', inject([NotificationDescriberService], (service: NotificationDescriberService) => {
    expect(service).toBeTruthy();
  }));
});

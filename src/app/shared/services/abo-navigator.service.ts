/**
 * Created by bothmena on 10/03/17.
 */

import { NgRedux } from '@angular-redux/store';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { isArray } from 'util';
import { AppActions } from '../redux/app/app.actions';
import { State } from '../redux/models/state';

@Injectable()
export class ABONavigatorService {

    constructor( private _router: Router, private ngRedux: NgRedux<State> ) {
    }

    public goto( routeName: string, ...params: string[] ) {

        const routeParams: {} = {

            // WebsiteModule
            'w_hm': [ '/' ],
            'w_abt': [ 'about-us' ],
            'w_ctu': [ 'contact-us' ],

            // SecurityModule
            'c_lgi': [ 'login' ],
            'c_lgo': [ 'logout' ],
            'c_psf': [ 'password-forgotten' ],
            'c_rpw': [ 'reset-password', params[ 0 ] ],
            'c_int': [ 'init-account', params[ 0 ] ],

            // StudentModule
            's_prf': [ 's', params[ 0 ] ],
            's_stg': [ 's', params[ 0 ], 'settings' ],
            's_st_aci': [ 's', params[ 0 ], 'settings', 'account-info' ],
            's_st_lam': [ 's', params[ 0 ], 'settings', 'location' ],
            's_st_cti': [ 's', params[ 0 ], 'settings', 'contact-info' ],
            's_st_spr': [ 's', params[ 0 ], 'settings', 'social-profiles' ],
            's_st_crd': [ 's', params[ 0 ], 'settings', 'credentials' ],
            's_st_wpr': [ 's', params[ 0 ], 'settings', 'website-preferences' ],
            's_td': [ 's', params[ 0 ], 'todo-app' ],
            's_tt': [ 's', params[ 0 ], 'timetable' ],
            's_sr': [ 's', params[ 0 ], 'session-reports' ],

            // ParentModule
            'r_prf': [ 'r', params[ 0 ] ],
            'r_stg': [ 'r', params[ 0 ], 'settings' ],
            'r_st_aci': [ 'r', params[ 0 ], 'settings', 'account-info' ],
            'r_st_lam': [ 'r', params[ 0 ], 'settings', 'location' ],
            'r_st_cti': [ 'r', params[ 0 ], 'settings', 'contact-info' ],
            'r_st_spr': [ 'r', params[ 0 ], 'settings', 'social-profiles' ],
            'r_st_crd': [ 'r', params[ 0 ], 'settings', 'credentials' ],
            'r_st_wpr': [ 'r', params[ 0 ], 'settings', 'website-preferences' ],
            'r_tdc': [ 'r', params[ 0 ], 'todo-app', params[ 1 ] ],
            'r_ttc': [ 'r', params[ 0 ], 'timetable', params[ 1 ] ],
            'r_src': [ 'r', params[ 0 ], 'session-reports', params[ 1 ] ],

            // ProfessorModule
            'p_prf': [ 'p', params[ 0 ] ],
            'p_stg': [ 'p', params[ 0 ], 'settings' ],
            'p_st_aci': [ 'p', params[ 0 ], 'settings', 'account-info' ],
            'p_st_lam': [ 'p', params[ 0 ], 'settings', 'location' ],
            'p_st_cti': [ 'p', params[ 0 ], 'settings', 'contact-info' ],
            'p_st_spr': [ 'p', params[ 0 ], 'settings', 'social-profiles' ],
            'p_st_crd': [ 'p', params[ 0 ], 'settings', 'credentials' ],
            'p_st_wpr': [ 'p', params[ 0 ], 'settings', 'website-preferences' ],
            'p_td': [ 'p', params[ 0 ], 'todo-app' ],
            'p_asg': [ 'p', params[ 0 ], 'assignments' ],
            'p_tt': [ 'p', params[ 0 ], 'timetable' ],
            'p_atd': [ 'p', params[ 0 ], 'attendance' ],
            'p_sr': [ 'p', params[ 0 ], 'session-reports' ],

            // AdminModule
            'a_dsh': [ 'admin', params[ 0 ], 'dashboard' ],
            'a_ins': [ 'admin', params[ 0 ], 'institutes' ],
            'a_ina': [ 'admin', params[ 0 ], 'ins-admins' ],
            'a_pr_aci': [ 'admin', params[ 0 ], 'account-info' ],
            'a_pr_lam': [ 'admin', params[ 0 ], 'location' ],
            'a_pr_cti': [ 'admin', params[ 0 ], 'contact-info' ],
            'a_pr_spr': [ 'admin', params[ 0 ], 'social-profiles' ],
            'a_pr_crd': [ 'admin', params[ 0 ], 'credentials' ],
            'a_st_aci': [ 'admin', params[ 0 ], 'settings', 'account-info' ],
            'a_st_lam': [ 'admin', params[ 0 ], 'settings', 'location' ],
            'a_st_cti': [ 'admin', params[ 0 ], 'settings', 'contact-info' ],
            'a_st_spr': [ 'admin', params[ 0 ], 'settings', 'social-profiles' ],
            'a_st_crd': [ 'admin', params[ 0 ], 'settings', 'credentials' ],
            'a_st_wpr': [ 'admin', params[ 0 ], 'settings', 'website-preferences' ],

            // InstituteModule
            'i_prf': [ 'i', params[ 0 ] ],
            'i_st_aci': [ 'i', params[ 0 ], 'settings', 'account-info' ],
            'i_st_lam': [ 'i', params[ 0 ], 'settings', 'location-and-map' ],
            'i_st_cti': [ 'i', params[ 0 ], 'settings', 'contact-info' ],
            'i_st_spr': [ 'i', params[ 0 ], 'settings', 'social-profiles' ],

            'i_mprf': [ 'i', params[ 0 ], 'admin', params[ 1 ] ],
            'i_mst_aci': [ 'i', params[ 0 ], 'admin', params[ 1 ], 'settings', 'account-info' ],
            'i_mst_lam': [ 'i', params[ 0 ], 'admin', params[ 1 ], 'settings', 'location' ],
            'i_mst_cti': [ 'i', params[ 0 ], 'admin', params[ 1 ], 'settings', 'contact-info' ],
            'i_mst_spr': [ 'i', params[ 0 ], 'admin', params[ 1 ], 'settings', 'social-profiles' ],
            'i_mst_crd': [ 'i', params[ 0 ], 'admin', params[ 1 ], 'settings', 'credentials' ],
            'i_mst_wpr': [ 'i', params[ 0 ], 'admin', params[ 1 ], 'settings', 'website-preferences' ],

            'i_str_infs': [ 'i', params[ 0 ], 'structure', 'infrastructures' ],
            'i_str_inf': [ 'i', params[ 0 ], 'structure', 'infrastructures', params[ 1 ] ],
            'i_str_spcs': [ 'i', params[ 0 ], 'structure', 'specialities' ],
            'i_str_spcms': [ 'i', params[ 0 ], 'structure', 's', params[ 1 ], 'modules' ],
            'i_str_spcm': [ 'i', params[ 0 ], 'structure', 's', params[ 1 ], 'm', params[ 2 ] ],
            'i_str_spccs': [ 'i', params[ 0 ], 'structure', 's', params[ 1 ], 'classes' ],
            'i_str_spcc': [ 'i', params[ 0 ], 'structure', 's', params[ 1 ], 'c', params[ 2 ] ],
            'i_str_deps': [ 'i', params[ 0 ], 'structure', 'departments' ],
            'i_str_dep': [ 'i', params[ 0 ], 'structure', 'd', params[ 1 ] ],
            'i_str_depss': [ 'i', params[ 0 ], 'structure', 'd', params[ 1 ], 'subjects', params[ 2 ] ],
            'i_str_depsc': [ 'i', params[ 0 ], 'structure', 'd', params[ 1 ], 'subjects', params[ 2 ], 'curriculum' ],
            'i_str_depse': [ 'i', params[ 0 ], 'structure', 'd', params[ 1 ], 'subjects', params[ 2 ], 'exams' ],

            'i_mmb_prfs': [ 'i', params[ 0 ], 'members', 'professors' ],
            'i_mmb_prf': [ 'i', params[ 0 ], 'members', 'professors', params[ 1 ] ],
            'i_mmb_stds': [ 'i', params[ 0 ], 'members', 'students' ],
            'i_mmb_std': [ 'i', params[ 0 ], 'members', 'students', params[ 1 ] ],
            'i_mmb_stfs': [ 'i', params[ 0 ], 'members', 'staff' ],
            'i_mmb_stf': [ 'i', params[ 0 ], 'members', 'staff', params[ 1 ] ],
            'i_mmb_grps': [ 'i', params[ 0 ], 'members', 'groups' ],
            'i_mmb_grp': [ 'i', params[ 0 ], 'members', 'groups', params[ 1 ] ],
            'i_mmb_psts': [ 'i', params[ 0 ], 'members', 'posts' ],

            'i_tmt': [ 'i', params[ 0 ], 'timetables' ],
            'i_tmt_prfs': [ 'i', params[ 0 ], 'timetables', 'professors' ],
            'i_tmt_prf': [ 'i', params[ 0 ], 'timetables', 'professors', params[ 1 ] ],
            'i_tmt_prf_ses': [ 'i', params[ 0 ], 'timetables', 'professors', params[ 1 ], params[ 2 ] ],
            'i_tmt_stds': [ 'i', params[ 0 ], 'timetables', 'students' ],
            'i_tmt_std': [ 'i', params[ 0 ], 'timetables', 'students', params[ 1 ] ],
            'i_tmt_std_ses': [ 'i', params[ 0 ], 'timetables', 'students', params[ 1 ], params[ 2 ] ],
        };

        console.log( 'navigating to ', routeName );

        if ( isArray( routeParams[ routeName ] ) ) {

            this.ngRedux.dispatch( AppActions.activeRouteChanged( routeName ) );
            this._router.navigate( routeParams[ routeName ] );
        } else {
            console.log( 'This route does not exist!', routeName );
        }
    }
}

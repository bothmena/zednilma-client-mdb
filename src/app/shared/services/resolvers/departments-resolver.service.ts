import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Department} from '../../models/department';
import {DepartmentService} from '../models/department.service';
import {first, map} from 'rxjs/operators';
import {Observable} from 'rxjs/internal/Observable';

@Injectable()
export class DepartmentsResolverService implements Resolve<Array<Department>> {

    constructor(private depService: DepartmentService) {
    }

    resolve(route: ActivatedRouteSnapshot): Observable<Array<Department>> { // , state: RouterStateSnapshot

        const slug = route.pathFromRoot[1].params['institute_slug'];

        return this.depService.getDepartments(slug, {subjects: 'true', members: 'true'}).pipe(
            first(),
            map(
                deps => {
                    const departments = [];

                    for (const dep of deps) {
                        departments.push(Department.build(dep));
                    }

                    return departments;
                }
            )
        );
    }
}

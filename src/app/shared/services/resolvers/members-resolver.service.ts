import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs/internal/Observable';
import {UserService} from '../models/user.service';
import {User} from '../../models/user';
import {first} from 'rxjs/operators';

@Injectable()
export class MembersResolverService implements Resolve<Array<User>> {

    constructor(private userService: UserService) {
    }

    resolve(route: ActivatedRouteSnapshot): Observable<Array<User>> { // , state: RouterStateSnapshot

        const slug = route.pathFromRoot[1].params['institute_slug'];
        let role = route.pathFromRoot[3].url[0].path;
        if (role === 'students')
            role = 'student';
        else if (role === 'parents')
            role = 'parent';
        else if (role === 'staff')
            role = 'ins_staff';
        else if (role === 'professors')
            role = 'professor';

        return this.userService.getInstituteMembers(slug, {role: role}).pipe(first());
    }
}

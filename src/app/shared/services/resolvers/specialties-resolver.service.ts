import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs/internal/Observable';
import {PathService} from '../models/path.service';
import {Speciality} from '../../models/speciality';

@Injectable()
export class SpecialtiesResolverService implements Resolve<Array<Speciality>> {

    constructor(private pathService: PathService) {
    }

    resolve(route: ActivatedRouteSnapshot): Observable<Array<Speciality>> {

        const slug = route.pathFromRoot[1].params['institute_slug'];
        return this.pathService.getPaths(slug);
    }
}

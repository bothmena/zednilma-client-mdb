import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Map} from 'immutable';
import {Infrastructure} from '../../models/infrastructure';
import {InfrastructureService} from '../models/infrastructure.service';
import {Observable} from 'rxjs';

@Injectable()
export class InfrastructureResolverService implements Resolve<Map<string, Infrastructure>> {

    private static getInfrastructures(infras: Infrastructure[]) {

        let mapInfra = Map<string, Infrastructure>();
        for (const infra of infras) {

            if (infra.type === 'BLOCK') {

                // infrastructure[ infra.id ] = infra;
                mapInfra = mapInfra.set('_' + infra.id, infra);
            } else if (infra.type === 'FLOOR') {

                // infrastructure[ infra.parentId ].addPath( infra );
                mapInfra.get('_' + infra.parentId).addChild(infra);
            }
        }
        /*mapInfra.valueSeq().forEach((block: Infrastructure) => {
            block.paths.valueSeq().forEach((floor: Infrastructure) => {

                for (const classroom of infras) {

                    if (classroom.parentId === floor.id) {
                        // infrastructure[ key ].paths[ fKey ].addPath( classroom );
                        floor.addPath(classroom);
                    }
                }
            });
        });*/

        return mapInfra;
    }

    constructor(private infraService: InfrastructureService) {
    }

    resolve(route: ActivatedRouteSnapshot): Observable<Map<string, Infrastructure>> | Promise<Map<string, Infrastructure>> | Map<string, Infrastructure> {

        const slug = route.pathFromRoot[1].params['institute_slug'];

        return this.infraService.getInfrastructuresByInsSlug(slug).toPromise()
            .then((infras: Infrastructure[]) => {

                return InfrastructureResolverService.getInfrastructures(infras);
            });
    }
}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {JWTToken} from '../models/jwt-token';
import {AppGlobals} from '../utils/app.globals';
import {ABONavigatorService} from './abo-navigator.service';
import {User} from '../models/user';
import {UserService} from './models/user.service';
import {Institute} from '../models/institute';
import {UserInstitute} from '../models/user-institute';
import {BehaviorSubject, Observable} from 'rxjs';
import {map, shareReplay, tap} from 'rxjs/operators';

@Injectable()
export class AuthService {

    jwt: JWTToken;
    user: User;
    institute: Institute;
    private jwtSubject = new BehaviorSubject<JWTToken>(null);
    jwtObservable: Observable<JWTToken> = this.jwtSubject.asObservable();
    isLoggedIn$: Observable<boolean> = this.jwtObservable.pipe(
        map(jwt => (jwt && jwt.isLoggedIn()))
    );
    isLoggedOut$: Observable<boolean> = this.isLoggedIn$.pipe(map(isLoggedIn => !isLoggedIn));
    public insSubject = new BehaviorSubject<Institute>(null);
    insObservable$: Observable<Institute> = this.insSubject.asObservable();
    private _authenticateUrl: string = AppGlobals.BASE_API_URL + AppGlobals.AUTHENTICATE_URL;
    private _refreshTokenUrl: string = AppGlobals.BASE_API_URL + AppGlobals.REFRESH_TOKEN_URL;
    private userSubject = new BehaviorSubject<User>(null);
    userObservable$: Observable<User> = this.userSubject.asObservable();

    constructor(private http: HttpClient, private aboNavigator: ABONavigatorService, private userService: UserService) {

        this.checkToken();
        this.jwtObservable.subscribe(
            (jwt: JWTToken) => {
                if (jwt && !!jwt.token) {
                    this.jwt = jwt;
                    this.saveJWTToken();
                    if (!jwt.hasExpired()) {
                        if (jwt.role === 'admin') {
                            this.userService.getUser(jwt.username).subscribe(
                                (user: User) => {
                                    this.user = user;
                                    this.institute = null;
                                    this.userSubject.next(user);
                                    this.insSubject.next(null);
                                }
                            );
                        } else {
                            this.userService.getUserInstitute(jwt.username).subscribe(
                                (userInstitute: UserInstitute) => {
                                    this.user = userInstitute.user;
                                    this.institute = userInstitute.institute;
                                    this.userSubject.next(userInstitute.user);
                                    this.insSubject.next(userInstitute.institute);
                                });
                        }
                    }
                } else {
                    this.jwt = jwt;
                    this.saveJWTToken();
                }
            }
        );
        setInterval(() => {
                if (this.jwt && this.jwt.hasExpired()) {
                    this.refreshToken().subscribe(
                        jwtToken => {
                            this.jwtSubject.next(jwtToken);
                        },
                        err => console.log(err)
                    )
                }
            }, 30000
        );
    }

    login(login: { username: string, password: string }): Observable<JWTToken> {

        const data = JSON.stringify({'_username': login.username, '_password': login.password});
        return this.http.post<JWTToken>(this._authenticateUrl, data)
            .pipe(
                shareReplay(),
                map(jwt => {
                    if (!!jwt.token && !!jwt.refresh_token) {
                        return new JWTToken(jwt.token, jwt.refresh_token);
                    } else {
                        return null;
                    }
                }),
                tap(jwt => {
                    this.jwtSubject.next(jwt);
                })
            )
            ;
    }

    goToProfile() {

        this.jwtObservable.subscribe(
            (jwt) => {
                switch (jwt.role) {
                    case 'student':
                        this.aboNavigator.goto('s_prf', jwt.username);
                        break;
                    case 'professor':
                        this.aboNavigator.goto('p_prf', jwt.username);
                        break;
                    case 'parent':
                        this.aboNavigator.goto('r_prf', jwt.username);
                        break;
                    case 'institute':
                        this.aboNavigator.goto('i_mprf', jwt.username);
                        break;
                    case 'ins_admin':
                        this.aboNavigator.goto('i_mprf', this.institute.slug, jwt.username);
                        break;
                    case 'ins_staff':
                        this.aboNavigator.goto('i_mmb_prfs', this.institute.slug, jwt.username);
                        break;
                    case 'admin':
                        this.aboNavigator.goto('a_dsh', jwt.username);
                        break;
                }
            }
        );
    }

    public logout(): any {

        this.jwtSubject.next(null);
    }

    /**
     * load the token and refresh_token from sessionStorage, if the token is expired it refreshes it!
     */
    private checkToken() {

        if (!!sessionStorage.getItem('token') && !!sessionStorage.getItem('refresh_token')) {

            const jwt = new JWTToken(sessionStorage.getItem('token'), sessionStorage.getItem('refresh_token'));
            this.jwtSubject.next(jwt);
            if (jwt.hasExpired()) {
                this.refreshToken().subscribe(
                    jwtToken => this.jwtSubject.next(jwtToken),
                    err => console.log(err)
                );
            }
        }
    }

    /**
     * refreshes the token and return an observable.
     * @returns {Observable<JWTToken>}
     */
    private refreshToken(): Observable<JWTToken> {

        const refToken = sessionStorage.getItem('refresh_token');
        if (!!refToken) {
            return this.http.post<{ token: string, refresh_token: string }>(this._refreshTokenUrl,
                JSON.stringify({'refresh_token': refToken})).pipe(map(jwt => JWTToken.build(jwt)));
        }
    }

    /**
     * save the jwt token, if it's null, it clears the session storage.
     */
    private saveJWTToken() {

        if (this.jwt && this.jwt.token) {
            sessionStorage.setItem('token', this.jwt.token);
            sessionStorage.setItem('refresh_token', this.jwt.refresh_token);
        } else {
            // sessionStorage.setItem('token', null);
            // sessionStorage.setItem('refresh_token', null);
            delete sessionStorage.token;
            delete sessionStorage.refresh_token;
        }
    }
}

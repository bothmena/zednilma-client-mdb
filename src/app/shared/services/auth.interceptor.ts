import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor() {
    }


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const token = sessionStorage.getItem('token');
        if (token) {

            const cloned = req.clone({
                headers: req.headers
                    .set('Authorization', 'Bearer ' + token)
                    .set('content-type', 'application/json')
            });
            return next.handle(cloned);

        } else {

            const cloned = req.clone({
                headers: req.headers
                    .set('content-type', 'application/json')
            });
            return next.handle(cloned);
        }
    }
}

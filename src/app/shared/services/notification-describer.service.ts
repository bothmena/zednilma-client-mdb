import {Injectable} from '@angular/core';
import {Notification} from '../models/notification';
import {TranslateService} from '@ngx-translate/core';
import {countdown} from '../utils/util-functions';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class NotificationDescriberService {

    constructor(private translator: TranslateService) {
    }

    describe(notification: Notification): Observable<string> {

        try {
            notification.data = JSON.parse(notification.description);
            switch (notification.type) {

                case 0:
                    return !!notification.title ? this.translator.get(notification.title) : this.translator.get(notification.description);
                case 1:
                    return this.translator.get('notifications.1', {
                        subject: notification.data['subject']['name'],
                        firstName: notification.data['prof']['firstName'],
                        lastName: notification.data['prof']['lastName']
                    });
                case 2:
                    // @ts-ignore
                    const key = notification.data['isPresent'] ? 'notifications.2p' : 'notifications.2a';
                    return this.translator.get(key, {
                        studentName: notification.data['student']['firstName'],
                        subject: notification.data['subject']['name'],
                        firstName: notification.data['prof']['firstName'],
                        lastName: notification.data['prof']['lastName']
                    });
                case 26: // 26: Department
                    break;
                case 121:
                    return this.translator.get('notifications.121', {
                        subject: notification.data['subject']['name'],
                        firstName: notification.data['prof']['firstName'],
                        lastName: notification.data['prof']['lastName']
                    });
                case 141:
                    // @ts-ignore
                    const dueDate = new Date(notification.data['task']['dueDate'].substr(0, 19));
                    return this.translator.get('notifications.141', {
                        className: notification.data['classe']['name'],
                        subject: notification.data['subject']['name'],
                        day: dueDate.toLocaleDateString('en-GB'),
                        time: dueDate.toLocaleTimeString('en-GB').substr(0, 5)
                    });
                case 156:
                    // @ts-ignore
                    const dueDate = new Date(notification.data['task']['dueDate'].substr(0, 19));
                    return this.translator.get('notifications.156', {
                        dueDate: dueDate.toLocaleDateString('en-GB'),
                        firstName: notification.data['assigner']['firstName'],
                        lastName: notification.data['assigner']['lastName']
                    });
                case 157:
                    // @ts-ignore
                    const dueDate = new Date(notification.data['task']['dueDate'].substr(0, 19));
                    return this.translator.get('notifications.157', {
                        dueDate: dueDate.toLocaleDateString('en-GB'),
                        studentName: notification.data['assignee']['firstName'],
                        firstName: notification.data['assigner']['firstName'],
                        lastName: notification.data['assigner']['lastName']
                    });
                case 158:
                    // @ts-ignore
                    const key = this.getReminderKey(countdown(new Date(notification.data['taskUser']['dueDate']), notification.date), 158);
                    return this.translator.get(key, {
                        taskName: notification.data['task']['name'],
                        firstName: notification.data['assigner']['firstName'],
                        lastName: notification.data['assigner']['lastName']
                    });
                case 159:
                    // @ts-ignore
                    const key = this.getReminderKey(countdown(new Date(notification.data['taskUser']['dueDate']), notification.date), 158);
                    return this.translator.get(key, {
                        studentName: notification.data['assignee']['firstName'],
                        taskName: notification.data['task']['name'],
                        firstName: notification.data['assigner']['firstName'],
                        lastName: notification.data['assigner']['lastName']
                    });
                // return '';
                // case 171: // 171: UserClasse
            }
        } catch (e) {

            if (e instanceof SyntaxError) {

                return !!notification.title ? this.translator.get(notification.title) : this.translator.get(notification.description);
            } else {

                return this.translator.get(' ');
            }
        }
    }

    getReminderKey(diff: {hours: number, days: number}, type: number) {

        if (diff.hours === 3)
            return 'notifications.' + type + '_3h';
        else if (diff.days === 1)
            return 'notifications.' + type + '_1d';
        else if (diff.days === 3)
            return 'notifications.' + type + '_3d';
        else if (diff.days === 7)
            return 'notifications.' + type + '_7d';
    }
}

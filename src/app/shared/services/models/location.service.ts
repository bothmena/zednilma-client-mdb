import { Injectable } from '@angular/core';
import { Location } from '../../models/location';
import { AppGlobals } from '../../utils/app.globals';
import { Observable } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable()
export class LocationService {

    private baseUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes/';

    constructor( private http: HttpClient ) {
    }

    getLocations( insId: number, options: {} = {} ): Observable<Array<Location>> {

        let url = this.baseUrl + insId + '/locations';
        let i = 0;
        for ( const option in options ) {

            const char = i++ === 0 ? '?' : '&';
            url += char + option + '=' + options[ option ];
        }
        return this.http.get<Array<Location>>( url ).pipe(
            map(locations => {
                const newLocations: Array<Location> = [];
                for (const location of locations) {
                    newLocations.push(Location.build(location))
                }
                return newLocations;
            })
        );
    }

    newLocation( insId: number, data: any ): Observable<any> {

        return this.http.post( this.baseUrl + insId + '/locations', JSON.stringify( data ), { observe: 'response' } );
    }

    patchLocation( insId: number, locationId: number, data: any ): Observable<any> {

        return this.http.patch( this.baseUrl + insId + '/locations/' + locationId, JSON.stringify( data ) );
    }

    deleteLocation( insId: number, locationId: number ): Observable<any> {

        return this.http.delete( this.baseUrl + insId + '/locations/' + locationId );
    }
}

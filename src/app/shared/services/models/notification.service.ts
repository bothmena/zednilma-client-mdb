import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Notification} from '../../models/notification';
import {AppGlobals} from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable()
export class NotificationService {

    private classesUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/users/';

    constructor(private http: HttpClient) {
    }

    getUserNotifications(userId: number, options: {} = {}): Observable<Array<Notification>> {

        let url = this.classesUrl + userId + '/notifications';
        for (const option in options) {

            if (option) {
                const char = url === this.classesUrl ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<Array<Notification>>(url).pipe(map(
            notifs => {
                const classes: Notification[] = [];
                for (const notif of notifs)
                    classes.push(Notification.build(notif))
                return classes;
            }
        ));
    }

    markNotification(userId: number, notifId: number, isRead: boolean): Observable<any> {

        const url = this.classesUrl + userId + '/notifications/' + notifId + '/' + (isRead ? 'read' : 'not-read');
        return this.http.post(url, null);
    }

    markAllRead(userId: number): Observable<any> {

        const url = this.classesUrl + userId + '/notifications/read';
        return this.http.post(url, null);
    }
}

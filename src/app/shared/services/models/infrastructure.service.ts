import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Infrastructure} from '../../models/infrastructure';
import {AppGlobals} from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable()
export class InfrastructureService {

    private baseUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes/';

    constructor(private http: HttpClient) {
    }

    getInfrastructures(insId: number, options: {} = {}): Observable<Array<Infrastructure>> {

        let url = this.baseUrl + insId + '/infrastructures';
        for (const option in options) {

            if (options.hasOwnProperty(option)) {

                if (Array.isArray(options[option])) {

                    for (const item of options[option]) {
                        const char = url === this.baseUrl + insId + '/infrastructures' ? '?' : '&';
                        url += char + option + '[]=' + item;
                    }
                } else {
                    const char = url === this.baseUrl ? '?' : '&';
                    url += char + option + '=' + options[option];
                }
            }
        }
        return this.http.get<Array<Infrastructure>>(url).pipe(
            map(
                infras => {
                    const infraClasses: Array<Infrastructure> = [];
                    for (const infra of infras) {
                        infraClasses.push(Infrastructure.build(infra));
                    }
                    return infraClasses;
                }
            )
        );
    }

    getInfrastructuresByInsSlug(insSlug: string, options: {} = {}): Observable<Infrastructure[]> {

        let url = this.baseUrl + insSlug + '/infrastructures';
        for (const option in options) {

            const char = url === this.baseUrl ? '?' : '&';
            url += char + option + '=' + options[option];
        }
        return this.http.get<Array<Infrastructure>>(url);
    }

    newInfrastructure(data: any, insId: number): Observable<any> {

        return this.http.post(this.baseUrl + insId + '/infrastructures', JSON.stringify(data), {observe: 'response'});
    }

    patchInfrastructure(insId: number, infraId: number, data: any): Observable<any> {

        const url = this.baseUrl + insId + '/infrastructures/' + infraId;
        return this.http.patch(url, JSON.stringify(data));
    }

    deleteInfrastructure(insId: number, infraId: number): Observable<any> {

        const url = this.baseUrl + insId + '/infrastructures/' + infraId;
        return this.http.delete(url);
    }
}

import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Post} from '../../models/post';
import {AppGlobals} from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable()
export class PostService {

    private postsUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes';

    constructor(private http: HttpClient) {
    }

    getPosts(instituteId: number, options: {} = {}): Observable<Array<Post>> {

        let url = this.postsUrl + '/' + instituteId + '/posts';
        let i = 0;
        for (const option in options) {

            if (options.hasOwnProperty(option)) {
                const char = i++ === 0 ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<Post[]>(url).pipe(
            map(
                posts => {
                    const postsClasses: Array<Post> = [];
                    for (const post of posts) {
                        postsClasses.push(Post.build(post));
                    }
                    return postsClasses;
                }
            )
        );
    }

    getPost(instituteId: number, postId: number, options: {} = {}): Observable<Post> {

        let url = this.postsUrl + '/' + instituteId + '/posts/' + postId;
        let i = 0;
        for (const option in options) {

            if (options.hasOwnProperty(option)) {
                const char = i++ === 0 ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<Post>(url).pipe(map(post => Post.build(post)));
    }

    newPost(data: any): Observable<any> {

        return this.http.post(this.postsUrl, JSON.stringify(data));
    }
}

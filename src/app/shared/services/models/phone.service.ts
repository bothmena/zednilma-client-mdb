import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Phone} from '../../models/phone';
import {AppGlobals} from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable()
export class PhoneService {

    private baseUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION;

    constructor(private http: HttpClient) {
    }

    getUserPhones(userId?: number, options: {} = {}): Observable<Array<Phone>> {

        // const uid = userId ? userId : this.jwtService.uid;
        let url = this.baseUrl + '/users/' + userId + '/phones';
        for (const option in options) {

            const char = url === this.baseUrl ? '?' : '&';
            url += char + option + '=' + options[option];
        }
        return this.http.get<Array<Phone>>(url).pipe(
            map(
                phones => {
                    const phoneClasses: Array<Phone> = [];
                    for (const phone of phones) {
                        phoneClasses.push(Phone.build(phone));
                    }
                    return phoneClasses;
                }
            )
        );
    }

    getInstitutePhones(insId: number, options: {} = {}): Observable<Array<Phone>> {

        let url = this.baseUrl + '/institutes/' + insId + '/phones';
        for (const option in options) {

            const char = url === this.baseUrl ? '?' : '&';
            url += char + option + '=' + options[option];
        }
        return this.http.get<Array<Phone>>(url).pipe(
            map(
                phones => {
                    const phoneClasses: Array<Phone> = [];
                    for (const phone of phones) {
                        phoneClasses.push(Phone.build(phone));
                    }
                    return phoneClasses;
                }
            )
        );
    }

    newUserPhone(data: any): Observable<any> {

        // const url = this.baseUrl + '/users/' + this.jwtService.uid + '/phones';
        return this.http.post('', JSON.stringify(data), { observe: 'response' });
    }

    newInstitutePhone(insId: number, data: any): Observable<any> {

        const url = this.baseUrl + '/institutes/' + insId + '/phones';
        return this.http.post(url, JSON.stringify(data), { observe: 'response' });
    }

    patchUserPhone(phoneId: number, data: any): Observable<any> {

        // const url = this.baseUrl + '/users/' + this.jwtService.uid + '/phones/' + phoneId;

        return this.http.patch('' + phoneId, JSON.stringify(data));
    }

    patchInstitutePhone(insId: number, phoneId: number, data: any): Observable<any> {

        const url = this.baseUrl + '/institutes/' + insId + '/phones/' + phoneId;
        return this.http.patch(url, JSON.stringify(data));
    }

    deleteUserPhone(phoneId: number): Observable<any> {

        // const url = this.baseUrl + '/users/' + this.jwtService.uid + '/phones/' + phoneId;

        return this.http.delete('' + phoneId);
    }

    deleteInstitutePhone(insId: number, phoneId: number): Observable<any> {

        const url = this.baseUrl + '/institutes/' + insId + '/phones/' + phoneId;

        return this.http.delete(url);
    }

    confirmUserPhone(phoneId: number, code: string): Observable<any> {

        // const url = this.baseUrl + '/users/' + this.jwtService.uid + '/confirm-phone/' + code;
        return this.http.post('' + phoneId + '' + code, null);
    }

    confirmInstitutePhone(insId: number, phoneId: number, code: string): Observable<any> {

        const url = this.baseUrl + '/institutes/' + insId + '/confirm-phone/' + code;

        return this.http.post(url + '' + phoneId, null);
    }
}

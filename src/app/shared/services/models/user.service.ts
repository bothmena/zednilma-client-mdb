import {Injectable} from '@angular/core';
import {User} from '../../models/user';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AppGlobals} from '../../utils/app.globals';
import {UserInstitute} from '../../models/user-institute';
import {map} from 'rxjs/operators';

@Injectable()
export class UserService {

    private usersUrl = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/users';

    constructor(private http: HttpClient) {
    }

    getUser(username: string): Observable<User> {

        const url = this.usersUrl + '/' + username;
        return this.http.get<User>(url).pipe(
            map(
                user => {
                    if (!!user.id) {
                        return User.build(user);
                    } else {
                        return null;
                    }
                }
            )
        );
    }

    getUserInstitute(username: string): Observable<UserInstitute> {

        const url = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/user-institute/' + username;
        return this.http.get<UserInstitute>(url).pipe(
            map(userInstitute => {
                return UserInstitute.build(userInstitute);
            })
        );
    }

    getInstituteMembers(insSlug: string, options: {}): Observable<User[]> {

        let url = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes/' + insSlug + '/members';
        let i = 0;
        for (const option in options) {

            if (options.hasOwnProperty(option)) {
                const char = i++ === 0 ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<User[]>(url).pipe(
            map(
                users => {
                    const usersClasses: Array<User> = [];
                    for (const user of users)
                        usersClasses.push(User.build(user));
                    return usersClasses;
                }
            )
        );
    }

    getUsers(options: {} = {}): Observable<Array<User>> {

        let url = this.usersUrl;
        let i = 0;
        for (const option in options) {

            if (options.hasOwnProperty(option)) {
                const char = i++ === 0 ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<Array<User>>(url).pipe(
            map(
                users => {
                    const usersClasses: Array<User> = [];
                    for (const user of users) {
                        usersClasses.push(User.build(user));
                    }
                    return usersClasses;
                }
            )
        );
    }

    getChildren(parentId: number): Observable<Array<User>> {

        const url = this.usersUrl + '/' + parentId + '/children';
        return this.http.get<Array<User>>(url).pipe(
            map(
                users => {
                    const usersClasses: Array<User> = [];
                    for (const user of users) {
                        usersClasses.push(User.build(user));
                    }
                    return usersClasses;
                }
            )
        );
    }

    newUser(data: any): Observable<any> {

        return this.http.post(this.usersUrl, JSON.stringify(data));
    }

    passwordForgotten(email: string) {

        const url = this.usersUrl + '/' + email + '/request-password-reset';
        return this.http.post<any>(url, null);
    }

    initUser(token: string, data: any): Observable<any> {

        return this.http.post(this.usersUrl + '/init-account/' + token, JSON.stringify(data));
    }

    getUserExistByToken(token: string, enabled?: boolean): Observable<boolean> {

        let url = this.usersUrl + '/t/' + token;
        if (enabled !== null) {

            url += enabled ? '?status=true' : '?status=false';
        }
        return this.http.get<{ exist: boolean }>(url).pipe(
            map(value => value.exist)
        );
    }

    patchUser(id: number, data: any): Observable<any> {

        return this.http.patch(this.usersUrl + '/' + id, JSON.stringify(data));
    }

    changeStatus(insId: number, userId: number, status: string): Observable<any> {

        const url = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes/' + insId + '/members/' + userId + '/' + status;
        return this.http.post(url, null);
    }

    changePassword(userId: number, data: any): Observable<any> {

        return this.http.post(this.usersUrl + '/' + userId + '/reset-password', JSON.stringify(data));
    }

    resetPassword(token: string, data: any): Observable<any> {

        return this.http.post(this.usersUrl + '/' + token + '/reset-password', JSON.stringify(data));
    }
}

import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AppGlobals} from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Path} from '../../models/path';
import {PathBranch} from '../../models/path-branch';
import {Speciality} from '../../models/speciality';
import {SpecialityPath} from '../../models/speciality-path';

@Injectable()
export class PathService {

    private baseUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes/';

    constructor(private http: HttpClient) {
    }

    getPaths(instituteId: number | string, options: {} = {}): Observable<Array<Speciality>> {

        let url = this.baseUrl + instituteId + '/paths';
        let i = 0;
        for (const option in options) {

            if (options.hasOwnProperty(option)) {
                const char = i++ === 0 ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<Path[]>(url).pipe(
            map(
                pathBranches => {
                    const specialities: Array<Speciality> = [];
                    for (const pb of pathBranches) {
                        const pathBranch = PathBranch.build(pb);
                        const path = Path.build(pb['path']);
                        let added = false;
                        for (const spec of specialities) {
                            if (spec.id === path.speciality.id) {
                                spec.addPath(new SpecialityPath(path.id, path.date), pathBranch);
                                spec.addChild(pathBranch.branch);
                                added = true;
                                break;
                            }
                        }
                        if (!added) {
                            path.speciality.addPath(new SpecialityPath(path.id, path.date), pathBranch);
                            path.speciality.addChild(pathBranch.branch);
                            specialities.push(path.speciality);
                        }
                    }
                    return specialities;
                }
            )
        );
    }

    newPath(insId: number, specId: number, data: any): Observable<any> {

        const url = this.baseUrl + insId + '/specialities/' + specId + '/paths';
        return this.http.post(url, JSON.stringify(data), {observe: 'response'});
    }

    deletePath(insId: number, specId: number, id: number): Observable<any> {

        const url = this.baseUrl + insId + '/specialities/' + specId + '/paths/' + id;
        return this.http.delete(url);
    }
}

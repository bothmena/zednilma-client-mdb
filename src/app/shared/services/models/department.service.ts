import {Injectable} from '@angular/core';
import {Department} from '../../models/department';
import {AppGlobals} from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {map} from 'rxjs/operators';
import {DepartmentMember} from '../../models/department-member';
import {UserDepartment} from '../../models/user-department';

@Injectable()
export class DepartmentService {

    private baseUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes/';

    constructor(private http: HttpClient) {
    }

    getDepartments(insIdOrSlug: number | string, options: {} = {}): Observable<Array<Department>> {

        let url = this.baseUrl + insIdOrSlug + '/departments';
        let i = 0;
        for (const option in options) {

            const char = i++ === 0 ? '?' : '&';
            url += char + option + '=' + options[option];
        }
        return this.http.get<Array<Department>>(url);
    }

    newDepartment(insId: number, data: any): Observable<any> {

        return this.http.post(this.baseUrl + insId + '/departments', JSON.stringify(data), {observe: 'response'});
    }

    patchDepartment(insId: number, depId: number, data: any): Observable<any> {

        const url = this.baseUrl + insId + '/departments/' + depId;
        return this.http.patch(url, JSON.stringify(data));
    }

    deleteDepartment(insId: number, depId: number): Observable<any> {

        const url = this.baseUrl + insId + '/departments/' + depId;
        return this.http.delete(url);
    }

    getMember(insId: number, depId: number, memId: number): Observable<DepartmentMember> {

        const url = this.baseUrl + insId + '/departments/' + depId + '/members/' + memId;
        return this.http.get<DepartmentMember>(url).pipe(map( data => DepartmentMember.build(data) ));
    }

    getMemberDep(insId: number, memId: number): Observable<UserDepartment> {

        const url = this.baseUrl + insId + '/members/' + memId + '/department';
        return this.http.get<UserDepartment>(url).pipe(map( data => UserDepartment.build(data) ));
    }

    addMember(insId: number, depId: number, data: {user: number, post: number}): Observable<any> {

        const url = this.baseUrl + insId + '/departments/' + depId + '/members';
        return this.http.post(url, JSON.stringify(data), { observe: 'response' });
    }

    editMember(insId: number, depId: number, memId: number, data: {user: number, post: number}): Observable<any> {

        const url = this.baseUrl + insId + '/departments/' + depId + '/members/' + memId;
        return this.http.put(url, JSON.stringify(data));
    }

    deleteMember(insId: number, depId: number, memId: number): Observable<any> {

        const url = this.baseUrl + insId + '/departments/' + depId + '/members/' + memId;
        return this.http.delete(url);
    }
}

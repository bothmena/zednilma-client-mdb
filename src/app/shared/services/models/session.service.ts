import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AppGlobals} from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';
import {Session} from '../../models/session';
import {map} from 'rxjs/operators';
import {User} from '../../models/user';
import {SessionContainer} from '../../models/session-container';
import {Timetable} from '../../models/timetable';

@Injectable()
export class SessionService {

    private baseUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes/';

    constructor(private http: HttpClient) {
    }

    static weekTTProcessing(data: {}): Timetable {

        const containers: SessionContainer[] = [];
        for (const object of data['containers']) {
            containers.push(SessionContainer.build(object))
        }

        for (const object of data['sessions']) {
            const session = Session.build(object);
            for (const cont of containers) {
                if (cont.id === session.container.id)
                    cont.session = session;
            }
        }
        const days = [];
        for (const cont of containers) {
            if (!!days[cont.day])
                days[cont.day].push(cont);
            else
                days[cont.day] = [cont];
        }
        const timetable = new Timetable();
        if (containers.length > 0)
            timetable.id = containers[0].timetableId;
        timetable.days = days;
        timetable.dirty = false;
        timetable.fillBreaks();
        timetable.calculateMaxMinutes();
        return timetable;
    }

    getActualSession(insId: number, options?: {}) {

        let url = this.baseUrl + insId + '/sessions/current';
        for (const option in options) {

            if (option) {
                const char = url === this.baseUrl ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<any>(url).pipe(
            map((object: Object) => {
                const result = {'session': null, 'students': [], 'assistants': []};
                if (object.hasOwnProperty('session'))
                    result['session'] = Session.build(object['session']);

                if (object.hasOwnProperty('students')) {
                    for (const stuObj of object['students'])
                        result['students'].push(User.build(stuObj))
                }

                if (object.hasOwnProperty('assistants')) {
                    for (const stuObj of object['assistants'])
                        result['assistants'].push(User.build(stuObj))
                }
                return result;
            })
        );
    }

    getProfessorSessionReports(insId: number, profId: number, options?: {}) {

        let url = this.baseUrl + insId + '/professors/' + profId + '/reports';
        for (const option in options) {

            if (option) {
                const char = url === this.baseUrl ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get(url).pipe(
            map((data: {}[]) => {
                const sessions: Session[] = [];
                for (const obj of data)
                    sessions.push(Session.build(obj))
                return sessions;
            })
        );
    }

    getStudentSessionReports(insId: number, stdUsername: string, options?: {}) {

        let url = this.baseUrl + insId + '/students/' + stdUsername + '/reports';
        for (const option in options) {

            if (option) {
                const char = url === this.baseUrl ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get(url).pipe(
            map((data: {}[]) => {
                const sessions: Session[] = [];
                for (const obj of data)
                    sessions.push(Session.build(obj))
                return sessions;
            })
        );
    }

    // /institutes/{insId}/sessions/{classId}/{week}
    getClassWeekSessions(insId: number, classSlug: string, week: number) {

        const url = this.baseUrl + insId + '/sessions/' + classSlug + '/' + week;
        return this.http.get(url).pipe(map(
            (objects: Object[]) => {

                const sessions: Session[] = [];
                for (const object of objects) {
                    sessions.push(Session.build(object))
                }
                return sessions;
            }
        ));
    }

    getStudentWeekTT(insId: number, stdUsername: string, week: number = -1): Observable<Timetable> {

        let url: string;
        if (week > 0)
            url = this.baseUrl + insId + '/students/' + stdUsername + '/sessions/' + week;
        else
            url = this.baseUrl + insId + '/students/' + stdUsername + '/sessions';
        return this.http.get<Timetable>(url).pipe(map((arr: {}) =>  SessionService.weekTTProcessing(arr)));
    }

    getProfWeekTT(insId: number, profId: number, week: number = -1): Observable<Timetable> {

        let url: string;
        if (week > 0)
            url = this.baseUrl + insId + '/professors/' + profId + '/sessions/' + week;
        else
            url = this.baseUrl + insId + '/professors/' + profId + '/sessions';
        return this.http.get<Timetable>(url).pipe(map((arr: {}) =>  SessionService.weekTTProcessing(arr)));
    }

    saveAttendances(insId: number, data) {

        const url = this.baseUrl + insId + '/attendances';
        return this.http.post(url, data);
    }

    newSession(insId: number, data: any): Observable<any> {

        const url = this.baseUrl + insId + '/sessions';
        return this.http.post(url, JSON.stringify(data));
    }

    patchSession(insId: number, id: number, data: any): Observable<any> {

        const url = this.baseUrl + insId + '/sessions/' + id;
        return this.http.patch(url, JSON.stringify(data));
    }

    deleteSession(insId: number, id: number): Observable<any> {

        const url = this.baseUrl + insId + '/sessions/' + id;
        return this.http.delete(url);
    }
}

import {Injectable} from '@angular/core';
import {Institute} from '../../models/institute';
import {AppGlobals} from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {map} from 'rxjs/operators';
import {User} from '../../models/user';
import {InstituteSettings} from '../../models/institute-settings';

@Injectable()
export class InstituteService {

    private institutesUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes';

    constructor(private http: HttpClient) {
    }

    getInstitute(instituteId: number): Observable<Institute> {

        const url = this.institutesUrl + '/' + instituteId;
        return this.http.get<Institute>(url).pipe(map(institute => Institute.build(institute)));
    }

    getInstituteMembers(instituteId: number, options: {} = {}): Observable<User[]> {

        let url = this.institutesUrl + '/' + instituteId + '/members';
        let i = 0;
        for (const option in options) {

            if ( options.hasOwnProperty(option) ) {
                const char = i++ === 0 ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<User[]>(url).pipe(
            map(
                users => {
                    const usersClasses: Array<User> = [];
                    for (const user of users) {
                        usersClasses.push(User.build(user));
                    }
                    return usersClasses;
                }
            )
        );
    }

    getInstituteBySlug(slug: string): Observable<Institute> {

        const url = this.institutesUrl + '/' + slug;
        return this.http.get<Institute>(url).pipe(map(institute => Institute.build(institute)));
    }

    getInstitutes(options: {} = {}): Observable<Institute[]> {

        let url = this.institutesUrl;
        let i = 0;
        for (const option in options) {

            if ( options.hasOwnProperty(option) ) {
                const char = i++ === 0 ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<Array<Institute>>(url).pipe(
            map(
                institutes => {
                    const institutesClasses: Array<Institute> = [];
                    for (const institute of institutes) {
                        institutesClasses.push(Institute.build(institute));
                    }
                    return institutesClasses;
                }
            )
        );
    }

    getInstituteSettings(insId: number): Observable<InstituteSettings> {

        return this.http.get<InstituteSettings>(this.institutesUrl + '/' + insId + '/settings').pipe(
            map( insSettings => InstituteSettings.build(insSettings) )
        )
    }

    newInstitute(data: any): Observable<any> {

        return this.http.post(this.institutesUrl, JSON.stringify(data));
    }

    patchInstitute(insId: number, data: any): Observable<any> {

        return this.http.patch(this.institutesUrl + '/' + insId, JSON.stringify(data));
    }
}

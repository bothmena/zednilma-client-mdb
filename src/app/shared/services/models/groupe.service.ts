import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Groupe } from '../../models/groupe';
import { AppGlobals } from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class GroupeService {

    private groupesUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/groupes';

    constructor( private http: HttpClient ) {
    }

    getGroupes( options: {} = {} ): Observable<Array<Groupe>> {

        let url = this.groupesUrl;
        for ( const option in options ) {

            const char = url === this.groupesUrl ? '?' : '&';
            url += char + option + '=' + options[ option ];
        }
        return this.http.get<Array<Groupe>>( url );
    }

    newGroupe( data: any ): Observable<any> {

        return this.http.post( this.groupesUrl, JSON.stringify( data ) );
    }
}

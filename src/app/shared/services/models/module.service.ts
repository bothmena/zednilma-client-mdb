import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Module} from '../../models/module';
import {AppGlobals} from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {ModuleSubject} from '../../models/module-subject';

@Injectable()
export class ModuleService {

    private baseUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes/';

    constructor(private http: HttpClient) {
    }

    getModules(insId: number, specId: number, options: {} = {}): Observable<Array<Module>> {

        let url = this.baseUrl + insId + '/specialities/' + specId + '/modules';
        for (const option in options) {

            if (options.hasOwnProperty(option)) {
                const char = url === this.baseUrl ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<Array<Module>>(url).pipe(map(
            mdls => {
                const modules: Array<Module> = [];
                for (const mdl of mdls)
                    modules.push(Module.build(mdl))
                return modules;
            }
        ));
    }

    getSpecModulesSubjects(insId: number, specId: number, options: {} = {}): Observable<Array<ModuleSubject>> {

        let url = this.baseUrl + insId + '/specialities/' + specId + '/module-subjects';
        for (const option in options) {

            if (options.hasOwnProperty(option)) {
                const char = url === this.baseUrl ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<Array<ModuleSubject>>(url).pipe(map(
            mdlSbjs => {
                const moduleSubjects: Array<ModuleSubject> = [];
                for (const mdlSbj of mdlSbjs)
                    moduleSubjects.push(ModuleSubject.build(mdlSbj))
                return moduleSubjects;
            }
        ));
    }

    newModule(insId: number, specId: number, data: any): Observable<any> {

        const url = this.baseUrl + insId + '/specialities/' + specId + '/modules';
        return this.http.post(url, JSON.stringify(data), {observe: 'response'});
    }

    newSpecModuleSubject(insId: number, specId: number, data: any): Observable<any> {

        const url = this.baseUrl + insId + '/specialities/' + specId + '/module-subjects';
        return this.http.post(url, JSON.stringify(data), {observe: 'response'});
    }

    deleteModule(insId: number, specId: number, moduleId: number): Observable<any> {

        const url = this.baseUrl + insId + '/specialities/' + specId + '/modules/' + moduleId;
        return this.http.delete(url);
    }

    deleteModuleSubject(insId: number, specId: number, mdlSbjId: number): Observable<any> {

        const url = this.baseUrl + insId + '/specialities/' + specId + '/module-subjects/' + mdlSbjId;
        return this.http.delete(url);
    }

    patchModule(insId: number, specId: number, moduleId: number, data: any) {

        const url = this.baseUrl + insId + '/specialities/' + specId + '/modules/' + moduleId;
        return this.http.patch(url, JSON.stringify(data));
    }

    patchModuleSubject(insId: number, specId: number, mdlSbjId: number, data: any) {

        const url = this.baseUrl + insId + '/specialities/' + specId + '/module-subjects/' + mdlSbjId;
        return this.http.patch(url, JSON.stringify(data));
    }
}

import { TestBed, inject } from '@angular/core/testing';

import { SubjectClasseService } from './subject-classe.service';

describe('SubjectClasseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubjectClasseService]
    });
  });

  it('should be created', inject([SubjectClasseService], (service: SubjectClasseService) => {
    expect(service).toBeTruthy();
  }));
});

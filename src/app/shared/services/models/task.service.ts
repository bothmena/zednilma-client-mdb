import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AppGlobals} from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {TaskList} from '../../models/task-list';
import {TaskUser} from '../../models/task-user';

@Injectable()
export class TaskService {

    private baseUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/users';

    constructor(private http: HttpClient) {
    }

    getTaskLists(userUID: number|string, options: {} = {}): Observable<Array<TaskList>> {

        let url = this.baseUrl + '/' + userUID + '/task-lists';
        let i = 0;
        for (const option in options) {

            if (options.hasOwnProperty(option)) {
                const char = i++ === 0 ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<TaskList[]>(url).pipe(
            map(
                objects => {
                    const taskLists: Array<TaskList> = [];
                    for (const object of objects) {
                        taskLists.push(TaskList.build(object));
                    }
                    return taskLists;
                }
            )
        );
    }

    getUserTasks(userUID: number|string, options: {} = {}): Observable<Array<TaskUser>> {

        let url = this.baseUrl + '/' + userUID + '/tasks';
        let i = 0;
        for (const option in options) {

            if (options.hasOwnProperty(option)) {
                const char = i++ === 0 ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<TaskUser[]>(url).pipe(
            map(
                objects => {
                    const userTasks: Array<TaskUser> = [];
                    for (const object of objects) {
                        userTasks.push(TaskUser.build(object));
                    }
                    return userTasks;
                }
            )
        );
    }

    newTask(userId: number, data: any): Observable<any> {

        const url = this.baseUrl + '/' + userId + '/tasks';
        return this.http.post(url, JSON.stringify(data), {'observe': 'response'});
    }

    newList(userId: number, data: any): Observable<any> {

        const url = this.baseUrl + '/' + userId + '/task-lists';
        return this.http.post(url, JSON.stringify(data), {'observe': 'response'});
    }

    patchTask(userId: number, taskId: number, data: any): Observable<any> {

        const url = this.baseUrl + '/' + userId + '/tasks/' + taskId;
        return this.http.patch(url, JSON.stringify(data));
    }

    patchList(userId: number, listId: number, data: any): Observable<any> {

        const url = this.baseUrl + '/' + userId + '/task-lists/' + listId;
        return this.http.patch(url, JSON.stringify(data));
    }

    markTUAsDone(userId: number, tUId: number, isDone: boolean): Observable<any> {

        const url = this.baseUrl + '/' + userId + '/task-user/' + tUId + '/' + (isDone ? 'done' : 'not-done');
        return this.http.post(url, null);
    }
}

import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Email} from '../../models/email';
import {AppGlobals} from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable()
export class EmailService {

    private baseUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION;

    constructor(private http: HttpClient) {
    }

    getUserEmails(userId?: number, options: {} = {}): Observable<Email[]> {

        const uid = userId; // ? userId : this.jwtService.uid;
        let url = this.baseUrl + '/users/' + uid + '/emails';
        for (const option of Object.keys(options)) {
            const char = (url === this.baseUrl + '/users/' + uid + '/emails') ? '?' : '&';
            url += char + option + '=' + options[option];
        }
        return this.http.get<Array<Email>>(url).pipe(
            map(
                emails => {
                    const emailClasses: Array<Email> = [];
                    for (const email of emails) {
                        emailClasses.push(Email.build(email));
                    }
                    return emailClasses;
                }
            )
        );
    }

    getInstituteEmails(insId: number, options: {} = {}): Observable<Email[]> {

        let url = this.baseUrl + '/institutes/' + insId + '/emails';
        for (const option of Object.keys(options)) {
            const char = (url === this.baseUrl + '/institutes/' + insId + '/emails') ? '?' : '&';
            url += char + option + '=' + options[option];
        }
        return this.http.get<Array<Email>>(url).pipe(
            map(
                emails => {
                    const emailClasses: Array<Email> = [];
                    for (const email of emails) {
                        emailClasses.push(Email.build(email));
                    }
                    return emailClasses;
                }
            )
        );
    }

    newUserEmail(data: any): Observable<any> {

        const url = this.baseUrl + '/users/emails';
        // const url = this.baseUrl + '/users/' + this.jwtService.uid + '/emails';

        return this.http.post(url, JSON.stringify(data), { observe: 'response' });
    }

    patchUserEmail(emailId: number, data: any): Observable<any> {

        const url = this.baseUrl + '/users/emails/' + emailId;

        return this.http.patch(url, JSON.stringify(data));
    }

    deleteUserEmail(emailId: number): Observable<any> {

        const url = this.baseUrl + '/users/emails/' + emailId;

        return this.http.delete(url);
    }

    newInstituteEmail(insId: number, data: any): Observable<any> {

        const url = this.baseUrl + '/institutes/' + insId + '/emails';

        return this.http.post(url, JSON.stringify(data), { observe: 'response' });
    }

    patchInstituteEmail(insId: number, emailId: number, data: any): Observable<any> {

        const url = this.baseUrl + '/institutes/' + insId + '/emails/' + emailId;

        return this.http.patch(url, JSON.stringify(data));
    }

    deleteInstituteEmail(insId: number, emailId: number): Observable<any> {

        const url = this.baseUrl + '/institutes/' + insId + '/emails/' + emailId;

        return this.http.delete(url);
    }

    confirmEmail(token: string): Observable<any> {

        const url = this.baseUrl + '/confirm-email/' + token;

        return this.http.get(url);
    }
}

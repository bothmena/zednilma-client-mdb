import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from '../../models/subject';
import { AppGlobals } from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable()
export class SubjectService {

    private baseUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes/';

    constructor( private http: HttpClient ) {
    }

    getSubjects( insId: number, options: {} = {} ): Observable<Array<Subject>> {

        let url = this.baseUrl + insId + '/subjects';
        for ( const option in options ) {

            if (options.hasOwnProperty(option)) {
                const char = url === this.baseUrl ? '?' : '&';
                url += char + option + '=' + options[ option ];
            }
        }
        return this.http.get<Array<Subject>>( url ).pipe(map(
            subs => {
                const subjects: Subject[] = [];
                for (const sub of subs)
                    subjects.push(Subject.build(sub))
                return subjects;
            }
        ));
    }

    getSubject( insId: number, depIdOrSlug: number|string, subId: number, options: {} = {} ): Observable<Subject> {

        let url = this.baseUrl + insId + '/departments/' + depIdOrSlug + '/subjects/' + subId;
        for ( const option in options ) {

            if (options.hasOwnProperty(option)) {
                const char = url === this.baseUrl ? '?' : '&';
                url += char + option + '=' + options[ option ];
            }
        }
        return this.http.get<Subject>( url ).pipe(map(sub => Subject.build(sub)));
    }

    newSubject( insId: number, depId: number, data: any ): Observable<any> {

        return this.http.post( this.baseUrl + insId + '/departments/' + depId + '/subjects', JSON.stringify( data ), {observe: 'response'} );
    }

    patchSubject( insId: number, depId: number, subId: number, data: any ): Observable<any> {

        const url = this.baseUrl + insId + '/departments/' + depId + '/subjects/' + subId;
        return this.http.patch( url, JSON.stringify( data ) );
    }

    deleteSubject( insId: number, depId: number, subId: number ): Observable<any> {

        const url = this.baseUrl + insId + '/departments/' + depId + '/subjects/' + subId;
        return this.http.delete( url );
    }
}

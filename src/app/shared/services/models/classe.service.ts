import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Classe} from '../../models/classe';
import {AppGlobals} from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable()
export class ClasseService {

    private classesUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes/';

    constructor(private http: HttpClient) {
    }

    getInsClasses(insId: number, options: {} = {}): Observable<Array<Classe>> {

        let url = this.classesUrl + insId + '/classes';
        for (const option in options) {

            if (option) {
                const char = url === this.classesUrl ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<Array<Classe>>(url).pipe(map(
            cls => {
                const classes: Classe[] = [];
                for (const cl of cls)
                    classes.push(Classe.build(cl))
                return classes;
            }
        ));
    }

    getSpecClasses(insId: number, specId: number, options: {} = {}): Observable<Array<Classe>> {

        let url = this.classesUrl + insId + '/specialities/' + specId + '/classes';
        for (const option in options) {

            if (option) {
                const char = url === this.classesUrl ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<Array<Classe>>(url).pipe(map(
            cls => {
                const classes: Classe[] = [];
                for (const cl of cls)
                    classes.push(Classe.build(cl))
                return classes;
            }
        ));
    }

    newClasse(insId: number, specId: number, data: any): Observable<any> {

        const url = this.classesUrl + insId + '/specialities/' + specId + '/classes';
        return this.http.post(url, JSON.stringify(data));
    }

    deleteClasse(insId: number, specId: number, id: number): Observable<any> {

        const url = this.classesUrl + insId + '/specialities/' + specId + '/classes/' + id;
        return this.http.delete(url);
    }
}

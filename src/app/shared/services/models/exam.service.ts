import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AppGlobals} from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Exam} from '../../models/exam';

@Injectable()
export class ExamService {

    private classesUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes/';

    constructor(private http: HttpClient) {
    }

    // /institutes/{insId}/subjects/{subId}/exams
    getSubjectExams(insId: number, subId: number, options: {} = {}): Observable<Array<Exam>> {

        let url = this.classesUrl + insId + '/subjects/' + subId + '/exams';
        for (const option in options) {

            if (option) {
                const char = url === this.classesUrl ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<Array<Exam>>(url).pipe(map(
            exs => {
                const exams: Exam[] = [];
                for (const ex of exs)
                    exams.push(Exam.build(ex))
                return exams;
            }
        ));
    }

    newExam(insId: number, subId: number, data: any): Observable<any> {

        const url = this.classesUrl + insId + '/subjects/' + subId + '/exams';
        return this.http.post(url, JSON.stringify(data), {'observe': 'response'});
    }

    patchExam(insId: number, subId: number, id: number, data: any): Observable<any> {

        const url = this.classesUrl + insId + '/subjects/' + subId + '/exams/' + id;
        return this.http.patch(url, JSON.stringify(data));
    }

    deleteExam(insId: number, subId: number, id: number): Observable<any> {

        const url = this.classesUrl + insId + '/subjects/' + subId + '/exams/' + id;
        return this.http.delete(url);
    }
}

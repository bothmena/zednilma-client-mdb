import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {SocialProfile} from '../../models/social-profile';
import {AppGlobals} from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class SocialProfileService {

    private baseUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION;

    constructor(private http: HttpClient) {
    }

    getUserSPs(userId?: number, options: {} = {}): Observable<Array<SocialProfile>> {

        // const uid = userId ? userId : this.jwtService.uid;
        let url = this.baseUrl + '/users/' + userId + '/social-profiles';
        for (const option in options) {

            const char = url === this.baseUrl ? '?' : '&';
            url += char + option + '=' + options[option];
        }
        return this.http.get<Array<SocialProfile>>(url);
    }

    getInstituteSPs(insId: number, options: {} = {}): Observable<Array<SocialProfile>> {

        let url = this.baseUrl + '/institutes/' + insId + '/social-profiles';
        for (const option in options) {

            const char = url === this.baseUrl ? '?' : '&';
            url += char + option + '=' + options[option];
        }
        return this.http.get<Array<SocialProfile>>(url);
    }

    newUserSP(data: any): Observable<any> {

        // const url = this.baseUrl + '/users/' + this.jwtService.uid + '/social-profiles';

        return this.http.post('', JSON.stringify(data), { observe: 'response' });
    }

    newInstituteSP(insId: number, data: any): Observable<any> {

        const url = this.baseUrl + '/institutes/' + insId + '/social-profiles';

        return this.http.post(url, JSON.stringify(data), { observe: 'response' });
    }

    patchUserSP(phoneId: number, data: any): Observable<any> {

        // const url = this.baseUrl + '/users/' + this.jwtService.uid + '/social-profiles/' + phoneId;
        return this.http.patch('' + phoneId, JSON.stringify(data));
    }

    patchInstituteSP(insId: number, phoneId: number, data: any): Observable<any> {

        const url = this.baseUrl + '/institutes/' + insId + '/social-profiles/' + phoneId;
        return this.http.patch(url, JSON.stringify(data));
    }

    deleteUserSP(phoneId: number): Observable<any> {

        // const url = this.baseUrl + '/users/' + this.jwtService.uid + '/social-profiles/' + phoneId;
        return this.http.delete('' + phoneId);
    }

    deleteInstituteSP(insId: number, phoneId: number): Observable<any> {

        const url = this.baseUrl + '/institutes/' + insId + '/social-profiles/' + phoneId;
        return this.http.delete(url);
    }
}

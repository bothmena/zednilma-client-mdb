import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppGlobals} from '../../utils/app.globals';
import {map} from 'rxjs/operators';
import {SubjectClasse} from '../../models/subject-classe';

@Injectable({
    providedIn: 'root'
})
export class SubjectClasseService {

    private baseUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes/';

    constructor(private http: HttpClient) {
    }

    getProfessorSubClasses(insId: number, profId: number) {

        const url = this.baseUrl + insId + '/professors/' + profId + '/subject-classes';
        return this.http.get<Array<SubjectClasse>>(url).pipe(map(
            (objects) => {

                const subClasses: SubjectClasse[] = [];
                for (const object of objects)
                    subClasses.push(SubjectClasse.build(object));
                return subClasses;
            }
        ));
    }

    getClasseSubjects(insId: number, classSlug: string) {

        const url = this.baseUrl + insId + '/class/' + classSlug + '/subjects';
        return this.http.get<Array<SubjectClasse>>(url).pipe(map(
            (objects) => {

                const subClasses: SubjectClasse[] = [];
                for (const object of objects)
                    subClasses.push(SubjectClasse.build(object));
                return subClasses;
            }
        ));
    }

    newProfessorSubClasses(insId: number, profId: number, data) {

        const url = this.baseUrl + insId + '/professors/' + profId + '/subject-classes';
        return this.http.post(url, JSON.stringify(data), {observe: 'response'});
    }

    deleteSubClasse(insId: number, subClasseId: number) {

        const url = this.baseUrl + insId + '/subject-classes/' + subClasseId;
        return this.http.delete(url);
    }
}

import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AppGlobals} from '../../utils/app.globals';
import {HttpClient} from '@angular/common/http';
import {Speciality} from '../../models/speciality';
import {map} from 'rxjs/operators';

@Injectable()
export class SpecialityService {

    private baseUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes/';

    constructor(private http: HttpClient) {
    }

    getSpeciality(insId: number, specId: number | string, options: {} = {}): Observable<Speciality> {

        let url = this.baseUrl + insId + '/specialities/' + specId;
        let i = 0;
        for (const option in options) {

            if (options.hasOwnProperty(option)) {
                const char = i++ === 0 ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<Speciality>(url).pipe(map(spec => Speciality.build(spec)));
    }

    getSpecialities(insIdOrSlug: number | string, options: {} = {}): Observable<Array<Speciality>> {

        let url = this.baseUrl + insIdOrSlug + '/specialities';
        let i = 0;
        for (const option in options) {

            if (options.hasOwnProperty(option)) {
                const char = i++ === 0 ? '?' : '&';
                url += char + option + '=' + options[option];
            }
        }
        return this.http.get<Array<Speciality>>(url).pipe(
            map(
                specialities => {
                    const specClasses: Array<Speciality> = [];
                    for (const speciality of specialities) {
                        specClasses.push(Speciality.build(speciality));
                    }
                    return specClasses;
                }
            )
        );
    }

    newSpeciality(insId: number, data: any): Observable<any> {

        const url = this.baseUrl + insId + '/specialities';
        return this.http.post(url, JSON.stringify(data), {observe: 'response'});
    }

    patchSpeciality(insId: number, specId: number, data: any): Observable<any> {

        const url = this.baseUrl + insId + '/specialities/' + specId;
        return this.http.patch(url, JSON.stringify(data));
    }

    deleteSpeciality(insId: number, specId: number): Observable<any> {

        const url = this.baseUrl + insId + '/specialities/' + specId;
        return this.http.delete(url);
    }
}

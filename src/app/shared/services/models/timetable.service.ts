import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppGlobals} from '../../utils/app.globals';
import {map} from 'rxjs/operators';
import {Timetable} from '../../models/timetable';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TimetableService {

    private baseUrl: string = AppGlobals.BASE_API_URL + AppGlobals.API_VERSION + '/institutes/';

    constructor(private http: HttpClient) {
    }

    getTimetables(insId: number): Observable<Array<Timetable>> {

        const url = this.baseUrl + insId + '/timetables';
        return this.http.get<Array<Timetable>>(url).pipe(map(
            (objects: Object[]) => {

                const timetables: Timetable[] = [];
                for (const object of objects) {
                    const timetable = Timetable.build(object);
                    timetable.fillBreaks();
                    timetables.push(timetable);
                }
                return timetables;
            }
        ));
    }

    getTimetableSessionContainers(insId: number, timeId: number): Observable<Timetable> {

        const url = this.baseUrl + insId + '/timetables/' + timeId;
        return this.http.get<Timetable>(url).pipe(map(object => Timetable.build(object)));
    }

    newTimetable(insId: number, data) {

        const url = this.baseUrl + insId + '/timetables';
        return this.http.post(url, JSON.stringify(data), {observe: 'response'});
    }

    editTimetable(insId: number, timeId: number, data) {

        const url = this.baseUrl + insId + '/timetables/' + timeId;
        return this.http.patch(url, JSON.stringify(data));
    }

    saveTimetableSessionContainers(insId: number, timeId: number, data) {

        const url = this.baseUrl + insId + '/timetables/' + timeId + '/containers';
        return this.http.post(url, JSON.stringify(data));
    }
}

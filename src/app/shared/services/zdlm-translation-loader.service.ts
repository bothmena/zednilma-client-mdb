import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class ZdlmTranslationLoaderService {

    constructor(private translate: TranslateService, private http: HttpClient) {
    }

    loadTranslations(module: string) {

        let url = '/assets/i18n/';
        url += this.translate.getBrowserLang();
        url += '/' + module + '.min.json';

        this.http.get(url).toPromise()
            .then(json => {
                // const data = json.json();
                this.translate.getTranslation('en').subscribe(
                    (trans) => {
                        this.translate.setTranslation(this.translate.getBrowserLang(), Object.assign(trans, json), false);
                    }
                );
            })
            .catch(() => {
                console.log('error occured while getting translations from server!');
            })
    }
}

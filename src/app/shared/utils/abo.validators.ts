/**
 * Created by bothmena on 29/01/17.
 */

import { AbstractControl, FormGroup, ValidatorFn } from '@angular/forms';

export interface IValidationError {

    [key: string]: {
        controlValue: any,
        params: {}
    }
}

export class ABOValidators {

    static onValueChanged( formGroup: FormGroup, model: string ): Object {

        const entities = ['location', 'email'];

        if ( !formGroup ) {
            return;
        }
        const formErrors: Object = {};
        for ( const field in formGroup.controls ) {

            const control: any  = formGroup.get( field );
            if ( control && !control.valid ) {

                if ( control.controls ) {

                    formErrors[ field ] = {};
                    for ( const field2 in control.controls ) {
                        const control2: AbstractControl = control.get( field2 );
                        if ( control2 && !control2.valid ) {

                            if ( control2.errors ) {

                                const key = Object.keys( control2.errors )[ 0 ];
                                formErrors[ field ][field2] = {};
                                if ( entities.indexOf( field ) === -1 ) {

                                    formErrors[ field ][field2].code = model + '.' + field + '.' + field2 + '.' + key;
                                } else {

                                    formErrors[ field ][field2].code = field + '.' + field2 + '.' + key;
                                }
                                formErrors[ field ][field2].params = ABOValidators.extractError( key, control2.errors[key] );
                            }
                        }
                    }
                } else {

                    const key = Object.keys( control.errors )[ 0 ];
                    formErrors[ field ] = {};
                    formErrors[ field ].code = model + '.' + field + '.' + key;
                    formErrors[ field ].params = ABOValidators.extractError( key, control.errors[key] );
                }
            }
        }
        return formErrors;
    }

    static inArray( array: any[] ): ValidatorFn {

        return ( control: AbstractControl ): IValidationError => {

            if ( control.value && control.value !== '' && array.indexOf( control.value ) === -1 ) {

                return {
                    'in_array': {
                        controlValue: control.value,
                        params      : {},
                    },
                };
            }
            return null;
        };
    }

    static dateLessThan( value: Date ): ValidatorFn {

        return ( control: AbstractControl ): IValidationError => {

            let date: Date = null;
            if ( control.value ) {
                const dateArr: number[] = control.value.split( '/' );
                date                    = new Date( dateArr[ 2 ], dateArr[ 1 ] - 1, dateArr[ 0 ] );
            }
            if ( date && ( +date - +value >= 0 ) ) {

                return {
                    'date_less_than': {
                        controlValue: control.value,
                        params      : {
                            maxDate: value.toDateString(),
                        },
                    },
                };
            }
            return null;
        };
    }

    static dateGreaterThan( value: Date ): ValidatorFn {

        return ( control: AbstractControl ): IValidationError => {

            let date: Date = null;
            if ( control.value ) {
                const dateArr: number[] = control.value.split( '/' );
                date                    = new Date( dateArr[ 2 ], dateArr[ 1 ] - 1, dateArr[ 0 ] );
            }
            if ( date && ( +date - +value <= 0 ) ) {

                return {
                    'date_greater_than': {
                        controlValue: control.value,
                        params      : {
                            minDate: value.toDateString(),
                        },
                    },
                };
            }
            return null;
        };
    }

    static extractError( key: string, error: any ): Object {

        switch ( key ) {

            case 'required':
                return {};
            case 'minlength':
                return { minlength: error.requiredLength };
            case 'maxlength':
                return { maxlength: error.requiredLength };
            case 'pattern':
                return {};
            case 'equalTo':
                return {};
            default:
                // my custom validations, they must contain a params object.
                return error.params;
        }
    }
}

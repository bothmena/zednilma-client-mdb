/**
 * Created by bothmena on 24/06/17.
 */

import {JWTToken} from '../models/jwt-token';

export function getJWTToken(): JWTToken {

    const token: string = localStorage.getItem('token');
    const refresh_token: string = localStorage.getItem('refresh_token');

    if (token && refresh_token) {

        return new JWTToken(token, refresh_token);
    } else {

        return null;
    }
}

export function countdown(dueDate: Date, date: Date) {

    // @ts-ignore
    const hours = (dueDate - date) / (1000 * 60 * 60);
    return {
        'hours': Math.floor(hours % 24),
        'days': Math.floor(hours / 24),
    }
}

export function strToDate(strDate: string): Date {

    if (strDate !== null && strDate !== '') {

        if (strDate.length > 10) {

            const dateTime = strDate.split(' ');
            const dateParts = dateTime[0].split('-');
            const timeParts = dateTime[1].split(':');
            return new Date(+dateParts[0], +dateParts[1] - 1, +dateParts[2],
                +timeParts[0], +timeParts[1], +timeParts[2]);
        } else {

            const dateParts = strDate.split('-');
            return new Date(+dateParts[0], +dateParts[1] - 1, +dateParts[2]);
        }
    }
    return null;
}

export function getCountryName(countryCode: string): string {

    if (isoCountries.hasOwnProperty(countryCode)) {

        return isoCountries[countryCode];
    }
    return '';
}

export function getWebsiteProfileUrl(website: string): string {

    const websites: {} = {
        'linkedin': 'linkedin.com/in/',
        'facebook': 'facebook.com/',
        'github': 'github.com/',
        'gitlab': 'gitlab.com/',
        'bitbucket': 'bitbucket.org/',
        'youtube': 'youtube.com/channel/',
        'google+': 'plus.google.com/',
        'twitter': 'twitter.com/',
        'instagram': 'instagram.com/',
        'whatsapp': 'whatsapp:',
        'skype': 'skype:',
        'vimeo': 'vimeo.com/',
    };
    if (websites.hasOwnProperty(website)) {

        return websites[website];
    }
    return null;
}

export function getDate(date: Date): string {

    let val = '';
    val += date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    val += '/' + (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '/';
    val += date.getFullYear();
    return val;
}

export function dateWithoutTimezone(dateStr: string): Date {

    if (!!dateStr) {

        const date = new Date();
        date.setFullYear(parseInt(dateStr.substr(0, 4), 10), parseInt(dateStr.substr(5, 2), 10), parseInt(dateStr.substr(8, 2), 10));
        date.setHours(parseInt(dateStr.substr(11, 2), 10), parseInt(dateStr.substr(14, 2), 10), parseInt(dateStr.substr(17, 2), 10));
        return date;
    }
    return null;
}

export function getNextNWeeks() {
    const weeks = [];
    const firstDate = new Date();
    const addition = 7 - firstDate.getDay();
    firstDate.setDate(firstDate.getDate() + addition);
    const lastDate = new Date();
    lastDate.setDate(firstDate.getDate() + 35);

    const end = new Date();
    end.setDate(firstDate.getDate() + 6);
    while (firstDate < lastDate) {
        weeks.push({value: weekNbr(firstDate), label: firstDate.toLocaleDateString('en-GB') + ' - ' + end.toLocaleDateString('en-GB')});
        firstDate.setDate(end.getDate() + 1);
        end.setDate(end.getDate() + 7);
    }
    return weeks;
}

export function weekNbr(date: Date) {
    const oneJan = new Date(date.getFullYear(), 0, 1);
    // @ts-ignore
    return Math.ceil((((date - oneJan) / 86400000) + oneJan.getDay()) / 7);
}

export const isoCountries = {
    'AF': 'Afghanistan',
    'AX': 'Aland Islands',
    'AL': 'Albania',
    'DZ': 'Algeria',
    'AS': 'American Samoa',
    'AD': 'Andorra',
    'AO': 'Angola',
    'AI': 'Anguilla',
    'AQ': 'Antarctica',
    'AG': 'Antigua And Barbuda',
    'AR': 'Argentina',
    'AM': 'Armenia',
    'AW': 'Aruba',
    'AU': 'Australia',
    'AT': 'Austria',
    'AZ': 'Azerbaijan',
    'BS': 'Bahamas',
    'BH': 'Bahrain',
    'BD': 'Bangladesh',
    'BB': 'Barbados',
    'BY': 'Belarus',
    'BE': 'Belgium',
    'BZ': 'Belize',
    'BJ': 'Benin',
    'BM': 'Bermuda',
    'BT': 'Bhutan',
    'BO': 'Bolivia',
    'BA': 'Bosnia And Herzegovina',
    'BW': 'Botswana',
    'BV': 'Bouvet Island',
    'BR': 'Brazil',
    'IO': 'British Indian Ocean Territory',
    'BN': 'Brunei Darussalam',
    'BG': 'Bulgaria',
    'BF': 'Burkina Faso',
    'BI': 'Burundi',
    'KH': 'Cambodia',
    'CM': 'Cameroon',
    'CA': 'Canada',
    'CV': 'Cape Verde',
    'KY': 'Cayman Islands',
    'CF': 'Central African Republic',
    'TD': 'Chad',
    'CL': 'Chile',
    'CN': 'China',
    'CX': 'Christmas Island',
    'CC': 'Cocos (Keeling) Islands',
    'CO': 'Colombia',
    'KM': 'Comoros',
    'CG': 'Congo',
    'CD': 'Congo, Democratic Republic',
    'CK': 'Cook Islands',
    'CR': 'Costa Rica',
    'CI': 'Cote D\'Ivoire',
    'HR': 'Croatia',
    'CU': 'Cuba',
    'CY': 'Cyprus',
    'CZ': 'Czech Republic',
    'DK': 'Denmark',
    'DJ': 'Djibouti',
    'DM': 'Dominica',
    'DO': 'Dominican Republic',
    'EC': 'Ecuador',
    'EG': 'Egypt',
    'SV': 'El Salvador',
    'GQ': 'Equatorial Guinea',
    'ER': 'Eritrea',
    'EE': 'Estonia',
    'ET': 'Ethiopia',
    'FK': 'Falkland Islands (Malvinas)',
    'FO': 'Faroe Islands',
    'FJ': 'Fiji',
    'FI': 'Finland',
    'FR': 'France',
    'GF': 'French Guiana',
    'PF': 'French Polynesia',
    'TF': 'French Southern Territories',
    'GA': 'Gabon',
    'GM': 'Gambia',
    'GE': 'Georgia',
    'DE': 'Germany',
    'GH': 'Ghana',
    'GI': 'Gibraltar',
    'GR': 'Greece',
    'GL': 'Greenland',
    'GD': 'Grenada',
    'GP': 'Guadeloupe',
    'GU': 'Guam',
    'GT': 'Guatemala',
    'GG': 'Guernsey',
    'GN': 'Guinea',
    'GW': 'Guinea-Bissau',
    'GY': 'Guyana',
    'HT': 'Haiti',
    'HM': 'Heard Island & Mcdonald Islands',
    'VA': 'Holy See (Vatican City State)',
    'HN': 'Honduras',
    'HK': 'Hong Kong',
    'HU': 'Hungary',
    'IS': 'Iceland',
    'IN': 'India',
    'ID': 'Indonesia',
    'IR': 'Iran, Islamic Republic Of',
    'IQ': 'Iraq',
    'IE': 'Ireland',
    'IM': 'Isle Of Man',
    'IL': 'Israel',
    'IT': 'Italy',
    'JM': 'Jamaica',
    'JP': 'Japan',
    'JE': 'Jersey',
    'JO': 'Jordan',
    'KZ': 'Kazakhstan',
    'KE': 'Kenya',
    'KI': 'Kiribati',
    'KR': 'Korea',
    'KW': 'Kuwait',
    'KG': 'Kyrgyzstan',
    'LA': 'Lao People\'s Democratic Republic',
    'LV': 'Latvia',
    'LB': 'Lebanon',
    'LS': 'Lesotho',
    'LR': 'Liberia',
    'LY': 'Libyan Arab Jamahiriya',
    'LI': 'Liechtenstein',
    'LT': 'Lithuania',
    'LU': 'Luxembourg',
    'MO': 'Macao',
    'MK': 'Macedonia',
    'MG': 'Madagascar',
    'MW': 'Malawi',
    'MY': 'Malaysia',
    'MV': 'Maldives',
    'ML': 'Mali',
    'MT': 'Malta',
    'MH': 'Marshall Islands',
    'MQ': 'Martinique',
    'MR': 'Mauritania',
    'MU': 'Mauritius',
    'YT': 'Mayotte',
    'MX': 'Mexico',
    'FM': 'Micronesia, Federated States Of',
    'MD': 'Moldova',
    'MC': 'Monaco',
    'MN': 'Mongolia',
    'ME': 'Montenegro',
    'MS': 'Montserrat',
    'MA': 'Morocco',
    'MZ': 'Mozambique',
    'MM': 'Myanmar',
    'NA': 'Namibia',
    'NR': 'Nauru',
    'NP': 'Nepal',
    'NL': 'Netherlands',
    'AN': 'Netherlands Antilles',
    'NC': 'New Caledonia',
    'NZ': 'New Zealand',
    'NI': 'Nicaragua',
    'NE': 'Niger',
    'NG': 'Nigeria',
    'NU': 'Niue',
    'NF': 'Norfolk Island',
    'MP': 'Northern Mariana Islands',
    'NO': 'Norway',
    'OM': 'Oman',
    'PK': 'Pakistan',
    'PW': 'Palau',
    'PS': 'Palestinian Territory, Occupied',
    'PA': 'Panama',
    'PG': 'Papua New Guinea',
    'PY': 'Paraguay',
    'PE': 'Peru',
    'PH': 'Philippines',
    'PN': 'Pitcairn',
    'PL': 'Poland',
    'PT': 'Portugal',
    'PR': 'Puerto Rico',
    'QA': 'Qatar',
    'RE': 'Reunion',
    'RO': 'Romania',
    'RU': 'Russian Federation',
    'RW': 'Rwanda',
    'BL': 'Saint Barthelemy',
    'SH': 'Saint Helena',
    'KN': 'Saint Kitts And Nevis',
    'LC': 'Saint Lucia',
    'MF': 'Saint Martin',
    'PM': 'Saint Pierre And Miquelon',
    'VC': 'Saint Vincent And Grenadines',
    'WS': 'Samoa',
    'SM': 'San Marino',
    'ST': 'Sao Tome And Principe',
    'SA': 'Saudi Arabia',
    'SN': 'Senegal',
    'RS': 'Serbia',
    'SC': 'Seychelles',
    'SL': 'Sierra Leone',
    'SG': 'Singapore',
    'SK': 'Slovakia',
    'SI': 'Slovenia',
    'SB': 'Solomon Islands',
    'SO': 'Somalia',
    'ZA': 'South Africa',
    'GS': 'South Georgia And Sandwich Isl.',
    'ES': 'Spain',
    'LK': 'Sri Lanka',
    'SD': 'Sudan',
    'SR': 'Suriname',
    'SJ': 'Svalbard And Jan Mayen',
    'SZ': 'Swaziland',
    'SE': 'Sweden',
    'CH': 'Switzerland',
    'SY': 'Syrian Arab Republic',
    'TW': 'Taiwan',
    'TJ': 'Tajikistan',
    'TZ': 'Tanzania',
    'TH': 'Thailand',
    'TL': 'Timor-Leste',
    'TG': 'Togo',
    'TK': 'Tokelau',
    'TO': 'Tonga',
    'TT': 'Trinidad And Tobago',
    'TN': 'Tunisia',
    'TR': 'Turkey',
    'TM': 'Turkmenistan',
    'TC': 'Turks And Caicos Islands',
    'TV': 'Tuvalu',
    'UG': 'Uganda',
    'UA': 'Ukraine',
    'AE': 'United Arab Emirates',
    'GB': 'United Kingdom',
    'US': 'United States',
    'UM': 'United States Outlying Islands',
    'UY': 'Uruguay',
    'UZ': 'Uzbekistan',
    'VU': 'Vanuatu',
    'VE': 'Venezuela',
    'VN': 'Viet Nam',
    'VG': 'Virgin Islands, British',
    'VI': 'Virgin Islands, U.S.',
    'WF': 'Wallis And Futuna',
    'EH': 'Western Sahara',
    'YE': 'Yemen',
    'ZM': 'Zambia',
    'ZW': 'Zimbabwe',
};

export const tnCityState = [
    {
        name: 'Tunis',
        cities: ['Tunis', 'Le Bardo', 'Le Kram', 'La Goulette', 'Carthage', 'Sidi Bou Said', 'La Marsa', 'Sidi Hassine'],
    },
    {
        name: 'Ariana',
        cities: ['Ariana', 'La Soukra', 'Raoued', 'Kalâat el-Andalous', 'Sidi Thabet', 'Ettadhamen-Mnihla',
        ],
    },
    {
        name: 'Ben Arous',
        cities: ['Ben Arous', 'El Mourouj', 'Hammam Lif', 'Hammam Chott', '	Bou Mhel el-Bassatine', 'Ezzahra', 'Radès', 'Mégrine', 'Mohamedia-Fouchana', 'Mornag', 'Khalidia'],
    },
    {
        name: 'Manouba',
        cities: ['Manouba', 'Den Den', 'Douar Hicher', 'Oued Ellil', 'Mornaguia', 'Borj El Amri', 'Djedeida', 'Tebourba', 'El Battan'],
    },
    {
        name: 'Nabeul',
        cities: ['Nabeul', 'Dar Chaabane', 'Béni Khiar', 'El Maâmoura', 'Somâa', 'Korba', 'Tazerka', 'Menzel Temime', 'Menzel Horr', 'El Mida', 'Kelibia', 'Azmour',
            'Hammam Ghezèze', 'Dar Allouch', 'El Haouaria', 'Takelsa', 'Soliman', 'Korbous', 'Menzel Bouzelfa', 'Béni Khalled', 'Zaouiet Djedidi', 'Grombalia', 'Bou Argoub',
            'Hammamet'],
    },
    {
        name: 'Zaghouan',
        cities: ['Zaghouan', 'Zriba', 'Bir Mcherga', 'Djebel Oust', 'El Fahs', 'Nadhour'],
    },
    {
        name: 'Bizerte',
        cities: ['Bizerte', 'Sejnane', 'Mateur', 'Menzel Bourguiba', 'Tinja', 'Ghar al Milh', 'Aousja', 'Menzel Jemil', 'Menzel Abderrahmane', 'El Alia', 'Ras Jebel', 'Metline',
            'Raf Raf'],
    },
    {
        name: 'Béja',
        cities: ['El Maâgoula', 'Zahret Medien', 'Nefza', 'Téboursouk', 'Testour', 'Goubellat', 'Majaz al Bab'],
    },
    {
        name: 'Jendouba',
        cities: ['Jendouba', 'Bou Salem', 'Tabarka', 'Aïn Draham', 'Fernana', 'Beni M\'Tir', 'Ghardimaou', 'Oued Melliz'],
    },
    {
        name: 'El Kef',
        cities: ['El Kef', 'Nebeur', 'Touiref', 'Sakiet Sidi Youssef', 'Tajerouine', 'Menzel Salem', 'Kalaat es Senam', 'Kalâat Khasba', 'Jérissa', 'El Ksour', 'Dahmani', 'Sers'],
    },
    {
        name: 'Siliana',
        cities: ['Siliana', 'Bou Arada', 'Gaâfour', 'El Krib', 'Sidi Bou Rouis', 'Maktar', 'Rouhia', 'Kesra', 'Bargou', 'El Aroussa'],
    },
    {
        name: 'Sousse',
        cities: ['Sousse', 'Ksibet Thrayet', 'Ezzouhour', 'Zaouiet Sousse', 'Hammam Sousse', 'Akouda', 'Kalâa Kebira', 'Sidi Bou Ali', 'Hergla', 'Enfidha', 'Bouficha',
            'Sidi El Hani', 'M\'saken', 'Kalâa Seghira', 'Messaadine', 'Kondar',
        ],
    },
    {
        name: 'Monastir',
        cities: ['Monastir', 'Khniss', 'Ouerdanin', 'Sahline Moôtmar', 'Sidi Ameur', 'Zéramdine', 'Beni Hassen', 'Ghenada', 'Jemmal', 'Menzel Kamel', 'Zaouiet Kontoch',
            'Bembla-Mnara', 'Menzel Ennour', 'El Masdour', 'Moknine', 'Sidi Bennour', 'Menzel Farsi', 'Amiret El Fhoul', 'Amiret Touazra', 'Amiret El Hojjaj', 'Cherahil',
            'Bekalta', 'Téboulba', 'Ksar Hellal', 'Ksibet El Mediouni', 'Benen Bodher', 'Touza', 'Sayada', 'Lemta', 'Bouhjar', 'Menzel Hayet'],
    },
    {
        name: 'Mahdia',
        cities: ['Mahdia', 'Rejiche', 'Bou Merdes', 'Ouled Chamekh', 'Chorbane', 'Hebira', 'Essouassi', 'El Djem', 'Kerker', 'Chebba', 'Melloulèche', 'Sidi Alouane', 'Ksour Essef',
            'El Bradâa'],
    },
    {
        name: 'Sfax',
        cities: ['Sfax', 'Sakiet Ezzit', 'Chihia', 'Sakiet Eddaïer', 'Gremda', 'El Ain', 'Thyna', 'Agareb', 'Jebiniana', 'El Hencha', 'Menzel Chaker', 'Ghraïba, Tunisia',
            'Bir Ali Ben Khélifa', 'Skhira', 'Mahares', 'Kerkennah'],
    },
    {
        name: 'Kairouan',
        cities: ['Kairouan', 'Chebika', 'Sbikha', 'Oueslatia', 'Aïn Djeloula', 'Haffouz', 'Alaâ', 'Hajeb El Ayoun', 'Nasrallah', 'Menzel Mehiri', 'Echrarda', 'Bou Hajla'],
    },
    {
        name: 'Kasserine',
        cities: ['Kasserine', 'Sbeitla', 'Sbiba', 'Jedelienne', 'Thala', 'Haïdra', 'Foussana', 'Fériana', 'Thélepte', 'Magel Bel Abbès'],
    },
    {
        name: 'Sidi Bouzid',
        cities: ['Sidi Bouzid', 'Jilma', 'Cebalet', 'Bir El Hafey', 'Sidi Ali Ben Aoun', 'Menzel Bouzaiane', 'Meknassy', 'Mezzouna', 'Regueb', 'Ouled Haffouz'],
    },
    {
        name: 'Gabès',
        cities: ['Gabès', 'Chenini Nahal', 'Ghannouch', 'Métouia', 'Oudhref', 'El Hamma', 'Matmata', 'Nouvelle Matmata', 'Mareth', 'Zarat'],
    },
    {
        name: 'Medenine',
        cities: ['Medenine', 'Beni Khedache', 'Ben Gardane', 'Zarzis', '	Houmt El Souk', 'Midoun', 'Ajim'],
    },
    {
        name: 'Tataouine',
        cities: ['Tataouine', 'Bir Lahmar', 'Ghomrassen', 'Dehiba', 'Remada'],
    },
    {
        name: 'Gafsa',
        cities: ['Gafsa', 'El Ksar', 'Moularès', 'Redeyef', 'Métlaoui', 'Mdhila', 'El Guettar', 'Sened'],
    },
    {
        name: 'Tozeur',
        cities: ['Tozeur', 'Degache', 'Hamet Jerid', 'Nafta', 'Tamerza'],
    },
    {
        name: 'Kebili',
        cities: ['Kebili', 'Djemna', 'Douz', 'El Golâa', 'Souk Lahad'],
    },
];

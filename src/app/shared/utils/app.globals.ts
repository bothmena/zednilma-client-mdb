/**
 * Created by bothmena on 22/01/17.
 */

export const AppGlobals = Object.freeze({
    BASE_RESOURCE_URL: 'http://localhost:8000',
    BASE_API_URL: 'http://localhost:8000/',
    API_VERSION: 'v1',
    AUTHENTICATE_URL: 'login_check',
    REFRESH_TOKEN_URL: 'token/refresh',
});

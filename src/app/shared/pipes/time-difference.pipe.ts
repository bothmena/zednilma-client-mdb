import {Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Pipe({
    name: 'timeDifference'
})
export class TimeDifferencePipe implements PipeTransform {


    public constructor( private translate: TranslateService) {}

    transform(value: Date, args?: any): any {

        const result = this.calculateDifference(value);
        if (args.hasOwnProperty('syntax')) {
            if (args['syntax'] === 'due_date')
                return this.dueDate(result);
        }
        return null;

        /**
         * s = 1000
         * m = 60000
         * h = 3600000
         * d = 86400000
         */
    }

    private calculateDifference(date: Date) {

        // @ts-ignore
        let difference = date - new Date();
        const result = {'newerDate': difference > 0};
        difference = Math.abs(difference);
        const diffDays = Math.floor(difference / 86400000);
        if (diffDays > 0) {

            if (diffDays < 28) {

                result['unit'] = 'days';
                result['diff'] = diffDays;
                return result;
            } else if (diffDays < 32) {

                result['unit'] = 'months';
                result['diff'] = 1;
                return result;
            } else {

                result['unit'] = 'month_days';
                result['diff_m'] = Math.floor(diffDays / 30);
                result['diff_d'] = diffDays % 30;
                return result;
            }
        } else {
            const diffHours = Math.floor(difference / 3600000);
            if (diffHours > 0) {

                result['unit'] = 'hours';
                result['diff'] = diffHours;
                return result;
            } else {

                const diffMinutes = Math.floor(difference / 60000);
                if (diffMinutes > 0) {

                    result['unit'] = 'minutes';
                    result['diff'] = diffMinutes;
                    return result;
                } else {

                    const diffSeconds = Math.floor(difference / 1000);
                    result['unit'] = 'seconds';
                    result['diff'] = diffSeconds;
                    return result;
                }
            }
        }
    }

    private dueDate(result) {

        if (!result['newerDate'])
            return this.translate.get('time_diff.due_date.passed');
        if (result['unit'] === 'hours') {

            if (result['diff'] > 1)
                return this.translate.get('time_diff.due_date.hours', {'diff': result['diff']});
            else
                return this.translate.get('time_diff.due_date.hour');
        } else if (result['unit'] === 'days') {

            if (result['diff'] > 1)
                return this.translate.get('time_diff.due_date.days', {'diff': result['diff']});
            else
                return this.translate.get('time_diff.due_date.day');
        } else if (result['unit'] === 'minutes') {

            if (result['diff'] > 1)
                return this.translate.get('time_diff.due_date.minutes', {'diff': result['diff']});
            else
                return this.translate.get('time_diff.due_date.minute');
        } else if (result['unit'] === 'seconds')
            return this.translate.get('time_diff.due_date.seconds');
        else if (result['unit'] === 'month_days') {

            if (result['diff_d'] > 1) {

                if (result['diff_m'] > 1)
                    return this.translate.get('time_diff.due_date.months_days', {'days': result['diff_d'], 'months': result['diff_m']});
                else
                    return this.translate.get('time_diff.due_date.month_days', {'days': result['diff_d']});
            } else {

                if (result['diff_m'] > 1)
                    return this.translate.get('time_diff.due_date.months_day', {'months': result['diff_m']});
                else
                    return this.translate.get('time_diff.due_date.month_day');
            }
        }
    }
}

import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AppGlobals } from '../utils/app.globals';

@Pipe( {
    name: 'trustResource',
} )
export class TrustResourcePipe implements PipeTransform {

    constructor( private sanitizer: DomSanitizer ) {
    }

    transform( value: any ): any {

        value = AppGlobals.BASE_RESOURCE_URL + value;
        return this.sanitizer.bypassSecurityTrustResourceUrl( value );
    }
}

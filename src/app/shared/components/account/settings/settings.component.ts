import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component( {
    templateUrl: './settings.component.html',
    styleUrls: [ './settings.component.min.css' ],
} )
export class SettingsComponent implements OnInit {

    path = '';

    constructor( private route: ActivatedRoute ) {
    }

    ngOnInit(): void {

        this.route.url.subscribe(
            () => this.path = this.route.snapshot.firstChild.routeConfig.path,
        );
    }
}

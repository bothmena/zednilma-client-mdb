import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Email} from '../../../models/email';
import {Phone} from '../../../models/phone';
import {EmailService} from '../../../services/models/email.service';
import {PhoneService} from '../../../services/models/phone.service';
import {UserService} from '../../../services/models/user.service';

declare var $: any;

@Component({
    templateUrl: './contact-edit.component.html',
    styleUrls: ['./contact-edit.component.min.css'],
})
export class ContactEditComponent implements OnInit {

    unsavedEmail = false;
    unsavedPhone = false;
    emails: Email[] = [];
    phones: Phone[] = [];
    @ViewChild('collapsible2') collapsible2: ElementRef;

    constructor(protected userService: UserService,
                private emailService: EmailService,
                private phoneService: PhoneService,) {
    }

    ngOnInit() {

        this.emailService.getUserEmails().toPromise()
            .then((emails: Email[]) => this.emails = emails)
            .catch((error) => console.log(error));

        this.phoneService.getUserPhones().toPromise()
            .then((phones: Phone[]) => this.phones = phones)
            .catch((error) => console.log(error));

    }

    onPopOut2(index: number) {

        $(this.collapsible2.nativeElement).collapsible('open', index);
    }

    addEmail() {

        this.emails.push(new Email);
        this.unsavedEmail = true;
        setTimeout(() => {
            // this.popOut( this.emails.length - 1 );
        }, 25);
    }

    addPhone() {

        this.phones.push(new Phone());
        this.unsavedPhone = true;
        setTimeout(() => {
            this.onPopOut2(this.phones.length - 1);
        }, 25);
    }

    emailDeteled(index: number) {

        if (index === this.emails.length - 1) {

            this.unsavedEmail = false;
        }
        this.emails.splice(index, 1);
    }

    phoneDeteled(index: number) {

        if (index === this.phones.length - 1) {

            this.unsavedPhone = false;
        }
        this.phones.splice(index, 1);
    }
}

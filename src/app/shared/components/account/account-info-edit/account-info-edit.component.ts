import {NgRedux} from '@angular-redux/store';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../models/user';
import {AppActions} from '../../../redux/app/app.actions';
import {State} from '../../../redux/models/state';
import {UserService} from '../../../services/models/user.service';
import {ABOValidators} from '../../../utils/abo.validators';

@Component({
    templateUrl: './account-info-edit.component.html',
    styleUrls: ['./account-info-edit.component.min.css'],
})
export class AccountInfoEditComponent implements OnInit {

    settingForm: FormGroup;
    isSubmitting = false;
    lessDate: Date = new Date(new Date().setFullYear(new Date().getUTCFullYear() - 3));
    greaterDate: Date = new Date(new Date().setFullYear(new Date().getUTCFullYear() - 100));
    formInit = false;
    protected user: User;

    constructor(private fb: FormBuilder,
                protected userService: UserService,
                // private translator: TranslateService,
                private ngRedux: NgRedux<State>) {

    }

    ngOnInit() {

        /*this.subscription = this.ngRedux.select().subscribe(
            (state: State) => {
                this.professor = User.build(state.firebase.data);
                if (this.settingForm && !this.formInit) {

                    this.resetForm();
                    this.formInit = true;
                }
            },
        );*/
        this.buildForm();
    }

    buildForm(): void {

        this.settingForm = this.fb.group({
            'firstName': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(31),
            ])],
            'middleName': ['', Validators.compose([
                Validators.minLength(3),
                Validators.maxLength(31),
            ])],
            'lastName': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(31),
            ])],
            'gender': ['', Validators.compose([
                Validators.required,
                ABOValidators.inArray(['male', 'female']),
            ])],
            'birthday': ['', Validators.compose([
                Validators.required,
                ABOValidators.dateLessThan(this.lessDate),
                ABOValidators.dateGreaterThan(this.greaterDate),
            ])],
            'cin': ['', Validators.compose([
                Validators.required,
                Validators.pattern(/^\d+$/),
                Validators.minLength(8),
                Validators.maxLength(8),
            ])],
        });
    }

    save(name: string, index: number) {

        if (this.settingForm.get(name).valid) {

            this.isSubmitting = true;
            this.ngRedux.dispatch(AppActions.incrementLoading());
            const data = {};
            data[name] = this.settingForm.get(name).value;
            console.log(index);
            /*this.userService.patchUser(this.professor.id, data).subscribe(
                () => this.onSubmitSuccess(index),
                (error) => this.onSubmitError(error),
            );*/
        }
    }

    submit() {

        this.isSubmitting = true;
        this.ngRedux.dispatch(AppActions.incrementLoading());
        /*this.userService.patchUser(this.professor.id, this.settingForm.value).subscribe(
            () => this.onSubmitSuccess(),
            (error) => this.onSubmitError(error),
        );*/
    }

    resetForm() {

        this.settingForm.get('firstName').setValue(this.user.firstName);
        this.settingForm.get('middleName').setValue(this.user.middleName);
        this.settingForm.get('lastName').setValue(this.user.lastName);
        this.settingForm.get('gender').setValue(this.user.gender);
        this.settingForm.get('birthday').setValue(this.user.birthday);
        this.settingForm.get('cin').setValue(this.user.cin);
        this.settingForm.markAsPristine();
    }

    onSubmitSuccess(index: number = null) {

        // this.userService.updateUserFireData();
        this.isSubmitting = false;
        this.ngRedux.dispatch(AppActions.decrementLoading());
        // this.translator.get('app.account.prf_updated').toPromise()
        //     .then(message => toast(message, 5000));
        if (index !== null) {
            // this.popOut(index, 'close');
        } else {

            let i = 0;
            while (i < 6) {
                // this.popOut(i++, 'close');
            }
            this.resetForm();
        }
    }

    /*onSubmitError() {

        this.isSubmitting = false;
        this.ngRedux.dispatch(AppActions.decrementLoading());
        this.translator.get('app.submit_error').toPromise()
            .then(message => toast(message, 5000));
    }*/
}

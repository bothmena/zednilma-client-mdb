import {NgRedux} from '@angular-redux/store';
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Phone} from '../../../models/phone';
import {AppActions} from '../../../redux/app/app.actions';
import {State} from '../../../redux/models/state';
import {PhoneService} from '../../../services/models/phone.service';

@Component({
    selector: 'zdlm-phone-edit,[zdlm-phone-edit]',
    templateUrl: './phone-edit.component.html',
    styleUrls: ['./phone-edit.component.min.css'],
})
export class PhoneEditComponent implements OnInit {

    @Input() phone: Phone;
    @Output() popOut = new EventEmitter<any>();
    @Output() onSaved = new EventEmitter<any>();
    @Output() onDelete = new EventEmitter<any>();
    editMode = true;
    isNew = false;
    code = '';
    originalNumber: string;

    constructor(private phoneService: PhoneService,
                // private translator: TranslateService,
                private ngRedux: NgRedux<State>,) {
    }

    ngOnInit() {

        this.originalNumber = this.phone.number.replace(/\s/g, '');
        this.isNew = this.originalNumber === '';
    }

    headerClick(event) {

        event.stopPropagation();
    }

    verify() {

        this.editMode = false;
        this.popOut.emit();
    }

    edit() {

        this.editMode = true;
        this.popOut.emit();
    }

    save() {

        const data = {
            type: this.phone.type,
            ccode: this.phone.ccode,
            number: this.phone.number,
            role: this.phone.role,
        };
        this.ngRedux.dispatch(AppActions.incrementLoading());
        let promise: Promise<any>;
        if (this.isNew) {

            if (this.phone.instituteId > 0) {

                promise = this.phoneService.newInstitutePhone(this.phone.instituteId, data).toPromise();
            } else {

                promise = this.phoneService.newUserPhone(data).toPromise();
            }
            promise.then((res: Response) => this.onNewPhoneSuccess(res))
                .catch((error) => this.onSubmitError(error));
        } else {

            if (this.phone.instituteId > 0) {

                promise = this.phoneService.patchInstitutePhone(this.phone.instituteId, this.phone.id, data).toPromise();
            } else {

                promise = this.phoneService.patchUserPhone(this.phone.id, data).toPromise();
            }
            promise.then(() => this.onSubmitSuccess())
                .catch((error) => this.onSubmitError(error));
        }
    }

    deletePhone() {

        if (this.isNew) {

            this.onDelete.emit();
            this.onSaved.emit();
        } else {

            this.ngRedux.dispatch(AppActions.incrementLoading());
            let promise: Promise<any>;
            if (this.phone.instituteId > 0) {

                promise = this.phoneService.deleteInstitutePhone(this.phone.instituteId, this.phone.id).toPromise();
            } else {

                promise = this.phoneService.deleteUserPhone(this.phone.id).toPromise();
            }
            promise.then(() => {

                this.ngRedux.dispatch(AppActions.decrementLoading());
                this.onDelete.emit();
            })
                .catch((error) => this.onSubmitError(error));
        }
    }

    confirm() {

        this.ngRedux.dispatch(AppActions.incrementLoading());
        let promise: Promise<any>;
        if (this.phone.instituteId > 0) {

            promise = this.phoneService.confirmInstitutePhone(this.phone.instituteId, this.phone.id, this.code).toPromise();
        } else {

            promise = this.phoneService.confirmUserPhone(this.phone.id, this.code).toPromise();
        }
        promise.then(() => {

            this.ngRedux.dispatch(AppActions.decrementLoading());
            this.phone.isVerified = true;
            this.popOut.emit();
            this.code = '';
        })
            .catch((error) => this.onSubmitError(error));
    }

    onSubmitSuccess() {

        this.ngRedux.dispatch(AppActions.decrementLoading());
        if (this.isNew) {

            // this.translator.get( 'phone.new_save_success' ).toPromise()
            //     .then( message => toast( message, 5000 ) );
        } else if (this.phone.number !== this.originalNumber) {

            this.phone.isVerified = false;
            // this.translator.get( 'phone.edit_save_success' ).toPromise()
            //     .then( message => toast( message, 5000 ) );
        }
        this.popOut.emit();
    }

    onSubmitError(error: any) {

        console.log('error', error);
        this.ngRedux.dispatch(AppActions.decrementLoading());
    }

    private onNewPhoneSuccess(res: Response) {

        this.onSaved.emit();
        this.onSubmitSuccess();
        if (res.headers.has('Location')) {
            const arr = res.headers.get('Location').split('/');
            this.phone.id = +arr[arr.length - 1];
            this.isNew = false;
        }
    }
}

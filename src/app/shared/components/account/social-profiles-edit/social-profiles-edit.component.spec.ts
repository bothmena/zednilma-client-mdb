import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialProfilesEditComponent } from './social-profiles-edit.component';

describe('SocialProfilesEditComponent', () => {
  let component: SocialProfilesEditComponent;
  let fixture: ComponentFixture<SocialProfilesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialProfilesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialProfilesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

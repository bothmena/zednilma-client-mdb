import {NgRedux} from '@angular-redux/store';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {SocialProfile} from '../../../models/social-profile';
import {AppActions} from '../../../redux/app/app.actions';
import {State} from '../../../redux/models/state';
import {SocialProfileService} from '../../../services/models/social-profile.service';
import {UserService} from '../../../services/models/user.service';
import {FormGroup} from '@angular/forms';

@Component({
    templateUrl: './social-profiles-edit.component.html',
    styleUrls: ['./social-profiles-edit.component.min.css'],
})
export class SocialProfilesEditComponent implements OnInit, OnDestroy { //

    settingForm: FormGroup;
    isSubmitting = false;
    unsaved = false;
    socialProfiles: SocialProfile[] = [];

    constructor(protected userService: UserService,
                protected ngRedux: NgRedux<State>,
                private spService: SocialProfileService) {

    }

    ngOnInit() {

        this.ngRedux.dispatch(AppActions.spWebsiteInit());
        this.spService.getUserSPs().toPromise()
            .then((sps: SocialProfile[]) => this.socialProfiles = sps)
        // .catch( ( error ) => this.translator.get( 'app.account.load_sps_err' ).toPromise()
        //     .then( msg => toast( msg, 5000 ) ) );

    }

    ngOnDestroy(): void {

        this.ngRedux.dispatch(AppActions.spWebsiteDestroy());
    }

    addSP() {

        this.socialProfiles.push(new SocialProfile());
        this.unsaved = true;
        setTimeout(() => {
            // this.popOut(this.socialProfiles.length - 1);
        }, 25);
    }

    spDeteled(index: number) {

        if (index === this.socialProfiles.length - 1) {
            this.unsaved = false;
        }
        this.socialProfiles.splice(index, 1);
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialProfileEditComponent } from './social-profile-edit.component';

describe('SocialProfileEditComponent', () => {
  let component: SocialProfileEditComponent;
  let fixture: ComponentFixture<SocialProfileEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialProfileEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialProfileEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

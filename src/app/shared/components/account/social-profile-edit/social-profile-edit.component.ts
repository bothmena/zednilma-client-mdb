import { NgRedux } from '@angular-redux/store';
import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output,
    ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { SocialProfile } from '../../../models/social-profile';
import { AppActions } from '../../../redux/app/app.actions';
import { State } from '../../../redux/models/state';
import { SocialProfileService } from '../../../services/models/social-profile.service';
import { getWebsiteProfileUrl } from '../../../utils/util-functions';
import { ABOValidators } from '../../../utils/abo.validators';
declare var $: any;

@Component( {
    selector: 'zdlm-sprf-edit,[zdlm-sprf-edit]',
    templateUrl: './social-profile-edit.component.html',
    styleUrls: [ './social-profile-edit.component.min.css' ],
} )
export class SocialProfileEditComponent implements OnInit, AfterViewInit, OnDestroy {

    @Input() profile: SocialProfile;
    @Output() popOut = new EventEmitter<any>();
    @Output() onSaved = new EventEmitter<any>();
    @Output() onDelete = new EventEmitter<any>();
    @ViewChild( 'webSelect' ) webSelect: ElementRef;
    spForm: FormGroup;
    formErrors: any = {};
    isNew = false;
    subscription: Subscription;
    websites: string[];
    prefix: string;
    oldWebsite: string;

    constructor( private fb: FormBuilder,
        private spService: SocialProfileService,
        private ngRedux: NgRedux<State> ) {
    }

    ngOnInit(): void {

        this.isNew = ( this.profile.username === '' );
        this.buildForm();
        this.subscription = this.ngRedux.select([ 'app', 'spWebsites' ]).subscribe(
            ( spWebsites: any ) => {
                this.websites = spWebsites;
                this.websites.sort();
                this.spForm.get( 'website' ).setValue( this.profile.website );
                setTimeout( () => {
                    $( this.webSelect.nativeElement ).material_select();
                }, 100 );
            },
        );
    }

    ngAfterViewInit(): void {

        if ( !this.isNew ) {

            this.ngRedux.dispatch( AppActions.spWebsiteRemove( this.profile.website ) );
            this.prefix = getWebsiteProfileUrl( this.profile.website );
        }
    }

    ngOnDestroy(): void {

        this.ngRedux.dispatch( AppActions.spWebsiteAdd( this.profile.website ) );
        this.subscription.unsubscribe();
    }

    buildForm() {

        this.spForm = this.fb.group( {
            'website': [ this.profile.website, Validators.compose( [
                Validators.required,
                ABOValidators.inArray( [ 'linkedin', 'github', 'gitlab', 'bitbucket', 'facebook', 'twitter',
                    'youtube', 'google+', 'instagram', 'vimeo', 'skype', 'whatsapp' ] ),
            ] ) ],
            'username': [ this.profile.username, Validators.compose( [
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(71),
                Validators.pattern( /^[\w.+-]+$/ ),
            ] ) ],
            'prefix': [ { value: getWebsiteProfileUrl( this.profile.website ), disabled: true } ],
        } );
    }

    websiteChanged() {
        this.spForm.get( 'prefix' ).setValue( getWebsiteProfileUrl( this.spForm.get( 'website' ).value ) );
    }

    headerClick( event ) {

        event.stopPropagation();
    }

    edit() {

        this.popOut.emit();
    }

    save() {

        this.oldWebsite = this.profile.website;
        this.profile.website = this.spForm.get( 'website' ).value;
        this.profile.username = this.spForm.get( 'username' ).value;
        this.profile.url = this.getUrl();
        const data = {
            url: this.profile.url,
            username: this.profile.username,
            website: this.profile.website,
        };
        this.ngRedux.dispatch( AppActions.incrementLoading() );
        let promise: Promise<any>;
        if ( this.isNew ) {

            if ( this.profile.instituteId > 0 ) {

                promise = this.spService.newInstituteSP( this.profile.instituteId, data ).toPromise();
            } else {

                promise = this.spService.newUserSP( data ).toPromise();
            }
            promise.then( ( res: Response ) => {
                    this.onSubmitSuccess();
                    if ( res.headers.has( 'Location' ) ) {
                        const arr = res.headers.get( 'Location' ).split( '/' );
                        this.profile.id = +arr[ arr.length - 1 ];
                    }
                } )
                .catch( ( error ) => this.onSubmitError( error ) );
        } else {

            if ( this.profile.instituteId > 0 ) {

                promise = this.spService.patchInstituteSP( this.profile.instituteId, this.profile.id, data ).toPromise();
            } else {

                promise = this.spService.patchUserSP( this.profile.id, data ).toPromise();
            }
            promise.then( () => {
                    this.onSubmitSuccess();
                } )
                .catch( ( error ) => this.onSubmitError( error ) );
        }
    }

    deleteProfile() {

        this.ngRedux.dispatch( AppActions.incrementLoading() );
        let promise: Promise<any>;
        if ( this.profile.instituteId > 0 ) {

            promise = this.spService.deleteInstituteSP( this.profile.instituteId, this.profile.id ).toPromise();
        } else {

            promise = this.spService.deleteUserSP( this.profile.id ).toPromise();
        }
        promise.then( () => {
                this.ngRedux.dispatch( AppActions.decrementLoading() );
                this.onDelete.emit();
            } )
            .catch( ( error ) => this.onSubmitError( error ) );
    }

    onSubmitSuccess() {

        this.ngRedux.dispatch( AppActions.spWebsiteRemove( this.profile.website ) );
        if ( this.oldWebsite !== '' ) {
            this.ngRedux.dispatch( AppActions.spWebsiteAdd( this.oldWebsite ) );
        }
        this.popOut.emit();
        if ( this.isNew ) {

            this.onSaved.emit();
            this.isNew = false;
        }
        this.ngRedux.dispatch( AppActions.decrementLoading() );
    }

    onSubmitError( error: any ) {

        console.log( 'error', error );
        this.ngRedux.dispatch( AppActions.decrementLoading() );
    }

    private getUrl() {

        const website = this.spForm.get( 'website' ).value;
        const username = this.spForm.get( 'username' ).value;

        switch ( website ) {
            case 'google+':
                return 'https://' + getWebsiteProfileUrl( website ) + username;
            case 'skype':
                return 'https://web.skype.com/' + username;
            case 'whatsapp':
                return 'https://web.whatsapp.com/' + username;
            default:
                return 'https://www.' + getWebsiteProfileUrl( website ) + username;
        }
    }
}

import { select } from '@angular-redux/store';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component( {
    templateUrl: './profile.component.html',
    styleUrls  : [ './profile.component.min.css' ],
} )
export class ProfileComponent implements OnInit {

    @select( [ 'firebase', 'data' ] ) readonly fireData$: Observable<any>;
    path = '';

    constructor( private route: ActivatedRoute ) {
    }

    ngOnInit(): void {

        this.path = this.route.snapshot.firstChild.routeConfig.path;
    }
}

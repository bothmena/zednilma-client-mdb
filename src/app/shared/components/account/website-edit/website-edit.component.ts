import {NgRedux} from '@angular-redux/store';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {State} from '../../../redux/models/state';
import {UserService} from '../../../services/models/user.service';
import {ABOValidators} from '../../../utils/abo.validators';
import {User} from '../../../models/user';
import {AppActions} from '../../../redux/app/app.actions';

@Component({
    templateUrl: './website-edit.component.html',
    styleUrls: ['./website-edit.component.scss'],
})
export class WebsiteEditComponent implements OnInit {

    settingForm: FormGroup;
    isSubmitting = false;
    languages: string[] = ['en', 'ar', 'fr'];
    user: User;

    constructor(private fb: FormBuilder,
                protected userService: UserService,
                protected ngRedux: NgRedux<State>) {

    }

    ngOnInit() {

        /*this.subscription = this.ngRedux.select().subscribe(
            (state: State) => {
                this.professor = User.build(state.firebase.data);
            },
        );*/
        this.buildForm();
    }

    buildForm(): void {

        this.settingForm = this.fb.group({
            'language': ['', Validators.compose([
                Validators.required,
                ABOValidators.inArray(this.languages),
            ])],
        });
    }

    submit() {

        this.isSubmitting = true;
        this.ngRedux.dispatch(AppActions.incrementLoading());
        /*this.userService.patchUser( this.professor.id, this.settingForm.value ).subscribe(
            () => this.onSubmitSuccess(),
            ( error ) => this.onSubmitError( error ),
        );*/
    }

    /*onSubmitSuccess() {

        // this.userService.updateUserFireData();
        this.isSubmitting = false;
        this.ngRedux.dispatch( AppActions.decrementLoading() );
        this.translator.get('app.account.web_set_updated').toPromise()
            .then( message => toast( message, 5000 ) );
    }

    onSubmitError( error ) {

        this.isSubmitting = false;
        this.ngRedux.dispatch( AppActions.decrementLoading() );
        this.translator.get('app.submit_error').toPromise()
            .then( message => toast( message, 5000 ) );
    }*/
}

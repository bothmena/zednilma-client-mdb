import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CredentialsEditComponent } from './credentials-edit.component';

describe('CredentialsEditComponent', () => {
  let component: CredentialsEditComponent;
  let fixture: ComponentFixture<CredentialsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CredentialsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CredentialsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

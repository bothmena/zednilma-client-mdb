import {NgRedux} from '@angular-redux/store';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {Observable} from 'rxjs';
import * as zxcvbn from 'zxcvbn';
import {Email} from '../../../models/email';
import {User} from '../../../models/user';
import {AppActions} from '../../../redux/app/app.actions';
import {State} from '../../../redux/models/state';
import {EmailService} from '../../../services/models/email.service';
import {UserService} from '../../../services/models/user.service';

@Component({
    templateUrl: './credentials-edit.component.html',
    styleUrls: ['./credentials-edit.component.min.css'],
})
export class CredentialsEditComponent implements OnInit {

    settingForm: FormGroup;
    isSubmitting = false;
    emailForm: FormGroup;
    passForm: FormGroup;
    formInit = false;
    passClass = 'determinate';
    emails: Email[] = [];
    protected user: User;

    constructor(private fb: FormBuilder,
                protected userService: UserService,
                protected emailService: EmailService,
                protected ngRedux: NgRedux<State>) {
    }

    ngOnInit() {

        this.buildForms();
        /*this.subscription = this.ngRedux.select().subscribe(
            (state: State) => {
                this.professor = User.build(state.firebase.data);
                if (this.professor.username != null && !this.formInit) {
                    this.settingForm.get('username').setValue(this.professor.username);
                    this.formInit = true;
                }
            },
        );*/
        this.emailService.getUserEmails(null, {verified: true}).toPromise()
            .then(emails => {
                this.emails = emails;
                const func = () => {
                    if (this.user.username == null) {
                        setTimeout(() => func(), 200);
                    } else {
                        for (const email of this.emails) {
                            if (email.email === this.user.email) {
                                this.emailForm.get('email').setValue(email.id);
                            }
                        }
                    }
                };
                func();
            })
        // .catch( err => this.translator.get( 'app.account.sss' ).toPromise()
        //     .then( msg => toast( msg, 5000 ) ) );
    }

    buildForms(): void {

        const oldPassword = new FormControl('', Validators.required);
        const password = new FormControl('', Validators.compose([
            Validators.required,
            CustomValidators.notEqualTo(oldPassword),
            Validators.minLength(8),
            Validators.maxLength(51),
            Validators.pattern(/^(?=\S*\d)(?=\S*[a-z])(?=\S*[A-Z])\S*$/),
        ]));
        const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

        this.passForm = this.fb.group({
            'oldPassword': oldPassword,
            'plainPassword': this.fb.group({
                'password': password,
                'confirm_password': confirmPassword,
            }),
        });

        this.settingForm = this.fb.group({
            'username': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(31),
                Validators.pattern(/^[a-zA-Z]{1}[\w.-]+$/),
            ])],
        });

        this.emailForm = this.fb.group({
            'email': [''],
        });
    }

    settingSubmit() {

        if (this.settingForm.get('username').valid) {

            this.isSubmitting = true;
            // this.modal.openModal();
        }
    }

    onLoginSubmit(event: { observable: Observable<any>, password: string }) {

        this.ngRedux.dispatch(AppActions.incrementLoading());
        // this.modal.closeModal();
        event.observable.toPromise()
            .then(() => {

                // this.reAuthFBUser( event.password, true );
            })
            .catch(() => {

                console.log('your password is wrong. after login check.');
                this.ngRedux.dispatch(AppActions.decrementLoading());
            });
    }

    emailSubmit() {

        this.isSubmitting = true;
        this.ngRedux.dispatch(AppActions.incrementLoading());
        const data = {isPrimary: true};
        this.emailService.patchUserEmail(this.emailForm.get('email').value, data).toPromise()
            .then(() => {
                // this.translator.get( 'email.isPrimary.updated' ).toPromise()
                //     .then( msg => toast( msg, 5000 ) );
                this.isSubmitting = false;
                this.ngRedux.dispatch(AppActions.decrementLoading());
            })
            .catch((err) => {
                console.log('email was not updated');
                this.onSubmitError(err);
            });
    }

    passSubmit() {

        this.isSubmitting = true;
        this.ngRedux.dispatch(AppActions.incrementLoading());
        // this.reAuthFBUser( this.passForm.get( 'oldPassword' ).value, false );
    }

    onSubmitSuccess(updateFire: boolean) {

        /**
         * @todo reset firebase password if professor reset password!
         */
        if (updateFire) {

            // this.userService.updateUserFireData();
            this.isSubmitting = false;
            this.ngRedux.dispatch(AppActions.decrementLoading());
            // this.translator.get( 'app.account.prf_updated' ).toPromise()
            //     .then( message => toast( message, 5000 ) );
        } else {

            // this.fauth.auth.reset
            this.isSubmitting = false;
            this.ngRedux.dispatch(AppActions.decrementLoading());
        }
    }

    onSubmitError(error) {

        this.isSubmitting = false;
        this.ngRedux.dispatch(AppActions.decrementLoading());
        console.log(error);
        // this.translator.get( 'app.submit_error' ).toPromise()
        //     .then( message => toast( message, 5000 ) );
    }

    /*private reAuthFBUser( password: string, username: boolean ) {

        this.fauth.auth.currentUser.reauthenticateWithCredential(
            EmailAuthProvider.credential( this.professor.username + '@zednilma.com', password ) )
            .then( () => {
                if ( username ) {
                    this.updateUsername( password );
                } else {
                    this.updatePassword();
                }
            } )
            .catch( (err) => {
                console.log( 'Your firebase crefentials are wrong. after Firebase reauthenticate.', err );
                this.ngRedux.dispatch( AppActions.decrementLoading() );
            } );
    }*/

    /*private updateUsername( password: string ) {

        this.userService.patchUser( this.professor.id, this.settingForm.value ).subscribe(
            () => {
                this.professor.username = this.settingForm.get( 'username' ).value;
                this.fauth.auth.currentUser.updateEmail( this.professor.username + '@zednilma.com' )
                    .then( () => {
                        this.authService.authenticate( { password: password, rememberMe: true,
                            username: this.professor.username, } ).toPromise()
                            .then( ( jwtToken: JWTToken ) => {
                                this.authService.onAuthenticateSuccess( jwtToken, false );
                                this.settingForm.get('username').setValue( this.professor.username );
                                this.onSubmitSuccess( true );
                            } )
                            .catch( (err) => {
                                this.authService.onAuthenticateFailure( err );
                            } );
                    } )
                    .catch( ( err ) => {
                        console.log( 'couldn\'t update the professor email', err );
                    } );

            },
            ( error ) => {
                this.onSubmitError( error );
            }
        );
    }*/

    /*updatePassword() {

        this.userService.resetPassword( this.professor.id, this.passForm.value ).toPromise()
            .then( () => {
                /!*this.fauth.auth.currentUser.updatePassword( this.passForm.get( 'plainPassword.password' ).value )
                    .then( () => {

                        this.onSubmitSuccess( false );
                    } )
                    .catch( ( err ) => {
                        console.log( 'couldn\'t update the professor firebase password', err );
                    } );*!/
            } )
            .catch( (err) => {
                this.onSubmitError( err );
            } );
    }*/

    passStrengthFn() {

        switch (zxcvbn(this.passForm.get('plainPassword.password').value).score) {

            case 0:
                this.passClass = 'determinate score-20';
                break;
            case 1:
                this.passClass = 'determinate score-40';
                break;
            case 2:
                this.passClass = 'determinate score-60';
                break;
            case 3:
                this.passClass = 'determinate score-80';
                break;
            case 4:
                this.passClass = 'determinate score-100';
                break;
        }
    }
}

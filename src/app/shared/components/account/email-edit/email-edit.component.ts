import {NgRedux} from '@angular-redux/store';
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Email} from '../../../models/email';
import {AppActions} from '../../../redux/app/app.actions';
import {State} from '../../../redux/models/state';
import {EmailService} from '../../../services/models/email.service';

@Component({
    selector: 'zdlm-email-edit,[zdlm-email-edit]',
    templateUrl: './email-edit.component.html',
    styleUrls: ['./email-edit.component.min.css'],
})
export class EmailEditComponent implements OnInit {

    @Input() email: Email;
    @Output() popOut = new EventEmitter<any>();
    @Output() onSaved = new EventEmitter<any>();
    @Output() onDelete = new EventEmitter<any>();
    editMode = true;
    isNew = false;
    originalEmail: string;
    code = '';

    constructor(private emailService: EmailService,
                // private translator: TranslateService,
                private ngRedux: NgRedux<State>,) {
    }

    ngOnInit(): void {

        this.isNew = this.email.email === '';
        this.originalEmail = this.email.email;
    }

    headerClick(event) {

        event.stopPropagation();
    }

    verify() {

        this.editMode = false;
        this.popOut.emit();
    }

    edit() {

        this.editMode = true;
        this.popOut.emit();
    }

    save() {

        const data = {
            email: this.email.email,
            isPrimary: this.email.isPrimary,
            role: this.email.role,
        };
        this.ngRedux.dispatch(AppActions.incrementLoading());
        if (this.isNew) {

            if (this.email.instituteId > 0) {

                this.emailService.newInstituteEmail(this.email.instituteId, data).toPromise()
                    .then((res: Response) => this.onNewEmailSuccess(res))
                    .catch((error) => this.onSubmitError(error));
            } else {

                this.emailService.newUserEmail(data).toPromise()
                    .then((res: Response) => this.onNewEmailSuccess(res))
                    .catch((error) => this.onSubmitError(error));
            }
        } else {

            if (this.email.instituteId > 0) {

                this.emailService.patchInstituteEmail(this.email.instituteId, this.email.id, data).toPromise()
                    .then(() => this.onSubmitSuccess())
                    .catch((error) => this.onSubmitError(error));
            } else {

                this.emailService.patchUserEmail(this.email.id, data).toPromise()
                    .then(() => this.onSubmitSuccess())
                    .catch((error) => this.onSubmitError(error));
            }
        }
    }

    confirm() {

        this.ngRedux.dispatch(AppActions.incrementLoading());
        this.emailService.confirmEmail(this.code).toPromise()
            .then(() => {

                this.ngRedux.dispatch(AppActions.decrementLoading());
                this.email.isVerified = true;
                this.popOut.emit();
                this.code = '';
            })
            .catch((error) => this.onSubmitError(error));
    }

    deleteEmail() {

        if (this.isNew) {

            this.onDelete.emit();
            this.onSaved.emit();
        } else {

            this.ngRedux.dispatch(AppActions.incrementLoading());
            if (this.email.instituteId > 0) {

                this.emailService.deleteInstituteEmail(this.email.instituteId, this.email.id).toPromise()
                    .then(() => {

                        this.ngRedux.dispatch(AppActions.decrementLoading());
                        this.onDelete.emit();
                    })
                    .catch((error) => this.onSubmitError(error));
            } else {

                this.emailService.deleteUserEmail(this.email.id).toPromise()
                    .then(() => {

                        this.ngRedux.dispatch(AppActions.decrementLoading());
                        this.onDelete.emit();
                    })
                    .catch((error) => this.onSubmitError(error));
            }
        }
    }

    onSubmitSuccess() {

        this.popOut.emit();
        if (this.isNew) {

            this.onSaved.emit();
            // this.translator.get('email.new_save_success').toPromise()
            //     .then( message => toast( message, 5000 ) );
        } else if (this.email.email !== this.originalEmail) {

            this.email.isVerified = false;
            // this.translator.get('email.edit_save_success').toPromise()
            //     .then( message => toast( message, 5000 ) );
        }
        this.originalEmail = this.email.email;
        this.ngRedux.dispatch(AppActions.decrementLoading());
    }

    onSubmitError(error: any) {

        console.log('error', error.json());
        this.ngRedux.dispatch(AppActions.decrementLoading());
    }

    private onNewEmailSuccess(res: Response) {

        this.onSubmitSuccess();
        if (res.headers.has('Location')) {
            const arr = res.headers.get('Location').split('/');
            this.email.id = +arr[arr.length - 1];
            this.isNew = false;
        }
    }
}

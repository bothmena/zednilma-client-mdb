import {NgRedux} from '@angular-redux/store';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {State} from '../../../redux/models/state';
import {UserService} from '../../../services/models/user.service';
import {tnCityState} from '../../../utils/util-functions';
import {User} from '../../../models/user';
import {AppActions} from '../../../redux/app/app.actions';

@Component({
    templateUrl: './location-map-edit.component.html',
    styleUrls: ['./location-map-edit.component.min.css'],
})
export class LocationMapEditComponent implements OnInit {

    settingForm: FormGroup;
    isSubmitting = false;
    cities: string[] = [];
    stateCities = tnCityState;
    formInit = false;
    protected user: User = new User();

    constructor(private fb: FormBuilder,
                protected userService: UserService,
                protected ngRedux: NgRedux<State>) {

    }

    ngOnInit() {

        /*this.subscription = this.ngRedux.select().subscribe(
            (state: State) => {
                this.professor = User.build(state.firebase.data);
                if (this.settingForm && !this.formInit) {
                    this.resetForm();
                    this.onStateChange();
                    this.settingForm.get('location.city').setValue(this.professor.location.city);
                    this.formInit = true;
                }
            },
        );*/
        this.buildForm();
    }

    buildForm(): void {

        this.settingForm = this.fb.group({
            'location': this.fb.group({
                'country': ['TN'],
                'state': ['', Validators.compose([
                    Validators.required,
                    Validators.minLength(3),
                    Validators.maxLength(31),
                ])],
                'city': ['', Validators.compose([
                    Validators.required,
                    Validators.minLength(3),
                    Validators.maxLength(31),
                ])],
                'address': ['', Validators.compose([
                    Validators.required,
                    Validators.minLength(15),
                    Validators.maxLength(101),
                ])],
                'zipCode': ['', Validators.compose([
                    Validators.required,
                    Validators.pattern(/^\d+$/),
                    Validators.minLength(4),
                    Validators.maxLength(4),
                ])],
            }),
        });
    }

    save(name: string, index: number) {

        if (this.settingForm.get(name).valid) {

            this.isSubmitting = true;
            this.ngRedux.dispatch(AppActions.incrementLoading());
            const data = {'location': {}};
            data['location'][name.substr(9)] = this.settingForm.get(name).value;
            console.log(index);
            /*this.userService.patchUser( this.professor.id, data ).subscribe(
                () => this.onSubmitSuccess( index ),
                ( error ) => this.onSubmitError( error ),
            );*/
        }
    }

    submit() {

        this.isSubmitting = true;
        this.ngRedux.dispatch(AppActions.incrementLoading());
        /*this.userService.patchUser( this.professor.id, this.settingForm.value ).subscribe(
            () => this.onSubmitSuccess(),
            ( error ) => this.onSubmitError( error ),
        );*/
    }

    resetForm() {

        this.settingForm.get('location.country').setValue(this.user.location.country);
        this.settingForm.get('location.state').setValue(this.user.location.state);
        this.settingForm.get('location.city').setValue(this.user.location.city);
        this.settingForm.get('location.address').setValue(this.user.location.address);
        this.settingForm.get('location.zipCode').setValue(this.user.location.zipCode);
        this.settingForm.markAsPristine();
    }

    onSubmitSuccess(index: number = null) {

        // this.userService.updateUserFireData();
        this.isSubmitting = false;
        this.ngRedux.dispatch(AppActions.decrementLoading());
        // this.translator.get('app.account.prf_updated').toPromise()
        //     .then( message => toast( message, 5000 ) );
        if (index !== null) {
            // this.popOut(index, 'close');
        } else {

            let i = 0;
            while (i < 5) {
                // this.popOut(i++, 'close');
            }
            this.resetForm();
        }
    }

    /*onSubmitError( error ) {

        this.isSubmitting = false;
        this.ngRedux.dispatch( AppActions.decrementLoading() );
        this.translator.get('app.submit_error').toPromise()
            .then( message => toast( message, 6000 ) );
    }*/

    onStateChange() {

        const state = this.settingForm.get('location.state').value;
        if (state === '') {

            this.cities = [];
        } else {

            for (const stt of this.stateCities) {

                if (state === stt.name) {

                    this.cities = stt.cities;
                }
            }
        }
        this.settingForm.get('location.city').setValue(state);
    }
}

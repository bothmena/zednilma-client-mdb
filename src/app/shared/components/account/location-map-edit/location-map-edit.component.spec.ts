import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationMapEditComponent } from './location-map-edit.component';

describe('LocationMapEditComponent', () => {
  let component: LocationMapEditComponent;
  let fixture: ComponentFixture<LocationMapEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationMapEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationMapEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

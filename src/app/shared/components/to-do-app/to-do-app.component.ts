import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {first, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {IMyOptions, MDBDatePickerComponent, ToastService} from 'ng-uikit-pro-standard';
import {TranslateService} from '@ngx-translate/core';
import {NgRedux} from '@angular-redux/store';
import {TaskList} from '../../models/task-list';
import {TaskUser} from '../../models/task-user';
import {AuthService} from '../../services/auth.service';
import {State} from '../../redux/models/state';
import {TaskService} from '../../services/models/task.service';
import {AppActions} from '../../redux/app/app.actions';

@Component({
    selector: 'zdlm-todo-app',
    templateUrl: './to-do-app.component.html',
    styleUrls: ['./to-do-app.component.scss']
})
export class ToDoAppComponent implements OnInit, OnDestroy {

    taskForm: FormGroup;
    listForm: FormGroup;
    lists: TaskList[];
    tasks: {};
    listsByThree = [];
    private ngUnsubscribe = new Subject<boolean>();
    isOwner = false;
    time: string;
    date: string;
    quantity: number;
    unit: string;
    checkbox = false;
    isDone: boolean;
    optionsSelect: Array<any>;
    currentTask: TaskUser;
    currentList: TaskList;
    @ViewChild('formModal') formModal;
    @ViewChild('listModal') listModal;
    @ViewChild('infoModal') infoModal;
    @ViewChild('slider') slider;
    @ViewChild('datePicker') datePicker: MDBDatePickerComponent;
    userId: number;
    @Input() username: string;
    /**
     * @todo change the year range.
     */
    public myDatePickerOptions: IMyOptions = {
        dateFormat: 'd/m/yyyy',
        minYear: 2018,
        maxYear: 2019
    };

    constructor(private fb: FormBuilder,
                private translate: TranslateService,
                private authService: AuthService,
                private ngRedux: NgRedux<State>,
                private toast: ToastService,
                private taskService: TaskService) {
    }

    ngOnInit() {

        this.authService.userObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            user => {
                if (!!user && user.id > 0) {
                    this.isOwner = user.username === this.username;
                    if (this.isOwner)
                        this.userId = user.id;
                }
            }
        );
        this.getViewData();
        this.optionsSelect = [];
        for (const value of ['d', 'w', 'm']) {
            this.translate.get('task.repeat.' + value).subscribe(
                trans => this.optionsSelect.push({value: value, label: trans})
            );
        }
        this.buildForm();
    }

    ngOnDestroy(): void {

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    markAsDone(checked: boolean) {

        if (this.isOwner) {
            this.ngRedux.dispatch(AppActions.incrementLoading());
            this.taskService.markTUAsDone(this.userId, this.currentTask.id, checked).subscribe(
                () => {
                    let arr: TaskUser[];
                    if (this.tasks.hasOwnProperty('task.list.' + this.currentTask.task.list.name))
                        arr = this.tasks['task.list.' + this.currentTask.task.list.name];
                    else if (this.tasks.hasOwnProperty(this.currentTask.task.list.name))
                        arr = this.tasks[this.currentTask.task.list.name];

                    for (const taskUser of arr) {
                        if (taskUser.id === this.currentTask.id) {
                            taskUser.doneAt = new Date();
                            return;
                        }
                    }
                },
                () => {
                    this.translate.get('task.mark_as_error').pipe(first()).subscribe(
                        trans => this.toast.error(trans)
                    );
                    this.ngRedux.dispatch(AppActions.decrementLoading());
                }
            );
        }
    }

    submit() {

        if (this.isOwner) {
            if (!!this.date && !!this.time)
                this.taskForm.get('dueDate').setValue(this.date + ' ' + this.time + ':00');

            if (this.checkbox && !!this.unit && !!this.quantity)
                this.taskForm.get('repeat').setValue(this.quantity + this.unit);
            else
                this.taskForm.get('repeat').setValue('s');

            this.ngRedux.dispatch(AppActions.incrementLoading());
            if (this.taskForm.valid) {

                if (!!this.currentTask) {

                    this.taskService.patchTask(this.userId, this.currentTask.task.id, this.taskForm.value).subscribe(
                        () => {
                            this.translate.get('task.edit_success').pipe(first()).subscribe(
                                trans => this.toast.success(trans)
                            );
                            this.getViewData();
                            this.ngRedux.dispatch(AppActions.decrementLoading());
                            this.formModal.hide();
                        },
                        () => {
                            this.translate.get('task.edit_error').pipe(first()).subscribe(
                                trans => this.toast.error(trans)
                            );
                            this.ngRedux.dispatch(AppActions.decrementLoading());
                            this.formModal.hide();
                        }
                    );
                } else {

                    this.taskService.newTask(this.userId, this.taskForm.value).subscribe(
                        () => { // response
                            this.translate.get('task.new_success').pipe(first()).subscribe(
                                trans => this.toast.success(trans)
                            );
                            this.getViewData();
                            this.ngRedux.dispatch(AppActions.decrementLoading());
                            this.formModal.hide();
                        },
                        () => {
                            this.translate.get('task.new_error').pipe(first()).subscribe(
                                trans => this.toast.error(trans)
                            );
                            this.ngRedux.dispatch(AppActions.decrementLoading());
                            this.formModal.hide();
                        }
                    );
                }
            }
        }
    }

    submitList() {

        if (this.isOwner) {
            this.ngRedux.dispatch(AppActions.incrementLoading());
            if (!!this.currentList) {

                this.taskService.patchList(this.userId, this.currentList.id, this.listForm.value).subscribe(
                    () => {
                        this.translate.get('taskList.edit_success').pipe(first()).subscribe(
                            trans => this.toast.success(trans)
                        );
                        this.getViewData();
                        this.ngRedux.dispatch(AppActions.decrementLoading());
                        this.listModal.hide();
                    },
                    () => {
                        this.translate.get('taskList.edit_error').pipe(first()).subscribe(
                            trans => this.toast.error(trans)
                        );
                        this.ngRedux.dispatch(AppActions.decrementLoading());
                        this.listModal.hide();
                    }
                );
            } else {

                this.taskService.newList(this.userId, this.listForm.value).subscribe(
                    () => { // response
                        this.translate.get('taskList.new_success').pipe(first()).subscribe(
                            trans => this.toast.success(trans)
                        );
                        this.getViewData();
                        this.ngRedux.dispatch(AppActions.decrementLoading());
                        this.listModal.hide();
                    },
                    () => {
                        this.translate.get('taskList.new_error').pipe(first()).subscribe(
                            trans => this.toast.error(trans)
                        );
                        this.ngRedux.dispatch(AppActions.decrementLoading());
                        this.listModal.hide();
                    }
                );
            }
        }
    }

    getRepeat(repeat: string) {

        if (!!repeat) {
            const i = parseInt(repeat, 10);
            return (!!i && i > 1) ? this.translate.get('task.repeat.n_' + repeat.substr(-1, 1), {'n': i}) :
                this.translate.get('task.repeat.1_' + repeat.substr(-1, 1));
        }
        return '';
    }

    prepFormModel(listId?: number) {

        if (this.isOwner) {
            if (!!this.currentTask) {
                this.taskForm.setValue({
                    'name': this.currentTask.task.name,
                    'description': this.currentTask.task.description,
                    'dueDate': this.currentTask.task.dueDate.toLocaleString('en-GB').replace(',', ''),
                    'repeat': this.currentTask.task.repeat,
                    'link': this.currentTask.task.link,
                    'list': this.currentTask.task.list.id,
                    'type': this.currentTask.task.type,
                    'subjectClasse': null,
                });
                this.time = this.currentTask.task.dueDate.toLocaleTimeString('en-GB').substr(0, 5);
                this.date = this.currentTask.task.dueDate.getDate() + '/' + (this.currentTask.task.dueDate.getMonth() + 1) + '/' + this.currentTask.task.dueDate.getFullYear();
                this.datePicker.onUserDateInput(this.date);
                if (this.currentTask.task.repeat !== 's') {
                    this.quantity = parseInt(this.currentTask.task.repeat, 10);
                    this.unit = this.currentTask.task.repeat.substr(-1, 1);
                }
            } else {
                this.date = '';
                this.time = '';
                this.taskForm.reset();
                this.taskForm.get('list').setValue(listId);
                this.taskForm.get('type').setValue(5);
            }

            this.formModal.show();
        }
    }

    prepListModel() {

        if (this.isOwner) {
            if (!!this.currentList) {
                this.listForm.setValue({
                    'name': this.currentList.name,
                });
            } else
                this.listForm.reset();

            this.listModal.show();
        }
    }

    getClockClass(dueDate: Date) {

        const now = new Date();
        // @ts-ignore
        const diff = dueDate - now;
        if (diff < 86400000)
            return 'red-text';
        else if (diff < 259200000)
            return 'amber-text';
        return '';
    }

    private updateLists(lists: Array<TaskList>) {

        this.lists = [
            new TaskList('task.list.exams'),
            new TaskList('task.list.homework'),
            new TaskList('task.list.file_submission'),
            new TaskList('task.list.project')
        ];
        this.lists = this.lists.concat(lists);
        for (const list of lists)
            this.tasks[list.name] = [];
        this.listsByThree = [];
        for (const index in this.lists) {
            if (!!this.lists[index]) {
                const i = Math.floor(+index / 3);
                if (!!this.listsByThree[i])
                    this.listsByThree[i].push(this.lists[index]);
                else
                    this.listsByThree[i] = [this.lists[index]];
            }
        }
    }

    private buildForm() {

        this.taskForm = this.fb.group({
            'name': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(60),
            ])],
            'description': ['', Validators.compose([
                Validators.minLength(45),
                Validators.maxLength(1000),
            ])],
            'dueDate': ['', Validators.compose([
                Validators.required,
            ])],
            'repeat': ['', Validators.compose([
                Validators.required,
            ])],
            'link': ['', Validators.compose([
                CustomValidators.url,
            ])],
            'list': ['', Validators.compose([
                Validators.required,
            ])],
            'type': [5],
            'subjectClasse': [null],
        });
        this.listForm = this.fb.group({
            'name': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(50),
            ])]
        });
    }

    private getViewData() {

        this.tasks = {'task.list.exams': [], 'task.list.homework': [], 'task.list.file_submission': [], 'task.list.project': []};
        this.taskService.getTaskLists(this.username).subscribe(
            lists => {
                this.updateLists(lists);
                this.taskService.getUserTasks(this.username).subscribe(
                    tasks => {
                        for (const taskUser of tasks) {
                            if (this.tasks.hasOwnProperty('task.list.' + taskUser.task.list.name))
                                this.tasks['task.list.' + taskUser.task.list.name].push(taskUser);
                            else if (this.tasks.hasOwnProperty(taskUser.task.list.name))
                                this.tasks[taskUser.task.list.name].push(taskUser);
                            else
                                this.tasks[taskUser.task.list.name] = [taskUser];
                        }
                        this.slider.previousSlide();
                        setTimeout(() => this.slider.nextSlide(), 700);
                    }
                );
            }
        );
    }
}

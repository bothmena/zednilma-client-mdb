import {Component, Input, OnChanges} from '@angular/core';
import {Session} from '../../../models/session';
import {SessionService} from '../../../services/models/session.service';
import {ObservableMedia} from '@angular/flex-layout';

@Component({
    selector: 'zdlm-session-reports',
    templateUrl: './session-reports.component.html',
    styleUrls: ['./session-reports.component.scss']
})
export class SessionReportsComponent implements OnChanges {

    sessions: Session[] = [];
    @Input() insId: number;
    @Input() stdUsername: string;
    currentSession: Session;

    constructor(private sessService: SessionService, public media: ObservableMedia) {
    }

    ngOnChanges(): void {

        if (!!this.stdUsername && !!this.insId)
            this.sessService.getStudentSessionReports(this.insId, this.stdUsername).subscribe(sessions => {
                if (sessions.length > 0) {
                    this.currentSession = sessions[0];
                    this.sessions = sessions;
                }
            });
    }
}

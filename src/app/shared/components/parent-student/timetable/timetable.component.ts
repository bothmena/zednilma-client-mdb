import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {IOption} from 'ng-uikit-pro-standard';
import {Timetable} from '../../../models/timetable';
import {weekNbr} from '../../../utils/util-functions';
import {SessionService} from '../../../services/models/session.service';

@Component({
    selector: 'zdlm-ps-timetable',
    templateUrl: './timetable.component.html',
    styleUrls: ['./timetable.component.scss']
})
export class TimetableComponent implements OnInit, OnChanges {

    weeksOptions: Array<{ value: number, label: string }>;
    selectedValue: number;
    selectedWeek: number;
    hours = [];
    timetable: Timetable;
    timetables: {int?: Timetable} = {};
    weekDataLoading = false;
    @Input() insId: number;
    @Input() stdUsername: string;

    constructor(private sessionService: SessionService) {
    }

    ngOnInit() {

        this.getCurrentAndNextWeeks();
    }

    ngOnChanges(): void {

        if (!!this.insId)
            this.loadWeekData();
    }

    onWeekSelect(event: IOption) {

        this.selectedWeek = +event.value;
        this.loadWeekData();
    }

    private getHours(max): number[] {
        let i = max;
        const hours = [];
        while (i > 0) {
            if (i / 60 >= 1) {
                i -= 60;
                hours.push(60);
            } else {
                hours.push(i % 60);
                i = 0;
            }
        }
        return hours;
    }

    getCurrentAndNextWeeks() {
        this.weeksOptions = [];
        const firstDate = new Date();
        const end = new Date();

        this.selectedWeek = weekNbr(firstDate);
        this.selectedValue = this.selectedWeek;
        firstDate.setDate(firstDate.getDate() - firstDate.getDay());
        end.setDate(firstDate.getDate() + 6);
        this.weeksOptions.push({value: this.selectedWeek, label: firstDate.toLocaleDateString('en-GB') + ' - ' + end.toLocaleDateString('en-GB')});

        firstDate.setDate(end.getDate() + 1);
        end.setDate(end.getDate() + 7);
        this.weeksOptions.push({value: weekNbr(firstDate), label: firstDate.toLocaleDateString('en-GB') + ' - ' + end.toLocaleDateString('en-GB')});
    }

    private loadWeekData() {

        this.weekDataLoading = true;
        if (!!this.timetables[this.selectedWeek]) {
            this.timetable = this.timetables[this.selectedWeek];
            this.weekDataLoading = false;
        }
        else {
            this.sessionService.getStudentWeekTT(this.insId, this.stdUsername, this.selectedWeek).subscribe(timetable => {
                this.timetable = timetable;
                this.timetables[this.selectedWeek] = timetable;
                this.hours = this.getHours(this.timetable.maxMinutes);
                this.weekDataLoading = false;
            });
        }
    }
}

import {Component, Input} from '@angular/core';

@Component({
    selector: 'zdlm-topnav',
    templateUrl: './topnav.component.html',
    styleUrls: ['./topnav.component.min.css'],
})
export class TopnavComponent {

    @Input() section: string;

    /*constructor(private navigator: ABONavigatorService,
                private ngRedux: NgRedux<State>,
                private jwtService: JWTTokenService,
                private authService: AuthenticationService) {
    }*/
}

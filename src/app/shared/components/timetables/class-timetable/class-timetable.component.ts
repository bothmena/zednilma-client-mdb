import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {IOption, ToastService} from 'ng-uikit-pro-standard';
import {AuthService} from '../../../services/auth.service';
import {SubjectClasseService} from '../../../services/models/subject-classe.service';
import {InfrastructureService} from '../../../services/models/infrastructure.service';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {Timetable} from '../../../models/timetable';
import {TimetableService} from '../../../services/models/timetable.service';
import {SessionContainer} from '../../../models/session-container';
import {SubjectClasse} from '../../../models/subject-classe';
import {getNextNWeeks} from '../../../utils/util-functions';
import {SessionService} from '../../../services/models/session.service';
import {AppActions} from '../../../redux/app/app.actions';
import {TranslateService} from '@ngx-translate/core';
import {NgRedux} from '@angular-redux/store';
import {State} from '../../../redux/models/state';
import {Session} from '../../../models/session';

@Component({
    templateUrl: './class-timetable.component.html',
    styleUrls: ['./class-timetable.component.scss']
})
export class ClassTimetableComponent implements OnInit, OnDestroy {

    sessionForm: FormGroup;
    subClasseOptions: Array<{ value: number, label: string }> = [];
    roomOptions: Array<{ value: number, label: string }> = [];
    timetableOptions: Array<{ value: number, label: string }>;
    weeksOptions: Array<{ value: number, label: string }>;
    insId: number;
    selectedValue: number;
    selectedWeek: number;
    classeSlug: string;
    hours = [];
    container: SessionContainer;
    session: Session;
    timetable: Timetable;
    timetables: Timetable[] = [];
    subClasses: SubjectClasse[];
    weekDisabled = false;
    weekDataLoading = false;
    @ViewChild('sessionModal') sessionModal;
    @ViewChild('warning') warning;
    private ngUnsubscribe: Subject<boolean> = new Subject();

    constructor(private fb: FormBuilder,
                private route: ActivatedRoute,
                private authService: AuthService,
                private subClsService: SubjectClasseService,
                private timeService: TimetableService,
                private sessionService: SessionService,
                private translator: TranslateService,
                private toast: ToastService,
                private ngRedux: NgRedux<State>,
                private infraService: InfrastructureService) {
    }

    ngOnInit() {

        this.authService.insObservable$.pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            institute => {
                if (!!institute && institute.id > 0) {
                    this.insId = institute.id;
                    this.classeSlug = this.route.params['value']['class_slug'];
                    this.timeService.getTimetables(institute.id).subscribe(
                        timetables => {
                            this.timetables = timetables;
                            for (const time of this.timetables)
                                time.dirty = false;
                            if (this.timetables.length > 0)
                                this.timetable = this.timetables[0];
                            this.updateTimeSelect();
                            this.hours = this.getHours(this.timetable.maxMinutes);
                        });
                    this.subClsService.getClasseSubjects(this.insId, this.classeSlug).subscribe(
                        subClasses => {
                            this.subClasseOptions = [];
                            this.subClasses = subClasses;
                            for (const subClass of subClasses)
                                this.subClasseOptions.push({value: subClass.id, label: subClass.subject.name});
                        }
                    );
                    this.infraService.getInfrastructures(this.insId, {type: ['CLASSROOM', 'LABORATORY', 'WORKSHOP', 'LIBRARY']}).subscribe(
                        infras => {
                            this.roomOptions = [];
                            for (const infra of infras)
                                this.roomOptions.push({value: infra.id, label: infra.name});
                        }
                    );
                }
            }
        );
        this.buildForm();
        this.weeksOptions = getNextNWeeks();
    }

    ngOnDestroy(): void {

        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    submit() {

        this.weekDisabled = true;
        this.sessionService.newSession(this.insId, this.sessionForm.value).subscribe(
            () => {
                this.translator.get('timetables.new_tt_success').subscribe(trans => this.toast.success(trans));
                this.ngRedux.dispatch(AppActions.decrementLoading());
                this.loadWeekData();
                this.sessionModal.hide();
            },
            () => {
                this.translator.get('timetables.new_tt_error').subscribe(trans => this.toast.error(trans));
                this.ngRedux.dispatch(AppActions.decrementLoading());
            }
        );
    }

    removeSession() {

        this.sessionService.deleteSession(this.insId, this.session.id).subscribe(
            () => {
                this.translator.get('timetables.session_rm_success').subscribe(trans => this.toast.success(trans));
                this.ngRedux.dispatch(AppActions.decrementLoading());
                this.loadWeekData();
                this.warning.hide();
            },
            () => {
                this.translator.get('timetables.session_rm_error').subscribe(trans => this.toast.error(trans));
                this.ngRedux.dispatch(AppActions.decrementLoading());
            }
        )
    }

    onTimetableSelect(event: IOption) {

        for (const time of this.timetables) {
            if (time.id === +event.value)
                this.timetable = time;
        }
    }

    onWeekSelect(event: IOption) {

        this.sessionForm.get('week').setValue(event.value);
        this.selectedWeek = +event.value;
        this.weekDataLoading = true;
        this.loadWeekData();
    }

    prepSessionModal(container: SessionContainer) {

        if (!!this.selectedWeek) {

            this.container = container;
            this.sessionForm.get('container').setValue(container.id);
            this.sessionModal.show();
        } else {
            this.translator.get('timetables.select_week').subscribe(trans => this.toast.warning(trans));
        }
    }

    prepWarning(event, session: Session) {

        event.stopPropagation();
        this.session = session;
        this.warning.show();
    }

    subClasseSelected(event: IOption) {

        for (const subCls of this.subClasses) {

            if (+event.value === subCls.id) {

                this.sessionForm.get('subject').setValue(subCls.subject.id);
                this.sessionForm.get('classe').setValue(subCls.classe.id);
                this.sessionForm.get('professor').setValue(subCls.professor.id);
                return;
            }
        }
    }

    private buildForm() {
        this.sessionForm = this.fb.group({
            'subject': ['', Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(50),
            ])],
            'classe': ['', Validators.compose([
                Validators.required,
            ])],
            'professor': ['', Validators.compose([
                Validators.required,
            ])],
            'classroom': ['', Validators.compose([
                Validators.required,
            ])],
            'container': ['', Validators.compose([
                Validators.required,
            ])],
            'week': ['', Validators.compose([
                Validators.required,
            ])],
        });
    }

    private updateTimeSelect() {

        this.timetableOptions = [];
        for (const time of this.timetables)
            this.timetableOptions.push({value: time.id, label: time.name});
        this.selectedValue = this.timetable.id;
    }

    private getHours(max): number[] {
        let i = max;
        const hours = [];
        while (i > 0) {
            if (i / 60 >= 1) {
                i -= 60;
                hours.push(60);
            } else {
                hours.push(i % 60);
                i = 0;
            }
        }
        return hours;
    }

    private loadWeekData() {

        this.sessionService.getClassWeekSessions(this.insId, this.classeSlug, this.selectedWeek).subscribe(
            sessions => {
                for (const timetable of this.timetables) {
                    for (const day of timetable.days) {
                        dayLoop:
                            for (const container of day) {
                                for (const session of sessions) {
                                    if (container.type === 'session' && container.id === session.container.id) {
                                        container.session = session;
                                        continue dayLoop;
                                    }
                                }
                                container.session = null;
                            }
                    }
                }
                this.weekDataLoading = false;
            }
        )
    }
}

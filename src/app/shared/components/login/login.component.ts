import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ModalDirective} from 'ng-uikit-pro-standard';
import {AuthService} from '../../services/auth.service';
import {UserService} from '../../services/models/user.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {HttpErrorResponse} from '@angular/common/http';
import {Subscriber} from 'rxjs';
import {first} from 'rxjs/operators';

@Component({
    selector: 'zdlm-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

    loginForm: FormGroup;
    forgottenForm: FormGroup;
    loginQuote = '';
    forgottenQuote = '';
    forgottenQuoteSuccess = '';
    loginLoading = false;
    forgottenLoading = false;
    forgottenSucceeded = false;
    loginSubsciber: Subscriber<any>;
    forgottenSubsciber: Subscriber<any>;
    @ViewChild('loginModal') public loginModal: ModalDirective;
    @ViewChild('forgottenModal') public forgottenModal: ModalDirective;

    constructor(private fb: FormBuilder,
                private translator: TranslateService,
                private authService: AuthService,
                private userService: UserService) {
    }

    ngOnInit() {

        this.buildForms();
        this.loginSubsciber = this.loginModal.onHidden.subscribe(
            () => {
                this.loginForm.reset();
                this.loginQuote = '';
            }
        );
        this.forgottenSubsciber = this.forgottenModal.onHidden.subscribe(
            () => {
                this.forgottenForm.reset();
                this.forgottenQuote = '';
                this.forgottenQuoteSuccess = '';
            }
        );
    }


    ngOnDestroy(): void {

        this.forgottenSubsciber.unsubscribe();
        this.loginSubsciber.unsubscribe();
    }

    buildForms(): void {

        this.loginForm = this.fb.group({
            'username': ['', [
                Validators.required,
            ]],
            'password': ['', [
                Validators.required,
            ]],
        });

        this.forgottenForm = this.fb.group({
            'email': ['', [
                Validators.required,
                Validators.email,
            ]],
        });
    }

    public showModal() {

        this.loginModal.show();
    }

    login() {

        if (this.loginForm.valid) {

            this.loginLoading = true;
            this.loginQuote = '';
            this.authService.login(this.loginForm.value).subscribe(
                () => {
                    this.loginModal.hide();
                },
                (error: HttpErrorResponse) => {
                    if (401 === +error.status) {
                        this.translator.get('app.bad_creds').pipe(first()).subscribe(
                            (trans) => this.loginQuote = trans
                        )
                    } else {
                        this.translator.get('app.login_err').pipe(first()).subscribe(
                            (trans) => this.loginQuote = trans
                        )
                    }
                    this.loginForm.get('password').setValue('');
                    this.loginLoading = false;
                }
            );
        }
    }

    resetPassword() {

        if (this.forgottenForm.valid) {

            this.forgottenLoading = true;
            this.forgottenQuote = '';
            const email = this.forgottenForm.get('email').value;
            this.userService.passwordForgotten(email).subscribe(
                () => {

                    this.forgottenLoading = false;
                    this.forgottenSucceeded = true;
                    this.translator.get('w_pg.login.forgotten_success').pipe(first()).subscribe(
                        (trans) => this.forgottenQuoteSuccess = trans
                    )
                },
                (error: HttpErrorResponse) => {

                    if (error.status === 404) {
                        this.translator.get('server_err.user.not_found.by_mail', {email})
                            .pipe(first()).subscribe(
                            (trans) => this.forgottenQuote = trans
                        )
                    } else {
                        this.translator.get('server_err.general').pipe(first()).subscribe(
                            (trans) => this.forgottenQuote = trans
                        )
                    }
                    this.forgottenLoading = false;
                }
            )
        }
    }

    passForgotten() {

        this.loginModal.isAnimated = false;
        this.forgottenModal.isAnimated = false;
        this.loginModal.hide();
        this.forgottenModal.show();
        this.loginModal.isAnimated = true;
        this.forgottenModal.isAnimated = true;
    }

    openLogin() {

        this.forgottenModal.isAnimated = false;
        this.loginModal.isAnimated = false;
        this.forgottenModal.hide();
        this.loginModal.show();
        this.forgottenModal.isAnimated = true;
        this.loginModal.isAnimated = true;
    }
}

import {User} from './user';
import {Classe} from './classe';
import {Subject} from './subject';
import {Infrastructure} from './infrastructure';
import {SessionContainer} from './session-container';

export class Session {

    private _id: number;
    private _professor: User;
    private _classe: Classe;
    private _subject: Subject;
    private _classroom: Infrastructure;
    private _container: SessionContainer;
    private _week: number;
    private _schoolYear: string;
    private _report: string;

    static build(object: Object): Session {

        const session = new Session();

        if (object != null) {

            session.id = +object['id'];
            session._professor = User.build(object['professor']);
            session._classe = Classe.build(object['classe']);
            session._subject = Subject.build(object['subject']);
            session._classroom = Infrastructure.build(object['classroom']);
            session._container = SessionContainer.build(object['container']);
            session._week = +object['week'];
            session._schoolYear = object['school_year'];
            session._report = object['report'];
        }
        return session;
    }

    constructor() {
    }

    getDate() {

        const date = new Date();
        date.setMonth(0, 1);
        date.setDate(date.getDate() + (this.week - 1) * 7 + this.container.day - 1);
        return date.toLocaleDateString('en-GB');
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get professor(): User {
        return this._professor;
    }

    set professor(value: User) {
        this._professor = value;
    }

    get classe(): Classe {
        return this._classe;
    }

    set classe(value: Classe) {
        this._classe = value;
    }

    get subject(): Subject {
        return this._subject;
    }

    set subject(value: Subject) {
        this._subject = value;
    }

    get classroom(): Infrastructure {
        return this._classroom;
    }

    set classroom(value: Infrastructure) {
        this._classroom = value;
    }

    get container(): SessionContainer {
        return this._container;
    }

    set container(value: SessionContainer) {
        this._container = value;
    }

    get week(): number {
        return this._week;
    }

    set week(value: number) {
        this._week = value;
    }

    get schoolYear(): string {
        return this._schoolYear;
    }

    set schoolYear(value: string) {
        this._schoolYear = value;
    }

    get report(): string {
        return this._report;
    }

    set report(value: string) {
        this._report = value;
    }
}

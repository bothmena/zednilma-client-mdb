import {User} from './user';
import {Classe} from './classe';
import {Subject} from './subject';
import {TaskList} from './task-list';
import {Task} from './task';

export class SubjectClasse {

    private _id: number;
    private _professor: User;
    private _classe: Classe;
    private _subject: Subject;
    private _color: string;
    private _role: number;
    private _tasks: Task[];

    static build(object: Object): SubjectClasse {

        const subjectClasse = new SubjectClasse();

        if (object != null) {

            subjectClasse._id = +object['id'];
            subjectClasse._professor = User.build(object['professor']);
            subjectClasse._classe = Classe.build(object['classe']);
            subjectClasse._subject = Subject.build(object['subject']);
            subjectClasse._tasks = [];
            for (const obj of object['tasks'])
                subjectClasse._tasks.push(Task.build(obj))
            subjectClasse._role = +object['role'];
            subjectClasse.color = TaskList.taskColors[subjectClasse.id % 16];
        }
        return subjectClasse;
    }

    constructor() {
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get professor(): User {
        return this._professor;
    }

    set professor(value: User) {
        this._professor = value;
    }

    get classe(): Classe {
        return this._classe;
    }

    set classe(value: Classe) {
        this._classe = value;
    }

    get subject(): Subject {
        return this._subject;
    }

    set subject(value: Subject) {
        this._subject = value;
    }

    get color(): string {
        return this._color;
    }

    set color(value: string) {
        this._color = value;
    }

    get role(): number {
        return this._role;
    }

    set role(value: number) {
        this._role = value;
    }

    get tasks(): Task[] {
        return this._tasks;
    }

    set tasks(value: Task[]) {
        this._tasks = value;
    }
}

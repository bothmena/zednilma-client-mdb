import {Module} from './module';
import {Subject} from './subject';
import {dateWithoutTimezone} from '../utils/util-functions';

export class ModuleSubject {

    private _id: number;
    private _module: Module;
    private _subject: Subject;
    private _coefficient: number;
    private _date: Date;

    static build(object: Object): ModuleSubject {

        const moduleSubject = new ModuleSubject();

        if (object != null) {

            moduleSubject.id = +object['id'];
            moduleSubject._subject = Subject.build(object['subject']);
            moduleSubject._module = Module.build(object['module']);
            moduleSubject._coefficient = +object['coefficient'];
            moduleSubject._date = dateWithoutTimezone(object['date']);
        }
        return moduleSubject;
    }

    constructor() {
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get module(): Module {
        return this._module;
    }

    set module(value: Module) {
        this._module = value;
    }

    get subject(): Subject {
        return this._subject;
    }

    set subject(value: Subject) {
        this._subject = value;
    }

    get coefficient(): number {
        return this._coefficient;
    }

    set coefficient(value: number) {
        this._coefficient = value;
    }

    get date(): Date {
        return this._date;
    }

    set date(value: Date) {
        this._date = value;
    }
}

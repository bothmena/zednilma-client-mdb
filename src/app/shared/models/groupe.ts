export class Groupe {

    private _id: number;
    private _instituteId: number;
    private _parentId: number;
    private _name: string;
    private _description: string;
    private _type: string;
    private _individuals: string;
    private _date: string;

    static build( object: Object ): Groupe {

        const groupe = new Groupe();

        if ( object != null ) {

            groupe.id = object.hasOwnProperty( 'id' ) ? +object[ 'id' ] : 0;
            groupe.instituteId = object.hasOwnProperty( 'institute_id' ) ? +object[ 'institute_id' ] : 0;
            groupe.parentId = object.hasOwnProperty( 'parent_id' ) ? +object[ 'parent_id' ] : 0;
            groupe.name = object.hasOwnProperty( 'name' ) ? object[ 'name' ] : '';
            groupe.description = object.hasOwnProperty( 'description' ) ? object[ 'description' ] : '';
            groupe.type = object.hasOwnProperty( 'type' ) ? object[ 'type' ] : '';
            groupe.individuals = object.hasOwnProperty( 'individuals' ) ? object[ 'individuals' ] : '';
            groupe.date = object.hasOwnProperty( 'date' ) ? object[ 'date' ] : '';
        }
        return groupe;
    }

    constructor( id: number = 0, instituteId: number = 0, parentId: number = 0, name: string = '',
        description: string = '', type: string = '', individuals: string = '', date: string = '' ) {

        this._id = id;
        this._instituteId = instituteId;
        this._parentId = parentId;
        this._name = name;
        this._description = description;
        this._type = type;
        this._individuals = individuals;
        this._date = date;
    }

    get id(): number {
        return this._id;
    }

    set id( value: number ) {
        this._id = value;
    }

    get instituteId(): number {
        return this._instituteId;
    }

    set instituteId( value: number ) {
        this._instituteId = value;
    }

    get parentId(): number {
        return this._parentId;
    }

    set parentId( value: number ) {
        this._parentId = value;
    }

    get name(): string {
        return this._name;
    }

    set name( value: string ) {
        this._name = value;
    }

    get description(): string {
        return this._description;
    }

    set description( value: string ) {
        this._description = value;
    }

    get type(): string {
        return this._type;
    }

    set type( value: string ) {
        this._type = value;
    }

    get individuals(): string {
        return this._individuals;
    }

    set individuals( value: string ) {
        this._individuals = value;
    }

    get date(): string {
        return this._date;
    }

    set date( value: string ) {
        this._date = value;
    }
}

import {Speciality} from './speciality';

export class Classe {

    private _id: number;
    private _branch: Speciality;
    private _name: string;
    private _level: number;
    private _slug: string;
    private _schoolYear: string;

    static build( object: Object ): Classe {

        const classe = new Classe();

        if ( object != null ) {

            classe.id = +object[ 'id' ];
            classe._branch = Speciality.build(object['branch']);
            classe._name = object[ 'name' ];
            classe.level = +object[ 'level' ];
            classe._slug = object[ 'specId' ];
            classe._schoolYear = object[ 'school_year' ];
        }
        return classe;
    }

    constructor() {
    }

    get id(): number {
        return this._id;
    }

    set id( value: number ) {
        this._id = value;
    }

    get branch(): Speciality {
        return this._branch;
    }

    set branch(value: Speciality) {
        this._branch = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get level(): number {
        return this._level;
    }

    set level(value: number) {
        this._level = value;
    }

    get slug(): string {
        return this._slug;
    }

    set slug(value: string) {
        this._slug = value;
    }

    get schoolYear(): string {
        return this._schoolYear;
    }

    set schoolYear(value: string) {
        this._schoolYear = value;
    }
}

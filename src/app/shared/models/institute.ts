/**
 * Created by bothmena on 22/06/17.
 */
import {dateWithoutTimezone} from '../utils/util-functions';

export class Institute {

    private _id: number;
    private _name: string;
    private _slug: string;
    private _slogan: string;
    private _description: string;
    private _foundationDate: Date;
    private _website: string;
    private _type: string;
    private _parentalControl: string;
    private _joinDate: Date;

    static build( object: Object ): Institute {

        const institute = new Institute();

        if ( object != null ) {
            institute.id                = +object[ 'id' ];
            institute.name              = object[ 'name' ];
            institute.slug              = object[ 'slug' ];
            institute.slogan            = object[ 'slogan' ];
            institute.description       = object[ 'description' ];
            institute.foundationDate    = dateWithoutTimezone(object[ 'foundation_date' ]);
            institute.website           = object[ 'website' ];
            institute.type              = object[ 'type' ];
            institute.parentalControl   = object[ 'parental_control' ];
            institute.joinDate          = dateWithoutTimezone(object['join_date']);
        }
        return institute;
    }

    get id(): number {
        return this._id;
    }

    set id( value: number ) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name( value: string ) {
        this._name = value;
    }

    get slug(): string {
        return this._slug;
    }

    set slug( value: string ) {
        this._slug = value;
    }

    get slogan(): string {
        return this._slogan;
    }

    set slogan( value: string ) {
        this._slogan = value;
    }

    get description(): string {
        return this._description;
    }

    set description( value: string ) {
        this._description = value;
    }

    get website(): string {
        return this._website;
    }

    set website( value: string ) {
        this._website = value;
    }

    get type(): string {
        return this._type;
    }

    set type( value: string ) {
        this._type = value;
    }

    get parentalControl(): string {
        return this._parentalControl;
    }

    set parentalControl( value: string ) {
        this._parentalControl = value;
    }


    get foundationDate(): Date {
        return this._foundationDate;
    }

    set foundationDate(value: Date) {
        this._foundationDate = value;
    }

    get joinDate(): Date {
        return this._joinDate;
    }

    set joinDate(value: Date) {
        this._joinDate = value;
    }
}

import {Location} from './location';
import {AppGlobals} from '../utils/app.globals';
import {dateWithoutTimezone} from '../utils/util-functions';

/**
 * Created by bothmena on 22/06/17.
 */

export class User {

    private _id: number;
    private _email: string;
    private _username: string;
    private _firstName: string;
    private _middleName: string;
    private _lastName: string;
    private _imageUrl: string;
    private _birthday: Date;
    private _gender: string;
    private _cin: string;
    private _language: string;
    private _location: Location;
    private _role: string;
    private _groups: string[];
    private _status: string;
    private _present: boolean;

    static build(object: Object): User {

        const user = new User();

        if (object != null) {
            user.id = +object['id'];
            user.email = object['email'];
            user.username = object['username'];
            user.firstName = object['first_name'];
            user.middleName = object['middle_name'];
            user.lastName = object['last_name'];
            user.imageUrl = AppGlobals.BASE_RESOURCE_URL + object['image_url'];
            user.birthday = dateWithoutTimezone(object['birthday']);
            user.gender = object['gender'];
            user.cin = object['cin'];
            user.language = object['language'];
            user.role = object['role'];
            user.groups = object['groups'];
            user.status = object['status'];

            if (object.hasOwnProperty('location') && object['location']) {
                user.location = Location.build(object['location']);
            } else {
                user.location = new Location();
            }
        }
        return user;
    }

    constructor(firstName = '', lastName = '', email = '') {

        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this._present = null;
        this.location = new Location();
    }

    name(): string {

        let name = '';
        if (this.firstName) {
            name += this.firstName + ' ';
        }
        if (this.lastName) {
            name += this.lastName;
        }
        return name;
    }

    fullName(): string {

        let name = '';
        if (this.firstName) {
            name += this.firstName + ' ';
        }
        if (this.middleName) {
            name += this.middleName + ' ';
        }
        if (this.lastName) {
            name += this.lastName;
        }
        return name;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    get username(): string {
        return this._username;
    }

    set username(value: string) {
        this._username = value;
    }

    get firstName(): string {
        return this._firstName;
    }

    set firstName(value: string) {
        this._firstName = value;
    }

    get middleName(): string {
        return this._middleName;
    }

    set middleName(value: string) {
        this._middleName = value;
    }

    get lastName(): string {
        return this._lastName;
    }

    set lastName(value: string) {
        this._lastName = value;
    }

    get imageUrl(): string {
        return this._imageUrl;
    }

    set imageUrl(value: string) {
        this._imageUrl = value;
    }

    get birthday(): Date {
        return this._birthday;
    }

    set birthday(value: Date) {
        this._birthday = value;
    }

    get gender(): string {
        return this._gender;
    }

    set gender(value: string) {
        this._gender = value;
    }

    get cin(): string {
        return this._cin;
    }

    set cin(value: string) {
        this._cin = value;
    }

    get language(): string {
        return this._language;
    }

    set language(value: string) {
        this._language = value;
    }

    get location(): Location {
        return this._location;
    }

    set location(value: Location) {
        this._location = value;
    }

    get role(): string {
        return this._role;
    }

    set role(value: string) {
        this._role = value;
    }

    get groups(): string[] {
        return this._groups;
    }

    set groups(value: string[]) {
        this._groups = value;
    }

    get status(): string {
        return this._status;
    }

    set status(value: string) {
        this._status = value;
    }

    get present(): boolean {
        return this._present;
    }

    set present(value: boolean) {
        this._present = value;
    }
}

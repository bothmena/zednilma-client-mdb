export class Post {

    private _id: number;
    private _instituteId: number;
    private _name: string;
    private _description: string;
    private _type: string;
    private _isMultiple: boolean;
    private _date: string;

    static build(object: Object): Post {

        const post = new Post();

        if (object != null) {

            post.id = +object['id'];
            post.instituteId = +object['institute_id'];
            post.name = object['name'];
            post.description = object['description'];
            post.type = object['type'];
            post.isMultiple = object['is_multiple'];
            post.date = object['date'];
        }
        return post;
    }

    constructor(id: number = 0, instituteId: number = 0, name: string = '', description: string = '',
                type: string = '', date: string = '') {

        this._id = id;
        this._instituteId = instituteId;
        this._name = name;
        this._description = description;
        this._type = type;
        this._date = date;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get instituteId(): number {
        return this._instituteId;
    }

    set instituteId(value: number) {
        this._instituteId = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get type(): string {
        return this._type;
    }

    set type(value: string) {
        this._type = value;
    }

    get isMultiple(): boolean {
        return this._isMultiple;
    }

    set isMultiple(value: boolean) {
        this._isMultiple = value;
    }

    get date(): string {
        return this._date;
    }

    set date(value: string) {
        this._date = value;
    }
}

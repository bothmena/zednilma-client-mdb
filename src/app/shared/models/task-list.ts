import {dateWithoutTimezone} from '../utils/util-functions';
import {User} from './user';

export class TaskList {

    static taskColors = ['pink', 'red', 'purple', 'deep-purple', 'indigo', 'blue', 'light-blue', 'teal', 'green', 'light-green', 'lime', 'yellow', 'amber', 'orange',
        'grey', 'mdb-color'];
    private _id: number;
    private _user: User;
    private _name: string;
    private _color: string;
    private _date: Date;

    static build(object: Object): TaskList {

        const taskList = new TaskList();

        if (object != null) {

            taskList._id = +object['id'];
            taskList._user = User.build(object['user']);
            taskList._name = object['name'];
            taskList._date = dateWithoutTimezone(object['date']);
            taskList.setColor();
        }
        return taskList;
    }

    constructor(name: string = null) {

        this.name = name;
        this.setColor();
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get user(): User {
        return this._user;
    }

    set user(value: User) {
        this._user = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get date(): Date {
        return this._date;
    }

    set date(value: Date) {
        this._date = value;
    }

    get color(): string {
        return this._color;
    }

    set color(value: string) {
        this._color = value;
    }

    setColor() {

        if (this.id > 0)
            this.color = TaskList.taskColors[this.id % 16];
        else {
            switch (this.name) {
                case 'task.list.exams':
                    this.color = 'blue-grey';
                    break;
                case 'task.list.homework':
                    this.color = 'cyan';
                    break;
                case 'task.list.file_submission':
                    this.color = 'deep-orange';
                    break;
                case 'task.list.project':
                    this.color = 'brown';
                    break;
            }
        }
    }
}

import {dateWithoutTimezone} from '../utils/util-functions';
import {Session} from './session';

export class SessionContainer {

    private _id: number;
    private _timetableId: number;
    private _startAt: Date;
    private _endAt: Date;
    private _type: string; // local variable: session || break
    private _minutes: number; // local variable: number of minutes i.e. width of cell.
    private _session: Session;
    private _day: number;

    static build(object: Object): SessionContainer {

        const container = new SessionContainer();

        if (object != null) {
            container.id = +object['id'];
            container.timetableId = +object['timetable_id'];
            container.startAt = dateWithoutTimezone(object['start_at']);
            container.endAt = dateWithoutTimezone(object['end_at']);
            container.type = 'session';
            container.day = +object['day'];
            container.calculateMinutes();
        }
        return container;
    }

    timeRange() {

        const func = (n) => n < 10 ? '0' + n : n;
        return func(this.startAt.getHours()) + ':' + func(this.startAt.getMinutes()) + ' - ' + func(this.endAt.getHours()) + ':' + func(this.endAt.getMinutes());
    }

    calculateMinutes() {

        if (!!this._startAt && !!this._endAt)
            this._minutes = (this._endAt.getHours() * 60 + this._endAt.getMinutes()) - (this._startAt.getHours() * 60 + this._startAt.getMinutes());
        else
            this._minutes = 0;
    }

    getFormData() {

        return {
            startAt: this.dateToTimeString(this._startAt),
            endAt: this.dateToTimeString(this._endAt),
            day: this._day,
        };
    }

    private dateToTimeString(date: Date) {

        const func = (n) => n < 10 ? '0' + n : n;
        return func(date.getHours()) + ':' + func(date.getMinutes()) + ':00';
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get timetableId(): number {
        return this._timetableId;
    }

    set timetableId(value: number) {
        this._timetableId = value;
    }

    get startAt(): Date {
        return this._startAt;
    }

    set startAt(value: Date) {

        value.setFullYear(1970, 1, 1);
        this._startAt = value;
    }

    get endAt(): Date {
        return this._endAt;
    }

    set endAt(value: Date) {

        value.setFullYear(1970, 1, 1);
        this._endAt = value;
    }

    get day(): number {
        return this._day;
    }

    set day(value: number) {
        this._day = value;
    }

    get type(): string {
        return this._type;
    }

    set type(value: string) {
        this._type = value;
    }

    get minutes(): number {
        return this._minutes;
    }

    set minutes(value: number) {
        this._minutes = value;
    }

    get session(): Session {
        return this._session;
    }

    set session(value: Session) {
        this._session = value;
    }
}

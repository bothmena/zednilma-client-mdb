import {dateWithoutTimezone} from '../utils/util-functions';
import {Task} from './task';
import {User} from './user';

export class TaskUser {

    private _id: number;
    private _task: Task;
    private _user: User;
    private _doneAt: Date;
    private _dueDate: Date;
    private _isValidated: boolean;

    static build(object: Object): TaskUser {

        const taskUser = new TaskUser();

        if (object != null) {

            taskUser._id = +object['id'];
            taskUser._user = User.build(object['user']);
            taskUser._task = Task.build(object['task']);
            taskUser._doneAt = dateWithoutTimezone(object['done_at']);
            taskUser._dueDate = dateWithoutTimezone(object['due_date']);
            taskUser._isValidated = object['is_validated'];
        }
        return taskUser;
    }

    constructor() {
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get task(): Task {
        return this._task;
    }

    set task(value: Task) {
        this._task = value;
    }

    get user(): User {
        return this._user;
    }

    set user(value: User) {
        this._user = value;
    }

    get doneAt(): Date {
        return this._doneAt;
    }

    set doneAt(value: Date) {
        this._doneAt = value;
    }

    get dueDate(): Date {
        return this._dueDate;
    }

    set dueDate(value: Date) {
        this._dueDate = value;
    }

    get isValidated(): boolean {
        return this._isValidated;
    }

    set isValidated(value: boolean) {
        this._isValidated = value;
    }
}

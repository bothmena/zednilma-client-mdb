/**
 * Created by bothmena on 22/06/17.
 */

export class SocialProfile {

    private _id: number;
    private _instituteId: number;
    private _userId: number;
    private _url: string;
    private _username: string;
    private _website: string;
    private _date: string;

    static build( object: Object ): SocialProfile {

        const socialProfile = new SocialProfile();

        if ( object != null ) {

            socialProfile.id = object.hasOwnProperty( 'id' ) ? object[ 'id' ] : 0;
            socialProfile.instituteId = object.hasOwnProperty( 'institute_id' ) ? object[ 'institute_id' ] : 0;
            socialProfile.userId = object.hasOwnProperty( 'user_id' ) ? object[ 'user_id' ] : 0;
            socialProfile.url = object.hasOwnProperty( 'url' ) ? object[ 'url' ] : '';
            socialProfile.username = object.hasOwnProperty( 'username' ) ? object[ 'username' ] : '';
            socialProfile.website = object.hasOwnProperty( 'website' ) ? object[ 'website' ] : '';
            socialProfile.date = object.hasOwnProperty( 'date' ) ? object[ 'date' ] : '';
        }
        return socialProfile;
    }

    constructor( url: string = '', username: string = '', website: string = '', date?: string ) {

        this._url = url;
        this._username = username;
        this._website = website;
        this._date = date;
    }

    btnClass() {

        switch (this.website) {
            case 'linkedin':
                return 'btn-li';
            case 'facebook':
                return 'btn-fb';
            case 'github':
                return 'btn-git';
            case 'gitlab':
                return 'btn-gitl';
            case 'bitbucket':
                return 'btn-bitb';
            case 'twitter':
                return 'btn-tw';
            case 'youtube':
                return 'btn-yt';
            case 'google-plus':
                return 'btn-gplus';
            case 'instagram':
                return 'btn-ins';
            case 'vimeo':
                return 'btn-vm';
            case 'skype':
                return 'btn-skp';
            case 'whatsapp':
                return 'btn-wht';
        }
    }

    get id(): number {
        return this._id;
    }

    set id( value: number ) {
        this._id = value;
    }

    get instituteId(): number {
        return this._instituteId;
    }

    set instituteId( value: number ) {
        this._instituteId = value;
    }

    get userId(): number {
        return this._userId;
    }

    set userId( value: number ) {
        this._userId = value;
    }

    get url(): string {
        return this._url;
    }

    set url( value: string ) {
        this._url = value;
    }

    get username(): string {
        return this._username;
    }

    set username( value: string ) {
        this._username = value;
    }

    get website(): string {
        return this._website;
    }

    set website( value: string ) {
        this._website = value;
    }

    get date(): string {
        return this._date;
    }

    set date( value: string ) {
        this._date = value;
    }
}

import {Subject} from './subject';
import {DepartmentMember} from './department-member';
import {dateWithoutTimezone} from '../utils/util-functions';

export class Department {

    private _id: number;
    private _instituteId: number;
    private _name: string;
    private _slug: string;
    private _description: string;
    private _date: Date;
    private _subjects: Array<Subject>;
    private _members: Array<DepartmentMember>;

    static build(object: Object): Department {

        const department = new Department();

        if (object != null) {
            department.id = +object['id'];
            department.instituteId = +object['institute_id'];
            department.name = object['name'];
            department.slug = object['slug'];
            department.description = object['description'];
            department.date = dateWithoutTimezone(object['date']);
            if (object.hasOwnProperty('subjects') && object['subjects'] != null) {

                for (const subject of object['subjects']) {

                    department.addSubject(Subject.build(subject));
                }
            }
            if (object.hasOwnProperty('members') && object['members'] != null) {

                for (const member of object['members']) {

                    department.addMember(DepartmentMember.build(member));
                }
            }
        }
        return department;
    }

    constructor() {

        this._subjects = [];
        this._members = [];
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get instituteId(): number {
        return this._instituteId;
    }

    set instituteId(value: number) {
        this._instituteId = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get slug(): string {
        return this._slug;
    }

    set slug(value: string) {
        this._slug = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get date(): Date {
        return this._date;
    }

    set date(value: Date) {
        this._date = value;
    }

    get subjects(): Array<Subject> {
        return this._subjects;
    }

    set subjects(value: Array<Subject>) {
        this._subjects = value;
    }

    addSubject(value: Subject): Department {

        this._subjects.push(value);
        return this;
    }

    get members(): Array<DepartmentMember> {
        return this._members;
    }

    set members(value: Array<DepartmentMember>) {
        this._members = value;
    }

    addMember(value: DepartmentMember): Department {

        this._members.push(value);
        return this;
    }
}

import {AppGlobals} from '../utils/app.globals';
import {Observable} from 'rxjs';
import {User} from './user';

export class Notification {

    private _id: number;
    private _notifier: User;
    private _notificationUserId: number;
    private _title: string;
    private _description: string;
    private _description$: Observable<any>;
    private _type: number;
    private _isRead: boolean;
    private _imageUrl: string;
    private _date: Date;
    private _data: any;

    static build(object: Object): Notification {

        const notification = new Notification();

        if (object != null) {

            notification._notificationUserId = object['id'];
            notification._id = +object['notification']['id'];
            notification._notifier = User.build(object['notification']['notifier']);
            notification._imageUrl = AppGlobals.BASE_RESOURCE_URL + object['notification']['image'];
            notification._title = object['notification']['title'];
            notification._description = object['notification']['description'];
            notification._type = object['notification']['type'];
            notification._isRead = object['is_read'];
            notification._date = new Date(object['date']);
        }
        return notification;
    }

    constructor() {
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get notifier(): User {
        return this._notifier;
    }

    set notifier(value: User) {
        this._notifier = value;
    }

    get notificationUserId(): number {
        return this._notificationUserId;
    }

    set notificationUserId(value: number) {
        this._notificationUserId = value;
    }

    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {

        this._description = value;
    }

    get type(): number {
        return this._type;
    }

    set type(value: number) {
        this._type = value;
    }

    get isRead(): boolean {
        return this._isRead;
    }

    set isRead(value: boolean) {
        this._isRead = value;
    }

    get imageUrl(): string {
        return this._imageUrl;
    }

    set imageUrl(value: string) {
        this._imageUrl = value;
    }

    get date(): Date {
        return this._date;
    }

    set date(value: Date) {
        this._date = value;
    }

    get description$(): Observable<string> {
        return this._description$;
    }

    set description$(value: Observable<string>) {
        this._description$ = value;
    }

    get data(): any {
        return this._data;
    }

    set data(value: any) {
        this._data = value;
    }
}

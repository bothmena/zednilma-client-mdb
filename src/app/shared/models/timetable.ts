import {Institute} from './institute';
import {SessionContainer} from './session-container';

export class Timetable {

    private _id: number;
    private _institute: Institute;
    private _name: string;
    private _locked: boolean;
    private _dirty: boolean;
    private _days: Array<Array<SessionContainer>>; // local variable to store session containers of all days, days[0] always empty.
    private _maxMinutes: number;

    static build(object: Object): Timetable {

        const timetable = new Timetable();

        if (object != null) {
            timetable.id = +object['id'];
            timetable.institute = Institute.build(object['institute']);
            timetable.name = object['name'];
            timetable.locked = object['locked'];
            for (const scObject of object['session_containers'])
                timetable.addSessionContainer(SessionContainer.build(scObject));
            timetable.fillBreaks();
        }
        return timetable;
    }

    addSessionContainer(container: SessionContainer) {

        if ([1, 2, 3, 4, 5, 6, 7].indexOf(container.day) === -1)
            return;
        if (container.startAt >= container.endAt)
            throw new Error('timetables.end_gt_start');
        this.removeDayBreaks(container.day);
        for (const cont of this._days[container.day]) {

            if ((cont.startAt < container.startAt && cont.endAt <= container.startAt) || (container.startAt < cont.startAt && container.endAt <= cont.startAt)) {
            } else
                throw new Error('timetables.collusion');
        }

        this._days[container.day].push(container);
        this._dirty = true;
    }

    sortDays() {

        for (const index in this._days) {

            if (!!this._days[index] && this._days[index].length > 1)
                this._days[index].sort(this.compare)
        }
    }

    fillBreaks() {

        this.sortDays();
        for (const index in this._days) {

            if (!!this._days[index])
                this.fillDayBreaks(index);
        }
        this.sortDays();
        this.calculateMaxMinutes();
    }

    fillDayBreaks(day) {

        if (this._days[day].length > 1) {

            this.removeDayBreaks(day);
            const containers = this._days[day];
            const newContainers = containers.slice();
            containers.sort(this.compare);
            for (let i = 0; i < containers.length - 1; i++) {

                const current = containers[i];
                const next = containers[i + 1];
                const difference = this.diffMinutes(current.endAt, next.startAt);
                if (difference > 0) {

                    const breakK = new SessionContainer();
                    breakK.day = current.day;
                    breakK.startAt = this.addMinutes(current.endAt, 0);
                    breakK.endAt = this.addMinutes(current.endAt, difference);
                    breakK.type = 'break';
                    breakK.calculateMinutes();
                    newContainers.push(breakK);
                }
            }
            this._days[day] = newContainers.sort(this.compare);
        }
    }

    removeDayBreaks(day: number) {

        if (!!this._days[day] && this._days[day].length > 1) {

            const newDay = [];
            for (const container of this._days[day]) {
                if (container.type === 'session')
                    newDay.push(container);
            }
            this._days[day] = newDay;
        }
    }

    clearDay(day: number): any {

        if (!!this._days[day] && this._days[day].length > 0) {

            this._days[day] = [];
            this._dirty = true;
        }
    }

    calculateMaxMinutes() {

        let maxSum = 0;
        for (const containers of this.days) {

            if (!!containers && containers.length > 0) {
                const sum = this.diffMinutes(containers[0].startAt, containers[containers.length - 1].endAt);
                if (sum > maxSum)
                    maxSum = sum;
            }
        }
        this.maxMinutes = maxSum;
    }

    getContainersFormData() {

        const data = [];
        for (const day of this._days) {
            for (const container of day) {
                if (container.type === 'session')
                    data.push(container.getFormData());
            }
        }
        return data;
    }

    private addMinutes(date: Date, minutes: number) {

        const sumMinutes = date.getHours() * 60 + date.getMinutes() + minutes;
        const newDate = new Date();
        newDate.setFullYear(1970, 1, 1);
        newDate.setHours(Math.floor(sumMinutes / 60), sumMinutes % 60, 0);
        return newDate;
    }

    private compare(a: SessionContainer, b: SessionContainer) {
        if (a.startAt < b.startAt)
            return -1;
        if (a.startAt > b.startAt)
            return 1;
        return 0;
    }

    private diffMinutes(start: Date, end: Date) {

        return 60 * end.getHours() + end.getMinutes() - 60 * start.getHours() - start.getMinutes();
    }

    constructor(id: number = null, name: string = null) {
        this._id = id;
        this._name = name;
        this._dirty = false;
        this._days = [[], [], [], [], [], [], [], []];
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get institute(): Institute {
        return this._institute;
    }

    set institute(value: Institute) {
        this._institute = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get days(): Array<Array<SessionContainer>> {
        return this._days;
    }

    getBusyDays() {

        const days = [];
        for (const index in this._days) {
            if (!!this._days[index]) {

                if ((+index === 6 || +index === 7) && this._days[+index].length === 0)
                    continue;
                days.push(this._days[+index]);
            }
        }
        return days;
    }

    set days(value: Array<Array<SessionContainer>>) {
        this._days = value;
    }

    get maxMinutes(): number {

        return this._maxMinutes;
    }

    set maxMinutes(value: number) {
        this._maxMinutes = value;
    }

    get dirty(): boolean {
        return this._dirty;
    }

    set dirty(value: boolean) {
        this._dirty = value;
    }

    get locked(): boolean {
        return this._locked;
    }

    set locked(value: boolean) {
        this._locked = value;
    }
}

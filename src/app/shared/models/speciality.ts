import {dateWithoutTimezone} from '../utils/util-functions';
import {SpecialityPath} from './speciality-path';
import {PathBranch} from './path-branch';

export class Speciality {

    private _id: number;
    private _instituteId: number;
    private _parentId: number;
    private _name: string;
    private _slug: string;
    private _alias: string;
    private _description: string;
    private _studyCycle: string;
    private _years: number;
    private _rootYears: number;
    private _depth: number;
    private _type: string;
    private _date: Date;
    private _paths: Array<SpecialityPath>;
    private _children: Array<Speciality>;
    private _next: Array<Speciality>;

    static build(object: Object): Speciality {

        const speciality = new Speciality();

        if (object != null) {
            speciality.id = +object['id'];
            speciality.instituteId = +object['institute_id'];
            speciality.parentId = +object['parent_id'] > 0 ? object['parent_id'] : null;
            speciality.name = object['name'];
            speciality.slug = object['slug'];
            speciality.alias = object['alias'];
            speciality.description = object['description'];
            speciality.studyCycle = object['study_cycle'];
            speciality.years = +object['years'];
            speciality.rootYears = +object['root_years'];
            speciality.type = object['type'];
            speciality.date = dateWithoutTimezone(object['date']);
        }
        return speciality;
    }

    constructor() {
        this._paths = [];
        this._children = [];
        this._next = [];
        this._depth = 0;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get instituteId(): number {
        return this._instituteId;
    }

    set instituteId(value: number) {
        this._instituteId = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get slug(): string {
        return this._slug;
    }

    set slug(value: string) {
        this._slug = value;
    }

    get alias(): string {
        return this._alias;
    }

    set alias(value: string) {
        this._alias = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get studyCycle(): string {
        return this._studyCycle;
    }

    set studyCycle(value: string) {
        this._studyCycle = value;
    }

    get years(): number {
        return this._years;
    }

    set years(value: number) {
        this._years = value;
    }

    get rootYears(): number {
        return this._rootYears;
    }

    set rootYears(value: number) {
        this._rootYears = value;
    }

    get depth(): number {
        return this._depth;
    }

    set depth(value: number) {
        this._depth = value;
    }

    get type(): string {
        return this._type;
    }

    set type(value: string) {
        this._type = value;
    }

    get date(): Date {
        return this._date;
    }

    set date(value: Date) {
        this._date = value;
    }

    get parentId(): number {
        return this._parentId;
    }

    set parentId(value: number) {
        this._parentId = value;
    }

    get paths(): Array<SpecialityPath> {
        return this._paths;
    }

    set paths(value: Array<SpecialityPath>) {
        this._paths = value;
    }

    addPath(value: SpecialityPath, pathBranch: PathBranch = null) {

        for (const path of this._paths) {
            if (path.id === value.id) {
                if (!!pathBranch)
                    path.addBranch(pathBranch);
                return;
            }
        }
        if (!!pathBranch)
            value.addBranch(pathBranch);
        this._paths.push(value);
    }

    get children(): Array<Speciality> {
        return this._children;
    }

    set children(value: Array<Speciality>) {
        this._children = value;
    }

    addChild(value: Speciality) {

        for (const child of this._children) {
            if (child.id === value.id)
                return;
        }
        this._children.push(value);
    }

    get next(): Array<Speciality> {
        return this._next;
    }

    set next(value: Array<Speciality>) {
        this._next = value;
    }

    addNext(value: Speciality) {

        for (const next of this._next) {
            if (next.id === value.id) {
                // for (const nxt of value.next)
                //     next.addNext(nxt);
                return;
            }
        }
        this._next.push(value);
        if (value.depth + 1 > this._depth)
            this.depth = value.depth + 1;
    }
}

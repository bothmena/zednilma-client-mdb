import {dateWithoutTimezone} from '../utils/util-functions';
import {Subject} from './subject';

export class Module {

    private _id: number;
    private _specialityId: number;
    private _name: string;
    private _description: string;
    private _coefficient: number;
    private _year: number;
    private _term: string;
    private _date: Date;
    private _subjects: Array<Subject>;

    static build( object: Object ): Module {

        const module = new Module();

        if ( object != null ) {

            module.id = +object[ 'id' ];
            module.specialityId = +object[ 'speciality_id' ];
            module.name = object[ 'name' ];
            module.description = object[ 'description' ];
            module.coefficient = +object[ 'coefficient' ];
            module.year = +object[ 'year' ];
            module.term = object[ 'term' ];
            module.date = dateWithoutTimezone(object['date']);
        }
        return module;
    }

    constructor() {

        this._subjects = [];
    }

    get id(): number {
        return this._id;
    }

    set id( value: number ) {
        this._id = value;
    }

    get specialityId(): number {
        return this._specialityId;
    }

    set specialityId( value: number ) {
        this._specialityId = value;
    }

    get name(): string {
        return this._name;
    }

    set name( value: string ) {
        this._name = value;
    }

    get description(): string {
        return this._description;
    }

    set description( value: string ) {
        this._description = value;
    }

    get coefficient(): number {
        return this._coefficient;
    }

    set coefficient( value: number ) {
        this._coefficient = value;
    }

    get date(): Date {
        return this._date;
    }

    set date( value: Date ) {
        this._date = value;
    }

    get year(): number {
        return this._year;
    }

    set year(value: number) {
        this._year = value;
    }

    get term(): string {
        return this._term;
    }

    set term(value: string) {
        this._term = value;
    }

    get subjects(): Array<Subject> {
        return this._subjects;
    }

    set subjects(value: Array<Subject>) {
        this._subjects = value;
    }

    addSubject(value: Subject) {
        for (const subject of this.subjects) {
            if (subject.id === value.id)
                return;
        }
        this.subjects.push(value);
    }
}

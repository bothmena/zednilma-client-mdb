import {Institute} from './institute';
import {User} from './user';

/**
 * Created by bothmena on 22/06/17.
 */

export class UserInstitute {

    private _id: number;
    private _user: User;
    private _institute: Institute;
    private _dateIn: Date;
    private _dateOut: Date;
    private _role: string;

    static build(object: Object): UserInstitute {

        const userInstitute = new UserInstitute();

        if (object != null) {
            userInstitute.id = object.hasOwnProperty('id') ? +object['id'] : 0;
            userInstitute.user = User.build(object['user']);
            userInstitute.institute = Institute.build(object['institute']);
            userInstitute.dateIn = object.hasOwnProperty('date_in') ? new Date(object['date_in']) : null;
            userInstitute.dateOut = object.hasOwnProperty('date_out') ? new Date(object['date_out']) : null;
            userInstitute.role = object.hasOwnProperty('role') ? object['role'] : '';
        }
        return userInstitute;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get user(): User {
        return this._user;
    }

    set user(value: User) {
        this._user = value;
    }

    get institute(): Institute {
        return this._institute;
    }

    set institute(value: Institute) {
        this._institute = value;
    }

    get dateIn(): Date {
        return this._dateIn;
    }

    set dateIn(value: Date) {
        this._dateIn = value;
    }

    get dateOut(): Date {
        return this._dateOut;
    }

    set dateOut(value: Date) {
        this._dateOut = value;
    }

    get role(): string {
        return this._role;
    }

    set role(value: string) {
        this._role = value;
    }
}

import {User} from './user';
import {Post} from './post';

/**
 * Created by bothmena on 22/06/17.
 */

export class DepartmentMember {

    private _id: number;
    private _user: User;
    private _post: Post;
    private _dateStarted: Date;
    private _dateEnded: Date;

    static build(object: Object): DepartmentMember {

        const userDepartment = new DepartmentMember();

        if (object != null) {
            userDepartment.id = object.hasOwnProperty('id') ? +object['id'] : 0;
            userDepartment.user = !!object['user'] ? User.build(object['user']) : null;
            userDepartment.post = !!object['post'] ? Post.build(object['post']) : null;
            userDepartment.dateStarted = !!object['date_started'] ? new Date(object['date_started']) : null;
            userDepartment.dateEnded = !!object['date_ended'] ? new Date(object['date_ended']) : null;
        }
        return userDepartment;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get user(): User {
        return this._user;
    }

    set user(value: User) {
        this._user = value;
    }

    get post(): Post {
        return this._post;
    }

    set post(value: Post) {
        this._post = value;
    }

    get dateStarted(): Date {
        return this._dateStarted;
    }

    set dateStarted(value: Date) {
        this._dateStarted = value;
    }

    get dateEnded(): Date {
        return this._dateEnded;
    }

    set dateEnded(value: Date) {
        this._dateEnded = value;
    }
}

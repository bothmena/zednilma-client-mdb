import {PathBranch} from './path-branch';

export class SpecialityPath {

    private _id: number;
    private _branches: PathBranch[];
    private _date: Date;

    constructor(id: number = null, date: Date = null) {

        this._id = id;
        this._branches = [];
        this._date = date;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get branches(): PathBranch[] {
        return this._branches;
    }

    set branches(value: PathBranch[]) {
        this._branches = value;
    }

    addBranch(value: PathBranch) {

        for (const pb of this._branches) {
            if (pb.id === value.id)
                return;
        }
        this._branches.push(value);
    }

    get date(): Date {
        return this._date;
    }

    set date(value: Date) {
        this._date = value;
    }
}

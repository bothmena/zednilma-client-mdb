export class InstituteSettings {

    private _id: number;
    private _terms: string;

    static build( object: Object ): InstituteSettings {

        const settings = new InstituteSettings();

        if ( object != null ) {

            settings.id = +object[ 'id' ];
            settings._terms = object[ 'terms' ];
        }
        return settings;
    }

    constructor() {
    }

    get id(): number {
        return this._id;
    }

    set id( value: number ) {
        this._id = value;
    }

    get terms(): string {
        return this._terms;
    }

    set terms(value: string) {
        this._terms = value;
    }
}

import {Speciality} from './speciality';

export class PathBranch {

    private _id: number;
    private _branch: Speciality;
    private _pathId: number;
    private _index: number;

    static build(object: Object): PathBranch {

        const path = new PathBranch();

        if (object != null && object['path'] != null) {

            path._id = +object['id'];
            path._branch = Speciality.build(object['branch']);
            path._pathId = object['path']['id'];
            path._index = object['index'];
        }
        return path;
    }

    constructor() {
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get branch(): Speciality {
        return this._branch;
    }

    set branch(value: Speciality) {
        this._branch = value;
    }

    get pathId(): number {
        return this._pathId;
    }

    set pathId(value: number) {
        this._pathId = value;
    }

    get index(): number {
        return this._index;
    }

    set index(value: number) {
        this._index = value;
    }
}

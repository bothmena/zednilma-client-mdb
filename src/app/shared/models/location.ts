/**
 * Created by bothmena on 22/06/17.
 */

export class Location {

    private _id: number;
    private _country: string;
    private _state: string;
    private _city: string;
    private _address: string;
    private _zipCode: string;
    private _mapLat: number;
    private _mapLong: number;
    private _isPrimary: boolean;

    static build(object: Object): Location {

        const location = new Location();

        location.id = +object['id'];
        location.country = object['country'];
        location.state = object['state'];
        location.city = object['city'];
        location.address = object['address'];
        location.zipCode = object['zip_code'];
        location.mapLat = +object['map_lat'];
        location.mapLong = +object['map_long'];
        location.isPrimary = object['is_primary'];

        return location;
    }

    constructor() {
    }

    toString() {

        return this._address + ', ' + this._city + ', ' + this._state; //  + ', ' + this._zipCode
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get country(): string {
        return this._country;
    }

    set country(value: string) {
        this._country = value;
    }

    get state(): string {
        return this._state;
    }

    set state(value: string) {
        this._state = value;
    }

    get city(): string {
        return this._city;
    }

    set city(value: string) {
        this._city = value;
    }

    get address(): string {
        return this._address;
    }

    set address(value: string) {
        this._address = value;
    }

    get zipCode(): string {
        return this._zipCode;
    }

    set zipCode(value: string) {
        this._zipCode = value;
    }

    get mapLat(): number {
        return this._mapLat;
    }

    set mapLat(value: number) {
        this._mapLat = value;
    }

    get mapLong(): number {
        return this._mapLong;
    }

    set mapLong(value: number) {
        this._mapLong = value;
    }

    get isPrimary(): boolean {
        return this._isPrimary;
    }

    set isPrimary(value: boolean) {
        this._isPrimary = value;
    }
}

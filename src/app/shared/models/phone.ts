/**
 * Created by bothmena on 22/06/17.
 */

export class Phone {

    private _id: number;
    private _instituteId: number;
    private _userId: number;
    private _type: string;
    private _ccode: string;
    private _number: string;
    private _isVerified: boolean;
    private _role: string;
    private _date: string;

    static build( object: Object ): Phone {

        const phone = new Phone();

        if ( object != null ) {
            phone.id = object.hasOwnProperty( 'id' ) ? object[ 'id' ] : 0;
            phone.instituteId = object.hasOwnProperty( 'institute_id' ) ? object[ 'institute_id' ] : 0;
            phone.userId = object.hasOwnProperty( 'user_id' ) ? object[ 'user_id' ] : 0;
            phone.type = object.hasOwnProperty( 'type' ) ? object[ 'type' ] : '';
            phone.ccode = object.hasOwnProperty( 'ccode' ) ? object[ 'ccode' ] : '';
            phone.number = object.hasOwnProperty( 'number' ) ? object[ 'number' ] : '';
            phone.isVerified = object.hasOwnProperty( 'is_verified' ) ? object[ 'is_verified' ] : false;
            phone.role = object.hasOwnProperty( 'role' ) ? object[ 'role' ] : '';
            phone.date = object.hasOwnProperty( 'date' ) ? object[ 'date' ] : '';
        }
        return phone;
    }

    constructor( type: string = 'MBL', ccode: string = '+216', number: string = '', role: string = '', date: string = '' ) {

        this._type = type;
        this._ccode = ccode;
        this._number = number;
        this._role = role;
        this._date = date;
    }

    get id(): number {
        return this._id;
    }

    set id( value: number ) {
        this._id = value;
    }

    get instituteId(): number {
        return this._instituteId;
    }

    set instituteId( value: number ) {
        this._instituteId = value;
    }

    get userId(): number {
        return this._userId;
    }

    set userId( value: number ) {
        this._userId = value;
    }

    get type(): string {
        return this._type;
    }

    set type( value: string ) {
        this._type = value;
    }

    get ccode(): string {
        return this._ccode;
    }

    set ccode( value: string ) {
        this._ccode = value;
    }

    get number(): string {
        return this._number;
    }

    set number( value: string ) {
        this._number = value;
    }

    get isVerified(): boolean {
        return this._isVerified;
    }

    set isVerified( value: boolean ) {
        this._isVerified = value;
    }

    get role(): string {
        return this._role;
    }

    set role( value: string ) {
        this._role = value;
    }

    get date(): string {
        return this._date;
    }

    set date( value: string ) {
        this._date = value;
    }
}

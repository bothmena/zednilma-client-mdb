import {Speciality} from './speciality';
import {dateWithoutTimezone} from '../utils/util-functions';
import {PathBranch} from './path-branch';

export class Path {

    private _id: number;
    private _speciality: Speciality;
    private _branches: PathBranch[];
    private _date: Date;

    static build(object: Object): Path {

        const path = new Path();

        if (object != null) {

            path._id = +object['id'];
            path._speciality = Speciality.build(object['speciality']);
            path._date = dateWithoutTimezone(object['date']);
        }
        return path;
    }

    constructor() {

        this._branches = [];
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get speciality(): Speciality {
        return this._speciality;
    }

    set speciality(value: Speciality) {
        this._speciality = value;
    }

    get branches(): PathBranch[] {
        return this._branches;
    }

    set branches(value: PathBranch[]) {
        this._branches = value;
    }

    addBranch(value: PathBranch) {
        this._branches.push(value);
    }

    get date(): Date {
        return this._date;
    }

    set date(value: Date) {
        this._date = value;
    }
}

import {dateWithoutTimezone} from '../utils/util-functions';
import {User} from './user';
import {TaskList} from './task-list';

export class Task {

    private _id: number;
    private _user: User;
    private _list: TaskList;
    private _name: string;
    private _description: string;
    private _dueDate: Date;
    private _repeat: string;
    private _type: number;
    private _link: string;

    static build(object: Object): Task {

        const task = new Task();

        if (object != null) {

            task._id = +object['id'];
            task._user = User.build(object['user']);
            task._list = TaskList.build(object['list']);
            task._name = object['name'];
            task._description = object['description'];
            task._repeat = object['repeat'];
            task._type = +object['type'];
            task._link = object['link'];
            task._dueDate = dateWithoutTimezone(object['due_date']);
        }
        return task;
    }

    constructor() {
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }


    get user(): User {
        return this._user;
    }

    set user(value: User) {
        this._user = value;
    }

    get list(): TaskList {
        return this._list;
    }

    set list(value: TaskList) {
        this._list = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get dueDate(): Date {
        return this._dueDate;
    }

    set dueDate(value: Date) {
        this._dueDate = value;
    }

    get repeat(): string {
        return this._repeat;
    }

    set repeat(value: string) {
        this._repeat = value;
    }

    get type(): number {
        return this._type;
    }

    set type(value: number) {
        this._type = value;
    }

    get link(): string {
        return this._link;
    }

    set link(value: string) {
        this._link = value;
    }
}

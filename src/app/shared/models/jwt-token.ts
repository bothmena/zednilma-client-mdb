/**
 * Created by bothmena on 22/01/17.
 */
import {JwtHelperService} from '@auth0/angular-jwt';

export class JWTToken {

    private _refresh_token: string;
    private _token: string;
    private _username: string;
    private _uid: string;
    private _role: string;
    private _exp: number;
    private _iat: number;
    private _jwtHelper: JwtHelperService = new JwtHelperService();

    static build(object: { token: string, refresh_token: string }): JWTToken {

        if (object && !!object.token && !!object.refresh_token) {
            return new JWTToken(object.token, object.refresh_token);
        }
        return null;
    }

    constructor(token: string, refresh_token: string) {

        this.token = token;
        this.refresh_token = refresh_token;
    }

    get token(): string | null {
        return this._token;
    }

    set token(value: string) {
        this._token = value;
        const decoded = this._jwtHelper.decodeToken(value);
        this._username = decoded.username;
        this._uid = decoded.uid;
        this._role = decoded.role;
        this._exp = +decoded.exp;
        this._iat = +decoded.iat;
    }

    get refresh_token(): string {
        return this._refresh_token;
    }

    set refresh_token(value: string) {
        this._refresh_token = value;
    }

    get username(): string {
        return this._username;
    }

    set username(value: string) {
        this._username = value;
    }

    get uid(): string {
        return this._uid;
    }

    set uid(value: string) {
        this._uid = value;
    }

    get role(): string {
        return this._role;
    }

    set role(value: string) {
        this._role = value;
    }

    get exp(): number {
        return this._exp;
    }

    set exp(value: number) {
        this._exp = value;
    }

    get iat(): number {
        return this._iat;
    }

    set iat(value: number) {
        this._iat = value;
    }

    hasExpired(offsetSeconds = 40) {

        return this._jwtHelper.isTokenExpired(this.token, offsetSeconds);
    }

    public isLoggedIn(): boolean {

        return !this.isLoggedOut()
    }

    public isLoggedOut() {

        return this._token in [null, undefined, '']
    }
}

import {Subject} from './subject';
import {Classe} from './classe';

export class Exam {

    private _id: number;
    private _subject: Subject;
    private _classe: Classe;
    private _name: string;
    private _type: number;
    private _coefficient: number;

    static build(object: Object): Exam {

        const exam = new Exam();

        if (object != null) {

            exam.id = +object['id'];
            exam._subject = Subject.build(object['subject']);
            exam._classe = Classe.build(object['classe']);
            exam._name = object['name'];
            exam._type = object['type'];
            exam._coefficient = object['coefficient'];
        }
        return exam;
    }

    constructor() {
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get subject(): Subject {
        return this._subject;
    }

    set subject(value: Subject) {
        this._subject = value;
    }

    get classe(): Classe {
        return this._classe;
    }

    set classe(value: Classe) {
        this._classe = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get type(): number {
        return this._type;
    }

    set type(value: number) {
        this._type = value;
    }

    get coefficient(): number {
        return this._coefficient;
    }

    set coefficient(value: number) {
        this._coefficient = value;
    }
}

import {Classe} from './classe';
import {dateWithoutTimezone} from '../utils/util-functions';

export class Subject {

    private _id: number;
    private _departmentId: number;
    private _name: string;
    private _description: string;
    private _date: Date;
    private _classes: Classe[];
    private _coefficient: number; // local variable

    static build( object: Object ): Subject {

        const subject = new Subject();

        if ( object != null ) {

            subject.id         = +object[ 'id' ];
            subject.departmentId      = +object[ 'department_id' ];
            subject.name  = object[ 'name' ];
            subject.description = object[ 'description' ];
            subject.date = dateWithoutTimezone(object[ 'date' ]);
        }
        return subject;
    }

    constructor() {

        this._classes = [];
    }

    get id(): number {
        return this._id;
    }

    set id( value: number ) {
        this._id = value;
    }

    get departmentId(): number {
        return this._departmentId;
    }

    set departmentId( value: number ) {
        this._departmentId = value;
    }

    get name(): string {
        return this._name;
    }

    set name( value: string ) {
        this._name = value;
    }

    get description(): string {
        return this._description;
    }

    set description( value: string ) {
        this._description = value;
    }

    get date(): Date {
        return this._date;
    }

    set date( value: Date ) {
        this._date = value;
    }

    get classes(): Classe[] {
        return this._classes;
    }

    set classes(value: Classe[]) {
        this._classes = value;
    }

    get coefficient(): number {
        return this._coefficient;
    }

    set coefficient(value: number) {
        this._coefficient = value;
    }
}

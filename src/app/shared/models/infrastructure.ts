export class Infrastructure {

    private _id: number;
    private _departmentId: number;
    private _instituteId: number;
    private _parentId: number;
    private _name: string;
    private _alias: string;
    private _index: number;
    private _capacity: number;
    private _type: string;
    private _date: string;
    private _children: Array<Infrastructure>;

    static build(object: Object): Infrastructure {

        const infrastructure = new Infrastructure();

        if (object != null) {
            infrastructure.id = object.hasOwnProperty('id') ? +object['id'] : 0;
            infrastructure.departmentId = object.hasOwnProperty('department_id') ? object['department_id'] : null;
            infrastructure.instituteId = object.hasOwnProperty('institute_id') ? object['institute_id'] : null;
            infrastructure.parentId = object.hasOwnProperty('parent_id') ? object['parent_id'] : null;
            infrastructure.name = object.hasOwnProperty('name') ? object['name'] : '';
            infrastructure.alias = object.hasOwnProperty('alias') ? object['alias'] : '';
            infrastructure.index = object.hasOwnProperty('index') ? +object['index'] : 0;
            infrastructure.capacity = object.hasOwnProperty('capacity') ? +object['capacity'] : 0;
            infrastructure.type = object.hasOwnProperty('type') ? object['type'] : '';
            infrastructure.date = object.hasOwnProperty('date') ? object['date'] : '';
        }
        return infrastructure;
    }

    constructor(type: string = '', name: string = '', alias: string = '', index: number = 0) {

        this._id = null;
        this._departmentId = null;
        this._instituteId = null;
        this._parentId = null;
        this._name = name;
        this._alias = alias;
        this._index = index;
        this._capacity = null;
        this._type = type;
        this._date = '';
        this._children = [];
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get departmentId(): number {
        return this._departmentId;
    }

    set departmentId(value: number) {
        this._departmentId = value;
    }

    get instituteId(): number {
        return this._instituteId;
    }

    set instituteId(value: number) {
        this._instituteId = value;
    }

    get parentId(): number {
        return this._parentId;
    }

    set parentId(value: number) {
        this._parentId = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get alias(): string {
        return this._alias;
    }

    set alias(value: string) {
        this._alias = value;
    }

    get index(): number {
        return this._index;
    }

    set index(value: number) {
        this._index = value;
    }

    get capacity(): number {
        return this._capacity;
    }

    set capacity(value: number) {
        this._capacity = value;
    }

    get type(): string {
        return this._type;
    }

    set type(value: string) {
        this._type = value;
    }

    get date(): string {
        return this._date;
    }

    set date(value: string) {
        this._date = value;
    }

    get children(): Array<Infrastructure> {
        return this._children;
    }

    set children(value: Array<Infrastructure>) {
        this._children = value;
    }

    addChild(value: Infrastructure) {
        this._children.push(value);
    }
}

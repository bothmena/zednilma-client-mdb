/**
 * Created by bothmena on 22/06/17.
 */
import {dateWithoutTimezone} from '../utils/util-functions';

export class Email {

    private _id: number;
    private _instituteId: number;
    private _userId: number;
    private _email: string;
    private _isPrimary: boolean;
    private _isVerified: boolean;
    private _role: string;
    private _date: Date;

    static build( object: Object ): Email {

        const email = new Email();

        if ( object != null ) {
            email.id = +object[ 'id' ];
            email.instituteId = +object[ 'institute_id' ];
            email.userId = +object[ 'user_id' ];
            email.email = object[ 'email' ];
            email.isPrimary = object[ 'is_primary' ];
            email.isVerified = object[ 'is_verified' ];
            email.role = object[ 'role' ];
            email.date = dateWithoutTimezone(object['date']);
        }
        return email;
    }

    get id(): number {
        return this._id;
    }

    set id( value: number ) {
        this._id = value;
    }

    get instituteId(): number {
        return this._instituteId;
    }

    set instituteId( value: number ) {
        this._instituteId = value;
    }

    get userId(): number {
        return this._userId;
    }

    set userId( value: number ) {
        this._userId = value;
    }

    get email(): string {
        return this._email;
    }

    set email( value: string ) {
        this._email = value;
    }

    get isPrimary(): boolean {
        return this._isPrimary;
    }

    set isPrimary( value: boolean ) {
        this._isPrimary = value;
    }

    get isVerified(): boolean {
        return this._isVerified;
    }

    set isVerified( value: boolean ) {
        this._isVerified = value;
    }

    get role(): string {
        return this._role;
    }

    set role( value: string ) {
        this._role = value;
    }

    get date(): Date {
        return this._date;
    }

    set date(value: Date) {
        this._date = value;
    }
}

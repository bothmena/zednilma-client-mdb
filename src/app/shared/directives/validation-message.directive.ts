import {Directive, ElementRef, Input, OnInit} from '@angular/core'; // ElementRef,
import {FormControl} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

@Directive({
    selector: '[zdlmValidMsg]',
})
export class ValidationMessageDirective implements OnInit {

    @Input('zdlmValidMsg') control: FormControl;

    // private el: ElementRef
    constructor(private el: ElementRef, private translator: TranslateService) {
    }

    ngOnInit(): void {

        this.control.valueChanges.subscribe(
            () => {
                if (this.control.invalid && this.control.dirty) {

                    this.getFormErrorMessage().then(msg => this.el.nativeElement.innerHTML = msg);
                } else {

                    this.el.nativeElement.innerHTML = '';
                }
            },
        );
    }

    private getFormErrorMessage(): Promise<string> {

        const key = Object.keys(this.control.errors)[0];
        const paramlessErrors = ['required', 'equalTo', 'notEqualTo', 'email', 'in_array', 'url'];
        let transKey: string = 'validation.' + key;
        let transParams: {};

        if (paramlessErrors.indexOf(key) > -1) {

            transParams = {};
        } else if (key === 'minlength' || key === 'maxlength') {

            transParams = {limit: this.control.errors[key].requiredLength};
        } else if (key === 'pattern') {

            transKey = 'validation.pattern.default';
            transParams = {};
        } else {

            transParams = this.control.errors[key].params;
        }

        return this.translator.get(transKey, transParams).toPromise();
    }
}

import { Directive, ElementRef, HostListener, Input, OnInit } from '@angular/core';
import { ABONavigatorService } from '../services/abo-navigator.service';

@Directive( {
    selector: '[zdlmNavigate]',
} )
export class ZdlmNavigateDirective implements OnInit {

    @Input('zdlmNavigate') route: string;
    @Input() paramOne = '';
    @Input() paramTwo = '';
    @Input() paramThree = '';

    constructor( private navigator: ABONavigatorService, private el: ElementRef ) {
    }

    ngOnInit(): void {

        this.el.nativeElement.style.cursor = 'pointer';
    }

    @HostListener( 'click' ) onClick() {

        if ( this.paramOne === '' ) {

            this.navigator.goto( this.route );
        } else if ( this.paramTwo === '' ) {

            this.navigator.goto( this.route, this.paramOne );
        } else {

            if ( this.paramThree === '' ) {

                this.navigator.goto( this.route, this.paramOne, this.paramTwo );
            } else {

                console.log(this.route, this.paramOne, this.paramTwo, this.paramThree);
                this.navigator.goto( this.route, this.paramOne, this.paramTwo, this.paramThree );
            }
        }

    }
}

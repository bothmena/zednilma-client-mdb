import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {isDevMode, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {AppComponent} from './app.component';
import {DevToolsExtension, NgRedux, NgReduxModule} from '@angular-redux/store';
import {WebsiteModule} from './website/website.module';
import {ABONavigatorService} from './shared/services/abo-navigator.service';
import {INIT_STATE, State} from './shared/redux/models/state';
import {rootReducer} from './shared/redux/store';
import {AppRoutingModule} from './app-routing.module';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {SecurityModule} from './security/security.module';
import {ZdlmTranslationLoaderService} from './shared/services/zdlm-translation-loader.service';
import {CollapseModule, MDBBootstrapModule, MDBBootstrapModulesPro, MDBSpinningPreloader, ToastModule} from 'ng-uikit-pro-standard';

import {AuthService} from './shared/services/auth.service';
import {AuthInterceptor} from './shared/services/auth.interceptor';
import {UserService} from './shared/services/models/user.service';
import {AdminModule} from './admin/admin.module';
import {InstituteService} from './shared/services/models/institute.service';
import {DepartmentsResolverService} from './shared/services/resolvers/departments-resolver.service';
import {InfrastructureResolverService} from './shared/services/resolvers/infrastructure-resolver.service';
import {InstituteModule} from './institute/institute.module';
import {InstituteGuardService} from './institute/shared/services/institute-guard.service';
import {LocationService} from './shared/services/models/location.service';
import {EmailService} from './shared/services/models/email.service';
import {PhoneService} from './shared/services/models/phone.service';
import {SocialProfileService} from './shared/services/models/social-profile.service';
import {InfrastructureService} from './shared/services/models/infrastructure.service';
import {DepartmentService} from './shared/services/models/department.service';
import {SubjectService} from './shared/services/models/subject.service';
import {PostService} from './shared/services/models/post.service';
import {SpecialtiesResolverService} from './shared/services/resolvers/specialties-resolver.service';
import {SpecialityService} from './shared/services/models/speciality.service';
import {PathService} from './shared/services/models/path.service';
import {MembersResolverService} from './shared/services/resolvers/members-resolver.service';
import {SubjectClasseService} from './shared/services/models/subject-classe.service';
import {ClasseService} from './shared/services/models/classe.service';
import {ModuleService} from './shared/services/models/module.service';
import {ProfessorModule} from './professor/professor.module';
import {SessionService} from './shared/services/models/session.service';
import {StudentModule} from './student/student.module';
import {NotificationService} from './shared/services/models/notification.service';
import {ExamService} from './shared/services/models/exam.service';
import {TaskService} from './shared/services/models/task.service';
import {TimetableService} from './shared/services/models/timetable.service';
import {ParentModule} from './parent/parent.module';
import {NotificationDescriberService} from './shared/services/notification-describer.service';


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    // return new TranslateHttpLoader(http);
    return new TranslateHttpLoader(http, './assets/i18n/', '/app.min.json');
}

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        WebsiteModule,
        SecurityModule,
        InstituteModule,
        AdminModule,
        ProfessorModule,
        StudentModule,
        ParentModule,
        AppRoutingModule,
        NgReduxModule,
        ToastModule.forRoot({progressBar: true, timeOut: 6000, positionClass: 'toast-bottom-right', maxOpened: 3}),
        MDBBootstrapModule.forRoot(),
        MDBBootstrapModulesPro.forRoot(),
        CollapseModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    providers: [
        MDBSpinningPreloader,
        ABONavigatorService,
        DepartmentsResolverService,
        SpecialtiesResolverService,
        InfrastructureResolverService,
        MembersResolverService,
        AuthService,
        UserService,
        InstituteService,
        PathService,
        ZdlmTranslationLoaderService,
        InstituteGuardService,
        LocationService,
        SpecialityService,
        EmailService,
        PhoneService,
        DepartmentService,
        SubjectService,
        PostService,
        SocialProfileService,
        SubjectClasseService,
        ClasseService,
        ModuleService,
        SessionService,
        NotificationService,
        ExamService,
        TaskService,
        TimetableService,
        InfrastructureService,
        NotificationDescriberService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
        }
    ],
    bootstrap: [AppComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {

    constructor(ngRedux: NgRedux<State>, devTools: DevToolsExtension, translate: TranslateService) {

        const storeEnhancers = devTools.isEnabled() && isDevMode() ? [devTools.enhancer()] : [];
        ngRedux.configureStore(rootReducer, INIT_STATE, [], storeEnhancers);

        translate.addLangs(['ar', 'en', 'fr']);
        translate.setDefaultLang('en');
        translate.use('en');
    }
}
